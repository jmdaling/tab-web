DROP TABLE IF EXISTS dim_tent;
CREATE TABLE dim_tent
(
    id INT AUTO_INCREMENT,
    tent VARCHAR(200),
    tent_code VARCHAR(3),
    PRIMARY KEY(id)
);

INSERT INTO dim_tent
      SELECT 1,     'Armbrustschützenzelt', 'ARM'
UNION SELECT 2,     'Augustiner-Festzelt', 'AUG'
UNION SELECT 3,     'Fischer-Vroni Festzelt', 'FIS'
UNION SELECT 4,     'Hacker-Festzelt', 'HAC'
UNION SELECT 5,     'Hofbräu Festzelt', 'HOF'
UNION SELECT 6,     'Käfer Wiesn-Schänke', 'KAF'
UNION SELECT 7,     'Kufflers Weinzelt', 'KUF'
UNION SELECT 8,     'Löwenbräu Festhalle', 'LOW'
UNION SELECT 9,     'Marstall Festzelt', 'MAR'
UNION SELECT 10,    'Ochsenbraterei', 'OSB'
UNION SELECT 11,    'Paulaner Festzelt Winzerer Fähndl', 'PAU'
UNION SELECT 12,    'Schottenhamel Festhalle', 'SOT'
UNION SELECT 13,    'Schützen-Festzelt', 'SCU'
UNION SELECT 14,    'Festhalle Pschorr-Bräurosl', 'PBR'
UNION SELECT 15,    'Spezial Paket', 'SPE';

DROP TABLE IF EXISTS dim_date;
CREATE TABLE dim_date
(
    id INT AUTO_INCREMENT,
    dt DATE,
    dt_code VARCHAR(2),
    PRIMARY KEY(id)
);

INSERT INTO dim_date(dt, dt_code)
      SELECT '2022-09-17', '01'
UNION SELECT '2022-09-18', '02'
UNION SELECT '2022-09-19', '03'
UNION SELECT '2022-09-20', '04'
UNION SELECT '2022-09-21', '05'
UNION SELECT '2022-09-22', '06'
UNION SELECT '2022-09-23', '07'
UNION SELECT '2022-09-24', '08'
UNION SELECT '2022-09-25', '09'
UNION SELECT '2022-09-26', '10'
UNION SELECT '2022-09-27', '11'
UNION SELECT '2022-09-28', '12'
UNION SELECT '2022-09-29', '13'
UNION SELECT '2022-09-30', '14'
UNION SELECT '2022-10-01', '15'
UNION SELECT '2022-10-02', '16'
UNION SELECT '2022-10-03', '17';


DROP TABLE IF EXISTS dim_timeslot;
CREATE TABLE dim_timeslot
(
    id INT AUTO_INCREMENT,
    ts VARCHAR(20),
    ts_code VARCHAR(2),
    PRIMARY KEY(id)
);

INSERT INTO dim_timeslot(ts, ts_code)
      SELECT 'Mittag',      '1'
UNION SELECT 'Nachmittag',  '2'
UNION SELECT 'Abend',       '3';


DROP TABLE IF EXISTS dim_pax;
CREATE TABLE dim_pax
(
    id INT AUTO_INCREMENT,
    pax INT,
    pax_code VARCHAR(2),
    PRIMARY KEY(id)
);

INSERT INTO dim_pax(pax, pax_code)
      SELECT 1, '01'
UNION SELECT 2, '02'
UNION SELECT 3, '03'
UNION SELECT 4, '04'
UNION SELECT 5, '05'
UNION SELECT 6, '06'
UNION SELECT 7, '07'
UNION SELECT 8, '08'
UNION SELECT 10, '10'
UNION SELECT 16, '16'
UNION SELECT 20, '20'
UNION SELECT 24, '24'
UNION SELECT 26, '26'
UNION SELECT 30, '30'
UNION SELECT 32, '32'
UNION SELECT 40, '40';


DROP TABLE IF EXISTS dim_area;
CREATE TABLE dim_area
(
    id INT AUTO_INCREMENT,
    area VARCHAR(30),
    area_code VARCHAR(2),
    PRIMARY KEY(id)
);

INSERT INTO dim_area(area, area_code)
      SELECT 'Kein bestimmter Bereich', ''
UNION SELECT 'Balkon',           'B'
UNION SELECT 'Mittelschiff',     'M'
UNION SELECT 'Box',              'X';

CREATE TABLE product_catalog
(
    sku VARCHAR(20),
    tent_code VARCHAR(3),
    dt_code VARCHAR(2),
    ts_code VARCHAR(2),
    pax_code VARCHAR(2),
    area_code VARCHAR(2),
    PRIMARY KEY(sku)
);

INSERT INTO product_catalog (sku, tent_code, dt_code, ts_code, pax_code, area_code)
SELECT 
    CASE
        WHEN area_code = "" THEN CONCAT(tent_code,"-", dt_code,"-", ts_code,"-", pax_code)
        ELSE CONCAT(tent_code,"-", dt_code,"-", ts_code,"-", pax_code,"-",area_code)
    END AS sku,
    tent_code, dt_code, ts_code, pax_code, area_code
FROM dim_tent JOIN dim_date JOIN dim_timeslot JOIN dim_pax JOIN dim_area;

-- SELECT tent_code, dt_code, ts_code, pax_code, area_code 
-- FROM dim_tent JOIN dim_date JOIN dim_timeslot JOIN dim_pax JOIN dim_area
-- WHERE pax >= 8 AND area_code <> "";