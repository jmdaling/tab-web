

DROP TABLE IF EXISTS fact_products;
CREATE TABLE fact_products
(
    id INT AUTO_INCREMENT,
    sku         VARCHAR(13),
    tent_code   VARCHAR(30),
    dt_code     VARCHAR(2),
    ts_code     VARCHAR(1),
    pax_code    VARCHAR(2),
    area_code   VARCHAR(1),
    price       DECIMAL(10,2),
    is_base     BIT DEFAULT FALSE,
    PRIMARY KEY(id),
    INDEX(sku ASC),
    UNIQUE(sku)
);

INSERT INTO fact_products(sku, tent_code, dt_code, ts_code, pax_code, area_code)
SELECT 
    CONCAT(tent_code, "-", dt_code, "-", ts_code, "-", pax_code, IF(area_code<>"", CONCAT("-",area_code), "") ) AS `sku`, 
    tent_code,
    dt_code,
    ts_code,
    pax_code,
    area_code
FROM 
         dim_tent 
    JOIN dim_date 
    JOIN dim_timeslot 
    JOIN dim_pax
    JOIN dim_area;
-- 48960

--Remove all small-group combinations with area
DELETE 
FROM  fact_products
WHERE pax_code <= 7
AND area_code <> ""
ORDER BY pax_code DESC;
-- 16065


-- SELECT sum(1) FROM fact_products;
-- 32895

-- Identify base prices
UPDATE fact_products
SET is_base = 1
WHERE id IN (
    SELECT id
    FROM fact_products
    WHERE pax_code IN (8, 10)
    AND area_code = "");
--1530


-- Update prices
UPDATE
    fact_products           fp
        INNER JOIN
    price_import            p  ON fp.sku = p.sku
SET fp.price = p.current_price
;




SELECT
    fp2.*
FROM
    fact_products           fp2 
        INNER JOIN
    (
        SELECT 
            tent_code,
            -- pax_code,
            dt_code,
            ts_code,
            COUNT(sku)
        FROM
            fact_products           fp
        WHERE
            fp.price IS NOT NULL
        AND fp.pax_code < 8
        GROUP BY
            tent_code, 
            dt_code,
            ts_code
        HAVING COUNT(sku) < 6
    ) AS f
    ON  fp2.tent_code   = f.tent_code
    -- AND fp2.pax_code    = f.pax_code
    AND fp2.dt_code     = f.dt_code
    AND fp2.ts_code     = f.ts_code
    AND fp2.price       IS NULL
WHERE fp2.pax_code < 8
        -- AND tent_code = 'ARM'
ORDER BY fp2.sku;



SELECT 
    tent_code,
    pax_code,
    dt_code,
    ts_code,
    COUNT(sku)
FROM
    fact_products           fp
WHERE
    fp.price IS NOT NULL
AND fp.pax_code < 8
AND tent_code = 'ARM'
GROUP BY
    tent_code, 
    dt_code,
    ts_code
HAVING COUNT(sku) < 6
;

SELECT * FROM fact_products 
WHERE 1=1
AND tent_code = 'ARM' 
AND pax_code < 8
AND dt_code = 3
AND ts_code = 3
AND price IS NULL
;


SELECT 
    fp.*
FROM
    fact_products           fp
        LEFT JOIN
    fact_products           fp2  ON fp2.sku = fp.sku
                                AND fp2.price IS NULL
WHERE
    fp.price IS NOT NULL
LIMIT 100
;



SELECT * FROM fact_products WHERE sku IS NULL LIMIT 10;
SELECT * FROM fact_products WHERE id = 1;
SELECT * FROM price_import LIMIT 10;
