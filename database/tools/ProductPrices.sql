/*

Pricing Rules:

Base price (for pax < 8)    = Price for equivalent Table of 10
Table splits (pax < 8)      = Base price for 10 / 10 - 20% = 1 Seat (rounded on 2 seat price & multiplied by pax)

Tables of 20                = 2 × table of 10 price + 500€
Tables of 30                = 2 × table of 10 price + 1500€
Tables of 40                = 2 × table of 10 price + 2000€

Area booking table 8        = Price for table of 8  + 500€
Area booking table 10       = Price for table of 10 + 500€
Area booking table 16       = Price for table of 16 + 1000€
Area booking table 20       = Price for table of 20 + 2000€
Area booking table 30       = Price for table of 30 + 3000€
Area booking table 40       = Price for table of 40 + 4000€


Table of 20 = Table of 10 x 2 + 500€
Areabooking table 20 = Table of 20 + 1500€
Tables of 30 and more (only available as special pack) = price on request
Small-Group Prices: table of 10/ 10 -20% = 1 Seat

Prices of full tables without area booking (10, 20) are rounded to the closest 00, 90 or 99
Prices for 1 person small group are rounded to the closest 00, 90 or 99 
*/


DROP TABLE IF EXISTS prices;
CREATE TABLE prices
(
    id          INT AUTO_INCREMENT,
    sku         VARCHAR(13),
    original    INT,
    increase    INT,
    sml_grp     INT,
    base        INT,
    area        INT,
    final       INT,
    PRIMARY     KEY(id),
    INDEX(sku ASC),
    UNIQUE(sku)
);

INSERT INTO prices(sku, original)
SELECT sku, current_price FROM price_import;

UPDATE prices
SET increase = ROUND(original * 1.2/10) * 10;

-- Set small group price

-- SELECT
--     p.sku,
--     fp.pax_code,
--     p.original,
--     bp.increase AS base_price_10,
--     bp.increase * 0.8 AS base_price_10_discount,
--     bp.increase * 0.8 / 10 * 2 AS base_price_2,
--     round(bp.increase * 0.8 / 10 * 2) AS base_price_2_round,
--     round(round(bp.increase * 0.8 / 10 * 2)/10)*10 AS base_price_2_round2,
--     (round(round(bp.increase * 0.8 / 10 * 2)/10)*10) / 2 * fp.pax_code AS new_sml_grp,
--     CASE
--         WHEN fp.pax_code < 8 THEN (round(round(bp.increase * 0.8 / 10 * 2)/10)*10) / 2 * fp.pax_code
--         ELSE NULL
--     END smlPrice
-- FROM
UPDATE
    prices              p
        INNER JOIN
    fact_products       fp ON p.sku = fp.sku
        INNER JOIN 
    prices              bp ON CONCAT(SUBSTRING(p.sku, 1, 9), "10") = bp.sku
SET 
    p.sml_grp = CASE
                    WHEN fp.pax_code < 8 THEN (round(round(bp.increase * 0.8 / 10 * 2)/10)*10) / 2 * fp.pax_code
                    ELSE NULL
                END,
    p.base = bp.increase
-- WHERE 1=1
-- AND fp.sku = 'ARM-01-1-02'
;


-- Set base price of tables of 8

-- SELECT
--     p.sku
-- FROM
UPDATE
    prices              p
        INNER JOIN
    fact_products       fp ON p.sku = fp.sku
        INNER JOIN 
    prices              bp ON CONCAT(SUBSTRING(p.sku, 1, 9), "08") = bp.sku
                          AND fp.pax_code = "08"
SET p.base = bp.increase
WHERE 1=1
AND p.sku = 'MAR-15-3-16-B'
-- AND fp.sku = 'PBR-02-3-01'
;

-- Set price for tables of 16 = table of 8 × 2

SELECT
    p.sku,
    p.base
FROM    
-- UPDATE
    prices              p
        INNER JOIN
    fact_products       fp ON p.sku = fp.sku
        LEFT JOIN 
    prices              bp ON CONCAT(SUBSTRING(p.sku, 1, 9), "08") = bp.sku
-- SET p.base = bp.increase
WHERE 1=1
AND fp.pax_code = "16" 
AND fp.area_code = ""
;

-- Set price for tables of 20 = table of 10 × 2

-- SELECT
--     p.sku,
--     p.base
-- FROM    
UPDATE
    prices              p
        INNER JOIN
    fact_products       fp ON p.sku = fp.sku
        LEFT JOIN 
    prices              bp ON CONCAT(SUBSTRING(p.sku, 1, 9), "10") = bp.sku
SET p.base = bp.increase
WHERE 
    fp.pax_code = "20" 
OR  fp.pax_code = "30" 
-- AND fp.area_code = ""
;


-- Set prices for combined tables
-- SELECT p.*
-- FROM
UPDATE 
    prices              p
        INNER JOIN
    fact_products       fp ON p.sku = fp.sku
                          AND fp.area_code = ""
        LEFT JOIN 
    prices              bp8 ON CONCAT(SUBSTRING(p.sku, 1, 9), "08") = bp8.sku
        LEFT JOIN 
    prices              bp10 ON CONCAT(SUBSTRING(p.sku, 1, 9), "10") = bp10.sku
SET p.final = 
    CASE
        WHEN fp.pax_code = 16 THEN (bp8.base * 2) + 500
        WHEN fp.pax_code = 20 THEN (bp10.base * 2) + 500
        WHEN fp.pax_code = 30 THEN (bp10.base * 3) + 1500
        WHEN fp.pax_code = 40 THEN (bp10.base * 4) + 2000
        ELSE NULL
    END
;



-- Set area prices

-- SELECT
--     p.sku,
--     bp8.sku,
--     bp10.sku,
--     bp10.base,
--     fp.pax_code,
--     CASE
--         WHEN fp.pax_code = 8  THEN bp8.base + 500
--         WHEN fp.pax_code = 10 THEN bp10.base + 500
--         WHEN fp.pax_code = 16 THEN (bp8.base * 2) + 1000
--         WHEN fp.pax_code = 20 THEN (bp10.base * 2) + 1500
--         WHEN fp.pax_code = 30 THEN (bp10.base * 3) + 1500
--         WHEN fp.pax_code = 40 THEN (bp10.base * 4) + 1500
--         ELSE 0
--     END AS area
--     -- bp.sku,
--     -- CONCAT(SUBSTRING(p.sku, 1, 9), "10") AS base_sku,
--     -- bp.increase base_price,
--     -- bp.increase + 500 area,
--     -- fp.area_code
-- FROM
UPDATE 
    prices              p
        INNER JOIN
    fact_products       fp ON p.sku = fp.sku
                          AND fp.area_code <> ""
        LEFT JOIN 
    prices              bp8 ON CONCAT(SUBSTRING(p.sku, 1, 9), "08") = bp8.sku
        LEFT JOIN 
    prices              bp10 ON CONCAT(SUBSTRING(p.sku, 1, 9), "10") = bp10.sku
SET p.area = 
    CASE
        WHEN fp.pax_code = 8  THEN bp8.base + 500
        WHEN fp.pax_code = 10 THEN bp10.base + 500
        WHEN fp.pax_code = 16 THEN (bp8.base * 2) + 2000
        WHEN fp.pax_code = 20 THEN (bp10.base * 2) + 2000
        WHEN fp.pax_code = 30 THEN (bp10.base * 3) + 3000
        WHEN fp.pax_code = 40 THEN (bp10.base * 4) + 4000
        ELSE NULL
    END
-- WHERE fp.sku = 'ARM-07-3-20-B'
-- LIMIT 100
;

-- FINAL PRICES
UPDATE
    prices
SET final = CASE
                WHEN final > 0 THEN final
                WHEN area IS NOT NULL THEN area
                WHEN sml_grp IS NOT NULL THEN sml_grp
                ELSE increase
            END;



SELECT
    sum(1)
FROM
    prices          p
        INNER JOIN
    fact_products   f ON p.sku = f.sku
WHERE 1=1
-- AND f.tent_code = "ARM"
-- AND f.pax_code = "20"
-- AND f.area_code <> ""
;



-- SELECT 
--     price mod 10000 - price mod 1000,
--     price mod 1000 - price mod 100,
--     price mod 100 - price mod 10,
--     price mod 10,
--     price,
--     CASE 
--         WHEN price mod 10 >= 5 THEN round(price/10)*10 + 9 ELSE price
--     END AS price_rounded,
--     round(price/10)*10
-- FROM (        SELECT 349 AS price 
--         UNION SELECT 278
--         UNION SELECT 338) AS p;

