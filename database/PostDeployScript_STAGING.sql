
-- LOCAL

/* REMEMBER TO:
- Run this script
- Change admin email
- LOG IN and go to WooCommerce Settings -> Change admin email recipients 

*/
USE tab_web;

UPDATE wp_shcga_site
SET domain = 'dev.oktoberfest-tischreservierungen.de',
    `path` = '/'
WHERE id = 1;

UPDATE wp_shcga_blogs
SET domain = 'dev.oktoberfest-tischreservierungen.de',
    `path` = '/'
WHERE blog_id = 1;

UPDATE wp_shcga_blogs
SET domain = 'dev.tab-ticketbroker.de',
    `path` = '/'
WHERE blog_id = 3;

UPDATE wp_shcga_options
SET option_value = 'http://dev.oktoberfest-tischreservierungen.de'
WHERE option_name = 'siteurl' OR option_name = 'home';

UPDATE wp_shcga_3_options
SET option_value = 'http://dev.tab-ticketbroker.de/'
WHERE option_name = 'siteurl' OR option_name = 'home';

UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'admin_email';
-- UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'woocommerce_stock_email_recipient';
-- UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'woocommerce_gzd_dhl_shipper_email';
-- UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'woocommerce_gzd_dhl_return_address_email';
-- UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'woocommerce_gzd_shipments_shipper_address_email';
-- UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'woocommerce_gzd_shipments_return_address_email';
-- UPDATE wp_shcga_options SET option_value = 'jmdaling@gmail.com' WHERE option_name = 'new_admin_email';

-- CHECK EMAILS:
SELECT * FROM `wp_shcga_options` where option_value like '%keyaccount@tab-ticketbroker.de%'; 
-- Change emails to DEV
update `wp_shcga_options` set option_value = replace(option_value, 'keyaccount@tab-ticketbroker.de', 'jmdaling@gmail.com') where option_value like '%keyaccount@tab-ticketbroker.de%'; 


-- Monitor existing config
SELECT id, domain
FROM wp_shcga_site
WHERE id IN (1);

SELECT blog_id, domain, path
FROM wp_shcga_blogs
WHERE blog_id IN (1, 3);

SELECT 
    option_name      AS Ofest_SiteName,
    option_value     AS Ofest_SiteURL
FROM wp_shcga_options
WHERE ( option_name = 'siteurl' OR option_name = 'home')
UNION
SELECT 
    option_name      AS Ofest_SiteName,
    option_value     AS Ofest_SiteURL
FROM wp_shcga_3_options
WHERE ( option_name = 'siteurl' OR option_name = 'home');

SELECT * FROM `wp_shcga_options` where option_value like '%keyaccount@tab-ticketbroker.de%'; 

