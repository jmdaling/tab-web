
DROP VIEW IF EXISTS `vw_product_variations`;

CREATE VIEW `vw_product_variations`  AS   (select distinct `p`.`post_parent` AS `post_parent`,`pm`.`post_id` AS `post_id`,`pp`.`post_title` AS `post_title`,`pm`.`meta_key` AS `meta_key`,`pm`.`meta_value` AS `meta_value` from ((`wp_shcga_posts` `p` join `wp_shcga_postmeta` `pm` on(`p`.`ID` = `pm`.`post_id`)) join `wp_shcga_posts` `pp` on(`p`.`post_parent` = `pp`.`ID`)) where `p`.`post_type` = 'product_variation' and `p`.`post_status` = 'publish' and `pm`.`meta_key` in ('attribute_pa_date','attribute_pa_pax','attribute_pa_timeslot','attribute_pa_area') order by `pm`.`post_id`,`pm`.`meta_id`,`pm`.`meta_key`,`pm`.`meta_value`)  ;

DROP VIEW IF EXISTS `vw_product_variations_pivot`;

CREATE VIEW `vw_product_variations_pivot`  AS   (select `t`.`post_parent` AS `post_parent`,`t`.`post_title` AS `post_title`,`t`.`post_id` AS `post_id`,max(case when `t`.`meta_key` = 'attribute_pa_date' then `t`.`meta_value` else NULL end) AS `attribute_pa_date`,max(case when `t`.`meta_key` = 'attribute_pa_pax' then `t`.`meta_value` else NULL end) AS `attribute_pa_pax`,max(case when `t`.`meta_key` = 'attribute_pa_timeslot' then `t`.`meta_value` else NULL end) AS `attribute_pa_timeslot` from `vw_product_variations` `t` group by `t`.`post_parent`,`t`.`post_title`,`t`.`post_id`)  ;
COMMIT;
