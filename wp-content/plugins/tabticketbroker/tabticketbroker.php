<?php
/** 
 * @package tabticketbroker
 */

/**
 * Plugin Name: Tab Ticket Broker
 * Description: An administration portal for Tab Ticket Broker to assist with managing the asynchronous nature of ticket procurement and sales.
 * Text Domain: tabticketbroker
 * Domain Path: /languages/
 * Version: 1.0
 * Author: Jan-Marten Daling
 * Author URI: https://www.linkedin.com/in/jan-marten-daling/
 * License: GPL2
 */

defined( 'ABSPATH' ) or die( 'Nice try. Wordpress not initialized' );

$plugin_desc_not_used = __( 'An administration portal for Tab Ticket Broker to assist with managing the asynchronous nature of ticket procurement and sales.', 'tabticketbroker' );

// Require autoload files
if ( file_exists( dirname( __FILE__) . '/vendor/autoload.php' ) ) {
    require_once dirname( __FILE__) . '/vendor/autoload.php';
}

// Initialize all the core classes of the plugin
if ( class_exists( 'Inc\\Init' ) ) {
    Inc\Init::register_services();
}

// echo '<br><br><br><br>          THIS NOW';
// $bc = new Inc\Base\BaseController;
// echo $bc->plugin_url;
//http://oktoberfest-tischreservierungen.de/wp-content/plugins/tabticketbroker/

/**
 * For the plugin activation and deactivation to work properly these functions
 * must be procedural and direct. In other words the functions passed to the registration
 * must be outside a class. And our dynamic register_services cannot be used. Thus they
 * are included and specified seperately here.
 * 
*/ 

function activate_tab_plugin() {
    Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'deactivate_tab_plugin' );

function deactivate_tab_plugin() {
    Inc\Base\Deactivate::Deactivate();
}
register_deactivation_hook( __FILE__, 'activate_tab_plugin' );
