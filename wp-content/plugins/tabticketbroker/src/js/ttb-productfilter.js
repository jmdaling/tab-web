
// Product filter behaviour
// Add event listners after DOM loaded with init
document.addEventListener("DOMContentLoaded", init, false);

// On Load: 
// Move Filter Toggle into header
var filter_toggle_el = jQuery('#ttb_filter_toggle');
var header_el = document.querySelectorAll('header.page-header');

filter_toggle_el.remove();
header_el[0].appendChild(filter_toggle_el[0]);

// On every scroll action move header down
// Note the default offset here is 50px
var filter_toggle_el = jQuery('#ttb_filter_toggle');
var filter_toggle_el_position = filter_toggle_el.offset();

jQuery(window).scroll(function(){
  if(jQuery(window).scrollTop() >= 50 || jQuery(window).scrollTop() > filter_toggle_el_position.top){
    filter_toggle_el.css('position','fixed').css('top','0').css('bottom','unset');
  } 
  else {
    filter_toggle_el.css('position','').css('top', '').css('bottom','');
  }    
});

// On Load:
// Add all required event listners
function init() {
  // Add listen events to all attribute elements
  var elements = document.getElementsByClassName("attribute_name");
  for (var i = 0; i < elements.length; i++) {
    elements[i].addEventListener("click", selectAttribute, false);
  }

  // Add listen events to show/hide.clear filter buttons
  var filter_show_btn = document.getElementById("ttb_show_filter");
  var filter_close_btn = document.getElementById("ttb_close_filter");
  var filter_clear_btn = document.getElementById("ttb_clear_filter");
  if(filter_show_btn) {filter_show_btn.addEventListener("click", toggleFilter, false);}
  if(filter_close_btn) {filter_close_btn.addEventListener("click", toggleFilter, false);}
  if(filter_clear_btn) {filter_clear_btn.addEventListener("click", clearFilter, false);}

  // Add listen events to View Results filter button
  var filter_view_results = document.getElementById("ttb_view_results");
  if(filter_view_results) {filter_view_results.addEventListener("click", submitFilterSearch, false);}
  
  // Add listen events to all products
  var product_elements = document.getElementsByClassName("product-link-js");
  for (var i = 0; i < product_elements.length; i++) {
    product_elements[i].addEventListener("click", goToProduct, false);
  }

  // Add listen events to View Results filter button
  var filter_toggle_el = document.getElementById("ttb_filter_toggle");
  if(filter_toggle_el) {filter_toggle_el.addEventListener("click", toggleFilter, false);}
}

function submitFilterSearch() {
  
  var filter_toggle_el = document.getElementById("ttb_filter_toggle"),
      filter = document.getElementById("ttb_filter_container"),
      dim_page = document.getElementById("loader_dim_page"),
      loader_wheel_els = document.getElementsByClassName("lds-dual-ring");

  // Dont hide the filter on the desktop
  // Identify if on desktop by checking the toggle visibility set by CSS
  if ( window.getComputedStyle(filter_toggle_el, null).display != 'none' ) {
    // Hide filter on mobile to reveal loader
    filter.style.display = "none"; 
  }

  // Dim page
  dim_page.style.display = "unset"; 

  // Unhide wheel (it is made hidden when filter is displayed)
  for (var i = 0; i < loader_wheel_els.length; i++) {
    loader_wheel_els[i].style.display = "unset"; 
  }

  document.getElementById('ttb_product_filter_form').submit();
}

function clearFilter() {  
  // Show loader
  document.getElementById('loader_dim_page').style.display = "unset"; 
  // Remove all inputs and submit form
  var filter_inputs = document.getElementById("filter_inputs");
  filter_inputs.remove();
  document.getElementById('ttb_product_filter_form').submit();
}

// When clicking on a product image, submit the product's form
var goToProduct = function () {
  var product_id = this.getAttribute("id");
  var form_id = product_id.replace("product_link_", "product_");

  document.getElementById('loader_dim_page').style.display = "unset";
  document.getElementById(form_id).submit();
}

// Action a attribute click
var selectAttribute = function () {
  var id = this.getAttribute("id");
  var siblings = this.parentNode.parentNode.getElementsByTagName('*');
  var attribute_type = this.parentNode.parentNode.getAttribute("id");
  var filter_inputs = document.getElementById("filter_inputs");

  // If not selected yet: ADD
  if (!this.classList.contains("attribute_name_selected")) {
    // First remove all selected (act as radio button for all but date which is multi select)
    if (attribute_type != "pa_date") {
      for(i=0; i<siblings.length; i++) {
        siblings[i].classList.remove("attribute_name_selected");
        var input = document.getElementById(siblings[i].id + "_input");
        if(input != null){
          filter_inputs.removeChild(input);
        }
      }
    }

    // Add class to selected
    this.classList.add("attribute_name_selected");

    // Add input to form
    var input = document.createElement("input");
    input.type = "hidden";
    input.id = id + "_input";
    input.name = attribute_type + "[]";
    input.value = id;
    filter_inputs.appendChild(input);

    
  }
  // If aready selected: REMOVE 
  else {
    // Remove class
    this.classList.remove("attribute_name_selected");

    // Remove input from form
    var input = document.getElementById(id + "_input");
    filter_inputs.removeChild(input);
  }
}

function toggleFilter() {
  var filter = document.getElementById("ttb_filter_container"),
      dim_page = document.getElementById("loader_dim_page"),
      loader_wheel_els = document.getElementsByClassName("lds-dual-ring");


      
  // Show filter by adding class
  if ( window.getComputedStyle(filter, null).display === 'none' ) {
    filter.classList.add("ttb-show-filter");
    dim_page.style.display = "unset";
    for (var i = 0; i < loader_wheel_els.length; i++) {
      loader_wheel_els[i].style.display = "none"; 
    }


  }
  else {
    // Hide filter (default CSS)
    filter.classList.remove("ttb-show-filter");
    dim_page.style.display = "none";
    for (var i = 0; i < loader_wheel_els.length; i++) {
      loader_wheel_els[i].style.display = "unset"; 
    }

  }
}
