// Change OFest product presentation in our recommendations
// This is required to have consistency in the product presentation &
// Ofest is an external product, so we can't change it in the backend
jQuery(document).ready(function ($) {

  // Find buy button a element with class button and a SKU of OCT (*hardcoded*)
  let buy_btn_el = $("a.button[data-product_sku='OCT']");

  // If buy_btn_el is not found, find buy button a element with class button and data-product_id="27079"
  if (buy_btn_el.length == 0) {
    buy_btn_el = $("a.button[data-product_id='27079']");
  }

  // If buy_btn_el is not found, log and exit
  if (buy_btn_el.length == 0) {
    console.log("ttb-homepage.js: buy button not found for SKU=OCT or ID=27079");
    return;
  }
  
  // Change buy_btn_el text to " Jetzt buchen"
  buy_btn_el.text("Jetzt buchen");

  // add id to buy button
  buy_btn_el.attr("id", "ttb_buy_btn");

  // add css script to add checkmark to buy button's before selector
  let css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = "a#ttb_buy_btn:before { content: \"\\f00c\"; font-family: \"Font Awesome 5 Free\"; margin-right: 5px; }";
  buy_btn_el.parent().append(css);
  
  // get parent element of buy_btn_el in variable prod_price
  let prod_parent_container = buy_btn_el.parent();

  // Find div with class="eael-product-price" of parent and add text "ab."
  let prod_price = prod_parent_container.find("div.eael-product-price");
  prod_price.text("ab. " + prod_price.text());

}
);