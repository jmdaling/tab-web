// Show loader when page opens, hides after doc is ready
document.onreadystatechange = function()
{
  showLoader(true);
}


// Single Product Page: Move the product description injected in WcCustomization.php (via woocommerce_after_add_to_cart_form) to under the 
// product images. This cannot be done by a hook. https://github.com/woocommerce/woocommerce/issues/16492 
window.onload = function() {
  // Get main gallery element (note that it is a selection of elements of which we use the first)
    var gallery_el = document.getElementsByClassName("woocommerce-product-gallery");
    // if (galery_el[0] === undefined){return}
  
    // Keep checking until the gallery has populated before appending child
    // Otherwise the text loads above the pictures which seems to be loaded by scripts also operating on onload
    var timesRun = 0;
    var i = setInterval(function ()
    {  
      timesRun++ //Brought in to ensure it executes even if only 1 picture is added to the product (then it will stay on one child element for the gallery_el)
  

      if (gallery_el[0].childElementCount > 1 || timesRun == 5)
        {
          clearInterval(i);
            
            // Get the custom description 
            var prod_desc_el = document.getElementById("custom_product_description");
            
            // Quit if no gallery element is found
            if (gallery_el[0] === undefined || prod_desc_el === undefined){return}
            
            // Move the description to the last element of the gallery
            gallery_el[0].appendChild(prod_desc_el);
            
        }
    }, 100);

    // Hide page loader after executing all JS
    showLoader(false);

  };
  
  // Hide titles: There is already a data-wvstooltip popup displaying the value. Presumably from the Swatches Plugin
  window.addEventListener("load", hideVariableItemTitles, false);
  function hideVariableItemTitles() {
    var var_items = document.getElementsByClassName("variable-item");
    for (var i = 0; i < var_items.length; i++) {
      var_items[i].removeAttribute("title");
    }
  };
  
  // Add info button and hover information text and image for Area bookings
  // Injected in WcCustomization.php via woocommerce_after_add_to_cart_form
  window.addEventListener("load", addAreaBookingInfoBox, false);
  function addAreaBookingInfoBox() {
    var area_title_el = document.querySelector("label[for='pa_area']"),
      elemRect = area_title_el.getBoundingClientRect(),
      x_pos = elemRect.bottom,
      y_pos = elemRect.left,
      info_icon = document.createElement('img'),
      area_infobox_wrapper = document.createElement('div'),
      area_infobox = document.createElement('div'),
      area_infobox_img = document.createElement('img');
  
    // Add the info icon
    info_icon.src = '/wp-content/uploads/2021/06/Info.png';
    info_icon.style.height = '30px';
    info_icon.style.marginLeft = '10px';
    info_icon.style.cursor = 'pointer';
    area_title_el.appendChild(info_icon);
  
    // Listen for hovers
    info_icon.addEventListener("mouseenter", showAreaInfoBox, false);
    info_icon.addEventListener("mouseout", hideAreaInfoBox, false);
  
  };
  
  // Moves and displays the information box to under the info icon
  function showAreaInfoBox() {
    var elemRect = this.getBoundingClientRect(),
        x_pos = elemRect.right+5,// - bodyRect.right,
        y_pos = elemRect.bottom-350,// - bodyRect.top;
        area_infobox_wrapper = document.getElementById("area_infobox_wrapper");
  
    area_infobox_wrapper.style.position = "fixed";
    area_infobox_wrapper.style.left = x_pos+'px';
    area_infobox_wrapper.style.top = y_pos+'px';
    area_infobox_wrapper.style.display = 'block';
    area_infobox_wrapper.style.zIndex = 10;
  }
  
  function hideAreaInfoBox() {
    var area_infobox_wrapper = document.getElementById("area_infobox_wrapper");
  
    area_infobox_wrapper.style.display = 'none';
    area_infobox_wrapper.style.zIndex = 0;
  }
  
  // Listen on table layout buttons and display visual layout on hover
  window.addEventListener("load", listenOnTableLayouts, false);
  function listenOnTableLayouts() {
    var pax_container  = document.querySelectorAll('[data-attribute_name="attribute_pa_pax"]'),
        pax_items = pax_container[1].getElementsByTagName("li");

    for (var i = 0; i < pax_items.length; i++) {
      pax_items[i].addEventListener("mouseenter", showTableLayout, false);
      pax_items[i].addEventListener("mouseout", hideTableLayout, false);
    }
  };
  
  function showTableLayout(e) {
    var pax = this.getAttribute("data-value"),
        elemRect = this.getBoundingClientRect(),
        x_pos = elemRect.left,// - bodyRect.right,
        y_pos = elemRect.bottom + 5,// - bodyRect.top;
        img_el = document.getElementById("table_layout_img_"+pax);
  
    if(img_el) {
      img_el.style.position = "fixed";
      img_el.style.left = x_pos+'px';
      img_el.style.top = y_pos+'px';
      img_el.style.display = 'block';
      img_el.style.zIndex = 10;
    }
  }
  
  function hideTableLayout(e) {
  
    var pax = this.getAttribute("data-value"),
        img_el = document.getElementById("table_layout_img_"+pax);

    if(img_el) {
      img_el.style.display = 'none';
      img_el.style.zIndex = 0;
    }  
  }
  
  // Listen on table layout buttons and display visual layout on hover
  window.addEventListener("load", listenOnTabButtons, false);
  function listenOnTabButtons() {
    var tab_buttons  = document.getElementsByClassName('tab_button');
    
    for (var i = 0; i < tab_buttons.length; i++) {
      tab_buttons[i].addEventListener("click", openTab, false);
    }
  };
  
  // Basic JS for tabs on single product page
  function openTab() {
    // Declare all variables
    var i,
        tabcontent,
        tab_buttons,
        selected_tab_id = this.value,
        selected_tab_content_el = document.getElementById(selected_tab_id);
  

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    // Get all elements with class="tab_buttons" and remove the class "active"
    tab_buttons = document.getElementsByClassName("tab_button");
    for (i = 0; i < tab_buttons.length; i++) {
      tab_buttons[i].classList.remove("active");
    }
  
    // Show the current tab, and add an "active" class to the button that opened the tab
    selected_tab_content_el.style.display = "block";
    this.className += " active";
  } 
  

function showLoader(toggle) {
  var dim_page = document.getElementById("loader_dim_page")

  if ( toggle == true ) {
    dim_page.style.display = "unset";
  }
  else {
    dim_page.style.display = "none";
  }
}
