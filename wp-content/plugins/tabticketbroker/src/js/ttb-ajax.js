// AJAX Callbacks
// Reservation update

// Add function to listen on all td's with class tab_reservation_status and show the child element
// with class change-status-links
jQuery(document).ready(function($) {
	// Show the class on hover for that column
	jQuery("td.tab_reservation_status.column-tab_reservation_status").hover(
		function() {
			jQuery(this).find(".change-status-wrap").show();
		}
		, function() {
			jQuery(this).find(".change-status-wrap").hide();
		}		
	);
});

// Update the reservation status function
function updateResStatus( res_id, status_slug, status_display ) {

	// Show page loader
	jQuery( '#loader_dim_page' ).show();

	jQuery.ajax({
		url: ajaxurl,
		type: "POST",
		data: {
			action: "update_reservation_status_jmd",
			res_id: res_id,
			status_slug: status_slug
		},
		success: function( response ) {
			console.log( response );

			// find and disable all inputs under #res_status_id > div.change-status-links > input
			jQuery('#res_status_' + res_id + ' > div.change-status-links > input').attr('disabled', 'disabled');

			// Show a message that it was updated
			jQuery('#res_status_' + res_id + ' > div.change-status-links').append('<p>Updated! You have to reload the page to update this status again.</p>');

			// Change the inner html of the parent td to the new status
			let parent_div = jQuery('#res_status_' + res_id).parent().find('div.reservation-status');

			// Change the inner html of the parent div to the new status
			parent_div.html(status_display);

			// Change the class of the parent div to the new status
			parent_div.removeClass();
			parent_div.addClass('reservation-status');
			parent_div.addClass('status-' + status_slug);
			
			// Hide the loader
			jQuery( '#loader_dim_page' ).hide();

			// Create a messagebox to show it was successful
			var success_msg = '<div id="ajax_update_success_msg"><p>Reservation status updated successfully! Reload the page to update again.</p></div>';
			jQuery('body').append(success_msg);
		},
		error: function( error ) {
			// Show a message that something went wrong
			jQuery('#res_status_' + res_id + ' > div.change-status-links').append('<p>Something went wrong. Please try again.</p>');

			// Hide the loader
			jQuery( '#loader_dim_page' ).hide();
		}
	});
}




