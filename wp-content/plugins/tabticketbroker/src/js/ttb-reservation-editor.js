jQuery(document).ready(function($) {
    // Add debugging statement to check if the script is loaded
    console.log('ttb-reservation-editor.js loaded');

    // Append the loader element to the body if it doesn't exist
    if ($("#loader_dim_page").length === 0) {
        $("body").append('<div id="loader_dim_page"><div id="loader_page"></div></div>');
    }

    var reservation_orders_table = $("#tab_reservation_wc_orders_linked table");

    reservation_orders_table.find("tr").each(function() {
        var order_item_id = $(this).find("td.order_item_id").find("data").text();

        // If the order_item_id is empty, return
        if (order_item_id === "") {
            return;
        }

        // Get the table_no cell
        var table_no_cell = $(this).find("td.table_no");

        // Get the table_no
        var table_no = table_no_cell.text();

        // Replace the contents with an input field
        table_no_cell.html("<input type='text' value='" + table_no + "' />");
    });

    // When a cell input of the column table_no is changed
    reservation_orders_table.on("change", "td.table_no input", function() {
        // Add debugging statement to check if the change event is triggered
        console.log('Table number input changed');

        // Get the reservation id from the post
        var reservation_id = $("#post_ID").val();

        // Get the table_no from the input
        var table_no = $(this).val();

        // Get the order_item_id from the parent row
        var order_item_id = $(this).closest("tr").find("td.order_item_id data").text();

        // Strip "Show more details" from the order_item_id
        order_item_id = order_item_id.replace("Show more details", "");

        // Show the loader
        $("#loader_dim_page").show();

        console.log('ajaxurl:', ajaxurl);
        console.log('reservation_id:', reservation_id);
        console.log('order_item_id:', order_item_id);
        console.log('table_no:', table_no);

        // Run the ajax call
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'save_table_nos',
                reservation_id: reservation_id,
                order_item_id: order_item_id,
                table_no: table_no
            },
            success: function(data) {
                console.log('AJAX success:', data);
                $("#loader_dim_page").hide();
            },
            error: function(xhr, status, error) {
                console.log('AJAX error:', status, error);
                $("#loader_dim_page").hide();
                alert("Error: " + error);
            }
        });
    });
});