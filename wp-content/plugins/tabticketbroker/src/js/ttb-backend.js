// Hide the elementor pro notification
jQuery(document).ready(function(){
	jQuery(document).ready(function ($) {
		$('h3:contains("Welcome to Elementor Pro!")').parent().parent().hide();
	});
});

// On the assignment page I have to activate screen options myself
jQuery(document).ready(function(){
	var urlParams = new URLSearchParams(window.location.search); //get all parameters
	var page_name = urlParams.get('page'); //extract the foo parameter - this will return NULL if foo isn't a parameter

	if(page_name){ // is the page set?
		if (page_name == 'tab_admin_assignment_mgmt') { // is the page the assignment page?
			jQuery("#show-settings-link").click(function () {	
				if(	jQuery("#screen-options-wrap").css('display') == 'none' || 
					jQuery("#screen-options-wrap").css('display') == '' || 
					jQuery("#screen-options-wrap").css('display') == 'none !important') {
						jQuery("#screen-options-wrap").css("cssText", "display: block !important;");
				} else
				if( jQuery("#screen-options-wrap").css('display') == 'block'){
					jQuery("#screen-options-wrap").css("cssText", "display: none !important;");
				}
			});
		}
	}
});

// Hack the reset button of the filter to reset the year to the current year
jQuery(window).load(function () {

	// Check the url for get variable post_type=tab_reservation
	if (window.location.href.indexOf('post_type=tab_reservation') > -1) {

		// Find the value of the select with the id tab_reservation_event_year and put it in a variable
		var tab_reservation_event_year = jQuery("#tab_reservation_event_year").val();

		// if tab_reservation_event_year = all use the current year
		if (tab_reservation_event_year == 'all') {
			var d = new Date();
			tab_reservation_event_year = d.getFullYear();

			// if the month is 11 or higher use the next year
			if (d.getMonth() >= 11) {
				tab_reservation_event_year = d.getFullYear() + 1;
			}
		}

		// Find the filter button that has an id of "post-query-submit" and name of "filter_action" and add another button after it
		jQuery("#post-query-submit").after(
			'<a class="button" href="edit.php?post_type=tab_reservation&tab_reservation_event_year='+tab_reservation_event_year+'" class="button">Reset</a>'
			);
	}
});

jQuery(document).on( 'click', '#ttb_linked_order_ids_btn', function() {
	var loader_el = jQuery( '#loader_dim_page' );
	var search_el_wrap = jQuery( '#ttb_search_orders_wrap' );
	var search_bg_el = jQuery( '#ttb_search_background' );

	loader_el.show();

	wp.ajax.post( "ajaxSearchOrders", {} )
		.done( function( response ) {
			search_el_wrap.html( '' );
			search_el_wrap.html( response );
			search_el_wrap.show();
			search_bg_el.show();
			loader_el.hide();
			jQuery( '#order_search' ).focus();
		} )
		.fail( function( response ) {
			loader_el.hide();
		} );
} );

// Close the search if the user clicks outside of the search box
jQuery(document).on( 'click', '#ttb_search_background', function() {
	jQuery( '#ttb_search_orders_wrap' ).hide();
	jQuery( '#ttb_search_background' ).hide();
} );

// Close the search if the user presses the escape key
jQuery(document).keyup(function(e) {
	if (e.keyCode == 27) {
		jQuery( '#ttb_search_orders_wrap' ).hide();
		jQuery( '#ttb_search_background' ).hide();	
	}
} );

jQuery(document).on( 'click', '#close_order_search', function() {
	jQuery( '#ttb_search_orders_wrap' ).hide();
	jQuery( '#ttb_search_background' ).hide();
} );

// Listen to typing in the search box
jQuery(document).on( 'keyup', '#order_search', function() {
	// run the orderSearch function 
	orderSearch();
} );

function orderSearch() {
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('order_search');
    filter = input.value.toUpperCase();

	// Get all child a elements of ttb_order_items
	var order_links = document.getElementById("ttb_order_items").getElementsByTagName("a");
	
	// Hide the order_links that don't match the search
	for (i = 0; i < order_links.length; i++) {
		// Skip the close button close_order_search
		if (order_links[i].id == 'close_order_search') {continue;}
		
		if (order_links[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
			order_links[i].style.display = "";
		} else {
			order_links[i].style.display = "none";
		}
	}
}

// Listen to clicking on order items except the close button
jQuery(document).on( 'click', '#ttb_order_items a:not(#close_order_search)', function() {
	var button = jQuery(this);
	var order_id = jQuery( '#post_ID' ).val();   // current order ID
	var link_order_id = button.data('order-id'); // order ID of the clicked link

	jQuery( '#ttb_search_orders_wrap' ).hide();
	jQuery( '#ttb_search_background' ).hide();

	// Run the ajaxAddLinked order function with the order_id as a parameter
	wp.ajax.post( "ajaxLinkOrder", {
		order_id: order_id,
		link_order_id: link_order_id
	} )
		.done( function( response ) {
			// Add the response to the ttb_linked_orders div
			// jQuery("#ttb_linked_orders").html( response );

			// Get the order_id of the clicked link and put it in a variable
			var link_order_id = button.data('order-id');
			var order_number = button.text();

			// Create a new order item in the linked_orders div
			jQuery("#ttb_linked_orders").append(
				'<a data-order-id="'+link_order_id+'" class="ttb_linked_order">'+order_number+'</a>'
				);

			// Hide the description for no orders found in the p tag with id ttb_no_orders_found
			jQuery("#ttb_no_orders_found").hide();
			
			// Hide the loader
			jQuery("#loader_dim_page").hide();

			// Log the response to the console
			console.log( response );
		} )
		.fail( function( response ) {
			// Hide the loader
			jQuery("#loader_dim_page").hide();
		} );

		// Hide the search box
		jQuery("#ttb_search_orders_wrap").hide();

} );

// Listen to all ttb_linked_orders a elements and run the ajaxRemoveLinkedOrder function when clicked
jQuery(document).on( 'click', '#ttb_linked_orders a', function() {
	var button = jQuery(this);
	var order_id = jQuery( '#post_ID' ).val();   // current order ID
	var link_order_id = button.data('order-id'); // order ID of the clicked link

	// Show the loader
	jQuery("#loader_dim_page").show();
	
	// Run the ajaxRemoveLinkedOrder function with the order_id as a parameter
	wp.ajax.post( "ajaxRemoveLinkedOrder", {
		order_id: order_id,
		link_order_id: link_order_id
	} )
		.done( function( response ) {
			// Remove the clicked link from the ttb_linked_orders div
			button.remove();
			
			// Hide the loader
			jQuery("#loader_dim_page").hide();
		} )
		.fail( function( response ) {
			// Hide the loader
			jQuery("#loader_dim_page").hide();
		} );
	// Remove the clicked order item from the linked_orders div
});


// When a reservation has been added from the supplier window and it is saved we 
// navigate to the supplier window with the variable new_reservation and highlight 
// the new reservation in the table in the div with id tab_supplier_reservations
jQuery(document).ready(function() {
	// Check if the variable new_reservation is set in the url
	if (window.location.href.indexOf("new_reservation") > -1) {
		// Get the reservation_id from the url
		var reservation_id = window.location.href.split('new_reservation=')[1];
		
		// // Remove the variable from the url
		// window.history.pushState("object or string", "Title", "/wp-admin/admin.php?page=supplier_reservations");
		
		// Find the table
		var table = jQuery("#tab_supplier_reservations table");

		// Find the row that has td with class reservation_id and an a value that matches "RS"+reservation_id
		var row = table.find("td.reservation_id a:contains('RS"+reservation_id+"')").closest("tr");

		// Add the class ttb_highlight to the row
		row.addClass("ttb_highlight");

		// Add text "(new)" to the row
		row.find("td.reservation_id a:contains('RS"+reservation_id+"')").text("RS"+reservation_id+" **new**");
	}
});

// When scroll to top button is clicked scroll to top
jQuery(document).on( 'click', '#scroll_to_top', function() {
	jQuery('html, body').animate({scrollTop:0}, 'slow');
});

