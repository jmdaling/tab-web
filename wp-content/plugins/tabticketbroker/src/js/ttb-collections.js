jQuery(document).ready(function($) {
    // When the sub reservation status is clicked
    $('.ttb-parcel-status').click(function(e) {
        // Prevent default
        e.preventDefault();
        
        // Show the page loader
        $('#loader_dim_page').css('display', 'block');

        // Get the post id from the clicked data-parcel-id attribute
        var post_id = $(this).data('parcel-id');

        var clicked_link = $(this);
        
        // Get the status
        var status = $(this).data('status');

        console.log( 'post_id: ' + post_id + ' status: ' + status );
        
        // Save status change
        $.ajax({
            type: 'POST',
            url: ajax_obj.ajaxurl,
            data: {
                action: 'parcel_meta_status_change',
                post_id: post_id,
                status: status,
                nonce: ajax_obj.nonce
            },
            success: function( response ) {

                // Add new log to the row
                var newLog = '<li>just now: ' + status + '</li>';
                // Find the <ul> element with the matching data-parcel-order-id attribute
                var logList = $('ul[data-parcel-order-id="' + post_id + '"]');
                // Append the new log entry to the <ul>
                logList.append(newLog);
                
                // Get the clicked cell
                var cell = $('.ttb-parcel-status[data-parcel-id="' + post_id + '"]');

                // Mark all spans as dashicons-no except the one that was clicked
                cell.find('span').removeClass('dashicons-yes dashicons-no');
                cell.find('span').addClass('dashicons-no');

                // Mark the clicked child span as dashicons-yes (data-status is not working)
                clicked_link.find('span').removeClass('dashicons-no');
                clicked_link.find('span').addClass('dashicons-yes');

                $('#loader_dim_page').hide();
                // If successful, refresh the page
                console.log( 'Success: ' + response );
            },
            error: function( response ) {
                $('#loader_dim_page').hide();
                // If error, show the error
                alert( response );
            }
        });

        // Hide the page loader
    });

    // When the FrontEnd collected button is clicked
    $('.ttb-collection-actions-btn').click(function(e) {
        // Prevent default
        e.preventDefault();

        console.log('clicked');

        // Request confirmation
        var confirm_msg = $(this).data('confirm-msg');
        // Replace \n with new line
        confirm_msg = confirm_msg.replace(/\\n/g, '\n');
        var confirm = window.confirm(confirm_msg);

        if ( ! confirm ) {
            return;
        }

        // Show the page loader
        $('#loader_dim_page').css('display', 'block');

        // Get the order id
        var order_id = $(this).data('order-id');

        // Get the next status from data-status-value
        var next_status = $(this).data('status-value');

        // Get the username
        var username = $('#ttb_collections_username').val() || '';

        // Clicked btn
        var clicked_btn = $(this);

        // Call the ajax function
        $.ajax({
            type: 'POST',
            url: ajax_obj.ajaxurl,
            data: {
                action: 'parcel_meta_status_change',
                post_id: order_id,
                status: next_status,
                username: username,
                nonce: ajax_obj.nonce
            },
            success: function( response ) {
                $('#loader_dim_page').hide();

                console.log('next status: '+next_status);

                // If next_status = collected
                if ( next_status == 'collected' ) {
                    console.log('status collected: '+next_status);
                    // Disable the button
                    clicked_btn.prop('disabled', true);
                    // Change the button text
                    clicked_btn.text('Collected');

                    // Flash the row green 
                    clicked_btn.closest('tr').css('background-color', '#dff0d8');

                    // fade out btn row after 5 seconds
                    setTimeout(function() {
                        clicked_btn.closest('tr').fadeOut();
                    }
                    , 5000);
                }
                // If next_status = received
                else if ( next_status == 'at_pickup_office' ) {
                    console.log('status received: '+next_status);
                    // Change the button text
                    clicked_btn.text('Collected by Customer');
                    // Flash the row green
                    clicked_btn.closest('tr').css('background-color', '#dff0d8');

                    // Return the row color to normal after 1.5 seconds
                    setTimeout(function() {
                        clicked_btn.closest('tr').css('background-color', '');
                        // remove button-secondary and add primary
                        clicked_btn.removeClass('btn-secondary');
                        clicked_btn.addClass('btn-primary');

                        // change status value
                        clicked_btn.data('status-value', 'collected');

                        // change confirm msg
                        clicked_btn.data('confirm-msg', 'Are you sure the customer collected this package?');
                        
                    }
                    , 1500);
                }
            },
            error: function( response ) {
                $('#loader_dim_page').hide();
                // If error, show the error
                alert( response );
            }
        });
    });
});



// jQuery(document).ready(function($) {

//     $('#loader_dim_page').show();

//     // When the Collect/Undo button is clicked
//     jQuery(document).ready(function($) {
//         $('.collection-btn').click(function() {
//             var activity = '';
//             // if the button has class btn-collected-undo, call the undoCollection function
//             if ($(this).hasClass('btn-received')) {
//                 activity = 'received';
//             } 
//             else if ($(this).hasClass('btn-received-undo')) {
//                 activity = 'undo received';
//             }
//             else if ($(this).hasClass('btn-collected')) {
//                 activity = 'undo collection';
//             }
//             else if ($(this).hasClass('btn-collected-undo')) {
//                 activity = 'undo collection';
//             }

//             logActivity($(this), activity, jQuery);
//         });
//     });

//     // Hide the loader
//     $('#loader_dim_page').hide();
// });

// function logActivity(btn, activity, $) {

//     if ( ! confirmActivity(activity) ) return;

//     // Show the loader
//     $('#loader_dim_page').show();

//     var order_id = btn.data('order-id');

//     console.log("clicked activity: "+activity+" order_id: "+order_id);

//     // get username from #ttb_collections_username, set to '' if the field doesn't exist
//     var username = $('#ttb_collections_username').val() || '';

//     // Send ajax request to update order
//     $.ajax({
//         url: ajax_obj.ajaxurl,
//         type: 'POST',
//         data: {
//             action: 'mark_pickup_item_collected',
//             order_id: order_id,
//             activity: activity,
//             status: 1,
//             username:username
//         },
//         success: function( response ) {            
//             doActivitySuccess(btn, activity, $);
//             doLimitedUserActivitySuccess(btn, activity, $, username);
//             $('#loader_dim_page').hide();
//         },
//         error: function(data) {
//             jQuery("#loader_dim_page").hide();
//             alert("Error: "+data);
//         }
//     });
// }

// function doActivitySuccess(btn, activity, $) {
//     // Update button
//     if (activity == 'received') {
//         btn.removeClass('btn-primary btn-received');
//         btn.addClass('btn-danger btn-received-undo');
//         btn.text('Undo Received');

//         // Add a new button to collect the order
//         var btn_collect = $('<button class="btn btn-primary collection-btn btn-collected">Collected</button>');
//         btn.closest('td').append(btn_collect);

//     }
//     else if (activity == 'undo received') {
//         btn.removeClass('btn-received-undo btn-danger');
//         btn.addClass('btn-primary');
//         btn.text('Received');

//         // Remove the collect button
//         btn.closest('td').find('button.btn-collected').remove();
//     }
//     else if (activity == 'collection') {
//         btn.removeClass('btn-primary btn-collected');
//         btn.addClass('btn-danger btn-collected-undo');
//         btn.text('Undo Collection');
//     }
//     else if (activity == 'undo collection') {
//         btn.removeClass('btn-collected-undo');
//         btn.addClass('btn-primary');
//         btn.text('Collected');
//     }

//     // Clear the cell with collected_info
//     btn.closest('tr').find('td.collected_info').text('');

//     // Flash the row green for a second
//     btn.closest('tr').css('background-color', '#dff0d8');

//     setTimeout(function() {
//         // unset background color
//         btn.closest('tr').css('background-color', '');
//     }, 1500);
// }

// function doLimitedUserActivitySuccess(btn, activity, $, username) {
//     // if username is set, remove the row
//     // This is because the username will only be set by the collection administrator at the 
//     // pickup location by logging into the frontend with php auth. The username is set then.
//     // And this role should not be able to undo it, only registered admins with access to the 
//     // Pickup report should be able to do this
//     if (username) {    
//         // If activity is collection, remove the row
//         if (activity == 'collection') {
//             // fade out btn row after 5 seconds
//             setTimeout(function() {
//                 btn.closest('tr').fadeOut();
//             }, 5000);
//         }
//     }
// }


// function confirmActivity(activity) {
//         // Show a confirmation dialog based on the activity
//         var confirm = '';
//         if (activity == 'received') {
//             confirm = window.confirm('Are you sure you want to mark this order as received?');
//         }
//         else if (activity == 'undo received') {
//             confirm = window.confirm('Are you sure you want to undo this?');
//         }
//         else if (activity == 'collection') {
//             confirm = window.confirm('Are you sure you want to mark this order as collected?');
//         }
//         else if (activity == 'undo collection') {
//             confirm = window.confirm('Are you sure you want to undo this?');
//         }
    
//         if ( ! confirm ) {
//             return;
//         }

//         return true;
// }

// //############################################################################################################

// function createFilterBtns() {
    
//     // Add a button to filter collected orders to div collections_filter
//     var btn_filter_collected = $('<button class="btn btn-primary">Filter Collected</button>');

//     // Add a button to filter collected orders to div #collections_filter
//     jQuery('#collections_filter').append(btn_filter_collected);

//     console.log("added!! ");
// }

// function undoCollection(btn) {

//     $ = jQuery;

//     // Show a confirmation dialog
//     var confirm = window.confirm('Are you sure you want to undo this?');

//     if ( ! confirm ) {
//         return;
//     }

//     // Show the loader
//     $('#loader_dim_page').show();

//     var order_id = btn.data('order-id');
//     var username = $('#ttb_collections_username').val() || '';

//     // Send ajax request to update order
//     $.ajax({
//         url: ajax_obj.ajaxurl,
//         type: 'POST',
//         data: {
//             action: 'mark_pickup_item_collected',
//             order_id: order_id,
//             is_collected: 0,
//             username: username
//         },
//         success: function( response ) {

//             console.log("undo-success");
//             // Update button
//             btn.removeClass('btn-danger btn-collected-undo'); 
//             btn.addClass('btn-primary');
//             btn.text('Collected');

//             // Clear the cell with collected_info
//             btn.closest('tr').find('td.collected_info').text('');
            
//             // Flash the row green for a second
//             btn.closest('tr').css('background-color', '#dff0d8');

//             setTimeout(function() {
//                 // unset background color
//                 btn.closest('tr').css('background-color', '');
//             }, 1500);
            
//             // Hide the loader
//             $('#loader_dim_page').hide();
//         },
//         error: function(data) {
//             jQuery("#loader_dim_page").hide();
//             alert("Error: "+data);
//         }
//     });
// }

