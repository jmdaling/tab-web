
    // When the sub reservation notes are changed
    $('.ttb-sub-reservation-notes').change(function(e) {
        // Prevent default
        e.preventDefault();

        // Show the page loader
        $('#loader_dim_page').css('display', 'block');
        
        // Get the sub reservation id
        var sub_res_id = $(this).data('sub-res-id');

        // Get the notes
        var notes = $(this).val();

        // Get the sub reservation
        var sub_res = sub_reservations[sub_res_id];

        // Set the notes
        sub_res['notes'] = notes;

        // Update the sub reservation
        sub_reservations[sub_res_id] = sub_res;

        // Save Notes
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'save_sub_reservations',
                'sub_reservations': sub_reservations,
                reservation_id: $('#post_ID').val(),
            },
            success: function( response ) {
                $('#loader_dim_page').hide();
                // If successful, refresh the page
                console.log( response );
            },
            error: function( response ) {
                $('#loader_dim_page').hide();
                // If error, show the error
                alert( response );
            }
        });
    });