// Show/Hide loader
function showLoader(toggle) {
  var dim_page = document.getElementById("loader_dim_page");

  if (toggle == true) {
    dim_page.style.display = "unset";
  } else {
    dim_page.style.display = "none";
  }
}

// Find the parent of the first figure element with class woocommerce-product-gallery__wrapper
// And find the div with id custom_prod_title and move it to the parent of the figure element
// This is to move the title above the image gallery
jQuery(document).ready(function ($) {
  // Show loader at start
  showLoader(true);

  var prod_title = document.getElementById("custom_prod_title");
  var prod_title_parent = prod_title.parentNode;

  var prod_gallery = document.getElementsByClassName(
    "woocommerce-product-gallery__wrapper"
  )[0];
  var prod_gallery_parent = prod_gallery.parentNode;

  prod_title_parent.removeChild(prod_title);
  prod_gallery_parent.insertBefore(prod_title, prod_gallery);

  // Hide page loader after executing all JS
  showLoader(false);
});
