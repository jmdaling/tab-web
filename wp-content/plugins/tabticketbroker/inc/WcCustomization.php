<?php

/**
 * @package tabticketbroker
 */

namespace Inc;

use Inc\Base\BaseController;
use Inc\Classes\Dimension;
use Inc\Classes\HardCoded;
use Inc\Tools\OrderTools;
use Inc\Classes\JMD_Logger;
use Inc\Classes\OrderLifeCycle;
use Inc\Classes\TabReservation;
use Inc\Classes\AssignmentManager;
use Inc\Tools\Utils;

use WP_Query;

use Vendidero\Germanized\Shipments\Order;

class WcCustomization extends BaseController {
    public $order           = null;
    public $order_tools     = null;
    public $ass_mgmt        = null;
    public $dimension       = null;
    public $utils           = null;

    public function register() {
        // Custom Order Meta Data
        $this->maintainOrderMetaData();
        
        // Order statuses
        $this->customizeOrderStatuses();

        // Order overview
        $this->customizeOrdersOverview();

        // Order page
        $this->customizeOrderDetailPage();

        // Customize WooCommerce Germanized
        $this->wcGzdCustomized();

        // Display any flash notices (see: https://webprogramo.com/admin-notices-after-a-page-refresh-on-wordpress/1183/)
        add_action('admin_notices', array($this, 'displayFlashNotices'), 12);

        // Save language for each order at order creation
        add_action('woocommerce_checkout_update_order_meta', array($this, 'saveOrderLanguage'), 10, 1);

        // Set the pickup flag for each order at order creation
        add_action('woocommerce_checkout_update_order_meta', array($this, 'saveOrderPickupFlag'), 10, 1);

        // Save event year for each order at order creation
        add_action('woocommerce_checkout_update_order_meta', array($this, 'saveOrderEventYear'), 10, 1);

        // Also save pickup address if chosen
        add_action('woocommerce_checkout_update_order_meta', array($this, 'overrideShippingAddress'), 10, 1);

        // Change price display
        add_filter('woocommerce_get_price_html', array($this, 'changePricePrefix'), 99, 2);

        // Web specific customizations
        if (TTB_SITE == 'ofest') {
            // Customize the Product pages
            add_action('woocommerce_before_single_product', array($this, 'customizeProductPage_okt'));
        }
        if (TTB_SITE == 'tabtickets') {
            // Customize the Product pages
            add_action('woocommerce_before_single_product', array($this, 'customizeProductPage_tt'));

            // Set custom title with dates and duration: shop product loop
            add_filter('the_title',  array($this, 'addDateAboveTitle_tt'), 10, 2);

            // Reset product ALT text to product title 
            // This was required because the above the_title filter also changed the alt text of the product image
            // that displayed <span> tags in the alt text as html on load, undesirable
            add_filter('wp_get_attachment_image_attributes', array($this,  'resetProductAltText_tt'), 20, 2);

            // Add action to saving of variation
            add_action('woocommerce_save_product_variation', array($this, 'saveEarliestDateToParentProduct'), 10, 2);

            // Filter products in the shop by date (override customerizer settings)
            add_filter('woocommerce_get_catalog_ordering_args', array($this, 'customSortProducts'), 1);

            // Override the option to hide empty terms of the Husky product filter
            // Additionally to hide a term in the filter the product status should be set to "draft"
            update_option('woof_hide_dynamic_empty_pos', 1);
        }

        // Hide shipping on cart page & display notice
        add_filter('woocommerce_cart_ready_to_calc_shipping', array($this, 'hideShippingOnCartPage'), 10);
        add_action('woocommerce_cart_totals_before_order_total', array($this, 'showShippingNotice'), 99);

        // Keeping this for possible future change:
        // Customize WC price front page, repeat germanized add ons
        // add_filter( 'woocommerce_get_price_html', array( $this, 'addInfoAfterPrice' ) );

        // Add active class to Nav menu when in single product page
        add_filter('nav_menu_css_class', array($this, 'wpsites_nav_class'), 10, 2);

        // Add back to products button on cart page
        add_action('woocommerce_before_cart', array($this, 'addBacktoProductsButton'), 10);

        // Add back to products button below cart totals
        add_action('woocommerce_after_cart_totals', array($this, 'addBacktoProductsButton'), 10);

        // Add back to products button after submit order button
        add_action('woocommerce_review_order_after_submit', array($this, 'addBacktoProductsButton'), 10);

        // Make telephone required in cart
        add_filter('woocommerce_billing_fields', array($this, 'makeTelephoneRequired'));

        // VariationsAdmin: Add additional save button on top of variations
        add_action('woocommerce_variable_product_before_variations', array($this, 'addSaveVariationsButton'), 10);

        // VariationsAdmin: Show SKU in variations
        add_action('admin_print_scripts', array($this, 'showSku'), 100);

        // Translate product variation labales and names using filters
        add_filter('woocommerce_variation_option_name', array($this, 'translateAttributesLabel'));
        add_filter('woocommerce_attribute_label', array($this, 'translateVariationOptionName'));

        // Customize Out of Stock (notification to admin) email
        add_filter('woocommerce_email_content_no_stock', array($this, 'customEmailContentNoStock'), 20, 2);

        // Check products for incompatible settings
        add_action('admin_notices', array($this, 'checkProducts'));

        // MINI DESCRIPTIONS & Faktur Pro: Dynamically replace dates in the description
        add_filter('woocommerce_product_variation_get__mini_desc', array($this, 'generateMiniDesc'), 10, 2);

        // Intercept DHL integration status change
        // add_filter( 'rest_dispatch_request', array( $this, 'jmdInterceptDhlOrderStatusChangeRequest' ), 10, 2 );

        // Intercept API data provided to DHL
        // add_filter( 'rest_post_dispatch', array( $this, 'jmdInterceptDhlShipmentData' ), 10, 3 );

    }

    /** 
     * Custom Order Meta Data
     * Add custom meta data to orders
     * For now only the earliest item date is added to the order from here
     */
    public function maintainOrderMetaData() {
        // Add custom meta data to orders
        $order_tools = new OrderTools();
        // Run on order creation
        add_action( 'woocommerce_new_order', array( $order_tools, 'setEarliestItemDate' ), 10, 1 );
        // Run on order update
        add_action( 'woocommerce_update_order', array( $order_tools, 'setEarliestItemDate' ), 10, 1 );
    }

    // public function jmdInterceptDhlOrderStatusChangeRequest( $blank, $request ) 
    // {
    //     // Only intercept if the request is for a WC order
    //     // if ( $request->namespace == 'wc/v3' && $request->route == '/orders/(?P<id>[\d]+)' ) {
    //     //     $user = get_user_by( 'id', get_current_user_id() );
    //     //     if ( $user->user_login == 'keyaccount' ) {
    //     //         return $blank;
    //     //     }
    //     // }

    //     // Check if the user is from DHL (keyaccount)
    //     // Check if the id parameter is set and if it is an order in the system, and if the status param is set to completed, else return
    //     $user = get_user_by( 'id', get_current_user_id() );
    //     if (   ! $request->get_param('id') 
    //         || ! wc_get_order( $request->get_param('id') ) 
    //         || ! $request->get_param('status') 
    //         || ! $user->user_login == 'keyaccount'
    //         ) {
    //         return $blank;
    //     }

    //     if ( $request->get_param('status') == 'completed' && $request->get_route() == '/wc/v3/orders/'. $request->get_param('id') ) {
    //         $request->set_param('status', 'labelled');
    //         return $request;
    //     }
    //     else {
    //         return $blank;
    //     }
    // }


    // /**
    //  * We do not want financial data or the tent name to display on the waybill for DHL. These
    //  * were sent through by the rest api to the integration. Here we intercept the data and clear
    //  * the financial data and replace the tent name with the SKU.
    //  */
    // public function jmdInterceptDhlShipmentData( $response, $server, $request )
    // {
    //     // If the route does not contain "wc/v3/orders" return
    //     if ( strpos( $request->get_route(), 'wc/v3/orders' ) === false ) {
    //         return $response;
    //     }

    //     // If not a request from the DHL user, return
    //     $user = get_user_by( 'id', get_current_user_id() );
    //     // And if user is not set
    //     if ( ! $user || $user->user_login != 'keyaccount' ) {
    //         return $response;
    //     }

    //     // Check if the response has data
    //     if ( ! isset( $response->data ) ) {
    //         return $response;
    //     }

    //     // Check response status
    //     if ( $response->status != 200 ) {
    //         return $response;
    //     }

    //     // If it is the response of and edit request, return the response
    //     if ( $request->get_param('status') == 'labelled' 
    //         && $request->get_param('context') == 'edit'
    //         ) {
    //         return $response;
    //     }

    //     // Loop through data in response and clear all financial data
    //     foreach ( $response->data as $main_key => $single_response ) {
    //         // if Single response is not an array, skip
    //         if ( !is_array( $single_response ) ) {
    //             continue;
    //         }

    //         foreach ( $single_response as $key => $value ) {
    //             if ( 
    //                 $key == 'discount_total' 
    //              || $key == 'discount_tax' 
    //              || $key == 'shipping_total'
    //              || $key == 'shipping_tax'
    //              || $key == 'cart_tax' 
    //              || $key == 'total' 
    //              || $key == 'total_tax' 
    //              ) {
    //              $response->data[$main_key][$key] = 0;
    //             }

    //             // Also clear all line items
    //             if ( $key == 'line_items' ) {
    //                 foreach ( $response->data[$main_key][$key] as $line_key => $line_value ) {
    //                     $response->data[$main_key][$key][$line_key]['name'] = $response->data[$main_key][$key][$line_key]['sku'];
    //                     $response->data[$main_key][$key][$line_key]['parent_name'] = $response->data[$main_key][$key][$line_key]['sku'];
    //                     $response->data[$main_key][$key][$line_key]['total'] = 0;
    //                     $response->data[$main_key][$key][$line_key]['total_tax'] = 0;
    //                     $response->data[$main_key][$key][$line_key]['subtotal'] = 0;
    //                     $response->data[$main_key][$key][$line_key]['subtotal_tax'] = 0;
    //                     $response->data[$main_key][$key][$line_key]['price'] = 500;
    //                 }
    //             }
    //         }
    //     }

    //     return $response;

    // }


    /**
     * DHL's integration picks up all orders in ready-to-ship status and sends them to DHL.
     * Although this status, the one that defines which orders are imported, is configurable 
     * it then writes back to WC and sets the order to completed. As we want more control we
     * use the user to identify that it is a DHL request and change the status to Labelled 
     * (wc-labelled) instead.
     */
    function jmdInterceptDhlStatusChange($order_id) {

        // Get the user id making the request
        $user_id = get_current_user_id();

        // Check if the username is keyaccount
        $user = get_user_by('id', $user_id);
        if ($user->user_login == 'keyaccount') {

            $order = wc_get_order($order_id);
            // if order status = 'wc-completed' then change to 'wc-labelled'
            if ($order->get_status() == 'completed') {
                $order->update_status('labelled');

                // Add note to order
                $order->add_order_note(
                    sprintf(
                        __('DHL writeback intercepted: Order status changed to %s', 'woocommerce'),
                        wc_get_order_status_name('wc-labelled')
                    )
                );
            }


            // Save the data to the order
            $order->save();
        }
    }

    /**
     * Mini descriptions OR Cart descriptions are exported to the Faktur Pro invoice (WooRechnung).
     * These descriptions are generic except for the date. This function replaces the date in the description. 
     * Mini descriptions are set on the main products
     * Below is the code-location of where woorechnung gets the desc (and where we hook our filter, check the test_fakturpro section in the debug page) 
     *      /wp-content/plugins/woorechnung/includes/common/class-fp-factory.php
     * Available placeholders:
     *  {prod_name}             : Tent name
     *  {pax}                   : Amount of people
     *  {date}                  : date in english
     *  {date_de}               : date in german
     *  {timeslot}              : timeslot
     *  {timeslot_de}           : timeslot in german
     *  {area}                  : area
     *  {area_de}               : area in german
     *  {table_arrangement}     : e.g. 04 seats on a 10-seater table
     *  {table_arrangement_de}  : e.g. 04 Sitzplätze an 10er Einzeltisch
     *  {kleingruppe}           : if < 7 pax = (Small groups)
     *  {kleingruppe_de}        : if < 7 pax = (Kleingruppe)
     */
    public function generateMiniDesc($mini_desc, $product) {
        // Check if the Dimension class is loaded and initialized
        if (!isset($this->dimension) || !$this->dimension->isInitialized()) {
            $this->dimension = new Dimension();
            $this->dimension->initialize();
        }

        // Get SKU from product
        $sku_attr = $this->dimension->getSkuAttributes($product->get_sku());

        // Get mini description parent product
        $parent_product = wc_get_product($product->get_parent_id());
        $mini_desc = $parent_product->get_meta('_mini_desc');

        // Check if mini description is set
        if ($mini_desc == '') {
            return '';
        }

        // Timeslot and area need to be retrieved and manually translated
        $timeslot_de = ucfirst($this->dimension->sku_timeslots[$sku_attr['timeslot']]);
        $timeslot = $this->dimension->translateTimeslot($timeslot_de);

        $area_de = $sku_attr['area'] ? ucfirst($this->dimension->sku_areas[$sku_attr['area']]) : '';
        $area = $this->dimension->translateArea($area_de);

        // Replace all placeholders with the correct values
        $placeholders = array(
            '{prod_name}'           => $parent_product->get_name(),
            '{pax}'                 => $sku_attr['pax'],

            '{date}'                => $this->getDisplayDateAdvanced($this->dimension->sku_dates[$sku_attr['day']], 'en_GB'),
            '{date_de}'             => $this->getDisplayDateAdvanced($this->dimension->sku_dates[$sku_attr['day']]),

            '{timeslot}'            => $timeslot,
            '{timeslot_de}'         => $timeslot_de,

            '{area}'                => $area ? ', Area ' . $area : '',
            '{area_de}'             => $area_de ? ', Bereich ' . $area_de : '',

            '{table_arrangement}'   => $this->getTableArrangementTxt($sku_attr['pax'])['en'],
            '{table_arrangement_de}' => $this->getTableArrangementTxt($sku_attr['pax'])['de'],

            '{kleingruppe}'         => $sku_attr['pax'] < 8 ? '(Small groups) ' : '',
            '{kleingruppe_de}'      => $sku_attr['pax'] < 8 ? '(Kleingruppe) ' : '',
        );

        // Replace all placeholders with the correct values
        $mini_desc = str_replace(array_keys($placeholders), array_values($placeholders), $mini_desc);

        return $mini_desc;
    }

    /**
     * From the pax amount get the table arrangement text which is specifically defined by 
     * table sizes.
     * @param  int $pax
     */
    public function getTableArrangementTxt($pax) {

        switch ($pax) {
            case $pax < 8:
                $table_txt['de'] = $pax . ' Sitzplätze an 10er Einzeltisch';
                $table_txt['en'] = $pax . ' seats on a 10-seater table';
                break;

                // check if pax is divisible by 8
            case $pax % 8 == 0:
                $no_of_tables = $pax / 8;
                $table_txt['de'] = $pax . ' Plätze an ' . $no_of_tables . ' Tisch(en)';
                $table_txt['en'] = $pax . ' seats on ' . $no_of_tables . ' table(s)';
                break;

            case $pax % 10 == 0:
                $no_of_tables = $pax / 10;
                $table_txt['de'] = $pax . ' Plätze an ' . $no_of_tables . ' Tisch(en)';
                $table_txt['en'] = $pax . ' seats on ' . $no_of_tables . ' table(s)';
                break;

            case $pax > 8:
                // divide pax by 10 and round up to nearest integer
                $no_of_tables = ceil($pax / 10);
                $table_txt['de'] = $pax . ' Sitzplätze an ' . $no_of_tables . ' Tisch(en)';
                $table_txt['en'] = $pax . ' seats on ' . $no_of_tables . ' table(s)';
                break;

            default:
                $table_txt['de'] = 'ERROR: table txt not defined';
                $table_txt['en'] = 'ERROR: table txt not defined';
                break;
        }

        return $table_txt;
    }

    /** 
     * Check products for misconfiguration of its attributes and variations. 
     * 
     * ONLY Date, Timeslot & Area is checked. These are the attributes used in the assignment comparison to match 
     * reservations/tickets with order items.
     * 
     * Best explained by example:
     * A product cannot have 2 dates in its attributes but not have them part of 2 variations. A customer can then 
     * buy a ticket without the system knowing which date they want to attend. The assignment module will be unable 
     * to match the reservation/ticket with the order.
     * 
     * Always perform the check on TABTICKETS (Due to the small number of variable products and the high risk of 
     * misconfiguration)
     * Only perform the check on OFEST if the product_check flag is set (Due to the large number of variable 
     * products and the reasonably stable configuration)
     * 
     * TECH DETAILS: Get the 3 attributes from the parent product and if there are variations, check each one and override
     * the parent attributes. For any of the 3 with multiple values, add a notice to the admin on the products page.
     */
    public function checkProducts() {
        // Check if user is on the products page
        if (! isset($_GET['post_type']) || $_GET['post_type'] != 'product') {
            return;
        }

        // Check if we are in ofest, if so, check if the product_check flag is set
        if (TTB_SITE == 'ofest' && ! isset($_GET['product_check'])) {
            return;
        }

        // Get all products
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'published',
        );

        $products = get_posts($args);

        // Check each product
        foreach ($products as $product) {

            $parent_product = wc_get_product($product->ID);

            $default_properties = array(
                'multiple_values' => false,
                'value'           => '',
            );

            $product_attribute = array(
                'date'      => $default_properties,
                'area'      => $default_properties,
                'timeslot'  => $default_properties,
            );

            $product_attribute['date']['value']       = $parent_product->get_attribute('pa_date');
            $product_attribute['area']['value']       = $parent_product->get_attribute('pa_area');
            $product_attribute['timeslot']['value']   = $parent_product->get_attribute('pa_timeslot');

            // For each attribute value check if there are multiple values
            foreach ($product_attribute as $attribute_name => $attribute_property) {
                if (strpos($attribute_property['value'], ',') !== false) {
                    $product_attribute[$attribute_name]['multiple_values'] = true;
                }
            }

            // Check if product is variable
            if ($parent_product->is_type('variable')) {
                // If it is, we go through all variations and overwrite the attribute values
                // Get all variations
                $variations = $parent_product->get_available_variations();

                // Check each variation
                foreach ($variations as $variation) {
                    $variation_product = wc_get_product($variation['variation_id']);

                    // Overwrite attribute value with non-blank values of the variation
                    $product_attribute['date']['value']       = $variation_product->get_attribute('pa_date') ? $variation_product->get_attribute('pa_date') : $product_attribute['date']['value'];
                    $product_attribute['area']['value']       = $variation_product->get_attribute('pa_area') ? $variation_product->get_attribute('pa_area') : $product_attribute['area']['value'];
                    $product_attribute['timeslot']['value']   = $variation_product->get_attribute('pa_timeslot') ? $variation_product->get_attribute('pa_timeslot') : $product_attribute['timeslot']['value'];

                    // For each attribute value check if there are multiple values
                    foreach ($product_attribute as $attribute_name => $attribute_property) {
                        if (strpos($attribute_property['value'], ',') !== false) {
                            $product_attribute[$attribute_name]['multiple_values'] = true;
                        } else {
                            // IMPORTANT: here we overwrite the parent attribute set above if only one attribute is present
                            $product_attribute[$attribute_name]['multiple_values'] = false;
                        }
                    }

                    // After each variation, check if any of the attributes have multiple values
                    // Check if any of the attributes have multiple values
                    $multiple_values = false;
                    foreach ($product_attribute as $attribute_name => $attribute_property) {
                        if ($attribute_property['multiple_values']) {
                            $multiple_values = true;
                        }
                    }

                    // If any of the attributes have multiple values, show a warning
                    if ($multiple_values) {
                        echo '<div class="notice notice-warning woocommerce-message"><p>Product <a href="' . get_edit_post_link($product->ID) . '">' . $product->post_title . '</a> (VariationID = ' . $variation['variation_id'] . ') has multiple values for one or more attributes. Dates, Areas & Timeslots cannot have multiple values. Split multiple attributes into variable products.</p></div>';
                    }
                }
            }

            // If not a variable product, check if any of the attributes have multiple values
            else {
                // Check if any of the attributes have multiple values
                $multiple_values = false;
                foreach ($product_attribute as $attribute_name => $attribute_property) {
                    if ($attribute_property['multiple_values']) {
                        $multiple_values = true;
                    }
                }

                // If any of the attributes have multiple values, show a warning
                if ($multiple_values) {
                    echo '<div class="notice notice-warning woocommerce-message"><p>Product <a href="' . get_edit_post_link($product->ID) . '">' . $product->post_title . '</a> has multiple values for one or more attributes. This is not supported by the plugin and will cause problems.</p></div>';
                }
            }
        }
    }

    /** Enhancements were requested to see a better overview on the order page in admin. 
     * This will allow users to more quickly assess which products are included in orders 
     * as well as their individual worth. */
    public function customizeOrdersOverview() {
        // Add new colums
        add_filter('manage_edit-shop_order_columns', array($this, 'addColumsOrderOverview'));

        // Make columns sortable
        add_filter("manage_edit-shop_order_sortable_columns", array($this, 'makeOrderColumnsSortable'));

        // Add sorting to the query
        add_action('pre_get_posts', array($this, 'sortOrdersCustom'));

        // Populate columns
        add_action('manage_shop_order_posts_custom_column', array($this, 'populateOrderColumns'), 2);

        // Make search look at invoice nos
        add_action('pre_get_posts', array($this, 'searchInvoices'));

        // Also filter the status counts depending on the year. 
        add_filter('wp_count_posts', array($this, 'filterOrderStatusCounts'), 10, 3);

        // Add event year filter default to Admin menu link
        add_action('admin_menu', array($this, 'addEventYearDefaultToShopOrder'), 99);

        // Add event year filter to the order status links
        add_filter('views_edit-shop_order', array($this, 'add_event_year_to_status_links'));
    }

    // Adds the event year to the order status links
    // Issue was that when you clicked on e.g. on hold, the event year was reset to 2022
    // The overall order menu was working because we added the event year here addEventYearDefaultToShopOrder
    // And now we repeat it in the status links
    function add_event_year_to_status_links($views) {
        if (isset($_GET['_ttb_event_year'])) {
            $event_year = sanitize_text_field($_GET['_ttb_event_year']);
            foreach ($views as $status => $view) {
                $views[$status] = preg_replace('/href="([^"]+)"/', 'href="$1&_ttb_event_year=' . $event_year . '"', $view);
            }
        }
        return $views;
    }


    /** Change shop_order end point to include event year default */
    public function addEventYearDefaultToShopOrder() {
        global $submenu;

        if (isset($submenu['woocommerce'])) {
            foreach ($submenu['woocommerce'] as $key => $menu_item) {
                if ($menu_item[2] == 'edit.php?post_type=shop_order') {
                    $submenu['woocommerce'][$key][2] = 'edit.php?post_type=shop_order&_ttb_event_year=' . TTB_EVENT_YEAR;
                    break;
                }
            }
        }
    }

    /** Filter order status counts depending on the event year */
    public function filterOrderStatusCounts($counts, $type, $perm) {
        // Check that we are on the orders page
        if (
            ! is_admin()
            || ! isset($_GET['post_type'])
            || $_GET['post_type'] != 'shop_order'   // Only change counts if on orders page
            || ! isset($_GET['_ttb_event_year'])    // Only change counts if year filter is set
            || $type != 'shop_order'
        ) {               // Only change counts if on orders page            
            return $counts;
        }

        global $wpdb;

        $event_year = $_GET['_ttb_event_year'];

        $query = "
            SELECT 
                p.post_status,
                COUNT(*) AS num_posts
            FROM
            {$wpdb->prefix}posts p INNER JOIN 
            {$wpdb->prefix}postmeta pm ON p.ID = pm.post_id
            WHERE 
                    p.post_type = '$type'
                AND pm.meta_key = '_ttb_event_year'
                AND pm.meta_value = '$event_year'
            GROUP BY
            p.post_status;";

        $results = (array) $wpdb->get_results($wpdb->prepare($query, $type), ARRAY_A);
        $counts  = array_fill_keys(get_post_stati(), 0);

        foreach ($results as $row) {
            $counts[$row['post_status']] = $row['num_posts'];
        }

        $counts = (object) $counts;

        // $counts->processing = $wpdb->get_var( $query );

        return $counts;
    }

    /** Search order meta data for invoice numbers */
    public function searchInvoices($query) {
        global $pagenow;
        if (
            isset($_GET['s'])
            && $_GET['s'] != ''
            && $query->is_search
            && $query->is_main_query()
            && is_admin()
            && $query->query_vars['post_type'] == 'shop_order'
            && $pagenow == 'edit.php'
        ) {

            $search_term = $_GET['s'];

            //Get original meta query
            $meta_query = (array)$query->get('meta_query');

            $meta_query[] = array(
                'key'     => '_ttb_invoice_no',
                'value'   => $search_term,
                'compare' => 'LIKE'
            );

            if (count($meta_query) > 1) {
                $meta_query['relation'] = 'OR';
            }

            $query->set('meta_query', $meta_query);

            // This was required to search both the title and the meta value, the default relationship between the two = AND which 
            // is changed to OR here.
            // https://wordpress.stackexchange.com/questions/78649/using-meta-query-meta-query-with-a-search-query-s#answer-242447
            // Another resource: https://wordpress.stackexchange.com/questions/178484/wp-query-args-title-or-meta-value
            add_filter('get_meta_sql', function ($sql) {
                global $wpdb;

                static $nr = 0;
                if (0 != $nr++) return $sql;

                $sql['where'] = mb_eregi_replace('^ AND', ' OR', $sql['where']);

                return $sql;
            });
        }
    }

    /** Add new columns to the order page in admin */
    function addColumsOrderOverview($columns) {
        // Get all columns
        $new_columns = (is_array($columns)) ? $columns : array();

        // Remove actions column
        unset($new_columns['wc_actions']);

        // Add new columns
        $new_columns['ttb_order_language'] = __('Language', 'tabticketbroker');
        $new_columns['ttb_ship_by_date'] = __('Ship By Date', 'tabticketbroker');
        $new_columns['ttb_shipments'] = __('Shipments', 'tabticketbroker');
        $new_columns['ttb_order_items'] = __('Invoice & Items', 'tabticketbroker');
        $new_columns['ttb_pickup'] = __('Pickup', 'tabticketbroker');
        $new_columns['ttb_earliest_date'] = __('Earliest Reservation Date', 'tabticketbroker');
        $new_columns['ttb_briefed'] = __('Customer Briefed', 'tabticketbroker');
        $new_columns['ttb_first_check'] = __('First Check', 'tabticketbroker');

        // Add actions to the back of the list
        $new_columns['wc_actions'] = $columns['wc_actions'];

        return $new_columns;
    }

    /** Add sortable columns */
    function makeOrderColumnsSortable($columns) {
        $custom = array(
            'ttb_ship_by_date'          => '_ttb_ship_by_date',
            'ttb_pickup'                => '_ttb_pickup',
            'ttb_earliest_date'         => '_ttb_earliest_date',
            'ttb_briefed'               => '_ttb_briefed',
            'ttb_first_check'           => '_ttb_first_check',
        );

        return wp_parse_args($custom, $columns);
    }

    /** Function to sort the orders by ship by date */
    function sortOrdersCustom($query) {
        global $pagenow;
        $orderby = $query->get('order');

        if (
            is_admin()
            && $query->is_main_query()
            && $query->query_vars['post_type'] == 'shop_order'
            && $pagenow == 'edit.php'
        ) {

            if ($query->get('orderby') == '_ttb_ship_by_date') {
                $query->set('orderby', 'meta_value');
                $query->set('meta_key', '_ttb_ship_by_date');
            }

            if ($query->get('orderby') == '_ttb_pickup') {
                $query->set('orderby', 'meta_value_num');
                $query->set('meta_key', '_ttb_pickup');
                // change to asc if desc
                if ($orderby == 'asc') {
                    $query->set('order', 'desc');
                } else {
                    $query->set('order', 'asc');
                }
            }

            if ($query->get('orderby') == '_ttb_briefed') {
                $query->set('orderby', 'meta_value');
                $query->set('meta_key', '_ttb_briefed');
                // change to asc if desc
                if ($orderby == 'asc') {
                    $query->set('order', 'desc');
                } else {
                    $query->set('order', 'asc');
                }
            }

            if ($query->get('orderby') == '_ttb_earliest_date') { 
                $query->set('orderby', 'meta_value');
                $query->set('meta_key', '_ttb_earliest_date');
                $query->set('meta_type', 'DATE');
            }

            // Debugging: Log the query parameters
            // error_log(print_r($query->query_vars, true));
        }
    }

    function populateOrderColumns($column) {
        // Get order
        global $post;

        // Get order object if not already set
        if (! $this->order || $this->order->get_id() != $post->ID) {
            $this->order = wc_get_order($post->ID);
        }

        if (! $this->order_tools) {
            $this->order_tools = new OrderTools();
        }

        if ($column == 'ttb_order_items') {
            // Print invoice number
            echo $this->order_tools->getAndCopyInvoiceNo($this->order->id) . '<br>';

            // Set ass_mgmt manager if not set
            if (! isset($this->ass_mgmt)) {
                $this->ass_mgmt = new AssignmentManager;
                $this->ass_mgmt->setReservations(); // Required to load all reservations for checking if they are assigned
            }

            $this->ass_mgmt->orders = $this->ass_mgmt->getEventOrders(array($this->order));

            $order_items = $this->ass_mgmt->getEventOrderItems();

            // Print the sku for each order item and the 2 flags: docs & ship
            // Flag titles visibility is toggled via CSS see here: 
            // https://stackoverflow.com/questions/23971488/how-would-i-show-text-inside-a-span-tag-when-hovering-over-a-different-element 
            $skus_html = '';
            foreach ($order_items as $order_item_id => $order_item) {
                if ($order_item['reservation_id'] == -1) {
                    $skus_html .= '<p>' . $order_item['sku'] . '</p>';
                } else {
                    $reservation = new TabReservation($order_item['reservation_id']);
                    $docs_ready_html = '<span class="showmeonhover">| ' . __('Docs:', 'tabticketbroker') . '</span>' . ($reservation->getDocsComplete() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>');
                    $ready_to_ship_html = '<span class="showmeonhover">' . __('Ship:', 'tabticketbroker') . '</span>' . ($reservation->getReadyToShip() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>');
                    $reservation_ready_html = $docs_ready_html . ' ' . $ready_to_ship_html;
                    $skus_html .= '<div class="alwaysshowme"><p>' . $order_item['sku'] . '</p> ' . $reservation_ready_html . '<br></div>';
                }
            }

            echo $skus_html;
        } elseif ($column == 'ttb_order_language') {
            echo LanguageControl::getLanguageFromLocale($this->order->get_meta('_ttb_order_language'));
        } elseif ($column == 'ttb_ship_by_date') {
            echo $this->order->get_meta('_ttb_ship_by_date');
        } elseif ($column == 'ttb_pickup') {
            $is_pickup = $this->order_tools->getLastPickupLog($this->order)['pickup'];
            echo $is_pickup ? 'Yes' : '-';
        } elseif ($column == 'ttb_briefed') {
            $last_log_string = JMD_Logger::getLastLog($this->order->get_id(), '_ttb_brief_log', 'briefed', '_ttb_briefed');

            if ($last_log_string == '' || ! $last_log_string || $last_log_string == '0' || ! isset($last_log_string)) {
                echo '-';
            } else {
                // Extract briefed, date and user from comma separated string
                $last_log = explode('|', $last_log_string);
                $briefed = $last_log[0];
                if ($briefed == '1') {
                    $date = $last_log[1];
                    $user = $last_log[2];
                    echo '<div class="alwaysshowme"><span class="dashicons dashicons-yes"></span><span class="showmeonhover">' . $date . ' by ' . $user . '</span></div>';
                } else {
                    echo '-';
                }
            }
        } elseif ($column == 'ttb_first_check') {
            $last_log_string = JMD_Logger::getLastLog($this->order->get_id(), '_ttb_first_check_log', 'first_check', '_ttb_first_check');

            if ($last_log_string == '' || ! $last_log_string || $last_log_string == '0' || ! isset($last_log_string)) {
                echo '-';
            } else {
                // Extract briefed, date and user from comma separated string
                $last_log = explode('|', $last_log_string);
                // Check if $last_log has 3 elements, if not, it is an old log and we need to add the date and user
                if (count($last_log) < 3) {
                    $first_check = $last_log[0];
                    if ($first_check == '1') {
                        // Add check if last log has array elements 1 and 2
                        if (isset($last_log[1]) && isset($last_log[2])) {
                            $date = $last_log[1];
                            $user = $last_log[2];
                            echo '<div class="alwaysshowme"><span class="dashicons dashicons-yes"></span><span class="showmeonhover">' . $date . ' by ' . $user . '</span></div>';
                        } else {
                            echo '-';
                        }
                    } else {
                        echo '-';
                    }
                } else {
                    echo '-';
                }
            }
        } elseif ($column == 'ttb_earliest_date') {
            $earliest_item_date = $this->order->get_meta('_ttb_earliest_date');
            if ($earliest_item_date == '' || $earliest_item_date == '-')
                echo $this->order_tools->getEarliestItemDate($this->order);
            else
                echo $earliest_item_date;
        } elseif ($column == 'ttb_shipments') {

            $order_shipment  = wc_gzd_get_shipment_order($this->order);
            $order_needs_shipping = $order_shipment->needs_shipping(); // This is requried to add the shipments to the Germanized shipment object

            $shipments = $order_shipment->get_shipments();
            foreach ($shipments as $shipment) {
                // Get shipment status
                $shipment_status = $shipment->get_status();
                $shipment_no = $shipment->get_shipment_number();

                // Print a link to the shipment using a search query
                echo '<a href="' . admin_url('admin.php?s=' . $shipment->get_id() . '&shipment_status=all&shipment_type=simple&type=shipment&page=wc-gzd-shipments&action=-1&m=0&order_id&paged=1&action2=-1') . '" target="_blank">' . __('Shipment', 'tabticketbroker') . ' #' . $shipment_no . ' - ' . $shipment_status . '</a><br>';
            }
        }
    }

    public function wcGzdCustomized() {
        // Customize germanized shipment statuses
        add_filter('woocommerce_gzd_order_shipping_statuses', array($this, 'addGzdShipmentStatus'), 20, 1);

        // Change status if order is picked up
        add_filter('woocommerce_gzd_shipment_order_shipping_status', array($this, 'markLocalPickupGermanizer'), 20, 2);

        // Add column to shipment overview
        add_filter('woocommerce_gzd_shipments_table_columns', array($this, 'addShipmentColumns'));

        // Add sortable columns to shipment overview
        add_filter('woocommerce_gzd_shipments_table_sortable_columns', array($this, 'addShipmentSortableColumns'), 10, 1);

        // Populate columns in shipment overview
        add_filter('woocommerce_gzd_shipments_table_custom_column', array($this, 'populateShipmentColumns'), 10, 2);

        // Add sorting to shipment overview
        add_filter('woocommerce_gzd_shipment_query', array($this, 'addShipmentSorting'), 10, 2);

        add_action('woocommerce_gzd_shipments_meta_box_shipment_after_left_column', array($this, 'addShipmentMetaBox'), 10, 1);

        add_filter('woocommerce_gzd_shipment_local_pickup_shipping_methods', array($this, 'customizeLocalPickupShippingMethods'), 10, 1);

        // Add a filter to the germanized shipment query
        add_filter('woocommerce_gzd_shipment_query_args', array($this, 'filterShipmentQuery'), 10, 1);

        // Change label "Shipping Cost" for shop products
        if (TTB_SITE == 'tabtickets')
            add_filter('woocommerce_gzd_shipping_costs_text', array($this, 'changeShippingCostLabel'), 10, 1);
    }

    // Filter the shipment query to show only shipments since 10 October the previous year
    public function filterShipmentQuery($args) {
        // If a specific date is already selected, skip applying this filter
        if (isset($args['date_created'])) {
            return $args;
        }

        // Calculate the start date (10 October of the previous year or current year)
        $start_date = $this->getEventStartDate();

        // Add one year to the start date to get the end date
        $end_date = date('Y-m-d', strtotime($start_date . ' +1 year'));

        // Set up the date query to include all orders after the calculated start date
        // add args to the query, e.g. '2023-10-10...2024-10-10' (one year later)
        $args['date_created'] = $start_date . '...' . $end_date;

        // Add an admin notice to confirm the filter is applied
        add_action('admin_notices', array($this, 'addShipmentNotice'));

        return $args;
    }

    // Function to determine the start date for filtering (10 October of the appropriate year)
    public function getEventStartDate($year = null) {
        // Get the current year, month, and day
        $current_year = date('Y');
        $current_month = date('m');
        $current_day = date('d');

        // If no year is provided, default to the current year
        if (!$year) {
            $year = $current_year;
        }

        // Determine whether to filter from this year or the previous year
        if ($current_month > 10 || ($current_month == 10 && $current_day >= 10)) {
            // If today is on or after October 10, show shipments from October 10 of the current year
            $report_year = $year;
        } else {
            // If today is before October 10, show shipments from October 10 of the previous year
            $report_year = $year - 1;
        }

        // Return the start date in 'Y-m-d' format
        $start_date = $report_year . '-10-10';

        return $start_date;
    }


    public function addShipmentNotice() {
        if (isset($_SESSION['shipment_notice_shown'])) {
            return; // Exit if the notice has already been shown this session
        }

        $start_date = $this->getEventStartDate();
        $year = date('Y', strtotime($start_date));
        echo '<div class="notice notice-info is-dismissible"><p>The default settings only show shipments from 10 October ' . $year . '.</p></div>';

        // Set the session variable
        $_SESSION['shipment_notice_shown'] = true;
    }

    public function customizeLocalPickupShippingMethods($shipping_methods) {
        // Remove local-pickup from the array        
        unset($shipping_methods[0]);

        // Add local pickup to array
        // $shipping_methods['local_pickup'] = 'Local pickup';

        return $shipping_methods;
    }

    // Add info to shipment meta box
    public function addShipmentMetaBox($shipment) {
        // add the tracking url
        $tracking_url = $shipment->get_tracking_url();

        if ($tracking_url) {
            echo '<p class="form-row" style="display:block;"><label>Tracking link:</label><br><a href="' . $tracking_url . '" target="_blank">' . $tracking_url . '</a></p>';
        }
    }


    /**
     * Change the shipping cost label. On the shop page under the products it says:
     *      "inkl. MwSt.
     *      plus Shipping Costs"
     * and sees the "Shipping Cost" text as German. So we are not able to translate it.
     * I also cant find the fk&*&56 damn setting in WC-Germanizer.
     */
    public function changeShippingCostLabel($old_label) {
        $new_shipping_cost_label = 'zzgl. <a href="/versand/" target="_blank">Versand</a>';
        return $new_shipping_cost_label;
    }

    //WC-GZD: Add shipment status: local-pickup
    public function addGzdShipmentStatus($shipment_statuses) {
        // Add local pickup to array
        $shipment_statuses['gzd-local-pickup'] = 'Local pickup';

        return $shipment_statuses;
    }

    // WC-GZD: If order is scheduled for pickup, mark as such. Currently it immediately shows as SHIPPED..confuses users
    public function markLocalPickupGermanizer($status, $order) {
        // NOTE: This referes to the Vendidero\Germanized\Shipments\Order class but takes in a normal WC_Order object
        $shipping_order = new Order($order);

        if ($shipping_order->has_local_pickup()) {
            $status = 'gzd-local-pickup';
        }

        return $status;
    }

    public function addShipmentColumns($columns) {
        $custom = array(
            'ship_by_date' => 'Ship by date',
        );

        // Add columns after the date column
        return array_slice($columns, 0, 3, true) + $custom + array_slice($columns, 3, NULL, true);
    }

    public function addShipmentSortableColumns($columns) {
        $columns['ship_by_date'] = 'ship_by_date';
        return $columns;
    }

    public function addShipmentSorting($results, $args) {
        // Add sorting to shipment overview
        if (isset($args['orderby']) && $args['orderby'] == 'ship_by_date') {

            // Get order from get variable
            $order = strtolower($_GET['order']);

            // Depending on the order in the args, sort results array of objects by the ship by date
            if ('asc' === strtolower($order)) {
                usort($results, function ($a, $b) {
                    return $a->get_meta('_ttb_ship_by_date') <=> $b->get_meta('_ttb_ship_by_date');
                });
            } else {
                usort($results, function ($b, $a) { // NOTICE the reverse order of $a and $b
                    return $a->get_meta('_ttb_ship_by_date') <=> $b->get_meta('_ttb_ship_by_date');
                });
            }
        }

        return $results;
    }

    public function populateShipmentColumns($column, $shipment_id) {
        // Check which column we are populating
        if ($column == 'ship_by_date') {

            // Get the shipment
            $shipment = wc_gzd_get_shipment($shipment_id);

            // Get the order
            $order = $shipment->get_order();

            // Print the ship by date
            echo $order->get_meta('_ttb_ship_by_date') ? $order->get_meta('_ttb_ship_by_date') : __('NOT SET', 'tabticketbroker');
        }
    }

    /**
     * Change the out of stock email to include a link to replenish the stock level to 1 available again.
     */
    function customEmailContentNoStock($content, $product) {
        $product_id = $product->get_id();

        $content .= "\r\n\r\n";
        $content .= 'Click to below to reset the product stock. ';
        $content .= "\r\n\r\n";
        $content .= site_url() . '/wp-admin/admin.php?page=tab_admin_plugin&admin_function=reset_product_stock&product_id=' . $product_id;

        return $content;
    }

    /**
     * Add custom order statuses
     */
    public function customizeOrderStatuses() {
        // Register new statuses
        add_action('init', array($this, 'registerNewOrderStatuses'));

        // Add new statuses to the status filter
        add_filter('wc_order_statuses', array($this, 'addStatusesToFilter'));

        // Add the status processing to paid statuses
        add_filter('woocommerce_order_is_paid_statuses', array($this, 'markProcessingAsPaid'));

        // Register report statuses
        add_filter('woocommerce_reports_order_statuses', array($this, 'addStatusesToReports'));
    }

    /**
     * Add the status processing to the "is paid" statuses of woocommerce
     * All orders in processing and beyond(see Ofest order process flow) are assumed paid
     * E.g. This is used in the emails as $order->is_paid();
     */
    public function markProcessingAsPaid($statuses) {
        $statuses[] = 'on-hold';
        $statuses[] = 'processing';
        $statuses[] = 'assigned';
        $statuses[] = 'ready-to-ship';
        $statuses[] = 'labelled';
        $statuses[] = 'shipped';
        $statuses[] = 'completed';
        $statuses[] = 'refunded';
        return $statuses;
    }

    function registerNewOrderStatuses() {
        register_post_status('wc-assigned', array(
            'label'                     => __('Assigned', 'tabticketbroker'),
            'public'                    => true,
            'show_in_admin_status_list' => true,
            'show_in_admin_all_list'    => true,
            'exclude_from_search'       => false,
            'label_count'               => _n_noop('Assigned <span class="count">(%s)</span>', 'Assigned <span class="count">(%s)</span>')
        ));

        register_post_status('wc-ready-to-ship', array(
            'label'                     => __('Ready to Ship', 'tabticketbroker'),
            'public'                    => true,
            'show_in_admin_status_list' => true,
            'show_in_admin_all_list'    => true,
            'exclude_from_search'       => false,
            'label_count'               => _n_noop('Ready to Ship <span class="count">(%s)</span>', 'Ready to Ship <span class="count">(%s)</span>')
        ));

        register_post_status('wc-labelled', array(
            'label'                     => __('Labelled', 'tabticketbroker'),
            'public'                    => true,
            'show_in_admin_status_list' => true,
            'show_in_admin_all_list'    => true,
            'exclude_from_search'       => false,
            'label_count'               => _n_noop('Labelled for shipment <span class="count">(%s)</span>', 'Labelled for shipment <span class="count">(%s)</span>')
        ));

        register_post_status('wc-shipped', array(
            'label'                     => __('Shipped', 'tabticketbroker'),
            'public'                    => true,
            'show_in_admin_status_list' => true,
            'show_in_admin_all_list'    => true,
            'exclude_from_search'       => false,
            'label_count'               => _n_noop('Shipped <span class="count">(%s)</span>', 'Shipped <span class="count">(%s)</span>')
        ));
    }

    function addStatusesToFilter($order_statuses) {
        $new_order_statuses = array();

        // Sort array
        $custom_status_order = array(
            'wc-on-hold',
            'wc-pending',
            'wc-processing',
            'wc-assigned',
            'wc-ready-to-ship',
            'wc-labelled',
            'wc-shipped',
            'wc-completed',

            'wc-cancelled',
            'wc-refunded',
            'wc-failed',
        );

        // Load WC statuses
        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[$key] = $status;
        }

        // Load new statuses
        $new_order_statuses['wc-assigned']      = __('Assigned', 'tabticketbroker');
        $new_order_statuses['wc-ready-to-ship'] = __('Ready to Ship', 'tabticketbroker');
        $new_order_statuses['wc-labelled'] = __('Labelled', 'tabticketbroker');
        $new_order_statuses['wc-shipped']       = __('Shipped', 'tabticketbroker');

        // Sort according to sort-array declared above
        // https://stackoverflow.com/questions/348410/sort-an-array-by-keys-based-on-another-array
        $new_order_statuses_sorted = array_merge(array_flip($custom_status_order), $new_order_statuses);

        return $new_order_statuses_sorted;
    }


    public function translateAttributesLabel($result) {
        return __($result, 'tabticketbroker');
    }
    public function translateVariationOptionName($name) {
        return __($name, 'tabticketbroker');
    }

    /**
     * Add additional save button above variation
     */
    public function addSaveVariationsButton() {
?>
        <div class="save-variations-button">
            <button type="button" class="button-primary save-variation-changes" disabled="disabled"><?php esc_html_e('Save changes', 'woocommerce'); ?></button>
            <button type="button" class="button cancel-variation-changes" disabled="disabled"><?php esc_html_e('Cancel', 'woocommerce'); ?></button>
        </div>
    <?php
    }

    // Use the admin_print_scripts action to put the script directly onto the page.
    // Allow multi-line strings and both sets of quotation marks without escaping by using NOWDOC notation.
    public function showSku() {
    ?>
        <script type="text/javascript" id="sku_script">
            jQuery(function($) {
                $('#woocommerce-product-data').on('woocommerce_variations_loaded', function() {
                    $(this).find('.woocommerce_variation').each(function() {

                        var self = $(this);
                        sku = self.find('input[name*="variable_sku"]').val();
                        price = self.find('input[name*="variable_regular_price"]').val();
                        stock = self.find('input[name*="variable_stock"]').val();
                        enabled = self.find('input[name*="variable_enabled"]').is(":checked");
                        stock_mgmt_enabled = self.find('input[name*="variable_manage_stock"]').is(":checked");

                        note = '';
                        style_class = 'variation-meta';

                        // If sku is blank, set it to 'no-sku'
                        if (!sku) {
                            sku = 'no-sku';
                        }

                        // Add disabled note
                        if (!enabled)
                            note += ' disabled';

                        // Add stock note
                        if (!stock_mgmt_enabled) {
                            style_class += ' var-stock-mgmt-disabled';
                            note += ' stock management disabled';
                        } else if (stock == 0) {
                            style_class += ' var-out-of-stock';
                            note += ' out of stock';
                        }

                        // Append SKU to ensure attributes are still more or less in line.
                        self.find('h3').append($('<div class="' + style_class + '"><strong>').text(sku + ' €' + price + ' ' + note));
                    });
                });
            });
        </script>
    <?php
    }

    public function addBacktoProductsButton() {
    ?>
        <a class="ttb_back_shop button button-primary" href="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
            &larr;<?php _e('Continue shopping', 'tabticketbroker'); ?>
        </a>
    <?php
    }

    function makeTelephoneRequired($fields) {
        $fields['billing_phone']['required'] = true;

        return $fields;
    }

    /**
     * Make the menu item for reservations active when on a product page
     */
    function wpsites_nav_class($classes, $item) {
        if (is_singular('product') && $item->title == "Tischreservierungen buchen") {
            $classes[] = "current-menu-item";
        }

        return $classes;
    }



    // public function removeGermanizedEmailFooterHook()
    // {
    //     $this->remove_filters_for_anonymous_class( 'woocommerce_email', 'WC_GZD_Emails', 'email_hooks' );
    // }



    /**
     * Allow to remove method for an hook when, it's a class method used and class don't have variable, but you know the class name :)
     * https://github.com/herewithme/wp-filters-extras/blob/master/wp-filters-extras.php
     */
    function remove_filters_for_anonymous_class($hook_name = '', $class_name = '', $method_name = '', $priority = 0) {
        global $wp_filter;

        // Take only filters on right hook name and priority
        if (! isset($wp_filter[$hook_name][$priority]) || ! is_array($wp_filter[$hook_name][$priority])) {
            return false;
        }

        // Loop on filters registered
        foreach ((array) $wp_filter[$hook_name][$priority] as $unique_id => $filter_array) {
            // Test if filter is an array ! (always for class/method)
            if (isset($filter_array['function']) && is_array($filter_array['function'])) {
                // Test if object is a class, class and method is equal to param !
                if (is_object($filter_array['function'][0]) && get_class($filter_array['function'][0]) && get_class($filter_array['function'][0]) == $class_name && $filter_array['function'][1] == $method_name) {
                    // Test for WordPress >= 4.7 WP_Hook class (https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/)
                    if (is_a($wp_filter[$hook_name], 'WP_Hook')) {
                        unset($wp_filter[$hook_name]->callbacks[$priority][$unique_id]);
                    } else {
                        unset($wp_filter[$hook_name][$priority][$unique_id]);
                    }
                }
            }
        }

        return false;
    }

    /**
     * Included here are changes built into tab for the admin page such 
     * as new meta boxes and the order id display
     */
    public function customizeOrderDetailPage() {
        // Display order ID in order admin page
        add_action('woocommerce_admin_order_data_after_order_details', array($this, 'displayOrderId'));

        // Add ajax handler for linking orders
        add_action('wp_ajax_ajaxSearchOrders', array($this, 'ajaxSearchOrders'));

        // Add ajax handler for linking orders
        add_action('wp_ajax_ajaxLinkOrder', array($this, 'ajaxLinkOrder'));

        // Add ajax handler for removing linking orders
        add_action('wp_ajax_ajaxRemoveLinkedOrder', array($this, 'ajaxRemoveLinkedOrder'));

        // Add Tab Order Info / Progress metabox
        // Invoice no's are copied from fakturpro in the callstack of the metabox loads (via getAndCopyInvoiceNo in OrderTools)
        $this->customOrderMetaboxes();
    }

    /**
     * Function to handle the ajax search for orders
     */
    public function ajaxSearchOrders() {
        ob_start();
        // Get all order numbers
        $orders = wc_get_orders(array(
            'post_type'         => 'shop_order',
            'limit'             => -1,
            'orderby'           => 'date',
            'order'             => 'ASC',
            'meta_key'          => '_ttb_event_year',
            'meta_value'        => TTB_EVENT_YEAR,
            'meta_compare'      => '=',
        ));

        // show modal with order numbers
        if (count($orders) > 0) {

            echo '<div id="ttb_order_items" class="order-items">';

            // Add search box input
            echo '<div id="search_header"><input type="text" id="order_search" placeholder="' . __('Search for order', 'tabticketbroker') . '" />';
            echo '<span>' . __('Click on an order to link it', 'tabticketbroker') . '</span></div>';

            // Loop through orders and add to modal
            foreach ($orders as $order) {
                // get order number from order id
                $order_number = $order->get_order_number();
                echo '<a href="#" data-order-id="' . $order->id . '">' . $order_number . '</a>';
            }
            // add close button
            echo '<a id="close_order_search" class="ttb_close">&times;</a>';
            echo '</div>';
        } else {
            echo '<div id="ttb_order_items">';
            echo '<p>' . _e('No orders found', 'tabticketbroker') . '</p>';
            echo '</div>';
        }

        $output = ob_get_clean();

        wp_send_json_success($output);

        die();
    }


    /**
     * Function to handle the ajax search for orders
     */
    public function ajaxLinkOrder() {
        // Get posted id
        $order_id = $_POST['order_id'];
        $link_order_id = $_POST['link_order_id'];

        // Link order meta update
        $this->linkOrderMeta($order_id, $link_order_id);

        // Reverse link order meta update
        $this->linkOrderMeta($link_order_id, $order_id);

        // Return success
        wp_send_json_success('Order linked');
    }

    /**
     * Link order meta update
     */
    public function linkOrderMeta($order_id, $link_order_id) {
        // Add custom field to order contianing an array of linked order ids
        $linked_orders = get_post_meta($order_id, '_ttb_linked_order_ids', true);

        // Ensure array is set
        if (! is_array($linked_orders)) {
            $linked_orders = array();
        }

        // If order is not already linked, add it
        if (! in_array($link_order_id, $linked_orders)) {
            $linked_orders[] = $link_order_id;
            update_post_meta($order_id, '_ttb_linked_order_ids', $linked_orders);
        }
    }

    /** 
     * Function to handle the ajax removing of a linked order
     */
    public function ajaxRemoveLinkedOrder() {
        // Get posted id
        $order_id = $_POST['order_id'];
        $link_order_id = $_POST['link_order_id'];

        // Remove link order meta update
        $this->removeLinkOrderMeta($order_id, $link_order_id);

        // Remove reverse link order meta update
        $this->removeLinkOrderMeta($link_order_id, $order_id);

        // Return success
        wp_send_json_success('Order unlinked');
    }

    /**
     * Remove link order meta update
     */
    public function removeLinkOrderMeta($order_id, $link_order_id) {
        // Add custom field to order contianing an array of linked order ids
        $linked_orders = get_post_meta($order_id, '_ttb_linked_order_ids', true);

        // Ensure array is set
        if (! is_array($linked_orders)) {
            $linked_orders = array();
        }

        // Remove order id from array and update
        $linked_orders = array_diff($linked_orders, array($link_order_id));
        update_post_meta($order_id, '_ttb_linked_order_ids', $linked_orders);
    }

    /**
     * NOT USED CURRENTLY
     * Front page has elementor widget which doesnt add the shipping info, 
     * It is added by WC Germanized but WcGz does not hook into the elementor widget correctly
     */
    public function addInfoAfterPrice($price) {
        $ship_msg = (! is_front_page() ? '' : ' <br><span id="ship_msg">' . __('including VAT, excluding shipping', 'tabticketbroker') . '</span>');

        return $price . $ship_msg;
    }

    /**
     * Hide shipping on cart page
     * The settings in WooCommerce did not have any effect
     */
    public function hideShippingOnCartPage($show_shipping) {
        if (is_cart()) {
            return false;
        }
        return $show_shipping;
    }

    /**
     * Add shippping notice label
     * Note this is in the format of the table with the original classes & structure so that its treated 
     * by woocommerce methods as normal shipping info as per method wc_cart_totals_shipping_html
     */
    public function showShippingNotice() {
        $title = __('Shipping', 'tabticketbroker');
        $msg = __('Shipping calculated at checkout', 'tabticketbroker');
        $html = '<tr class="woocommerce-shipping-totals shipping">
                    <th>' . $title . '</th>
                    <td id="ttb_shipping_notice" data-title="' . $title . '">
                        <p>' . $msg . '</p>
                    </td>
                </tr>';

        echo $html;
    }

    // Remove the additional information tab in order to add it under the product image
    function removeAdditionalInformationTab($tabs) {
        unset($tabs['additional_information']);

        return $tabs;
    }

    // This adds the info tab under the product image (after the add to cart button)
    // First it is removed by the woocommerce_product_tabs hook above
    // source: https://stackoverflow.com/questions/52263008/move-additional-information-from-product-tab-under-add-to-cart-button-in-woocomm
    function addAdditionalInfoTab() {
        global $product;

        if ($product && ($product->has_attributes() || apply_filters('wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions()))) {
            wc_display_product_attributes($product);
        }
    }

    /**
     * Product dates are stored as YYYY-MM-DD and on the product additonal information tab we want to format them differently.
     * 
     * This function changes the display date for product date attribute values. It uses the php International Date Formatter which can 
     * display month & day names in different locales. It uses different format patterns.
     * For pattern syntax: https://unicode-org.github.io/icu/userguide/format_parse/datetime/#datetime-format-syntax
     */
    public function changeDisplayDate($attr_tag, $attr_data, $attr_values) {
        // Check if we are dealing with the date attribute
        if ($attr_data['name'] == 'pa_date') {

            // Get the date format from the options
            $format = get_option('ttb_display_date_format') ? get_option('ttb_display_date_format') : 'E d.M.yyyy';

            // Loop through the values involved
            foreach ($attr_values as $value) {
                // Check if the value is contained in an a tag
                if (strpos($value, '<a') !== false) {
                    // If so, get the value from the inner html
                    $value = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $value);
                }

                // Get wordpress locale
                $locale = get_locale() ? get_locale() : 'de_DE';

                // Format the date
                $fmt = new \IntlDateFormatter($locale, 0, 0, null, null, $format);

                // There is no validation being done on product attributes, so we need to cater for invalid dates
                try {
                    // Set the date object from the string
                    $date = new \DateTime($value);

                    // Format it using the IntlDateFormatter
                    $display_date = $fmt->format($date) ? $fmt->format($date) : $value; // Fallback to original value if formatting fails

                } catch (\Throwable $th) {
                    // Catch the error and ensure the user is notified
                    $error_msg = sprintf(__('[TabTicketBroker Plugin] Date Error: Please check that all the product date attributes are formatted correctly. The error message: %s', 'tabticketbroker'), $th->getMessage());

                    $this->addFlashNotice($error_msg, "error", true);

                    // If the date is invalid, just display the value
                    $display_date = $value;
                }

                // Replace the value with the formatted date
                $attr_tag = str_replace($value, $display_date, $attr_tag);
            }
        }

        return $attr_tag;
    }
    public function customizeProductPage_tt() {
        // Add page loader (hidden by default, activated by JS)
        add_action('woocommerce_before_single_product_summary', array($this, 'addPageLoader'), 9);

        // Breadcrumbs above product image
        add_action('woocommerce_before_single_product_summary', array($this, 'printBreadcrumbs'), 10, 1);

        // Add the category Festzelte in the breadcrumbs of the tent pages
        add_filter('breadcrumb_trail_items', array($this, 'filterBreadcrumbs_tt'), 10, 1);

        // Product Title below breadcrumbs
        add_action('woocommerce_before_single_product_summary', array($this, 'printProductTitle_tt'), 10);

        /* REMOVE PRICE ON SINGLE PROD PAGE:
            OceanWP has a price display setting in the customizer that does not hook into the woocommerce
            price display hook on woocommerce_single_product_summary but runs the woocommerce_template_single_price() 
            function and adds it to the page. It needs to be switched off there and then added with priority 40 in 
            order to display it after the product options but above the add to cart button.*/
        // Remove action woocommerce single price (DOESNT WORK: OceanWP has a customizer setting that adds the price)
        // remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

        // Displays the price after the product options but above the add to cart button
        add_action('woocommerce_before_single_variation', 'woocommerce_template_single_price', 40);

        // Change title of "Related Products" on the bottom of the product page to "Our Recommendation"
        add_filter('woocommerce_product_related_products_heading', array($this, 'changeRelatedProductsTitle'));

        // Change the date format of the product attributes (especially on the Product Additional Information tab)
        add_filter('woocommerce_attribute', array($this, 'changeDisplayDate'), 10, 3);

        // Remove additional information tab
        add_filter('woocommerce_product_tabs', array($this, 'removeAdditionalInformationTab'), 100, 1);

        // Add "additional information" after add to cart
        add_action('woocommerce_product_thumbnails', array($this, 'addAdditionalInfoTab'), 35);

        // Static header above product selection
        add_action('woocommerce_single_product_summary', array($this, 'addStaticTitle_tt'), 9);

        // // This displays the main variation form for users to select the product options 
        // add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10 );

        // Single Product Page: Add a section below "Recommended Products" pointing to the request form
        add_action('woocommerce_after_single_product', array($this, 'addRequestProductSection'), 9);
    }

    public function customizeProductPage_okt() {
        // Breadcrumbs above product image
        add_action('woocommerce_before_single_product_summary', array($this, 'printBreadcrumbs'), 10, 1);

        // Add the category Festzelte in the breadcrumbs of the tent pages
        add_filter('breadcrumb_trail_items', array($this, 'filterBreadcrumbs_okt'), 10, 1);

        // Product Title below breadcrumbs
        add_action('woocommerce_before_single_product_summary', array($this, 'printProductTitle'), 10);

        // Add title image used in mobile only
        add_action('woocommerce_before_single_product_summary', array($this, 'addMobileHeader'), 9);

        // Add page loader (hidden by default, activated by JS)
        add_action('woocommerce_before_single_product_summary', array($this, 'addPageLoader'), 9);

        // Static header above product selection
        add_action('woocommerce_single_product_summary', array($this, 'addStaticTitle_okt'), 9);

        // This displays the main variation form for users to select the product options 
        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10);

        // Displays the price after the product options but above the add to cart button
        add_action('woocommerce_before_single_variation', 'woocommerce_template_single_price', 40);

        // Add product except to below the summary.. this is then moved to under the thumbnails with JS
        add_action('woocommerce_after_add_to_cart_form', array($this, 'printProductExerpt'), 10);

        // Here we add the hidden items which is displayed by JS on hover
        // Table layout images
        add_action('woocommerce_after_add_to_cart_form', array($this, 'addTablePlacingImages'), 20);
        // Area booking info box
        add_action('woocommerce_after_add_to_cart_form', array($this, 'addAreaBookingInfoBox'), 20);

        // Increase thumbnail size
        add_filter('woocommerce_gallery_thumbnail_size', array($this, 'changeThumbnailSize'), 10, 1);

        // Remove the tab creation action 
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

        // Create own custom tabs
        add_action('woocommerce_after_single_product_summary', array($this, 'createCustomTabs'), 10);

        // Change title of "Related Products" on the bottom of the product page to "Our Favourites"
        add_filter('woocommerce_product_related_products_heading', array($this, 'changeRelatedProductsTitle'));
    }

    public function addRequestProductSection() {
        $hardcoded = new HardCoded;
        echo $hardcoded->getEventMoreInfo_tt();
    }

    public function filterBreadcrumbs_tt($breadcrumb_trail_items) {
        // For some reason the home text was Home (Start) so we replace it with Home
        $breadcrumb_trail_items[0] = str_replace('Home (Start)', 'Home', $breadcrumb_trail_items[0]);

        return $breadcrumb_trail_items;
    }


    // define the bcn_breadcrumb_trail_object callback 
    public function filterBreadcrumbs_okt($breadcrumb_trail_items) {
        global $post;

        // For the tent pages, rewrite the middle crumb to a custom parent tent page
        if (has_category('Festzelte', $post)) {
            $breadcrumb_trail_items = array(
                $breadcrumb_trail_items[0],
                '<a href="/die-festzelte/">Festzelte</a>',
                $breadcrumb_trail_items[1]
            );
        }
        // On the product pages, rewrite the middle crumb to point to the custom shop page
        elseif (get_post_type($post) == 'product') {

            $shop_label = __('Table Reservations', 'tabticketbroker');

            $shop_url = site_url() . "/tischreservierungen-buchen/";

            $breadcrumb_trail_items[1] = '"<a href="' . $shop_url . '">' . $shop_label . '</a>"';
        }

        return $breadcrumb_trail_items;
    }

    public function changeRelatedProductsTitle() {
        if (TTB_SITE == 'ofest')
            return __('Our Recommendation', 'tabticketbroker') . '*'; // required for the legal caption reference
        else
            return __('Our Recommendation', 'tabticketbroker');
    }

    /**
     * Page loader html gets injected and hidden to be displayed while loading the page
     */
    public function addPageLoader() {
        echo $this->getLoaderHtml();
    }

    public function addMobileHeader() {
        global $product;

        ob_start();
    ?>
        <div id="mobile_header">
            <div class="wp-block-button is-style-fill">
                <a class="wp-block-button__link has-background tab-button" href="<?php echo $this->getLanguageLink('/tischreservierungen-buchen'); ?>">
                    < <?php _e('To all table reservations', 'tabticketbroker'); ?></a>
            </div>
            <div id="prod_title_img">
                <?php echo $product->get_image('600x400'); ?>
            </div>
        </div>

    <?php

        echo ob_get_clean();
        return;
    }

    function changeThumbnailSize($size) {
        $size = array(500, 300);
        return $size;
    }

    /**
     * Remove product data tabs
     */

    function createCustomTabs($tabs) {
        // Get information from central hardcoded class
        $hardcoded = new HardCoded;

        ob_start(); ?>
        <div id="prod_info_container">
            <div id="prod_info_tabs">
                <div id="tab">
                    <button value="info_tab" class="tab_button active"><?php _e('Information', 'tabticketbroker'); ?></button>
                    <button value="shipping_tab" class="tab_button"><?php _e('Shipping', 'tabticketbroker'); ?></button>
                    <button value="price_tab" class="tab_button"><?php _e('Prices', 'tabticketbroker'); ?></button>
                </div>

                <?php echo $hardcoded->getEventStartTimeInfo(); ?>

                <?php echo $hardcoded->getEventShippingInfo(); ?>

                <?php echo $hardcoded->getEventPriceInfo(); ?>
            </div>

            <div id="prod_info_more_container">
                <?php echo $hardcoded->getEventMoreInfo(); ?>
            </div>
        </div>

    <?php
        echo ob_get_clean();
    }


    public function addAreaBookingInfoBox() {

        ob_start();
    ?>
        <div id="area_infobox_wrapper">
            <div id="area_infobox">
                <span>
                    <h5><?php _e('Optional area booking (Middle, Box, and Gallery / Balcony)', 'tabticketbroker'); ?></h5>
                    <p><?php _e('8/10 person single table: plus 500 € VAT included.', 'tabticketbroker'); ?></p>
                    <p><?php _e('16/20 person double table: plus 1500 € incl.VAT.', 'tabticketbroker'); ?></p>
                    <p><?php _e('24/30 person tripple table: plus 2250 € incl.VAT.', 'tabticketbroker'); ?></p>
                </span>
                <img src="/wp-content/uploads/2021/07/bereiche_hover.png" alt="<?php _e('Area booking info', 'tabticketbroker'); ?>">
            </div>
        </div>

    <?php
        $html = ob_get_clean();
        echo $html;
    }


    public function addTablePlacingImages() {
        $hardcoded = new HardCoded;
        $table_layout_images = $hardcoded->getTableLayoutImageArray();

        $html = '';
        $html .= '<div id="tbl_layout_img_container">';

        foreach ($table_layout_images as $amt => $img_url) {
            $html .= ' <div id="table_layout_img_' . $amt . '" class="tbl_hidden"><img src="' . $img_url . '" alt="Table layout ' . $amt . '"></div>';
        }

        $html .= '</div>';
        echo $html;
    }

    public function printProductExerpt() {
        global $product;
        echo '<div id="custom_product_description">' . $product->post->post_content . '</div>';
    }

    /**
     * Add static title to product page above the ticket selection
     */
    public function addStaticTitle_okt() {
    ?>
        <div id="custom_static_title">
            <h4><?php _e('Table Reservation', 'tabticketbroker'); ?>*</h4>
            <div id="legal_text">
                <p>
                    <?php
                    echo HardCoded::getLegalShopText();
                    ?>
                </p>
            </div>
        </div>
    <?php
    }

    /**
     * Add static title to product page above the ticket selection
     */
    public function addStaticTitle_tt() {
    ?>
        <div id="custom_static_title">
            <h1><?php _e('Available Tickets', 'tabticketbroker'); ?></h1>
        </div>
    <?php
    }

    public function printBreadcrumbs() {
        echo do_shortcode('[oceanwp_breadcrumb]');
    }

    public function printProductTitle() {
        global $product;
        echo '<div id="custom_prod_title"><h3>' . $product->get_title() . '</h3></div>';
    }

    public function printProductTitle_tt() {
        global $product;
        echo '<div id="custom_prod_title"><h1>' . $product->get_title() . '</h1></div>';
    }

    function woo_new_product_tab($tabs) {
        // Adds the new tab
        $tabs['desc_tab'] = array(
            'title'     => __('Additional Information', 'woocommerce'),
            'priority'  => 50,
            'callback'  => 'woo_new_product_tab_content'
        );
    }

    /**
     * Text change on price prefix as per UI UX design requirement
     */
    function changePricePrefix($price, $product) {
        $price = str_replace('Ab:', 'ab.', $price);
        return $price;
    }

    public function orderReservationCallback() {
        global $post;

        $order_id = $post->ID;

        if (! $this->order_tools) {
            $this->order_tools = new OrderTools();
        }

        $this->order_tools->printLinkedOrderReservationDetails($order_id);
    }

    public function courierInfoCallback() {
        global $post;

        // Build metabox content
        // Courier name
        $courier_name = get_post_meta($post->ID, '_ttb_courier_name', true) ? get_post_meta($post->ID, '_ttb_courier_name', true) : 'Go Express';

        // Tracking Code
        $tracking_code = get_post_meta($post->ID, '_ttb_tracking_code', true) ? get_post_meta($post->ID, '_ttb_tracking_code', true) : '';

        // Dispatch date
        $dispatch_date = get_post_meta($post->ID, '_ttb_dispatch_date', true) ? get_post_meta($post->ID, '_ttb_dispatch_date', true) : '';

        // Pickup Code
        $pickup_code = get_post_meta($post->ID, '_ttb_pickup_code', true) ? get_post_meta($post->ID, '_ttb_pickup_code', true) : '';

    ?>
        <h6><?php _e('Shipping info', 'tabticketbroker'); ?></h6>
        <input type="hidden" name="ttb_courier_name_nonce" value="<?php echo wp_create_nonce(); ?>">

        <div class="form-field">
            <label for="ttb_courier_name_field"><?php _e('Courier name', 'tabticketbroker'); ?></label>
            <input
                type="text"
                id="ttb_courier_name_field"
                name="ttb_courier_name"
                placeholder="<?php echo $courier_name; ?>"
                value="<?php echo $courier_name; ?>">
        </div>

        <div class="form-field">
            <label for="ttb_tracking_code_field"><?php _e('Tracking code', 'tabticketbroker'); ?></label>
            <input
                type="text"
                id="ttb_tracking_code_field"
                name="ttb_tracking_code"
                placeholder="<?php _e('Enter tracking code here', 'tabticketbroker'); ?>"
                value="<?php echo $tracking_code; ?>">
        </div>

        <div class="form-field">
            <label for="ttb_dispatch_date_field"><?php _e('Dispatch date', 'tabticketbroker'); ?></label>
            <input
                type="text"
                id="ttb_dispatch_date_field"
                name="ttb_dispatch_date"
                placeholder="<?php _e('Enter dispatch date here', 'tabticketbroker'); ?>"
                value="<?php echo $dispatch_date; ?>">
        </div>
        <hr>

        <h6><?php _e('Pickup Info', 'tabticketbroker'); ?></h6>
        <div class="form-field">
            <label for="ttb_pickup_code_field"><?php _e('Pickup code', 'tabticketbroker'); ?></label>
            <input
                type="text"
                id="ttb_pickup_code_field"
                name="ttb_pickup_code"
                placeholder="<?php _e('Enter pickup code here', 'tabticketbroker'); ?>"
                value="<?php echo $pickup_code; ?>">
        </div>
    <?php
    }

    public function saveCourierInfo($post_id) {

        // We need to verify this with the proper authorization (security stuff).

        // Check if our nonce is set.
        if (! isset($_POST['ttb_courier_name_nonce'])) {
            return $post_id;
        }
        $nonce = $_REQUEST['ttb_courier_name_nonce'];

        //Verify that the nonce is valid.
        if (! wp_verify_nonce($nonce)) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // Check the user's permissions.
        if ('page' == $_POST['post_type']) {

            if (! current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } else {

            if (! current_user_can('edit_post', $post_id)) {
                return $post_id;
            }
        }
        // --- Its safe for us to save the data ! --- //

        // Sanitize user input  and update the meta field in the database.
        update_post_meta($post_id, '_ttb_courier_name', $_POST['ttb_courier_name']);
        update_post_meta($post_id, '_ttb_tracking_code', $_POST['ttb_tracking_code']);
        update_post_meta($post_id, '_ttb_dispatch_date', $_POST['ttb_dispatch_date']);
        update_post_meta($post_id, '_ttb_pickup_code', $_POST['ttb_pickup_code']);
    }

    public function customOrderMetaboxes() {
        // Add meta boxes for order
        add_action('add_meta_boxes', array($this, 'addCustomOrderMetaBoxes'));

        // Save the data of the Meta field
        add_action('save_post', array($this, 'saveOrderMeta'), 10, 1);

        // Display field value on the order edit page (not in custom fields metabox)
        add_action('woocommerce_admin_order_data_after_billing_address', array($this, 'displayInvoiceNo'), 10, 1);
    }

    public function addCustomOrderMetaBoxes() {
        // Add the Ofest Info metabox
        add_meta_box('ttb_custom_order_metabox', __('Tab Order Info', 'tabticketbroker'), array($this, 'customOrderDataCallback'), 'shop_order', 'normal', 'core');

        // Add the Reservations metabox
        add_meta_box('ttb_order_reservation_metabox', __('Order Reservations', 'tabticketbroker'), array($this, 'orderReservationCallback'), 'shop_order', 'normal', 'default');
    }

    public function customOrderDataCallback() {
        global $post;

        // Get order from 
        $order_id = $post->ID;
        $order = wc_get_order($order_id);

        // Create new instance of OrderTools if it doesnt exist and check if order is pickup
        if (! $this->order_tools) {
            $this->order_tools = new OrderTools();
        }

        // Get last pickup log entry
        $last_pickup_log = $this->order_tools->getLastPickupLog($order);

        // Infer progress data from order position, $order_meta is used in the template admin_order_metabox.php
        $order_life_cycle = new OrderLifeCycle;

        $order_meta = $order_life_cycle->getOrderProgressData($order);

        // Get order notes ($order_notes used in template)
        $order_notes = get_post_meta($order_id, '_ttb_order_notes', true);

        $ship_by_date = get_post_meta($order_id, '_ttb_ship_by_date', true);

        // Get brief status from log
        $brief_log          = get_post_meta($order_id, '_ttb_brief_log', true);
        $last_brief_log     = $brief_log ? end($brief_log) : array();
        // Because this log can be empty, we need to check if the key exists and set the vars here
        $brief_status       = isset($last_brief_log['briefed']) ? filter_var($last_brief_log['briefed'], FILTER_VALIDATE_BOOLEAN) : false;
        if ($brief_status) {
            $brief_date = $last_brief_log['date'];
            $brief_user = $last_brief_log['user'];
        }

        // Get First Check log
        $first_check_log    = get_post_meta($order_id, '_ttb_first_check_log', true);
        $last_first_check   = $first_check_log ? end($first_check_log) : array();
        // Because this log can be empty, we need to check if the key exists and set the vars here
        $first_check_status = isset($last_first_check['first_check']) ? filter_var($last_first_check['first_check'], FILTER_VALIDATE_BOOLEAN) : false;
        if ($first_check_status) {
            $first_check_date = $last_first_check['date'];
            $first_check_user = $last_first_check['user'];
        }

        // Get order meta
        $order_items_meta = $this->order_tools->getOrderItemMeta($order);

        // Get linked orders
        $linked_orders = get_post_meta($order_id, '_ttb_linked_order_ids', true);
        $linked_orders = is_array($linked_orders) ? $linked_orders : array();

        // Get event year
        $event_year = get_post_meta($order_id, '_ttb_event_year', true);

        if ($event_year == '') {
            if (! $this->order_tools) {
                $this->order_tools = new OrderTools();
            }
            // $event_year = $this->order_tools->calculateEventYear( $order );
        }

        $pickup_code = $last_pickup_log['pickup'] ? $this->getPickupCode($order) : '';

        include_once($this->plugin_path . 'templates/woocommerce/admin_order_metabox.php');
    }

    public function getPickupCode($order) {
        $pickup_code = get_post_meta($order->get_id(), '_ttb_pickup_code', true);

        // If pickup code is empty, generate a new one
        if ($pickup_code == '') {
            $pickup_code = $this->generatePickupCode($order);

            // Update pickup code
            update_post_meta($order->get_id(), '_ttb_pickup_code', $pickup_code);
        }

        return $pickup_code;
    }

    // function to generatePickupCode from the order, use the linked reservation code if it exists
    public function generatePickupCode($order) {
        // Get order reservations
        if (! $this->order_tools) {
            $this->order_tools = new OrderTools();
        }

        // Check if utils is instantiated
        if (! $this->utils instanceof Utils) {
            $this->utils = new Utils();
        }

        $linked_reservations = $this->order_tools->getOrderReservations($order->get_id());

        // If there are linked reservations, combine the codes
        $pickup_code = '';
        if ($linked_reservations) {
            foreach ($linked_reservations as $linked_data) {
                $reservation = $linked_data['reservation'];
                $pickup_code .= $reservation->getCode() . '-' . $reservation->getSku() . '[' . $this->utils->generateRandomString(4) . '] | ';
            }
        }

        // Remove the last 3 characters (space, &, space)
        $pickup_code = substr($pickup_code, 0, -3);

        return $pickup_code;
    }



    public function saveOrderMeta($post_id) {
        // We need to verify this with the proper authorization (security stuff).
        // Check if our nonce is set.
        if (! isset($_POST['ttb_order_meta_nonce'])) {
            return $post_id;
        }
        $nonce = $_REQUEST['ttb_order_meta_nonce'];

        //Verify that the nonce is valid.
        if (! wp_verify_nonce($nonce)) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // Check the user's permissions.
        if ('page' == $_POST['post_type']) {

            if (! current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } else {

            if (! current_user_can('edit_post', $post_id)) {
                return $post_id;
            }
        }
        // --- Its safe for us to save the data ! --- //
        // Set the order
        $order = wc_get_order($post_id);

        // Sanitize user input  and update the meta field in the database.
        if (isset($_POST['ttb_invoice_no'])) {
            update_post_meta($post_id, '_ttb_invoice_no', $_POST['ttb_invoice_no']);
        }

        if (isset($_POST['ttb_order_language'])) {
            update_post_meta($post_id, '_ttb_order_language', $_POST['ttb_order_language']);
        }

        if (isset($_POST['ttb_order_notes'])) {
            update_post_meta($post_id, '_ttb_order_notes', $_POST['ttb_order_notes']);
        }

        if (isset($_POST['ttb_ship_by_date'])) {
            update_post_meta($post_id, '_ttb_ship_by_date', $_POST['ttb_ship_by_date']);
        }

        if (isset($_POST['ttb_event_year'])) {
            update_post_meta($post_id, '_ttb_event_year', $_POST['ttb_event_year']);
        }

        if (isset($_POST['ttb_pickup_code'])) {
            update_post_meta($post_id, '_ttb_pickup_code', $_POST['ttb_pickup_code']);
        }

        // Get existing brief log
        $last_brief_log = end(JMD_Logger::metalog($order->get_id(), '_ttb_brief_log'));

        // If last brief log entry is not the same as the current one, add new entry
        $new_brief_value = isset($_POST['ttb_briefed']) ? filter_var($_POST['ttb_briefed'], FILTER_VALIDATE_BOOLEAN) : false;

        if ($last_brief_log['briefed'] !== $new_brief_value) {

            // Update status (used for display and sorting)
            update_post_meta($post_id, '_ttb_briefed', $new_brief_value);

            // Update log for history and date
            JMD_Logger::metalog($order->get_id(), '_ttb_brief_log', array(
                'briefed' => $new_brief_value,
            ));

            // Add order note
            $order->add_order_note(
                sprintf(
                    __('Briefing status changed to <b>%s</b> by %s', 'tabticketbroker'),
                    $new_brief_value ? __('briefed', 'tabticketbroker') : __('unbriefed', 'tabticketbroker'),
                    get_user_by('id', get_current_user_id())->display_name
                )
            );
        }

        // Get existing first check log
        $last_first_check_log = end(JMD_Logger::metalog($order->get_id(), '_ttb_first_check_log'));

        // If last first check log entry is not the same as the current one, add new entry
        $new_first_check_value = isset($_POST['ttb_first_check']) ? filter_var($_POST['ttb_first_check'], FILTER_VALIDATE_BOOLEAN) : false;

        if ($last_first_check_log['first_check'] !== $new_first_check_value) {

            // Update status (used for display and sorting)
            update_post_meta($post_id, '_ttb_first_check', $new_first_check_value);

            // Update log for history and date
            JMD_Logger::metalog($order->get_id(), '_ttb_first_check_log', array(
                'first_check' => $new_first_check_value,
            ));

            // Add order note
            $order->add_order_note(
                sprintf(
                    __('First check status changed to <b>%s</b> by %s', 'tabticketbroker'),
                    $new_first_check_value ? __('checked', 'tabticketbroker') : __('unchecked', 'tabticketbroker'),
                    get_user_by('id', get_current_user_id())->display_name
                )
            );
        }

        // Get the pickup log to compare if the pickup status has changed
        $last_pickup_log_entry = end(JMD_Logger::metalog($order->get_id(), '_ttb_pickup_log'));

        // If last pickup log entry is not the same as the current one, add new entry
        $new_pickup_value = isset($_POST['ttb_pickup']) ? filter_var($_POST['ttb_pickup'], FILTER_VALIDATE_BOOLEAN) : false;

        if ($last_pickup_log_entry['pickup'] !== $new_pickup_value) {

            // Update pickup status (used for display and sorting)
            update_post_meta($post_id, '_ttb_pickup', $new_pickup_value);

            // Update log for history and date
            JMD_Logger::metalog($order->get_id(), '_ttb_pickup_log', array(
                'pickup' => $new_pickup_value,
            ));

            // Add order note
            $order->add_order_note(
                sprintf(
                    __('Pickup changed to <b>%s</b> by %s', 'tabticketbroker'),
                    $new_pickup_value ? __('Yes', 'tabticketbroker') : __('No', 'tabticketbroker'),
                    get_user_by('id', get_current_user_id())->display_name
                )
            );

            // Override the shipping address with the pickup address
            $this->overrideShippingAddress($order->get_id());

            // If the order is pickup, add an order item of type pickup
            if ($new_pickup_value) {
                // $this->order_tools->addPickupOrderItem( $order );
            }
        }

        // Get OrderTools if not set
        if (! isset($this->order_tools)) {
            $this->order_tools = new OrderTools();
        }

        // Set the earliest item date on every save of the order 
        // (items might have changed, small accepted processing overhead here)
        $this->order_tools->getEarliestItemDate($order);
    }

    public function addPickupOrderItem($order) {
        // Find woocommerce shipping methods and find the pickup method
        $shipping_methods = $order->get_shipping_methods();
        $pickup_method = false;
        foreach ($shipping_methods as $shipping_method) {
            if ($shipping_method->get_method_id() == 'local_pickup') {
                $pickup_method = $shipping_method;
            }
        }

        // If the pickup method was found, add a pickup order item


    }

    public function displayInvoiceNo($order) {
        $invoice_no = get_post_meta($order->id, '_ttb_invoice_no', true);
        if (! empty($invoice_no)) {
            echo '<p><strong>' . __("Invoice no", "tabticketbroker") . ':</strong> ' . get_post_meta($order->id, '_ttb_invoice_no', true) . '</p>';
        } else {
            echo '<p><strong>' . __("Invoice no", "tabticketbroker") . ':</strong> ' . __("to be created", "tabticketbroker") . '</p>';
        }
    }


    // Display the order id under the General section of the order details
    public function displayOrderId($order) {
    ?>
        <div style="position: absolute; color: #777; font-style: italic;">
            <?php echo __('Order ID: ') . $order->get_id(); ?>
        </div>
<?php
    }

    /**
     * Use the global variable of TranslatePress to save the language the user is in on the website as the default
     * communication language. This is saved  per order and will be used by the email controller. 
     * At time of writing the only 2 languages used are: de_DE_formal(default saved to order) & en_US (site language)
     */
    function saveOrderLanguage($order_id) {
        global $TRP_LANGUAGE;

        // Get language or default to formal german
        $language = $TRP_LANGUAGE ? $TRP_LANGUAGE : $this->tab_default_lang_code;

        // Save the language to the order's meta 
        update_post_meta($order_id, '_ttb_order_language', $language);
    }

    /**
     * Set pickup flag on order creation
     */
    public function saveOrderPickupFlag($order_id) {
        // Get the order
        $order = wc_get_order($order_id);

        $shipping_methods = $order->get_shipping_methods();

        // Loop through shipping methods and see if one has the id of local_pickup
        $is_pickup = false;
        foreach ($shipping_methods as $shipping_method) {
            if ($shipping_method->get_method_id() == 'local_pickup') {
                $is_pickup = true;
                break;
            }
        }

        // Update the meta
        update_post_meta($order_id, '_ttb_pickup', $is_pickup);

        // Log the pickup status
        JMD_Logger::metalog($order_id, '_ttb_pickup_log', array(
            'pickup' => $is_pickup,
        ));
    }

    /** Add the event year meta data */
    public function saveOrderEventYear($order_id) {
        // Get the order
        $order = wc_get_order($order_id);

        // Get OrderTools if not set
        if (! isset($this->order_tools)) {
            $this->order_tools = new OrderTools();
        }

        // Calculate the event year
        $event_year = $this->order_tools->calculateEventYear($order);

        // Save the event year to the order's meta 
        update_post_meta($order_id, '_ttb_event_year', $event_year);
    }

    public function overrideShippingAddress($order_id) {
        // Get the order
        $order = wc_get_order($order_id);

        // Check if exists
        if ($order) {

            // Get OrderTools if not set
            if (! isset($this->order_tools)) {
                $this->order_tools = new OrderTools();
            }

            // Get the last pickup log
            $is_pickup = $this->order_tools->getLastPickupLog($order)['pickup'];

            // If the order is pickup, save the override address
            if ($is_pickup) {

                // Get the pickup address
                // $pickup_address = $this->order_tools->getPickupAddress(); // Anke: decided not to use // Function removed from OrderTools
                // Just show pickup in shipping address
                $pickup_address = array(
                    'address_1' => 'PICKUP',
                    'address_2' => '',
                    'city' => '',
                    'postcode' => '',
                    'country' => '',
                    'company' => '',
                    'state' => '',
                );

                // get order's shipping address
                $shipping_address = $order->get_address('shipping');

                // Check if the shipping address is the same as the pickup address
                if (
                    $shipping_address['address_1'] == $pickup_address['address_1']
                    && $shipping_address['address_2'] == $pickup_address['address_2']
                    && $shipping_address['city'] == $pickup_address['city']
                    && $shipping_address['postcode'] == $pickup_address['postcode']
                    && $shipping_address['country'] == $pickup_address['country']
                    && $shipping_address['company'] == $pickup_address['company']
                    && $shipping_address['state'] == $pickup_address['state']
                ) {

                    // If so, nothing to do
                    return;
                } else {

                    // If not, save the shipping address as a backup
                    update_post_meta($order_id, '_ttb_initial_shipping_address_backup', $shipping_address);

                    // Set order shipping address to pickup address
                    $order->set_shipping_address_1($pickup_address['address_1']);
                    $order->set_shipping_address_2($pickup_address['address_2']);
                    $order->set_shipping_city($pickup_address['city']);
                    $order->set_shipping_postcode($pickup_address['postcode']);
                    $order->set_shipping_country($pickup_address['country']);
                    $order->set_shipping_company($pickup_address['company']);
                    $order->set_shipping_state($pickup_address['state']);

                    // Save order
                    $order->save();

                    // Note the change
                    $order->add_order_note('Order shipping address replaced by the pickup address', 0, false);
                }
            } elseif (! $is_pickup) {
                // If the order is not pickup, check if there is a backup shipping address
                $backup_shipping_address = get_post_meta($order_id, '_ttb_initial_shipping_address_backup', true);

                // If there is a backup shipping address, restore it
                if ($backup_shipping_address) {

                    // Set order shipping address to backup address
                    $order->set_shipping_address_1($backup_shipping_address['address_1']);
                    $order->set_shipping_address_2($backup_shipping_address['address_2']);
                    $order->set_shipping_city($backup_shipping_address['city']);
                    $order->set_shipping_postcode($backup_shipping_address['postcode']);
                    $order->set_shipping_country($backup_shipping_address['country']);
                    $order->set_shipping_company($backup_shipping_address['company']);
                    $order->set_shipping_state($backup_shipping_address['state']);

                    // Save order
                    $order->save();

                    // Note the change
                    $order->add_order_note('Order shipping address restored from backup', 0, false);
                }
            }
        }
    }

    /**
     * Add the first date of the event above the product title in the shop loop
     */
    function addDateAboveTitle_tt($title, $post_id) {
        // Only trigger on titles in the shop                   // quit when:
        if (
            is_admin()                                  // in admin
            ||  ! (is_shop())                                 // not on the shop page
            ||  ! $post_id                                    // no post id
            ||      is_product()                                // on the single product page
            // || ! ($post_id == 864 )  // JMD_DEBUG remove
            ||      get_post_type($post_id) != 'product'
        )    // if not a product
            return $title;

        // Get the product
        $product = wc_get_product($post_id);

        // Get the pa_date attributes
        $terms  = wp_get_post_terms($post_id, 'pa_date');

        // If no date attribute, quit
        if (! $terms) return $title;

        // put all dates in a comma separated string
        $event_dates = '';

        foreach ($terms as $term) {
            $event_dates .= $term->name . ',';
        }

        // remove last comma
        $event_dates = rtrim($event_dates, ',');

        // Get the first date from the comma separated string
        $first_variation_date = explode(',', $event_dates)[0];

        // Get the display date from the getDisplayDateAdvanced
        $first_variation_date_display = $this->getDisplayDateAdvanced($first_variation_date);

        // If multiple dates, create a multi date string
        $multi_day_txt = '';
        if (count(explode(',', $event_dates)) > 1) {
            $multi_day_txt = ' (' . count(explode(',', $event_dates)) . ' ' . __('days', 'tabticketbroker') . ')';
        }

        // If there is a date, display it
        if (!empty($first_variation_date_display)) {
            // Add date before title
            $title = '<span id="ttb_date_attr">' . $first_variation_date_display . $multi_day_txt . '</span>' . $title;
        }

        return $title;
    }

    /**
     * For tabtickets we added a filter on the title that adds the date and event duration
     * This unfortunatly also added the span html to the title which displays in the alt text
     * of the product on load. So this function resets the alt text to the product title
     */
    public function resetProductAltText_tt($attr, $attachment) {
        // Get post parent
        $parent = get_post_field('post_parent', $attachment);

        // Get post type to check if it's product
        $type = get_post_field('post_type', $parent);
        if ($type != 'product') {
            return $attr;
        }

        if (isset($attr['class']) && 'custom-logo' === $attr['class']) {
            return $attr;
        }

        /// Get title
        $title = get_post_field('post_title', $parent);

        $attr['alt'] = $title;
        $attr['title'] = $title;

        return $attr;
    }

    /**
     * With each variation change the parent product is update with the event date meta data
     * jm_event_dates       : comma separated string of all dates, formatted as YYYY-MM-DD
     * jm_event_first_date  : first date of the event, used for sorting, formatted as YYYYMMDD
     */
    public function saveEarliestDateToParentProduct($variation_id, $i) {
        // Get the parent product ID
        $parent_id = wp_get_post_parent_id($variation_id);

        // If there is no parent product ID, quit
        if (! $parent_id) return;

        // Get the date attributes
        $terms  = wp_get_post_terms($parent_id, 'pa_date');

        // If no date attribute, quit
        if (! $terms) return;

        // put all dates in a comma separated string
        $new_event_dates = '';
        foreach ($terms as $term) {
            $new_event_dates .= $term->name . ',';
        }

        // remove last comma
        $new_event_dates = rtrim($new_event_dates, ',');

        // Get the existing jm_event_dates custom field
        $event_dates = get_post_meta($parent_id, 'jm_event_dates', true);

        if ($event_dates != $new_event_dates) {
            // set a custom field jm_event_dates
            update_post_meta($parent_id, 'jm_event_dates', $new_event_dates);
            // save the first date in a custom field jm_event_first_date and strip out '-'
            $order_by_date_fmt = str_replace('-', '', explode(',', $new_event_dates)[0]);
            update_post_meta($parent_id, 'jm_event_first_date', $order_by_date_fmt);
        }
    }

    /**
     * Filter on the product title in the shop loop to add custom sorting
     * NOTE: This only worked if the settings are as follows Appearance > Customize > WooCommerce > Archives > Default Product Sorting
     */
    public function customSortProducts($args) {
        // Change args to sort products by pa_date attribute
        $args['orderby'] = 'meta_value_num';
        $args['order'] = 'asc';
        $args['meta_key'] = 'jm_event_first_date';

        return $args;
    }


    public function addStatusesToReports($statuses) {

        // Check if $statuses is an array
        if (! is_array($statuses)) {
            $statuses = array();
        }

        // Add your custom statuses
        $custom_statuses = array(
            'assigned',
            'ready-to-ship',
            'labelled',
            'shipped',
        );
        return array_merge($statuses, $custom_statuses);
    }
}
