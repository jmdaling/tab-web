<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Tools;

use Inc\Base\BaseController;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class ShipmentTools extends BaseController
{
    // Create a callback for getting the pickup location details html box
    public function getPickupAddressHtmlBox()
    {
        // Get the pickup location details from the settings
        $address = $this->getPickupAddress();

        // Create html box
        ob_start(); ?>
        <div class="ttb-pickup-address">
            <h3><?php _e( 'Pickup Address', 'tabticketbroker' ); ?></h3>
            <?php echo $address['line1']; ?><br>
            <?php echo $address['line2']; ?><br>
            <?php echo $address['postcode'];?> <?php echo $address['city']; ?><br>
        </div>
        <?php
        $html = ob_get_clean();

        return $html;
    }

    // getPickupContactHtml
    public function getPickupContactHtml()
    {
        ob_start(); ?>
        <p id="pickup_contact"><?php echo get_option( 'ttb_pickup_contact_name' ) . ' - ' . get_option( 'ttb_hotline' ); ?></p>
        <?php
        $html = ob_get_clean();

        return $html;
    }

    // getPickupCode from order
    public function getPickupCode( $order )
    {
        $pickup_code = get_post_meta( $order->get_id(), '_ttb_pickup_code', true );

        return $pickup_code;
    }


    /**
     * Function to retrieve the pickup address
     * @return array $address
     */
    public function getPickupAddress()
    {
        $address = array();

        $address['line1'] = get_option( 'ttb_pickup_address_line_1' );
        $address['line2'] = get_option( 'ttb_pickup_address_line_2' );
        $address['city'] = get_option( 'ttb_pickup_address_city' );
        $address['postcode'] = get_option( 'ttb_pickup_address_postcode' );

        return $address;
        
    }
}