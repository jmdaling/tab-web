<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Tools;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Utils {
    // Function to generate a random string
    public static function generateRandomString($length = 4, $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
