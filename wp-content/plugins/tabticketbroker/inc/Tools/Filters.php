<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Tools;

use Inc\Classes\TabSupplier;

class Filters
{
    public function renderOperationsFilters()
    {
    ?>
        <div class="wrap filter">
            <h3>Filter</h3>
            <?php echo $this->getSupplierFilterHtml(); ?>

        </div>
    <?php
    }

    public function getSupplierFilterHtml()
    {
        $supplier = new TabSupplier();
        $suppliers = $supplier->getSuppliers();
    ?>

        <div class="supplier-filter">
            <h4>Supplier</h4>
            <!-- add checkbox for each supplier -->
            <?php foreach ($suppliers as $supplier) : ?>
                <div class="supplier">
                    <input type="checkbox" name="supplier[]" value="<?php echo $supplier->getId(); ?>" />
                    <label for="supplier"><?php echo $supplier->getName(); ?></label>
                </div>
            <?php endforeach; ?>

        </div>

<?php
    }
}
