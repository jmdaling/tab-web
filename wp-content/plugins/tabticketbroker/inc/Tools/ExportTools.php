<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Tools;

use Inc\Base\BaseController;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * ExportTools class
 * Here is the code that is required to implement an export on a typical report. You might need to change paths & names are required.
 * 
 * ---  IN A TEMPLATE ---
 * 
 *      <!-- Print button to export CSV -->
 *      <div class="tabticketbroker-print-button">
 *          <a href="<?php echo admin_url('admin.php?page=tab_admin_reports&report_slug=pickup&export_csv=1'); ?>" class="button button-primary">Export CSV</a>
 *      </div>
 * 
 * --- IN THE CONTROLLER ---
 * 
 * // Run maybeExportCsv() function after main data fetch
 * // for example, after $this->report_data = $this->getPickupData();
 * 
 *   public function maybeExportCsv()
 *   {
 *       // Export to CSV if set
 *       $export_csv = isset($_GET['export_csv']) ? $_GET['export_csv'] : false;
 *       if ($export_csv) {
 *           // Remove non-export fields
 *           $data_to_export = array_map( function( $row ) {
 *               unset( $row['parcel_status'] );
 *               return $row;
 *           }, $this->report_data );
 *
 *           // Export to CSV
 *           $export_tools = new ExportTools();
 *           $filename = 'pickup-report.csv';
 *           $data_to_export_cleaned = $export_tools->stripHtml( $$data_to_export );
 *           $filepath = $export_tools->exportCsv( $data_to_export_cleaned, $filename );
 *           $export_tools->downloadSendHeaders( $filepath );
 *           exit;
 *       }
 *   }
 * 
 */

class ExportTools {
    /**
     * Export CSV function
     */
    public function exportCsv($data, $filename = '') {

        $base_controller = new BaseController();

        if ($filename == '') {
            $filename = 'export_' . time() . '.csv';
        } else {
            // If the filename does not end in '.csv', add '.csv' to the filename
            if (substr($filename, -4) != '.csv') {
                $filename .= '.csv';
            }

            // Add the current time to the filename before the extension
            $filename = substr($filename, 0, -4) . '_' . time() . '.csv';
        }

        $filepath = $base_controller->plugin_path . 'exports/' . $filename;

        // check if file is writable
        if (!is_writable($base_controller->plugin_path . '/exports/')) {
            return false;
        }

        $out = fopen($filepath, 'w');

        foreach (array_keys(reset($data)) as $key) {
            $header[] = $key;
        }

        fputcsv($out, $header);

        foreach ($data as $data_item) {
            $row = array();

            foreach ($data_item as $key => $value) {
                $row[] = $value;
            }

            fputcsv($out, $row);
        }

        fclose($out);

        return $filepath;
    }

    function downloadSendHeaders($filepath) {
        // Extract the filename
        $filename = basename($filepath);

        // // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Type: text/x-csv');
        ob_clean();
        readfile($filepath);
    }

    // Function to strip parcel_data of html tags
    function stripHtml($parcel_data) {
        $stripped_data = array();
        foreach ($parcel_data as $key => $value) {
            // Strip each value of html tags, value is an array
            $stripped_data[] = array_map('strip_tags', $value);

            // Remove parcel_status from stripped data
            unset($stripped_data[$key]['parcel_status']);
        }
        return $stripped_data;
    }
}
