<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Tools;

use Inc\Classes\HardCoded;
use Inc\Tools\OrderTools;
use Inc\LanguageControl;
use Inc\Tools\ShipmentTools;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class EmailTools
{
    /** 
     * Get all configured placeholders
     * @return array
     */
    public function getPlaceholders( $email_placeholders = array() )
    {
        $placeholders = array(
            '{order_id}'                    => __( 'Order ID (internal use)', 'tabticketbroker' ),
            '{order_no}'                    => __( 'Order Number', 'tabticketbroker' ),
            '{order_date}'                  => __( 'Order Date', 'tabticketbroker' ),
            '{order_status}'                => __( 'Order Status', 'tabticketbroker' ),
            '{invoice_no}'                  => __( 'Invoice Number', 'tabticketbroker' ),
            '{invoice_total}'               => __( 'Invoice total amount', 'tabticketbroker' ),
            '{payment_method_selected}'     => __( 'Payment method selected by user', 'tabticketbroker' ),
            '{customer_email}'              => __( 'Order Billing Email', 'tabticketbroker' ),
            '{first_name}'                  => __( 'Order Billing First Name', 'tabticketbroker' ),
            '{last_name}'                   => __( 'Order Billing Last Name', 'tabticketbroker' ),
            '{website_url}'                 => __( 'Website URL', 'tabticketbroker' ),
            '{website_name}'                => __( 'Website Name: Oktoberfest-Tischreservierungen.de or Tab-Ticketbroker.de ', 'tabticketbroker' ),
            '{event_year}'                  => __( 'Year of event derived from order', 'tabticketbroker' ),
            '{reservation_infobox}'         => __( 'Reservation infobox. Used in final instructions email', 'tabticketbroker' ),
            '{reservation_owner}'           => __( 'Reservation owner name. Used in final instructions email', 'tabticketbroker' ),
            
            // From Settings
            '{pickup_address}'              => __( 'Settings: Pickup location. Used in collection ready email', 'tabticketbroker' ),
            '{pickup_contact}'              => __( 'Settings: Pickup contact name. Used in collection ready email', 'tabticketbroker' ),
            '{ttb_pickup_address_map_link}' => __( 'Settings: Pickup location map link. Used in collection ready email', 'tabticketbroker' ),
            '{pickup_open_hours}'           => __( 'Settings: Pickup location open hours. Used in collection ready email', 'tabticketbroker' ),
            '{hotline}'                     => __( 'Settings: Hotline number for emergencies.', 'tabticketbroker' ),
            '{contact_email}'               => __( 'Settings: Contact email for emergencies.', 'tabticketbroker' ),
            '{pickup_code}'                 => __( 'Settings: Pickup code for the order. Used in collection ready email', 'tabticketbroker' ),

            // Hardcoded placeholders
            '{order_admin_email}'       => 'order@oktoberfest-tischreservierungen.de',
            '{web_admin_email}'         => get_option( 'admin_email' ),
            '{office_tel}'              => HardCoded::getOfficeDetails()['phone'],
            '{mobile_tel}'              => HardCoded::getOfficeDetails()['mobile_phone'],
        );

        if ( ! empty( $email_placeholders ) ) {
            $placeholders = array_merge( $placeholders, $email_placeholders );
        }
        
        return $placeholders;
    }

    /**
     * Return all configured placeholders
     * @return array
     */
    public function setPlaceholders( $order, $placeholders = array(), $args = null )
    {
        // Get the reservation from the args if they are passed
        if ( ! empty( $args ) && array_key_exists( 'reservation', $args ) ) {
            $reservation = $args['reservation'];
        } else {
            $reservation = null;
        }
        
        $placeholders['{order_id}']                 = $order->get_id();
        $placeholders['{order_no}']                 = $order->get_order_number();
        $placeholders['{order_date}']               = $order->get_date_created()->date_i18n( wc_date_format() );
        $placeholders['{order_status}']             = wc_get_order_status_name( $order->get_status() );
        $placeholders['{invoice_no}']               = get_post_meta( $order->id, '_ttb_invoice_no', true );
        $placeholders['{invoice_total}']            = $order->get_formatted_order_total();
        $placeholders['{payment_method_selected}']  = $order->get_payment_method_title();
        $placeholders['{customer_email}']           = $order->get_billing_email();
        $placeholders['{first_name}']               = $order->get_billing_first_name();
        $placeholders['{last_name}']                = $order->get_billing_last_name();
        $placeholders['{website_url}']              = '<a href="' . get_site_url() . '">' . preg_replace( '#^https?://#', '', get_site_url() ) . '</a>';
        $placeholders['{website_name}']             = TTB_SITE == 'ofest' ? 'Oktoberfest-Tischreservierungen.de' : 'Tab-Ticketbroker.de';
        $placeholders['{event_year}']               = $order->get_date_created()->date_i18n( 'Y' );

        // only set reservation infobox if the placeholder is used
        if ( array_key_exists( '{reservation_infobox}', $placeholders ) ) {
            $placeholders['{reservation_infobox}'] = $this->getReservationInfobox( $order );
        } else {
            $placeholders['{reservation_infobox}'] = '{reservation_infobox}';
        }

        // Takes plenty of time to run:
        // only set shared shared_reservation_infobox if the placeholder is used
        if ( array_key_exists( '{shared_reservation_infobox}', $placeholders ) && ! empty( $reservation ) ) {
            $placeholders['{shared_reservation_infobox}'] = $this->getSharedReservationInfo( $order, $reservation );
        } else {
            $placeholders['{shared_reservation_infobox}'] = '{shared_reservation_infobox}';
        }

        if ( array_key_exists( '{reservation_owner}', $placeholders ) && ! empty( $reservation ) ) {
            $placeholders['{reservation_owner}'] = $reservation->getOwnerName() ? $reservation->getOwnerName() : '{reservation_owner}';
        } else {
            $placeholders['{reservation_owner}'] = '{reservation_owner}';
        }

        // only set {parent_reservation_pax} if the placeholder is used
        if ( array_key_exists( '{parent_reservation_pax}', $placeholders ) && ! empty( $reservation ) ) {
            $placeholders['{parent_reservation_pax}'] = $reservation->getPaxAssigned() ? $reservation->getPaxAssigned() : '{parent_reservation_pax}';
        } else {
            $placeholders['{parent_reservation_pax}'] = '{parent_reservation_pax}';
        }

        if ( array_key_exists( '{pickup_address}', $placeholders ) ) {
            $shipment_tools = new ShipmentTools;
            $placeholders['{pickup_address}'] = $shipment_tools->getPickupAddressHtmlBox();
        } else {
            $placeholders['{pickup_address}'] = '{pickup_address}';
        }
        
        if ( array_key_exists( '{pickup_contact}', $placeholders ) ) {
            $placeholders['{pickup_contact}'] = get_option( 'ttb_pickup_contact_name' );
        } else {
            $placeholders['{pickup_contact}'] = '{pickup_contact}';
        }

        if ( array_key_exists( '{ttb_pickup_address_map_link}', $placeholders ) ) {
            $placeholders['{ttb_pickup_address_map_link}'] = get_option( 'ttb_pickup_address_map_link' );
        } else {
            $placeholders['{ttb_pickup_address_map_link}'] = '{ttb_pickup_address_map_link}';
        }

        if ( array_key_exists( '{pickup_open_hours}', $placeholders ) ) {
            $open_hours = get_option( 'ttb_pickup_open_hours' );
            // replace line breaks with <br />
            $open_hours = str_replace( "\n", '<br />', $open_hours );
            $placeholders['{pickup_open_hours}'] = $open_hours;            
        } else {
            $placeholders['{pickup_open_hours}'] = '{pickup_open_hours}';
        }

        if ( array_key_exists( '{hotline}', $placeholders ) ) {
            $placeholders['{hotline}'] = get_option( 'ttb_hotline' );
        } else {
            $placeholders['{hotline}'] = '{hotline}';
        }

        if ( array_key_exists( '{contact_email}', $placeholders ) ) {
            $placeholders['{contact_email}'] = get_option( 'ttb_contact_email' );
        } else {
            $placeholders['{contact_email}'] = '{contact_email}';
        }

        if ( array_key_exists( '{pickup_code}', $placeholders ) ) {
            $shipment_tools = new ShipmentTools;
            $placeholders['{pickup_code}'] = $shipment_tools->getPickupCode( $order );
        } else {
            $placeholders['{pickup_code}'] = '{pickup_code}';
        }

        return $placeholders;
    }

    /**
     * checkRequiredPlaceholders
     * @param  array $required_placeholders
     * @return string|bool                  Returns true if all required placeholders are set, otherwise returns an error message
     *  */ 
    public function checkRequiredPlaceholders( $required_placeholders, $placeholders )
    {
        // Check if all required placeholders are set and not empty
        foreach ($required_placeholders as $placeholder) {
            if ( ! isset( $placeholders[$placeholder] ) || empty( $placeholders[$placeholder] ) || $placeholders[$placeholder] == $placeholder ) {
                return __( 'Email not sent: Missing or empty required placeholder = ' . $placeholder, 'tabticketbroker' );
            }
        }

        return true;
    }

    /**
     * Get the setting for the rules download link
     * @return string
     */
    public function getRulesDownloadPlaceholder( $email_obj )
    {
        // Get the language key
        $lang_key = $this->getLanguageKey( $email_obj, null );
        
        if ( $lang_key == '_de' ) {
            $golden_rules_shortcode_default = $email_obj->golden_rules_shortcode_de;
        } else {
            $golden_rules_shortcode_default = $email_obj->golden_rules_shortcode;
        }

        $golden_rules_shortcode = $email_obj->format_string( $email_obj->get_option( 'golden_rules_shortcode'. $lang_key, $golden_rules_shortcode_default ) );

        return do_shortcode( $golden_rules_shortcode );
        
    }

    /** 
     * Returns the reservation infobox for the email
     * @return string
     */
    public function getReservationInfobox( $order )
    {
        
        $order_tools = new OrderTools;
        $reservations = $order_tools->getOrderReservations( $order->get_id() );

        if ( empty( $reservations ) ) {
            return '';
        }
        
        $html = '';
        $reservation_counter = 1;
        $reservation_count = count( $reservations );
        foreach ( $reservations as $reservation_array ) {
            
            $reservation = $reservation_array['reservation'];

            $order_item_table_nos = $reservation->getOrderItemTableNos();

            $html .= '<div style="border:1px solid black;background-color:#e5e5e5ff;padding:20px 30px 0px 30px;width:30%;margin:0 0 30px 0;">';
          
            // show counter if there is more than one reservation
            if ( $reservation_count > 1 ) {
                $html .= '<h3>'. __( 'Reservation', 'tabticketbroker' ) .' #'. $reservation_counter .'</h3>';
            }

            $html .= '<p><b>' . __( 'Beer hall', 'tabticketbroker' ) . ':</b> ' . $reservation->getProductTitle() . '</p>';
            $html .= '<p><b>' . __( 'Area', 'tabticketbroker' ) . ':</b> ' . $reservation->getAreaDisplay() . '</p>';
            $html .= '<p><b>' . __( 'Date', 'tabticketbroker' ) . ':</b> ' . $reservation->getResDisplayDate() . '</p>';
            $html .= '<p><b>' . __( 'Start Time', 'tabticketbroker' ) . ':</b> ' . $reservation->getStartTime() . '</p>';
            $html .= '<p><b>' . __( 'Guest of', 'tabticketbroker' ) . ':</b> ' . $reservation->getOwnerName() . '</p>';

            // Table number
            if ( get_option( 'ttb_show_tent_table_no' ) ) {
                // If order item table numbers are set, populate the txt
                $table_no_2_txt = '';
                if ( isset( $order_item_table_nos[$reservation_array['order_item_id']] ) ) {
                    $table_no_2_txt = ' - ' . $order_item_table_nos[$reservation_array['order_item_id']];
                }
                $html .= $reservation->getTableNo() ? '<p><b>' . __( 'Table No.', 'tabticketbroker' ) . ':</b> ' . $reservation->getTableNo() . $table_no_2_txt .'</p>' : '';
            }         

            if ( get_option( 'ttb_show_tent_customer_no' ) ) {
                $html .= $reservation->getCustomerNo() ? '<p><b>' . __( 'Customer No.', 'tabticketbroker' ) . ':</b> ' . $reservation->getCustomerNo() . '</p>' : '';
            }
            if ( get_option( 'ttb_show_tent_booking_no' ) ) {
                $html .= $reservation->getBookingNo() ? '<p><b>' . __( 'Booking No.', 'tabticketbroker' ) . ':</b> ' . $reservation->getBookingNo() . '</p>' : '';
            }

            $html .= '</div>';
            $reservation_counter++;
        }
        return $html;
    }


    /** 
     * Returns the reservation infobox for the email
     * This function takes a lot of time to run, no thorough testing done, can most likely be improved
     * @return string
     */
    public function getSharedReservationInfo( $order, $reservation = null )
    {
        $order_tools = new OrderTools;

        // Get all order item ids from the order
        // $order_item_ids = $order_tools->getOrderItemIds( $order->get_id() );

        // Get the reservations from the args if they are passed
        if ( empty( $reservation ) ) return '';
        
        $html = '';
        $reservation_counter = 1;
        // $reservation_count = count( $reservations );
            
        $html .= '<div style="border:1px solid black;background-color:#e5e5e5ff;padding:20px 30px 0px 30px;width:50%;margin:0 0 30px 0;">';
        $html .= '<h3 style="color:#e5e5e5ff;height:0;font-size:0.001em;">'. __( 'Reservation', 'tabticketbroker' ) .' #'. $reservation->getCode() .'</h3>';
        $html .= '<h3>'. __( 'Reservation', 'tabticketbroker' ) .'</h3>';
        $html .= '<p><b>' . __( 'Beer hall', 'tabticketbroker' ) . ':</b> ' . $reservation->getProductTitle() . '</p>';
        $html .= '<p><b>' . __( 'Date', 'tabticketbroker' ) . ':</b> ' . $reservation->getResDisplayDate() . '</p>';
        $html .= '<p><b>' . __( 'Start Time', 'tabticketbroker' ) . ':</b> ' . $reservation->getStartTime() . '</p>';
        $html .= '<p><b>' . __( 'Guest of', 'tabticketbroker' ) . ':</b> ' . $reservation->getOwnerName() . '</p>';
        $html .= '<p><b>' . __( 'Table area', 'tabticketbroker' ) . ':</b> ' . $reservation->getTableNo() . '</p>';

        $html .= '<hr>';
        $html .= '<h4>'. __( 'All guests on this reservation', 'tabticketbroker' ).':</h4>';

        // Get other assigned orders to the reservation
        $res_cnt = 1;
        // $reservation_orders = $reservation->getAssignedOrders( $reservation );
        // $reservation_orders = $reservation->getOrderItems();
        $reservation_order_item_ids = $reservation->getOrderItemIds();
        $order_item_table_nos = $reservation->getOrderItemTableNos();

        $voucher_owners = $reservation->getVoucherOwners() ? $reservation->getVoucherOwners() : array();

        foreach ( $reservation_order_item_ids as $reservation_order_item_id ) {
            
            $reservation_order_id = wc_get_order_id_by_order_item_id( $reservation_order_item_id );
            $reservation_order = wc_get_order( $reservation_order_id );

            if ( $reservation_order->get_id() == $order->get_id() ) {
                $your_group_txt = '<b>['.__( 'Your group', 'tabticketbroker' ).']</b>';
            }
            else {
                $your_group_txt = '';
            }

            if ( array_key_exists( $reservation_order_item_id, $voucher_owners ) ) {
                $voucher_owner_txt = '<b>** '.__( 'received Brotzeitbrett\'l', 'tabticketbroker' ).' **</b>';
            }
            else {
                $voucher_owner_txt = '';
            }
        
            // Get the pa_pax attribute from the order item
            $reservation_pax = wc_get_order_item_meta( $reservation_order_item_id, 'pa_pax', true );

            // Get the table number from the order item
            $reservation_table_no_txt = ( isset( $order_item_table_nos[$reservation_order_item_id] ) && ! empty( $order_item_table_nos[$reservation_order_item_id] ) ) ? ' @ '. $order_item_table_nos[$reservation_order_item_id] : '';

            $html .= '<p><b>' . __( 'Group', 'tabticketbroker' ) . ' # '. $res_cnt .' '. $reservation_table_no_txt .':</b> ' . $reservation_order->get_billing_last_name() .' ('.$reservation_pax.' people ) '.$your_group_txt.' '.$voucher_owner_txt.'</p>';
            // $html .= '<p><b>' . __( 'Group', 'tabticketbroker' ) . ' # '. $res_cnt .':</b> ' . $reservation_order->get_billing_last_name() .' ('.$reservation_pax.' people ) '. $your_group_txt.'</p>';

            $res_cnt++;
        }

        // if there is a voucher, add a notice in the html
        if( $voucher_owners ){
            $html .= '<p>'.__( 'The reservation includes an additional voucher for the ‘Brozeitbrett’l (a mixed starter plate). The voucher is for the entire table. Please see above who has received the voucher(s).', 'tabticketbroker' ).'</p>';
        }

        $html .= '</div>';

        // Add hidden text with reservation id
        // $html .= '<p style="color:#FFF;">RS' . $reservation->getId() . '</p>';
        
        $reservation_counter++;

        return $html;
    }


    /**
     * Get placeholder description for email settings
     * @return string
     */
    public function addPlaceholderDescription( $placeholders, $description = '' )
    {
        $placeholder_description = $description .'<br /><br />';

        // Notify the user of the possible placeholders in the description
        $placeholder_description = __('You can use the following placeholders in the email content:', 'tabticketbroker');
        
        foreach ( $placeholders as $key => $value) {
            $placeholder_description .= '<br/><code>' . $key . '</code>' . $value;
        }

        return $placeholder_description;
    }
    
    /** 
     * Returns the email heading depending on the language
     * @return string
     */
    public function getHeading( $email_obj )
    {
        $lang_key = $this->getLanguageKey( $email_obj );

        if ( $lang_key == '_de' ) {
            $default_heading = $email_obj->heading_de;
        } else {
            $default_heading = $email_obj->heading;
        }
        
        $heading = $email_obj->format_string( $email_obj->get_option( 'heading'. $lang_key, $default_heading ) );

        return $heading;
    }

    /** 
     * Returns the email subject depending on the language
     * @return string
     */
    public function getSubject( $email_obj )
    {
        $lang_key = $this->getLanguageKey( $email_obj );

        if ( $lang_key == '_de' ) {
            $default_subject = $email_obj->subject_de;
        } else {
            $default_subject = $email_obj->subject;
        }
        
        $subject = $email_obj->format_string( $email_obj->get_option( 'subject'. $lang_key, $default_subject ) );

        return $subject;
    }

    /**
     * Message box contents for the email
     * $lang_key: only en or de is currently supported
     * @return string
     */
    public function getMessageboxContent( $email_obj, $messagebox_id )
    {
        $lang_key = $this->getLanguageKey( $email_obj );

        if ( $lang_key == '_de' ) {
            $default_message = $email_obj->{'messagebox_'. $messagebox_id .'_de'};
        } else {
            $default_message = $email_obj->{'messagebox_'. $messagebox_id};
        }

        $messagebox_content = $email_obj->format_string( $email_obj->get_option( 'messagebox_'. $messagebox_id . $lang_key, $default_message ) );
     
        return $messagebox_content;
    }

    /**
     * Returns the language key determined from the order
     * It will only add an _de for german language
     * @return string only: '' or '_de'
     */
    public function getLanguageKey( $email_obj, $order = null )
    {
        if ( $email_obj ) {
            $order_language = LanguageControl::getLanguageFromLocale( get_post_meta( $email_obj->order->id, '_ttb_order_language', true ) );
        }
        elseif ( $order ) {
            $order_language = LanguageControl::getLanguageFromLocale( get_post_meta( $order->get_id(), '_ttb_order_language', true ) );
        }
        else {
            $order_language = 'english';
        }

        if ( strtolower( $order_language ) == 'german' ) {
            $lang_key = '_de';
        } else {
            $lang_key = ''; // default for english is empty
        }

        return $lang_key;
    }

    /**
     * Return the shipments infobox for the email
     * @param  WC_Order $order The order object
     * @return string               The html for the shipment infobox
     */
    public function getShipmentInfobox( $shipment )
    {
        // Get all the details for the shipment
        $shipment_id = str_pad( $shipment->get_id(), 4, '0', STR_PAD_LEFT );
        $status = $shipment->get_status();
        $shipping_provider = $shipment->get_shipping_provider_title();
        $get_tracking_url = $shipment->get_tracking_url();
        $tracking_id = $shipment->get_tracking_id();
        $date_sent = $shipment->get_date_sent();
        $date_sent = $date_sent ? $date_sent->date_i18n( 'd.m.Y' ) : '';
        
        // Start the html
        $html = '<div class="ttb-shipment-infobox">';
        $html .= '<div style="border:1px solid black;background-color:#e5e5e5ff;padding:20px 30px 0px 30px;width:40%;margin:0 0 30px 0;">';
        $html .= '<p style="float:right;margin:0;padding:0;">'. __( 'Shipment', 'tabticketbroker' ) .' #'. $shipment_id .'</p>';
        
        if ( $status != 'shipped' ) {
            $html .= '<p style="cursor:info;" title="'.__( 'This only displays if the shipment is not in a shipped status.', 'tabticketbroker' ).'">'. __( 'Status', 'tabticketbroker' ) .': '. $status .' &#9432;</p>';
        }

        $html .= '<p>'. __( 'Courier', 'tabticketbroker' ) .': <b>'. $shipping_provider .'</b></p>';
        $html .= '<p>'. __( 'Tracking code', 'tabticketbroker' ) .': <b>'. $tracking_id .'</b></p>';
        $html .= '<p>'. __( 'Dispatch date', 'tabticketbroker' ) .': <b>'. $date_sent .'</b></p>';
        $html .= '<p>'. __( 'Track your reservation package here', 'tabticketbroker' ) .': <a target="_blank" href="'. $get_tracking_url .'">'. __( 'Track my order', 'tabticketbroker' ) .'</a></p>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}
