<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Tools;

use Inc\Base\BaseController;
use Inc\Classes\TabReservation;
use Inc\Classes\ListTable;
use Inc\Classes\JMD_Logger;

Class OrderTools extends BaseController
{
    // Set class properties
    public $order               = null;
    public $reservations        = array();
    public $order_reservations  = array();
    public $order_items_meta    = array();

    // WIP: 2022-09-13: 
    // More urgent tasks to do first, but this is indirectly implemented, 
    // I just wanted to round it off with porper class initialization. 
    // Everything is in working order at time of writing
    // /** 
    //  * Custom initialization
    //  */
    // public function initialize( $order )
    // {
    //     $this->order = $order;
    //     $this->reservations = $this->setReservations();
    //     $this->order_reservations = $this->getOrderReservations( $this->order );
    // }

    /**
     * Function to check if a given country code is within the European continent
     * @param string Country code
     * @return boolean
     */
    public function isEuContinent( $country_code ) 
    {
        
        $eu_countrycodes = array(
            'AL','AD','AM','AT','BY','BE','BA','BG','CH','CY','CZ','DE','DK','EE',
            'ES','FO','FI','FR','GB','GE','GI','GR','HU','HR','IE','IS','IT','LT',
            'LU','LV','MC','MK','MT','NO','NL','PO','PT','RO','RU','SE','SI','SK',
            'SM','TR','UA','VA'
        );

        return ( in_array( $country_code, $eu_countrycodes ) );
    }

    public function setReservations( $args = array() ) 
    {
        $reservation = new TabReservation();    
        $this->reservations = $reservation->getReservations( $args );
    }
    
    /**
     * An order function to retrieve reservation information if the order is linked to any reservation
     * @param int Order ID
     * @return array Reservation data
     */
    public function getOrderReservations( $order_id )
    {
        // Check if the order reservations for the order id have already been retrieved
        if ( isset( $this->order_reservations[$order_id] ) ) {
            return $this->order_reservations[$order_id];
        }
        
        $this->order = wc_get_order( $order_id );

        if ( ! $this->reservations ) $this->setReservations( $args = array('ttb_event_year'=> $this->calculateEventYear( $this->order ) ) );

        $order_reservations = array();

        foreach( $this->order->get_items() as $order_item_id => $item ) {
            foreach ( $this->reservations as $reservation ) {
                if ( in_array( $order_item_id, $reservation->order_item_ids ) ) {
                    $order_reservations[] = array(
                        'order_id'                  => $order_id,
                        'order_item_id'             => $order_item_id,
                        'reservation'               => $reservation,
                    );
                }
            }
        } 

        // Add this order to the order reservations array
        $this->order_reservations[$order_id] = $order_reservations;

        return $this->order_reservations[$order_id];
    }
    
    /**
     * An order function to retrieve reservation information if the order is linked to any reservation
     * @param int Order ID
     * @return array Reservation data
     */
    public static function getAllOrderReservations()
    {
        // Get all orders
        $orders = wc_get_orders( array( 'status' => 'any' ) );

        $reservation = new TabReservation();    
        $reservations = $reservation->getReservations();

        $order_reservations = array();

        foreach( $orders as $order ) {
            $order_id = $order->get_id();
            foreach( $order->get_items() as $order_item_id => $item ) {
                foreach ( $reservations as $reservation ) {
                    if ( in_array( $order_item_id, $reservation->order_item_ids ) ) {
                        $order_reservations[] = array(
                            'order_id'                  => $order_id,
                            'order_item_id'             => $order_item_id,
                            'reservation'               => $reservation,
                        );
                    }
                }
            }
        }
        
        return $order_reservations;
    }

    public function getReservationDetailsHtml( $order_id ) 
    {
        $order_reservations = $this->getOrderReservations( $order_id );

        $reservation_details_html = '';

        foreach( $order_reservations as $order_reservation ) {
         
            $reservation_details_html .= __( 'Reservation', 'tabticketbroker' ) .': '. $order_reservation['reservation']->product_title .'<br>';
            $reservation_details_html .= __( 'Date', 'tabticketbroker' ) .': '. $order_reservation['reservation']->date_de .'<br>';
            $reservation_details_html .= __( 'Start time', 'tabticketbroker' ) .': '. $order_reservation['reservation']->start_time .'<br>';

        }

        return $reservation_details_html;
    }

    public function printLinkedOrderReservationDetails( $order_id ) 
    {
        // Check if order reservations have already been set
        if ( ! isset( $this->order_reservations[$order_id] ) ) {
            $this->order_reservations[$order_id] = $this->getOrderReservations( $order_id );
        }        

        // If there are no reservations, return
        if ( empty( $this->order_reservations[$order_id] ) ) {
            echo '<p>'. __( 'No reservations assigned', 'tabticketbroker' ). '</p>';
            
            // Show button linking to unassigned orders report
            echo '<a href="'. admin_url( 'admin.php?page=tab_admin_reports&order_id='. $order_id ) .'" class="button">'. __( 'Show unassigned orders', 'tabticketbroker' ) .'</a>';
            return;
        }

        // Print reservation details
        foreach( $this->order_reservations[$order_id] as $order_reservation ) {
            
            // Get assignment link
            $assignment_link = 'admin.php?page='. $this->page_names['Assignments'] .'&reservation_id_direct='. $order_reservation['reservation']->id;
            $supplier_link = 'post.php?post='. $order_reservation['reservation']->supplier_id. '&action=edit';
            $reservation_link = 'post.php?post='.$order_reservation['reservation']->id .'&action=edit';
            
            // show div box for each reservation with details and id as link to reservation
            ?>
            <div class="reservation-details">
                <div class="code">RS<?php echo $order_reservation['reservation']->id; ?></div>
                <div class="code reservation_sku"><?php echo $order_reservation['reservation']->sku; ?></div>
                <div class="owner"><?php echo $order_reservation['reservation']->owner_name; ?> (<?php _e( 'Supplier', 'tabticketbroker' ); ?> : <?php echo $order_reservation['reservation']->supplier_name; ?>)</div>
                <div class="table-no"><?php _e('TableNo', 'tabticketbroker'); ?>: <?php echo $order_reservation['reservation']->getTableNo() ? $order_reservation['reservation']->getTableNo() : '_____'; ?></div>
                <div class="title"><?php echo $order_reservation['reservation']->product_title; ?></div>
                <?php echo __( 'Start', 'tabticketbroker' ) .': '. $order_reservation['reservation']->display_date .' '. $order_reservation['reservation']->start_time; ?>
                <br>
                <?php echo __( 'Pax', 'tabticketbroker' ) .': '. $order_reservation['reservation']->pax .' - '. $order_reservation['reservation']->pax_assigned .' = '. $order_reservation['reservation']->pax_open; ?>
                <br>
                <?php echo __( 'Status', 'tabticketbroker' ) .': '. $order_reservation['reservation']->status; ?>
                <br>
                <?php 
                    echo __( 'Docs', 'tabticketbroker' ) .': ';
                    echo $order_reservation['reservation']->getDocsComplete() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>';
                
                    echo __( 'Ship', 'tabticketbroker' ) .': ';
                    echo $order_reservation['reservation']->getReadyToShip() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>';
                ?>
                
                <br>
                <?php 
                    echo __( 'Notes', 'tabticketbroker' ) .': ';
                    echo $order_reservation['reservation']->notes ? $order_reservation['reservation']->notes : '-'; 
                ?>
                <br>
                <div class="res-actions">
                    <!-- Add link to reservation -->
                    <div class="reservation-link">
                        <a target="_blank" href="<?php echo $reservation_link; ?>" class="button" title="<?php _e( 'Go to reservation', 'tabticketbroker' ); ?>">
                            <span class="dashicons dashicons-welcome-write-blog"></span>
                        </a>
                    </div>
                    <!-- Add link to supplier -->
                    <div class="supplier-link">
                        <a target="_blank" href="<?php echo $supplier_link; ?>" class="button" title="<?php _e( 'Go to supplier', 'tabticketbroker' ); ?>">
                            <span class="dashicons dashicons-admin-users"></span>
                        </a>
                    </div>
                    <!-- Add link to assignment  -->
                    <div class="assignment-link">
                        <a target="_blank" href="<?php echo $assignment_link; ?>" class="button" title="<?php _e( 'Go to assignments', 'tabticketbroker' ); ?>">
                            <span class="dashicons dashicons-forms"></span>
                        </a>
                    </div>
                </div>
            </div>
            <?php
        }
    }

    /**
     * Function to keep track of order pickup status
     * It returns the last log entry and if not set it uses logic (see note below) to determine the 
     * status & creates a new log entry
     * 
     * NOTE: The logic part of this function was stolen from woocommerce germanized here: 
     * Vendidero\Germanized\Shipments\<Order:824>    * I could not refer to this class from 
     * my class & didnt have time to investigate, todo: use original function
     * from WcCustomization->overrideShippingAddress
     * 
     * @param object Order
     * @return array Last log entry of the pickup status incl status, datetime & user
     *
     */
    public function getLastPickupLog( $order ) {

        // Get meta log from order id using metalog in JMD_Logger
        $meta_log = JMD_Logger::metalog( $order->get_id(), '_ttb_pickup_log' );

        // If meta log is empty, return false
        if ( empty( $meta_log ) ) {
            $shipping_methods = $order->get_shipping_methods();
            $is_pickup       = false;
    
            /**
             * Filters which shipping methods are considered local pickup method
             * which by default do not require shipment.
             *
             * @param string[] $pickup_methods Array of local pickup shipping method ids.
             *
             * @since 3.1.6
             * @package Vendidero/Germanized/Shipments
             */
            $pickup_methods = apply_filters( 'woocommerce_gzd_shipment_local_pickup_shipping_methods', array( 'local_pickup' ) );
    
            foreach( $shipping_methods as $shipping_method ) {
                if ( in_array( $shipping_method->get_method_id(), $pickup_methods ) ) {
                    $is_pickup = true;
                    break;
                }
            }

            // Save the pickup to the order first (used in the order overview as a column)
            update_post_meta( $order->get_id(), '_ttb_pickup', $is_pickup );
            
            // Log meta log with pickup
            $log_array = array(
                'pickup' => $is_pickup,
                'user'   => 'system',
            );

            $meta_log = JMD_Logger::metalog( $order->get_id(), '_ttb_pickup_log', $log_array );
            $last_log_entry = end( $meta_log );
        }
        else {
            // Get last log entry
            $last_log_entry = end( $meta_log );

            // Ensure the log and meta field are in sync
            if ( $order->get_meta( '_ttb_pickup' ) != $last_log_entry['pickup'] ) {
                update_post_meta( $order->get_id(), '_ttb_pickup', $last_log_entry['pickup'] );
            }
        }

		return $last_log_entry;
	}

    /**
     * Return, for a given order ID, all the items in the order with their IDs, SKUs and flag
     * @param mixed $order the order ID or order object
     * @return array 
     */
    public static function getOrderItemMeta( $order ) 
    {
        // Get order
        $order = ( is_int( $order ) ? wc_get_order( $order ) : $order );

        $order_id = $order->get_id();
        
        // Get already flagged order items 
        $flagged_items = ( get_post_meta( $order_id, '_ttb_flagged_items', true ) == '' ? array() : get_post_meta( $order_id, '_ttb_flagged_items', true ) );
        
        // If this meta data is not saved as an array reset it
        if ( ! is_array( $flagged_items ) ) $flagged_items = array();

        // Cycle all order items 
        foreach ( $order->get_items() as $order_item_id => $item ) {

            // Get variation as product
            $product = wc_get_product($item->get_variation_id());

            // Get SKU for variation (Handle exception for if a variation has been deleted)
            if ( $product ) {
                $item_sku = $product->get_sku();
            } else {
                $item_sku = 'no-sku';
            }

            // Get SKU for variation
            $order_items_meta[$order_item_id] = array( 
                    'order_id'          => $order_id ,
                    'order_item_id'     => $order_item_id,
                    'sku'               => $item_sku,
                    'flagged'           => ( in_array( $order_item_id, $flagged_items ) ),
                );
        }

        // If blank, return empty array
        if ( empty( $order_items_meta ) ) return array();

        return $order_items_meta;
    }
    
    /**
     * Generate report that shows reservations for the supplier
     */
    public function OrderItemsReport( $order ) 
    {
        $order_id = $order->get_id();
        $report_title = 'Order items';

        $report_columns = array(
            'sku'           => __( 'SKU', 'tabticketbroker' ),
            'flagged'       => __( 'Flagged', 'tabticketbroker' ),
            'assignments'   => __( 'Assignment', 'tabticketbroker' ),
            'vouchers'      => __( 'Vouchers', 'tabticketbroker' ),
        );
        
        $report_columns_hidden = array( 'date' );
        
        $report_columns_sortable = array(
            'reservation_id'    => array( 'reservation_id', 'asc' ),
            'product_title'     => array( 'product_title', 'asc' ),
            'display_date'      => array( 'date', 'asc' ), 
        );

        // Get order items
        $this->order_items_meta = $this->getOrderItemMeta( $order );

        // Get reservations
        $order_reservations[$order_id] = $this->getOrderReservations( $order->get_id() );

        // Import reservation data into array for the report
        foreach ($this->order_items_meta as $item_id => $item) {
            $report_data[] = array(
                'sku'           => $item['sku'], 
                'flagged'       => '<input type="checkbox" name="ttb_flagged_items[]" value="' . $item_id . '" ' . checked( $item['flagged'], true, false ) . '>',
                'assignments'   => '',
                'vouchers'      => '',
            );
        }

        $per_page = 50;
        
        $list_table = new ListTable;

        $list_table->setupTable( 
            $report_columns, 
            $report_columns_hidden, 
            $report_columns_sortable, 
            $report_data, 
            $per_page
        );
        
        $list_table->prepare_items();
        
        // Show the table
        $args = array(
            'hide_table_nav' => true,
            'hide_footer' => true,
            'hide_bottom_nav' => true,
        );
        $list_table->displayWithoutNonce( $args );
    }

    public function getEarliestItemDate( $order ) 
    {
        // loop through the order items and find the earliest pa_date attribute
        $earliest_pa_date = null;
        $order_items = $order->get_items();
        foreach( $order_items as $item ) {
            $pa_date = wc_get_order_item_meta( $item->get_id(), 'pa_date' );
            if( $pa_date ) {
                $pa_date = new \DateTime( $pa_date, wp_timezone() );
                if( $earliest_pa_date === null || $pa_date < $earliest_pa_date ) {
                    $earliest_pa_date = $pa_date;
                }
            }
        }

        // Set the date return string, else return empty string
        $earliest_pa_date_str = $earliest_pa_date ? $earliest_pa_date->format( 'Y-m-d' ) : '';

        return $earliest_pa_date_str;
    }

    public function setEarliestItemDate( $order_id ) 
    {
        // Get the order
        $order = wc_get_order( $order_id );

        // Get the earliest item date
        $earliest_item_date = $this->getEarliestItemDate( $order );

        // Update the order meta
        update_post_meta( $order_id, '_ttb_earliest_date', $earliest_item_date );
    }

    /**
     * Calculate the event year based on the earliest item date.
     * @param WC_Order $order
     * @return string NOTE: returns the current event year if the order is not a valid
     *                      WC_Order or if no date could be found from the order items
     */
    public function calculateEventYear( $order )
    {
        // Check if the order is a valid wc order, if not we use the current year
        if( ! $order instanceof \WC_Order ) {
            $event_year = TTB_EVENT_YEAR;
        }
        else {
            // Get the earliest item date
            $earliest_item_date = $this->getEarliestItemDate( $order );
            
            // If there is no date we also use the current year
            if( $earliest_item_date === '' ) {
                $event_year = TTB_EVENT_YEAR;
            }
            else {
                // Convert to DateTime object
                $earliest_item_date = new \DateTime( $earliest_item_date, wp_timezone() );
                // Get the event year
                $event_year = $earliest_item_date->format( 'Y' );
            }
        }
        
        return $event_year;
    }

    /**
     * Used by the metabox on the order to copy the invoice no from fakturapro
     * This is only done if the invoice no is empty and, at time of writing, only
     * called from the order metabox when the order is loaded.
     */
    public static function getAndCopyInvoiceNo( $order_id )
    {
        // Get the ttb invoice no
        $invoice_no = get_post_meta( $order_id, '_ttb_invoice_no', true );
        
        // If the invoice has been set, return it
        if ( $invoice_no != '' ) {
            return $invoice_no;
        }

        // Otherwise we try to get the invoice no from fakturapro
        $invoice_no_fakturpro = get_post_meta( $order_id, 'fakturpro_number', true );

        // If we have an invoice no from fakturapro, we copy it to the ttb invoice no
        if ( $invoice_no == '' && $invoice_no_fakturpro != '' ) {
            update_post_meta( $order_id, '_ttb_invoice_no', $invoice_no_fakturpro );
            $invoice_no = $invoice_no_fakturpro;
        }

        return $invoice_no;
    }

    // public static function markPickupItemCollected()
    // {        
    //     $order_id = (int) $_POST['order_id'];
        
    //     $activity = ( $_POST['activity'] ? $_POST['activity'] : 'no activity set' );
    //     $activity_status = ( $_POST['status'] == '1' ? true : false );

    //     $user = ( $_POST['username'] ? $_POST['username'] : 
    //         ( get_current_user_id() == 0 ? 'system' : get_user_by( 'id', get_current_user_id() )->display_name ) );
    
    //     // If the order id is not set, return
    //     if ( ! $order_id ) {
    //         echo json_encode( array( 'success' => false, 'message' => __( 'No order id set', 'tabticketbroker' ) ) );
    //         wp_die();
    //     }

    //     // Use the JMD_Logger to update the collected log
    //     JMD_Logger::metalog( $order_id, '_ttb_collected_log', array( 
    //         'user' => $user,
    //         'activity' => $activity,
    //         'status' => $activity_status ,
    //         ) );

    //     // Return success
    //     echo json_encode( array( 'success' => true ) );
    //     wp_die();
    // }

    public static function updateParcelMeta()
    {
        if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'ttb-collections-nonce')) {
            wp_die(-1);
        }

        $post_id = (int) $_POST['post_id'];
        // $item_id = (int) $_POST['item_id'];

        if( ! $post_id ) {
            echo json_encode( array( 'success' => false, 'message' => __( 'No post id set', 'tabticketbroker' ) ) );
            wp_die();
        }

        if( $_POST['action'] == 'parcel_meta_status_change' ) {
            $status = ( $_POST['status'] ? $_POST['status'] : 'no status set' );
            $user = ( $_POST['username'] ? $_POST['username'] : 
                ( get_current_user_id() == 0 ? 'system' : get_user_by( 'id', get_current_user_id() )->display_name ) );
            
            // Use the JMD_Logger to update the parcel status log
            JMD_Logger::metalog( $post_id, '_ttb_parcel_status_log', array( 
                'user' => $user,
                'status' => $status,
                ) );

            // Get the parcel meta
            $parcel_meta = get_post_meta( $post_id, '_ttb_parcel_meta', true );

            // Update all statuses to zero except the one that was just updated
            foreach( $parcel_meta[$post_id]['status'] as $key => $value ) {
                if ( $key == $status ) {
                    $parcel_meta[$post_id]['status'][$key] = 1;
                }
                else {
                    $parcel_meta[$post_id]['status'][$key] = 0;
                }
            }

            // Update the parcel meta
            update_post_meta( $post_id, '_ttb_parcel_meta', $parcel_meta );

            // Send collection ready email 
            // Decision was made to send it out the moment the pickup status goes to shipped
            // This is because it is shipped at such sort notice and then the agent at the head office knows the client gets the email
            if ( $status == 'shipped' ) {
                do_action( 'ttb_collection_ready_email', $post_id );
            }

            // Return success
            echo json_encode( array( 'success' => true ) );
            wp_die();
        }
    }
}