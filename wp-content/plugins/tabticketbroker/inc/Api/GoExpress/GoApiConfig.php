<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Api\GoExpress;

class GoApiConfig
{
    public $debug_mode = 1;

    public $api_endpoints = array();

    public $credentials = array();

    public function configureApi()
    {
        $this->setStation();

        $this->setApiEndpoints();

        $this->setCredentials();
    }

    public function setApiEndpoints()
    {
        
        if ( $this->debug_mode ) {
            $this->api_endpoints = array(
                'create_order' => array(
                    'method' => 'POST',
                    'url' => 'https://ws-tst.api.general-overnight.com/external/ci/order/api/v1/createOrder',
                ),
                'update_order_status' => array(
                    'method' => 'POST',
                    'url' => 'https://ws-tst.api.general-overnight.com/external/ci/order/api/v1/updateOrderStatus',
                ),
                'request_label_info' => array(
                    'method' => 'POST',
                    'url' => 'https://ws-tst.api.general-overnight.com/external/ci/label/api/v1/generateLabelForhwb',
                ),
            );
        } 
        else {
            $this->api_endpoints = array(
                'create_order' => array(
                    'method' => 'POST',
                    'url' => 'https://ws.api.general-overnight.com/external/ci/order/api/v1/createOrder',
                ),
                'update_order_status' => array(
                    'method' => 'POST',
                    'url' => 'https://ws.api.general-overnight.com/external/ci/order/api/v1/updateOrderStatus',
                ),
                'request_label_info' => array(
                    'method' => 'POST',
                    'url' => 'https://ws-tst.api.general-overnight.com/external/ci/label/api/v1/generateLabelForhwb',
                ),
            );
        }
        return $this;
    }

    public function setCredentials()
    {
        // if ( $this->debug_mode ) {
            $this->credentials = array(
                'username'  => 'tab_ticketbroker',
                'password'  => 'VzDl9mUfvXqyPiJttWWi',
            );
        // } 
        // else {
        // $this->credentials = array(
        //     'username'  => '40205ticketbroker',
        //     'password'  => 'RAQMBC@4GmP*fKGn4',
        // );
        // }
    }

    public function setStation()
    {
        $this->station = array(
            'responsible_station'   => 'TXL',
            'customer_id'           => '40205',
        );
    }
}