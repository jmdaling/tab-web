<?php
/**
 * @package tabticketbroker
 */

 namespace Inc\Api\GoExpress;

use MyThemeShop\Helpers\Arr;

// use Vendidero\Germanized\Shipments;

/**
 * A class to handle Go Express API data processing required for their REST API
 */
class GoApiData extends GoApiTools
{
    
    public $create_order_data = '';         // JSON data for create order API
    public $update_order_status_data = '';  // JSON data for update order status API
    public $request_label_info_data = '';   // JSON data for request label info API

    /**
     * Put together array of data for API from an order shipment
     * @param  Shipment $shipment
     * @return string JSON data
     */
    public function getAllShipmentData( $shipment )
    {
        // Get order from order shipment
        $order = $shipment->get_order();

        // Get all billing details from order
        $billing_details = $order->get_address( 'billing' );
        $shipping_details = $order->get_address( 'shipping' );

        // Create customer reference from the order number and customer id
        $order_number = $order->get_order_number();

        // Get weight from shipment
        $weight = $shipment->get_weight();

        // Get shipment items
        $shipment_items = $shipment->get_items();

        // Get shipment id
        $shipment_id = $shipment->get_id();

        // Get consignor address from settings
        $consignor_address = array(
            'name'          => get_option( 'ttb_consignor_address' ),
            'street'        => get_option( 'ttb_consignor_street' ),
            'house_no'      => get_option( 'ttb_consignor_house_no' ),
            'postcode'      => get_option( 'ttb_consignor_postcode' ),
            'city'          => get_option( 'ttb_consignor_city' ),
            'email'         => get_option( 'ttb_consignor_email' ),
            'phone'         => get_option( 'ttb_consignor_phone' ),
        );

        // Combine in an array
        $shipment_creation_data = array(
            'order_id'          => $order->get_id(),
            'order_number'      => $order_number,
            'shipping_details'  => $shipping_details,
            'billing_details'   => $billing_details,
            'shipment_id'       => $shipment_id,
            'weight'            => $weight,
            'consignor_address' => $consignor_address,
            'shipment_items'    => $shipment_items,
        );

        return $shipment_creation_data;
    }

    /**
     * Build JSON data for create order API from an order shipment
     * @param  array $shipment_data
     * @return string JSON data
     */
    public function getCreateOrderData( $shipment_data )
    {
        $create_order_json =
        '{
            "responsibleStation": "'. $this->station['responsible_station'] .'",
            "customerId": "'. $this->station['customer_id'] .'",
            "shipment": {
                "hwbNumber": "",
                "orderStatus": "New",
                "validation": "",
                "service": "ON",
                "weight": "'.$shipment_data['weight'].'",
                "content": "",
                "customerReference": "'.$shipment_data['order_no'].'",
                "selfPickup": "",
                "selfDelivery": "",
                "dimensions": "",
                "packageCount": "2",
                "freightCollect": "",
                "receiptNotice": "",
                "isNeutralPickup": "",
                "pickup": {
                    "date": "01.12.2022",
                    "timeFrom": "15:00",
                    "timeTill": "17:00"
                },
                "delivery": {
                    "date": "",
                    "avisFrom": "",
                    "avisTill": "",
                    "timeFrom": "09:00",
                    "timeTill": "10:00",
                    "weekendOrHolidayIndicator": ""
                },
                "insurance": {
                    "amount": "",
                    "currency": ""
                },
                "valueOfGoods": {
                    "amount": "",
                    "currency": ""
                },
                "cashOnDelivery": {
                    "amount": "",
                    "currency": ""
                }
            },
            "consignorAddress": {
                "name1": "'.$shipment_data['consignor_address']['name'].'",
                "name2": "",
                "name3": "",
                "street": "'.$shipment_data['consignor_address']['street'].'",
                "houseNumber": "'.$shipment_data['consignor_address']['house_no'].'",
                "zipCode": "'.$shipment_data['consignor_address']['postcode'].'",
                "city": "'.$shipment_data['consignor_address']['city'].'",
                "country": "DE",
                "phoneNumber": "'.$shipment_data['consignor_address']['phone'].'",
                "remarks": "",
                "email": "'.$shipment_data['consignor_address']['email'].'"
            },
            "neutralAddress": {
                "name1": "",
                "name2": "",
                "name3": "",
                "street": "",
                "houseNumber": "",
                "zipCode": "",
                "city": "",
                "country": ""
            },
            "consigneeAddress": {
                "name1": "'.$shipment_data['shipping_details']['first_name'].' '.$shipment_data['shipping_details']['last_name'].'",
                "name2": "",
                "name3": "",
                "street": "'.$shipment_data['shipping_details']['address_1'].'",
                "houseNumber": "",
                "zipCode": "'.$shipment_data['shipping_details']['postcode'].'",
                "city": "'.$shipment_data['shipping_details']['city'].'",
                "country": "DE",
                "phoneNumber": "",
                "remarks": "",
                "email": "'.$shipment_data['shipping_details']['email'].'"
            },
            "label": "1",
            "packages": [
                {
                    "length": "",
                    "width": "",
                    "height": ""
                }
            ]
        }';

        return $create_order_json;
    }



    /**
     * Set test order data
     * @return void
     */
    public function setTestOrderData()
    {
        $this->create_order_data =
        '{
            "responsibleStation": "'. $this->station['responsible_station'] .'",
            "customerId": "'. $this->station['customer_id'] .'",
            "shipment": {
                "hwbNumber": "",
                "orderStatus": "New",
                "validation": "",
                "service": "ON",
                "weight": "0.25",
                "content": "",
                "customerReference": "Reference 123",
                "selfPickup": "",
                "selfDelivery": "",
                "dimensions": "",
                "packageCount": "2",
                "freightCollect": "",
                "receiptNotice": "",
                "isNeutralPickup": "",
                "pickup": {
                    "date": "01.12.2022",
                    "timeFrom": "15:00",
                    "timeTill": "17:00"
                },
                "delivery": {
                    "date": "",
                    "avisFrom": "",
                    "avisTill": "",
                    "timeFrom": "09:00",
                    "timeTill": "10:00",
                    "weekendOrHolidayIndicator": ""
                },
                "insurance": {
                    "amount": "",
                    "currency": ""
                },
                "valueOfGoods": {
                    "amount": "",
                    "currency": ""
                },
                "cashOnDelivery": {
                    "amount": "",
                    "currency": ""
                }
            },
            "consignorAddress": {
                "name1": "Test AG",
                "name2": "",
                "name3": "",
                "street": "Nowhere Straße",
                "houseNumber": "12",
                "zipCode": "53119",
                "city": "Berlin",
                "country": "DE",
                "phoneNumber": "",
                "remarks": "",
                "email": ""
            },
            "neutralAddress": {
                "name1": "",
                "name2": "",
                "name3": "",
                "street": "",
                "houseNumber": "",
                "zipCode": "",
                "city": "",
                "country": ""
            },
            "consigneeAddress": {
                "name1": "Test Consignee 1",
                "name2": "",
                "name3": "",
                "street": " Greeck Str.",
                "houseNumber": "11",
                "zipCode": "36272",
                "city": "Schotten",
                "country": "DE",
                "phoneNumber": "",
                "remarks": "",
                "email": ""
            },
            "label": "1",
            "packages": [
                {
                    "length": "",
                    "width": "",
                    "height": ""
                }
            ]
        }';
    }
}