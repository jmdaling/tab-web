<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Api\GoExpress;

class GoApiTools extends GoApiConfig
{
    public $create_order_data = '';

    public function __construct()
    {
        // if Go Api debug mode config option is set use that value
        // otherwise set debug mode to true
        if ( get_option( 'ttb_go_api_debug_mode' ) ) {
            $this->debug_mode = get_option( 'ttb_go_api_debug_mode' );
        }
        else {
            $this->debug_mode = true;
            update_option( 'ttb_go_api_debug_mode', $this->debug_mode );
        }
        
        $this->configureApi();
    }

    /**
     * Call create order API
     * @param  array $data
     * @return array
     */
    public function createOrder( $shipment_order )
    {
        // Set the data
        $this->create_order_data = $shipment_order;

        // Call create order API
        $response = $this->callApi('create_order', $this->create_order_data);
    }

    // create function to call api
    public function callApi( $endpoint, $data )
    {
        $credentials = $this->credentials;

        $url = $this->api_endpoints[$endpoint]['url'];
        $method = $this->api_endpoints[$endpoint]['method'];

        // $data = json_encode( $data );
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode($credentials['username'] . ':' . $credentials['password']),
        ));

        $result = curl_exec( $ch );

        curl_close($ch);
        
        return $result;
    }
}