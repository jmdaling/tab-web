<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;
use Inc\Classes\ProductTools;
use Inc\Classes\TabReservation;
use Inc\Reports\UnassignedOrders;

class AdminCallbacks extends BaseController
{
    public function adminDashboard()
    {
        return require_once("$this->plugin_path/templates/dashboard.php");
    }

    public function getAdminFunctionsHtml()
    {
        $user_allowed = ( user_can( wp_get_current_user(), 'tab_admin_functions' ) ? true : false );
?>
        <h3><?php _e( 'Admin Functions', 'tabticketbroker' ); ?></h3>
        <div id="admin_functions">
            <!-- Button trigger modal -->
            <span class="tooltip-wrapper<?php echo ( !$user_allowed ? ' disabled' : '' ); ?>" tabindex="0" data-toggle="tooltip" data-placement="right" title="<?php echo __( 'This function re-calculates all assignment counts. It can be run at any time and does no harm. Please notify the developer if assignment counts are going out of sync regularly.', 'tabticketbroker' ); ?>">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reset_assignment_counts">
                    <?php _e( 'Reset assignment counts', 'tabticketbroker' ); ?>
                </button>
            </span>
            <br>
            <br>
            <span class="tooltip-wrapper<?php echo ( !$user_allowed ? ' disabled' : '' ); ?>" tabindex="0" data-html=true data-toggle="tooltip" data-placement="right" title="<?php echo ( ! $user_allowed ? __( 'Only administrators can execute this function.', 'tabticketbroker' ) : __( 'This function clears all assignments. All orders will be removed from reservations. All reservations will be empty and have zero pax assigned. Run this function ONLY if you know what you are doing!', 'tabticketbroker' ) ); ?>">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#clear_assignment_modal" <?php echo ( ! $user_allowed ? ' disabled' : '' ); ?>>
                    <?php _e( 'Clear all assignments', 'tabticketbroker' ); ?>
                </button>
            </span>
            <br>
            <br>
            <span class="tooltip-wrapper<?php echo ( !$user_allowed ? ' disabled' : '' ); ?>" tabindex="0" data-html=true data-toggle="tooltip" data-placement="right" title="<?php echo ( ! $user_allowed ? __( 'Only administrators can execute this function.', 'tabticketbroker' ) : __( 'This function recreates all event date meta data and can be run safely.', 'tabticketbroker' ) ); ?>">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create_product_event_dates" <?php echo ( ! $user_allowed ? ' disabled' : '' ); ?>>
                    <?php _e( 'Create all event date meta data', 'tabticketbroker' ); ?>
                </button>
            </span>
            <br>
            <br>
            <span class="tooltip-wrapper<?php echo ( !$user_allowed ? ' disabled' : '' ); ?>" tabindex="0" data-html=true data-toggle="tooltip" data-placement="right" title="<?php echo ( ! $user_allowed ? __( 'Only administrators can execute this function.', 'tabticketbroker' ) : __( 'This function recreates product variation SKU\'s.', 'tabticketbroker' ) ); ?>">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reset_var_skus" <?php echo ( ! $user_allowed ? ' disabled' : '' ); ?>>
                    <?php _e( 'Reset Variation SKU\'s', 'tabticketbroker' ); ?>
                </button>
            </span>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="clear_assignment_modal" tabindex="-1" role="dialog" aria-labelledby="clear_assignment_modal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="clear_assignment_modal_title"><?php echo __( 'Confirm Action', 'tabticketbroker' ); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="centre badge badge-pill badge-danger"><?php echo __( 'This action cannot be undone', 'tabticketbroker' ); ?></span>
                        <br>
                        <?php echo __( 'Are you sure? Have you made a backup?', 'tabticketbroker' ); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __( 'Cancel', 'tabticketbroker' ); ?></button>
                        <a class="btn btn-outline-danger" href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "admin_function" => "remove_all_assignments", 'active_tab' => 2 ) ) ); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                                <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                                <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                            </svg>
                            <?php _e( 'Clear all assignments', 'tabticketbroker' ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="reset_assignment_counts" tabindex="-1" role="dialog" aria-labelledby="reset_assignment_counts" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="reset_assignment_counts_title"><?php echo __( 'Confirm Action', 'tabticketbroker' ); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="centre badge badge-pill badge-warning"><?php echo __( 'You are about to reset the assignment counts', 'tabticketbroker' ); ?></span>
                        <br>
                        <?php echo __( 'Although this action can be done safely, please inform the developer if this action is required regularly.', 'tabticketbroker' ); ?>
                        <p><em><?php echo __( 'It might take a while, be patient.', 'tabticketbroker' ); ?></em></p>
                        <hr>
                        <p class="text-muted"><?php echo __( 'Counts get out of sync when a user action was only half completed. For example when a order was removed from a reservation and the process got interrupted (browser error or power failure) thus resulting in the order being removed successfully yet the count was not updated. ', 'tabticketbroker' ); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __( 'Cancel', 'tabticketbroker' ); ?></button>
                        <a class="btn btn-outline-warning" href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "admin_function" => "reset_assignment_counts", 'active_tab' => 2 ) ) );?> ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                                <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                                <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                            </svg>
                            <?php _e( 'Reset assignment counts', 'tabticketbroker' ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="create_product_event_dates" tabindex="-1" role="dialog" aria-labelledby="create_product_event_dates" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="create_product_event_dates_title"><?php echo __( 'Confirm Action', 'tabticketbroker' ); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="centre badge badge-pill badge-warning"><?php echo __( 'You are about to recreate all event date meta data for all products', 'tabticketbroker' ); ?></span>
                        <br>
                        <?php echo __( 'Although this action can be done safely, please inform the developer if this action is required regularly.', 'tabticketbroker' ); ?>
                        <p><em><?php echo __( 'It might take a while, be patient.', 'tabticketbroker' ); ?></em></p>
                        <hr>
                        <p class="text-muted"><?php echo __( 'In the shop, parent products are displayed. These are sorted by the dates linked to the variations underneath. This process creates a custom field with the name of jm_event_dates (all dates comma separated) and jm_event_first_date (first variation date without -\'s ). The latter of which is used to sort the shop products.', 'tabticketbroker' ); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __( 'Cancel', 'tabticketbroker' ); ?></button>
                        <a class="btn btn-outline-warning" href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "admin_function" => "create_product_event_dates", 'active_tab' => 2 ) ) );?> ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                                <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                                <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                            </svg>
                            <?php _e( 'Create all event date meta data', 'tabticketbroker' ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="reset_var_skus" tabindex="-1" role="dialog" aria-labelledby="reset_var_skus" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="reset_var_skus_title"><?php echo __( 'THIS IS A DEVELOPER FUNCTION', 'tabticketbroker' ); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="centre badge badge-pill badge-warning"><?php echo __( 'You are about to reset all product variations\'s SKUS with the auto-generate function', 'tabticketbroker' ); ?></span>
                        <br>
                        <?php echo __( 'This process will loop through all the variations and attempt to auto-generate each\'s SKU. If a variation is not configured correctly the SKU will be ignored. A log file with the changes and errors is saved in the plugin\'s log folder.', 'tabticketbroker' ); ?>
                        <p><em><?php echo __( 'It might take a while, be patient.', 'tabticketbroker' ); ?></em></p>
                        <hr>
                        <p class="text-muted"><?php echo __( 'Use the Price Report to identify blank SKU\'s', 'tabticketbroker' ); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __( 'Cancel', 'tabticketbroker' ); ?></button>
                        <a class="btn btn-outline-warning" href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "admin_function" => "reset_var_skus", 'mock_run' => true, 'active_tab' => 2 ) ) );?> ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                                <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                                <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                            </svg>
                            <?php _e( 'Test run (logs changes on server)', 'tabticketbroker' ); ?>
                        </a>
                        <a class="btn btn-outline-warning" href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "admin_function" => "reset_var_skus", 'mock_run' => false, 'active_tab' => 2 ) ) );?> ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                                <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"></path>
                                <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"></path>
                            </svg>
                            <?php _e( 'Reset ALL product variation SKU\'s', 'tabticketbroker' ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    <?php

    }

    /**
     * Triggers the function methods selected to execute from the admin functions
     * @void
     */
    public function processAdminFunction( string $function_name = '', array $args = array() )
    {
        // JMD_TODO: Find a way to not execute this on refresh
        switch ( $function_name ) {
            case 'remove_all_assignments':
                $reservation_instance = new TabReservation;
                $reservation_instance->removeAllAssignments();
                $this->addFlashNotice( __( 'All order-reservation assignments removed', 'tabticketbroker' ) );
                break;
            case 'reset_assignment_counts':
                $reservation_instance = new TabReservation;
                $reservation_instance->resetAssignedPaxCount();
                $this->addFlashNotice( __( 'All assignment counts have been reset', 'tabticketbroker' ) );
                break;
            case 'create_product_event_dates':
                ProductTools::createFirstEventDateAllProducts();
                $this->addFlashNotice( __( 'All event dates have been created', 'tabticketbroker' ) );
                break;
            case 'reset_product_stock':
                $product_id = $args['product_id'];
                if ( ! $args || ! $product_id ) {
                    $this->addFlashNotice( __( 'ERROR: No product id found. Product stock reset NOT done', 'tabticketbroker' ) );
                }
                else {
                    
                    wc_update_product_stock( $product_id, 1, 'set', false );
                    $this->addFlashNotice( __( 'Product stock reset DONE', 'tabticketbroker' ) );
                }
                break;
            case 'reset_var_skus':
                if ( isset( $args['mock_run'] ) ) {
                    $mock_run = $args['mock_run'];
                } else {
                    $mock_run = true;
                }
                $changes_log = ProductTools::resetVariationSkus($mock_run);

                // Email log to admin
                $subject = 'Product SKU changes log';
                $message = print_r( $changes_log, true );
                wp_mail( get_option( 'admin_email' ), $subject, $message );

                $this->addFlashNotice( __( 'All variation SKU\'s have been reset. See the the admin email inbox with a report OR the plugin log folder for a report in sku_changes.log.', 'tabticketbroker' ) );
                // Log changes to console
                echo '<pre>';
                print_r( $changes_log );
                echo '</pre>';
                break;
        }
    }

    public function supplierCallback()
    {
        return require_once( "$this->plugin_path/templates/supplier_mgmt.php" );
    }

    /**
     * Placeholder method for future development
     * Should retrieve a count with all reservations that have no orders assigned
     */
    public function getOpenReservations()
    {
        return 13;
    }
    /**
     * Placeholder method for future development
     * Should retrieve a count with all critcal orders (to be defined)
     */
    public function getCriticalOrderCount()
    {
        return 1;
    }

    public function getDashboardHtml()
    {
        
        $unassigned_orders = new UnassignedOrders;

        $order_count = $unassigned_orders->getUnassignedOrderCount();
        
        ?>
        <h3><?php _e( 'Dashboard', 'tabticketbroker' ); ?></h3>
        <div class="card-deck">
            <div class="card">
                <div class="card-header text-center">
                    <h1><?php echo $order_count; ?></h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?php _e( 'Unassigned Orders', 'tabticketbroker' ); ?></h5>
                    <p class="card-text"><?php printf( __( 'There are currently %d unassigned orders', 'tabticketbroker' ), $order_count ); ?></p>
                    <a href="/wp-admin/admin.php?page=tab_admin_reports&report_slug=unassigned_orders" class="btn btn-primary"><?php _e( 'View', 'tabticketbroker' ); ?></a>
                </div>
            </div>

            <!-- <div class="card" data-toggle="tooltip" data-placement="bottom" title="<?php _e( "Example only", 'tabticketbroker' ); ?>">
                <div class="card-header text-center">
                    <h1><?php echo $this->getOpenReservations() ?></h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?php _e( 'Empty Reservations', 'tabticketbroker' ); ?></h5>
                    <p class="card-text"><?php printf( __( 'There are currently %d reservations with no orders assigned to them', 'tabticketbroker' ), $this->getOpenReservations() ); ?></p>
                    <a href="/wp-admin/admin.php?page=tab_admin_reports&report_slug=empty_reservations" class="btn btn-primary"><?php _e( 'View', 'tabticketbroker' ); ?></a>
                </div>
            </div>

            <div class="card" data-toggle="tooltip" data-placement="bottom" title="<?php _e( "Example only", 'tabticketbroker' ); ?>">
                <div class="card-header text-center">
                    <h1><?php echo $this->getCriticalOrderCount(); ?></h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?php _e( 'Critical Orders', 'tabticketbroker' ); ?></h5>
                    <p class="card-text">'Display critical orders here</p>
                    <a href="/wp-admin/admin.php?page=tab_admin_reports&report_slug=critical_orders" class="btn btn-primary"><?php _e( 'View', 'tabticketbroker' ); ?></a>
                </div>
            </div>

            <div class="card" data-toggle="tooltip" data-placement="bottom" title="<?php _e( "Example only", 'tabticketbroker' ); ?>">
                <div class="card-header text-center">
                    <h1>21</h1>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?php _e( 'Orders to be shipped', 'tabticketbroker' ); ?></h5>
                    <p class="card-text"><?php _e( 'Here we can display an order count and link to a report which will group orders which are ready to be sent and/or should be sent to either the pickup location or the customer\'s shipping address.', 'tabticketbroker' ); ?></p>
                    <a href="/wp-admin/admin.php?page=tab_admin_reports&report_slug=orders_to_ship" class="btn btn-primary"><?php _e( 'View', 'tabticketbroker' ); ?></a>
                </div>
            </div> -->

        </div>

        <?php
    }

    
    public function getSettings()
    {
        ?>
        <div>
            <h3><?php _e( 'System Settings', 'tabticketbroker' ); ?></h3>
        </div>
        <?php
    }


    public function tabticketbrokerOptionsGroup( $input )
    {
        return $input;
    }

    public function tabticketbrokerAdminSection()
    {
        echo __( 'Event Settings', 'tabticketbroker' );
    }

    public function ttbPickupAddressSection()
    {
        echo __( 'Ofest Pickup Address Settings', 'tabticketbroker' );
    }

    public function ttbConsignerAddressSection()
    {
        echo __( 'Consignor Address Settings', 'tabticketbroker' );
        echo '<p>' . __( 'This is the address that will be used for the consignor on the shipping label', 'tabticketbroker' ) . '</p>';
    }

    public function ttbApiSettingsSection()
    {
        echo __( 'API Settings', 'tabticketbroker' );
    }

    public function ttbEmailSettingsSection()
    {
        echo __( 'Ofest Email Settings', 'tabticketbroker' );
    }

    public function ttbEventSettingsSection()
    {
        echo __( 'Event Settings', 'tabticketbroker' );
    }

    // Toggle pickup 
    public function pickupToggle()
    {
        $value = get_option( 'ttb_pickup_enabled' );

        ?>
        <div class="ttb-option">
            <input type="hidden" name="ttb_pickup_enabled" value="">
            <input type="checkbox" id="ttb_pickup_enabled" name="ttb_pickup_enabled" value="1" <?php checked( $value, 1 ); ?> />
            <label class="ttb-option-label" for="ttb_pickup_enabled"><?php _e( 'Enable or disable pickup for customers.', 'tabticketbroker' ); ?></label> 
            <p><?php _e( 'Pickup information (in for example emails etc) and options within the Tab/Ofest realm is disabled. You still need to disable pickup from the shipping options from woocommerce to prevent orders with pickups selected', 'tabticketbroker' ); ?></p> 
        </div>
        <?php
    }

    public function pickupAddressLine1()
    {
        $value = esc_attr( get_option( 'ttb_pickup_address_line_1' ) );

        ?>
        <input id='ttb_pickup_address_line_1' name='ttb_pickup_address_line_1' value="<?php echo $value; ?>" placeholder="<?php _e( 'Line 1', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    public function pickupAddressLine2()
    {
        $value = esc_attr( get_option( 'ttb_pickup_address_line_2' ) );

        ?>
        <input id='ttb_pickup_address_line_2' name='ttb_pickup_address_line_2' value="<?php echo $value; ?>" placeholder="<?php _e( 'Line 2', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    public function pickupAddressCity()
    {
        $value = esc_attr( get_option( 'ttb_pickup_address_city' ) );

        ?>
        <input id='ttb_pickup_address_city' name='ttb_pickup_address_city' value="<?php echo $value; ?>" placeholder="<?php _e( 'City', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    public function pickupAddressCountry()
    {
        $value = esc_attr( get_option( 'ttb_pickup_address_country' ) );

        ?>
        <input id='ttb_pickup_address_country' name='ttb_pickup_address_country' value="<?php echo $value; ?>" placeholder="<?php _e( 'Country', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    public function pickupAddressPostCode()
    {
        $value = esc_attr( get_option( 'ttb_pickup_address_postcode' ) );

        ?>
        <input id='ttb_pickup_address_postcode' name='ttb_pickup_address_postcode' value="<?php echo $value; ?>" placeholder="<?php _e( 'Post code', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    public function pickupAddressMapLink()
    {
        $value = esc_attr( get_option( 'ttb_pickup_address_map_link' ) );
        ?>
        <input id='ttb_pickup_address_map_link' name='ttb_pickup_address_map_link' value="<?php echo $value; ?>" placeholder="<?php _e( 'Map link', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    // Pickup contact name
    public function pickupContactName()
    {
        $value = esc_attr( get_option( 'ttb_pickup_contact_name' ) );

        ?>
        <input id='ttb_pickup_contact_name' name='ttb_pickup_contact_name' value="<?php echo $value; ?>" placeholder="<?php _e( 'Contact name', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    // Pickup contact phone
    public function hotlinePhone()
    {
        $value = esc_attr( get_option( 'ttb_hotline' ) );

        ?>
        <input id='ttb_hotline' name='ttb_hotline' value="<?php echo $value; ?>" placeholder="<?php _e( 'Contact phone', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    // Contact email
    public function contactEmail()
    {
        $value = esc_attr( get_option( 'ttb_contact_email' ) );

        ?>
        <input id='ttb_contact_email' name='ttb_contact_email' value="<?php echo $value; ?>" placeholder="<?php _e( 'Contact email', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    // Opening hours
    public function pickupOpeningHours()
    {
        $value = esc_attr( get_option( 'ttb_pickup_open_hours' ) );

        // Create a textarea for the opening hours, with a height of 100px and width of 50%
        ?>
        <textarea id='ttb_pickup_open_hours' name='ttb_pickup_open_hours' rows="5" cols="50" placeholder="<?php _e( 'Opening hours', 'tabticketbroker' ); ?>"><?php echo $value; ?></textarea>
        <?php
    }

    // Username for the collections administrator front end
    public function collectionAdminUserName()
    {
        $value = esc_attr( get_option( 'ttb_collections_admin_username' ) );

        ?>
        <input id='ttb_collections_admin_username' name='ttb_collections_admin_username' value="<?php echo $value; ?>" placeholder="<?php _e( 'Username', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    // Password for the collections administrator front end
    public function collectionAdminPassword()
    {
        $value = esc_attr( get_option( 'ttb_collections_admin_password' ) );

        ?>
        <input id='ttb_collections_admin_password' name='ttb_collections_admin_password' value="<?php echo $value; ?>" placeholder="<?php _e( 'Password', 'tabticketbroker' ); ?>"></input>
        <?php
    }

    // Setting to show or hide Tent Table No in emails
    public function getEmailShowTentTableNo()
    {
        // ttb_show_tent_table_no
        $value = get_option( 'ttb_show_tent_table_no' );

        // Add checkbox to show or hide tent table number in email
        ?>
        <div class="ttb-option">
            <input type="hidden" name="ttb_show_tent_table_no" value="">
            <input type="checkbox" id="ttb_show_tent_table_no" name="ttb_show_tent_table_no" value="1" <?php checked( $value, 1 ); ?> />
            <label class="ttb-option-label" for="ttb_show_tent_table_no"><?php _e( 'Show Tent Table No in final instructions email', 'tabticketbroker' ); ?></label>
            <p><?php _e( 'This will show the tent table number in the emails sent to the customer.', 'tabticketbroker' ); ?></p>
        </div>
        <?php
    }

    // Setting to show or hide Tent Customer No in emails
    public function getEmailShowTentCustomerNo()
    {
        //ttb_show_tent_table_no
        $value = get_option( 'ttb_show_tent_customer_no' );

        // Add checkbox to show or hide customer number in email
        ?>
        <div class="ttb-option">
            <input type="hidden" name="ttb_show_tent_customer_no" value="">
            <input type="checkbox" id="ttb_show_tent_customer_no" name="ttb_show_tent_customer_no" value="1" <?php checked( $value, 1 ); ?> />
            <label class="ttb-option-label" for="ttb_show_tent_customer_no"><?php _e( 'Show Tent Customer No in final instructions email', 'tabticketbroker' ); ?></label> 
            <p><?php _e( 'This will show the customer number in the emails sent to the customer.', 'tabticketbroker' ); ?></p>
        </div>
        <?php
    }

    // Setting to show or hide Tent Booking no in emails
    public function getEmailShowTentBookingNo()
    {
        //ttb_show_tent_booking_no
        $value = get_option( 'ttb_show_tent_booking_no' );

        // Add checkbox to show or hide booking number in email
        ?>
        <div class="ttb-option">
            <input type="hidden" name="ttb_show_tent_booking_no" value="">
            <input type="checkbox" id="ttb_show_tent_booking_no" name="ttb_show_tent_booking_no" value="1" <?php checked( $value, 1 ); ?> />
            <label class="ttb-option-label" for="ttb_show_tent_booking_no"><?php _e( 'Show Tent Booking No in final instructions email', 'tabticketbroker' ); ?></label> 
            <p><?php _e( 'This will show the booking number in the emails sent to the customer.', 'tabticketbroker' ); ?></p>
        </div>
        <?php
    }

    // Setting for event year
    public function eventYear()
    {
        // get the year from the first day of the event
        $event_first_day = esc_attr( get_option( 'ttb_event_first_day' ) );
        $event_last_day = esc_attr( get_option( 'ttb_event_last_day' ) );

        // Get difference in days between first and last day of event
        $date_diff = date_diff( date_create( $event_first_day ), date_create( $event_last_day ) ) ;

        // Get the number of days between the first and last day of the event
        $days = $date_diff->format( '%a' );

        // Build date dimension array
        $date_dimension = array();
        for ( $i = 0; $i <= $days; $i++ ) {
            $date_slug = date( 'Y-m-d', strtotime( $event_first_day . ' + ' . $i . ' days' ) );
            $date_name = $this->getDisplayDateAdvanced( date( 'Y-m-d', strtotime( $event_first_day . ' + ' . $i . ' days' ) ), 'de_DE_formal', 'E dd.MM' );
            $date_dimension[$date_slug] = $date_name;

        }

        $value = date( 'Y', strtotime( $event_first_day ) );

        ?>
        <p><b><?php echo $value;?></b> | <?php echo __( 'Duration', 'tabticketbroker' ) .' = '.  $days .' '. __( 'days', 'tabticketbroker' );?></p>
        
        <p><?php echo __( 'Please ensure that the date attribute terms are set as shown in the table below.', 'tabticketbroker' ); ?></p>
        <p><?php echo __( 'These are calculated from the first and last day settings on this page.', 'tabticketbroker' ); ?></p>
        <a href="/wp-admin/edit-tags.php?taxonomy=pa_date&post_type=product"><?php _e( 'Click here to go to the date attribute', 'tabticketbroker' ); ?></a></p>
        <table id="settings_event_dates">
            <tr>
                <th><?php _e( 'Day', 'tabticketbroker' ); ?></th>
                <th><?php _e( 'Date Name', 'tabticketbroker' ); ?></th>
                <th><?php _e( 'Date Slug', 'tabticketbroker' ); ?></th>
            </tr>
            <?php
            $i = 0;
            foreach ( $date_dimension as $date_slug => $date_name ) {
                ?>
                <tr>
                    <td><?php echo ++$i; ?></td>
                    <td><?php echo $date_name; ?></td>
                    <td><?php echo $date_slug; ?></td>
                </tr>
                <?php
            }
            ?>
        </table>

        <input type="hidden" id="ttb_event_year" name="ttb_event_year" value="<?php echo $value; ?>">
        <?php
    }

    // Setting for first day of event (date)
    public function eventFirstDay()
    {
        $value = esc_attr( get_option( 'ttb_event_first_day' ) );

        // Get all product attributes for pa_date
        $args = array(
            'taxonomy' => 'pa_date',
            'hide_empty' => false,
        );

        $terms = get_terms( $args );

        // Get the min date from the terms
        $min_date = min( $terms );

        // If the min date is note the same as the value, set warning text var
        $warning_text = '';
        if ( $min_date->name != $value ) {
            $warning_text = __( 'WARNING: The first day of the event is not the same as the first date in the pa_date attribute.', 'tabticketbroker' );
        }

        $warning_text_html = '<p class="text-danger">' . $warning_text . '</p>';

        // create a date picker for the first day of the event
        ?>
        <!-- date picker -->
        <input type="date" id="ttb_event_first_day" name="ttb_event_first_day" value="<?php echo $value; ?>" placeholder="<?php _e( 'First day of event', 'tabticketbroker' ); ?>">
        <?php echo $warning_text_html; ?>
        <?php
    }

    // Setting for last day of event (date)
    public function eventLastDay()
    {
        $value = esc_attr( get_option( 'ttb_event_last_day' ) );

        // create a date picker for the last day of the event
        ?>
        <!-- date picker -->
        <input type="date" id="ttb_event_last_day" name="ttb_event_last_day" value="<?php echo $value; ?>" placeholder="<?php _e( 'Last day of event', 'tabticketbroker' ); ?>">
        <?php
    }
}
