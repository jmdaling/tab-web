<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Api;

use Inc\Reports\PriceReport;

class ApiRoutes
{
    public function register() 
    {
        add_action( 'rest_api_init', array( $this, 'registerRoutes' ) );
    }

    public function registerRoutes() 
    {
        register_rest_route( 'ttb/v1', '/variations', array(
            'methods' => 'GET',
            'callback' => array( $this, 'getVariations' ),
            'permission_callback' => function() {
                // return true;
                return current_user_can( 'manage_options' );
            }
        ) );
    }

    public function getVariations() 
    {
        $price_report = new PriceReport();
        $price_report->setVariations();
        $variations = $price_report->variations;
        
        $res = new \WP_REST_Response( array_values($variations), 200 );
        return $res;

    }
}

