<?php
/** 
 * @package tabticketbroker
 */

namespace Inc;

 final class Init
 {
    /**
     * Store all classes inside an array
     * @return array Full list of classes
     */
    public static function get_services() {
        return array(
            WpCustomization::class,
            Classes\OrderLifeCycle::class,
            RestApiCustomization::class,
            Api\ApiRoutes::class,
            Classes\ConfigCheck::class,
            Classes\Automation::class,
            LanguageControl::class,
            AdminPages::class,
            EmailController::class,
            WcCustomization::class,
            ShortcodeController::class,
            Base\Enqueue::class,

            // Custom Post Types
            Classes\RegisterCPTs::class,
            
            // Custom Editors
            Classes\TabSupplierEditor::class,
            Classes\TabReservationEditor::class,

            Classes\GenericFilters::class,

            Base\SettingsLinks::class,
            Reports\UnassignedOrders::class,
        );
    }

    /**
     * Loop through the classes, initialize them,
     * and call the register() method if it exists
     * @return
     */
    public static function register_services() {
        foreach ( self::get_services() as $class ) {
            $service = self::instantiate ( $class );
            if ( method_exists( $service, 'register' ) ) {
                $service->register();
            }
        }
    }

    /**
     * Initialize the class
     * @param class $class Class from the services array
     * @return class instance New instance of the class
     */
    private static function instantiate( $class ) {
        $service = new $class();
        return $service;
    }
}
