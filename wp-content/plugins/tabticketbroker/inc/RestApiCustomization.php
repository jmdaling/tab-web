<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Vendidero\Germanized\Shipments\Order;

class RestApiCustomization
{
    public function register()
    {
        // Intercept API integration import data provided to DHL
        add_filter( 'rest_post_dispatch', array( $this, 'jmdInterceptDhlShipmentData' ), 10, 3 );
        
        // Intercept DHL order status change request
        add_filter( 'rest_dispatch_request', array( $this, 'jmdInterceptDhlRequest' ), 10, 2 );        
    }

    /**
     * Function to intercept calls from DHL express. From DHL the express integration
     * 
     * Because we changed orders to shipments, the integration will attempt to update the 
     * order but use the shipment id. This will enable us to update the shipment status
     */
    
    public function jmdInterceptDhlRequest( $blank, $request ) 
    {
        // Only run on PUT & POST requests
        if ( $request->get_method() != 'PUT' && $request->get_method() != 'POST' ) {
            return $blank;
        }
        
        // Get calling user
        $user = get_user_by( 'id', get_current_user_id() );

        // Check that either the id or the note is set and that the user is keyaccount
        if ( ! ( $request->get_param('id') || $request->get_param('note') ) || $user->user_login != 'keyaccount' ) {
            return $blank;
        }

        // if id param is set use it for the shipment id, else check if the order_id param is set
        if ( $request->get_param('id') ) {
            $shipment_id = $request->get_param('id');
        }
        elseif ( $request->get_param('order_id') ) {
            $shipment_id = $request->get_param('order_id');
        }
        else {
            return $blank;
        }

        // If we have gotten this far, we expect a shipment
        $shipment = wc_gzd_get_shipment( $shipment_id );
        if ( ! $shipment ) {
            // Return a 404 error if no shipment is found
            $response = new \WP_REST_Response();
            // respond with error message
            $response->set_data( array(
                'code' => 'rest_order_status_change',
                'message' => __( 'Shipment not found.', 'tabticketbroker' ),
                'data' => array(
                    'status' => 404,
                ),
            ) );

            return $response;            
        }

        // Check if the request is to change the status to completed and is for a single order
        if (    $request->get_method()          == 'PUT'
            &&  $request->get_param('status')   == 'completed' 
            &&  $request->get_route()           == '/wc/v3/orders/'. $request->get_param('id') ) {

            // Now we know the order id is actually a shipment id, so we find the shipment
            $return = $this->updateShipmentStatus( $shipment );

            if ( $return ) {

                // Here we end the request and return a 200 status
                $response = new \WP_REST_Response();
                // respond with success message
                $response->set_data( array(
                    'code' => 'rest_order_status_change',
                    'message' => __( 'Shipment status has been updated.', 'tabticketbroker' ),
                    'data' => array(
                        'status' => 200,
                    ),
                ) );

                return $response;
            } 
            else {
                // Trow wordpress error with message in $return
                return new \WP_Error( 'empty_result', $return, array( 'status' => 404 ) );
            }
        }

        elseif (    $request->get_method()      == 'POST' // only post requests for notes
                &&  $request->get_param('note') !== null
                &&  $request->get_route()       == '/wc/v3/orders/'. $request->get_param('order_id') . '/notes' ) {

            // Now we know the order id is actually a shipment id, so we find the shipment and 
            // extract the tracking id from the note and add it to the shipment
            $update_response = $this->processShipmentWriteback( $shipment, $request->get_param('note') );
            
            if ( $update_response == 'success' ) {
                
                // Here we end the request and return a 200 status
                $response = new \WP_REST_Response();
                // respond with success message
                $response->set_data( array(
                    'code' => 'rest_order_note_request',
                    'message' => __( 'Shipment tracking id updated.', 'tabticketbroker' ),
                    'data' => array(
                        'status' => 200,
                    ),
                ) );

                return $response;
            } else if ( $update_response == 'already_updated' ) {
                
                // Here we end the request and return a 200 status
                $response = new \WP_REST_Response();
                // respond with success message
                $response->set_data( array(
                    'code' => 'rest_order_note_request',
                    'message' => __( 'Shipment tracking id already updated.', 'tabticketbroker' ),
                    'data' => array(
                        'status' => 200,
                    ),
                ) );

                return $response;
            }
            else {
                // Trow wordpress error with message in $update_response
                return new \WP_Error( 'empty_result', $update_response, array( 'status' => 404 ) );
            }
        }

        else {
            return $blank;
        }
    }

    public function processShipmentWriteback( $shipment, $note )
    {
        // Get the tracking id from the note
        $tracking_id = $this->getTrackingIdFromNote ( $note );

        // Get the current tracking id from the shipment
        $current_tracking_id = $shipment->get_tracking_id();

        // If the tracking id is the same as the current tracking id, return
        if ( $tracking_id == $current_tracking_id ) {
            // Log this in the error log
            error_log( 'DHL writeback, tracking id already updated, shipment id = ' . $shipment->get_id(). ', tracking id = ' . $tracking_id . ', note = ' . $note );
            return 'already_updated';
        }

        // Add the tracking id to the shipment
        $shipment->set_tracking_id( $tracking_id );

        // Save the shipment
        $shipment->save();

        // Get the order
        $order = wc_get_order( $shipment->get_order_id() );

        // Add a note to the order
        $order->add_order_note(
            sprintf(
                __( 'Shipment %s\'s tracking no updated = %s.', 'tabticketbroker' ),
                $shipment->get_id(),
                $tracking_id
            )
        );

        $order->add_order_note(
            sprintf(
                __( 'The Tracking Number was derived from DHL note = %s', 'tabticketbroker' ),
                $note
            )
        );

        return 'success';
    }

    public function getTrackingNoRegex()
    {
        // Tracking number regex pattern
        $tracking_no_regex = get_option( 'dhl_express_order_note_tracking_no_regex' );

        // If no tracking number regex is set, set default
        if ( ! $tracking_no_regex ) {
            
            // Get default
            $tracking_no_regex = $this->getDefaultTrackingNoRegex();

            // Set default in options
            update_option( 'dhl_express_order_note_tracking_no_regex', $tracking_no_regex );
        }

        return $tracking_no_regex;
    }

    public function getOrderItemValue()
    {
        // Tracking number regex pattern
        $order_item_value = 1; // cant be zero, remove and replace with below once settings page is created
        //get_option( 'dhl_express_default_order_item_value' );

        // If no tracking number regex is set, set default
        if ( ! $order_item_value ) {
            
            // Get default
            $order_item_value = $this->getDefaultOrderItemValue();

            // Set default in options
            update_option( 'dhl_express_default_order_item_value', $order_item_value );
        }

        return $order_item_value;
    }
    

    public function getDefaultOrderItemValue()
    {
        return 0;
    }


    public function getDefaultTrackingNoRegex()
    {
        return '/The Tracking Number is: ([0-9]+)/';
    }

    public function getTrackingIdFromNote( $note )
    {
        
        // Tracking number regex pattern
        $tracking_no_regex = $this->getTrackingNoRegex();

        // Get the tracking number from the note
        preg_match( $tracking_no_regex, $note, $matches );

        // If no tracking number is found, return
        if ( ! isset( $matches[1] ) ) {
            return 'No tracking number found';
        }

        // Return the tracking number
        return $matches[1];
    }

    public function updateShipmentStatus( $shipment )
    {
        // Check if shipment is in processing status.
        if ( ! $shipment->has_status( 'processing' ) ) {
            return;
        }

        // Check if it is a DHL express shipment.
        if ( $shipment->get_shipping_provider() != 'dhlexpress' ) {
            // set shipping provider to DHL express
            $shipment->set_shipping_provider( 'dhlexpress' );
        }

        // Update the shipment status to shipped
        $shipment->set_status( 'shipped' );
        $shipment->save();

        // Get the order
        $order = wc_get_order( $shipment->get_order_id() );

        // if no order is found, return
        if ( ! $order ) {
            return 'No order found';
        }

        // Add a note to the order
        $order->add_order_note(
            sprintf(
                __( 'Shipment %s has been shipped.', 'tabticketbroker' ),
                $shipment->get_id()
            )
        );

        // Save the order   
        // $order->save();

        return true;
    }


    /**
     * Function to intercept calls from DHL express. From DHL the express integration 
     * comes a call to the default WC rest api to import orders that need to be shipped.
     * The integration only gets orders. We will send it shipments that look like orders 
     * to process. These shipment objects we get from WooCommerce Germanized.
     */
    public function jmdInterceptDhlShipmentData( $response, $server, $request )
    {
        // Only run on GET requests
        if ( $request->get_method() != 'GET' ) {
            return $response;
        }

        // If the route does not contain "wc/v3/orders" return
        if ( strpos( $request->get_route(), 'wc/v3/orders' ) === false ) {
            return $response;
        }

        // Check that it is a part of the DHL request
        $user = get_user_by( 'id', get_current_user_id() );
        if ( ! $user->user_login == 'keyaccount' ) {
            return $response;
        }

        // Check if an ID was set, if so, check if a shipment exists with that id
        if ( $request->get_param('id') ) {
            $shipment = wc_gzd_get_shipment( $request->get_param('id') );
            if ( ! $shipment ) {
                return $response;
            }
        }

        // Check if the response has data
        if ( ! isset( $response->data ) ) {
            return $response;
        }

        // Check response status
        if ( $response->status != 200 ) {
            return $response;
        }

        // If all checks are passed, we go ahead and manipulate the response
        $new_repsonse = $this->manipulateResponse( $response );

        return $new_repsonse;
    }

    // Set the keys to set in the response, by implication all other keys will be removed
    public $keys_set = array(
        "id",   
        "parent_id",    
        "status",   
        "currency", 
        "version",  
        "prices_include_tax",   
        "date_created", 
        "date_modified",    
        "discount_total",   
        "discount_tax", 
        "shipping_total",   
        "shipping_tax", 
        "cart_tax", 
        "total",    
        "total_tax",    
        // "customer_id",  
        // "order_key",    
        "billing",  
        // 'meta_data',
        "shipping",
        // "payment_method",
        // "payment_method_title",
        // "transaction_id",
        // "customer_ip_address",
        // "customer_user_agent",
        // "created_via",
        // "customer_note",
        // "date_completed",
        // "date_paid",
        // "cart_hash",
        "number",
        // "meta_data",
        "line_items",
        // "tax_lines",
        "shipping_lines",
        // "fee_lines",
        // "coupon_lines",
        // "refunds",
        // "payment_url",
        // "is_editable",
        // "needs_payment",
        // "needs_processing",
        "date_created_gmt",
        "date_modified_gmt",
        "date_completed_gmt",
        "date_paid_gmt",
        // "shipments",
        // "shipping_status",
        // "currency_symbol",
        // "parcel_delivery_opted_in",
        // "direct_debit",
        // "_links",
    );

    // Set the keys to set in the line items response, by implication all other keys will be removed
    public $line_item_keys_set = array(
        "id",
        "name",
        // "product_id",
        // "variation_id",
        "quantity",
        // "tax_class",
        // "subtotal",
        // "subtotal_tax",
        // "total",
        // "total_tax",
        // "taxes",
        // "meta_data",
        "sku",
        // "price",
    );

    // Set the keys to set in the shipping lines response, by implication all other keys will be removed
    public $shipping_lines_keys_set = array(
        "id",
        "method_title",
        // "method_id",
        // "instance_id",
        // "total",
        // "total_tax",
        // "taxes",
        // "meta_data",
    );

    function manipulateResponse( $response )
    {
        // return $response;
        
        // Loop through data in response and clear all financial data
        foreach ( $response->data as $main_key => $single_response ) {
            // if Single response is not an array, skip
            if ( !is_array( $single_response ) ) {
                continue;
            }

            foreach ( $single_response as $key => $value ) {
                // Remove all orders except the one with id ...
                // prod id 30847
                // dev id 30649
                // if ( $key == 'id' && $value != 30847 ) {
                //     unset( $response->data[$main_key] );
                //     break;
                // }

                // Remove all keys that are not in the keys to set array
                if ( ! in_array( $key, $this->keys_set ) ) {
                    unset( $response->data[$main_key][$key] );
                    continue;
                }

                // Only include relevant line item keys
                if ( $key == 'line_items' ) {
                    foreach ( $value as $line_item_key => $line_item_value ) {
                        foreach ( $line_item_value as $line_item_value_key => $line_item_value_value ) {
                            if ( ! in_array( $line_item_value_key, $this->line_item_keys_set ) ) {
                                unset( $response->data[$main_key][$key][$line_item_key][$line_item_value_key] );
                            }
                        }
                    }
                }

                // Only include relevant shipping line keys
                if ( $key == 'shipping_lines' ) {
                    foreach ( $value as $shipping_line_key => $shipping_line_value ) {
                        foreach ( $shipping_line_value as $shipping_line_value_key => $shipping_line_value_value ) {
                            if ( ! in_array( $shipping_line_value_key, $this->shipping_lines_keys_set ) ) {
                                unset( $response->data[$main_key][$key][$shipping_line_key][$shipping_line_value_key] );
                            }
                        }
                    }
                }
            }
        }
        
        // Re-sequence the array
        $response->data = array_values( $response->data );

        // Now we adapt the order data to shipment data
        $response->data = $this->getChangeToShipments( $response->data );

        return $response;
    }

    /**
     * Function to change the order data to shipment data.
     */
    function getChangeToShipments( $response_data ) 
    {
        $new_repsonse_data = array();

        // Loop through all the reponse orders and get the order objects
        foreach ( $response_data as $response_order ) {
            $order = wc_get_order( $response_order['id'] );

            // Get WC GZ shipment object.
            $order_shipment = new Order( $order );

            $shipments = $order_shipment->get_shipments();

            // Loop through all shipments and get the shipment data.
            foreach ($shipments as $shipment) {

                // Check if shipment is in processing status.
                if ( ! $shipment->has_status( 'processing' ) ) {
                    continue;
                }

                // Check if it is a DHL express shipment.
                if ( $shipment->get_shipping_provider() != 'dhlexpress' ) {
                    continue;
                }

                // Get the line items from the shipment.
                $line_items = $shipment->get_items();
                $line_items_array = array();

                // Loop through all line items and get the line item data.
                foreach ($line_items as $line_item) {
                    $line_items_array[] = array(
                        'id'                => $line_item->get_id(),
                        'name'              => $line_item->get_sku(),
                        'quantity'          => $line_item->get_quantity(),
                        'sku'               => 'Document',
                        'parent_name'       => $line_item->get_sku(),
                        'total'             => 0,
                        'total_tax'         => 0,
                        'subtotal'          => 0,
                        'subtotal_tax'      => 0,
                        'price'             => $this->getOrderItemValue(),
                    );
                }

                $new_repsonse_data[] = array(
                    'id'                => $shipment->get_id(),
                    'parent_id'         => $response_order['parent_id'],
                    'status'            => $order->get_status(),
                    'currency'          => $order->get_currency(),
                    'version'           => $response_order['version'],
                    'prices_include_tax'=> $response_order['prices_include_tax'],
                    "date_created"      => $order->get_date_created()->date('Y-m-d\TH:i:s'),
                    "date_modified"     => $order->get_date_modified()->date('Y-m-d\TH:i:s'),
                    
                    'discount_total'    => 0,
                    'discount_tax'      => 0,
                    'shipping_total'    => 0,
                    'shipping_tax'      => 0,
                    'cart_tax'          => 0,
                    'total'             => 0,
                    'total_tax'         => 0,
                    'billing'        => array(
                        "first_name" => $order->get_billing_first_name(),
                        "last_name"  => $order->get_billing_last_name(),
                        "company"    => $order->get_billing_company(),
                        "address_1"  => $order->get_billing_address_1(),
                        "address_2"  => $order->get_billing_address_2(),
                        "city"       => $order->get_billing_city(),
                        "state"      => $order->get_billing_state(),
                        "postcode"   => $order->get_billing_postcode(),
                        "country"    => $order->get_billing_country(),
                        "phone"      => $order->get_billing_phone(),
                        "email"      => $order->get_billing_email(),
                    ),
                    "shipping"          => array(
                        "first_name" => $order->get_shipping_first_name(),
                        "last_name"  => $order->get_shipping_last_name(),
                        "company"    => $order->get_shipping_company(),
                        "address_1"  => $order->get_shipping_address_1(),
                        "address_2"  => $order->get_shipping_address_2(),
                        "city"       => $order->get_shipping_city(),
                        "state"      => $order->get_shipping_state(),
                        "postcode"   => $order->get_shipping_postcode(),
                        "country"    => $order->get_shipping_country(),
                        "email"      => $order->get_billing_email(),
                        "phone"      => $order->get_billing_phone(),
                    ),
                    "number"            => $order->get_order_number() . '-' . $shipment->get_id(),
                    'line_items'        => $line_items_array,
                    'shipping_lines'    => $response_order['shipping_lines'],
                    'date_created_gmt'  => $response_order['date_created_gmt'],
                    'date_modified_gmt' => $response_order['date_modified_gmt'],
                    'date_completed_gmt'=> $response_order['date_completed_gmt'],
                    'date_paid_gmt'     => $response_order['date_paid_gmt'],
                );

            }
        }

        return $new_repsonse_data;
    }
}

//     /**
//      * Function to get all orders from WooCommerce Germanized and return them to DHL express.
//      * Get all orders that are in the status "ready to ship" and return them to DHL express.
//      */
//     public function getOrders( $request )
//     {
//         // Set default values
//         $is_single_order_request = false;
//         $result = array();
//         $orders = array();

//         // Check if request is for a single order.
//         $order_id = $request->get_param( 'id' );

//         // If the request contains a order id, get only that order.
//         if ( ! empty( $order_id ) ) {
//             $is_single_order_request = true;
//             $order = wc_get_order( $order_id );
//             $orders[] = $order;
//         } else {
//             $orders = wc_get_orders(array(
//                 'status' => 'ready-to-ship',
//                 'limit' => -1,
//             ));
//         }

//         // If no orders are found, return 404.
//         if ( empty( $orders ) ) {
//             return new \WP_Error( 'empty_result', 'No orders found', array( 'status' => 404 ) );
//         }

//         // debugging 30649; //29828; //30649
//         // loop through orders and remove all but one with id 30649
//         foreach ($orders as $key => $order) {
//             if ( $order->get_id() != 30847 ) {
//                 unset( $orders[$key] );
//             }
//         }

//         // Loop through all orders and get the order data.
//         foreach ($orders as $order) {

//             // Get WC GZ shipment object.
//             $order_shipment = new Order( $order );

//             $shipments = $order_shipment->get_shipments();

//             // Loop through all shipments and get the shipment data.
//             foreach ($shipments as $shipment) {

//                 // Check if shipment is in processing status.
//                 if ( ! $shipment->has_status( 'processing' ) ) {
//                     continue;
//                 }

//                 // Check if it is a DHL express shipment.
//                 if ( $shipment->get_shipping_provider() != 'dhlexpress' ) {
//                     continue;
//                 }

//                 // Get the line items from the shipment.
//                 $line_items = $shipment->get_items();
//                 $line_items_array = array();

//                 // Loop through all line items and get the line item data.
//                 foreach ($line_items as $line_item) {
//                     $line_items_array[] = array(
//                         'id'                => $line_item->get_id(),
//                         'sku'               => $line_item->get_sku(),
//                         'name'              => $line_item->get_sku(),
//                         'parent_name'       => $line_item->get_sku(),
//                         'quantity'          => $line_item->get_quantity(),
//                         'total'             => 0,
//                         'total_tax'         => 0,
//                         'subtotal'          => 0,
//                         'subtotal_tax'      => 0,
//                         'price'             => 500,
//                     );
//                 }

//                 $result[] = array(
//                     'id'                => $shipment->get_id(),
//                     'status'            => $order->get_status(),
//                     'currency'          => $order->get_currency(),
//                     "date_created"      => $order->get_date_created()->date('Y-m-d\TH:i:s'),
//                     "date_modified"     => $order->get_date_modified()->date('Y-m-d\TH:i:s'),
//                     "number"            => $order->get_order_number() . '-' . $shipment->get_id(),
//                     "shipping"          => array(
//                         "first_name" => $order->get_shipping_first_name(),
//                         "last_name"  => $order->get_shipping_last_name(),
//                         "company"    => $order->get_shipping_company(),
//                         "address_1"  => $order->get_shipping_address_1(),
//                         "address_2"  => $order->get_shipping_address_2(),
//                         "city"       => $order->get_shipping_city(),
//                         "state"      => $order->get_shipping_state(),
//                         "postcode"   => $order->get_shipping_postcode(),
//                         "country"    => $order->get_shipping_country(),
//                         "phone"      => $order->get_billing_phone(),
//                         "email"      => $order->get_billing_email(),
//                     ),
//                     'billing'        => array(
//                         "first_name" => $order->get_billing_first_name(),
//                         "last_name"  => $order->get_billing_last_name(),
//                         "company"    => $order->get_billing_company(),
//                         "address_1"  => $order->get_billing_address_1(),
//                         "address_2"  => $order->get_billing_address_2(),
//                         "city"       => $order->get_billing_city(),
//                         "state"      => $order->get_billing_state(),
//                         "postcode"   => $order->get_billing_postcode(),
//                         "country"    => $order->get_billing_country(),
//                         "phone"      => $order->get_billing_phone(),
//                         "email"      => $order->get_billing_email(),
//                     ),
//                     'shipping-lines'    => array(
//                         array(
//                             'id'            => $shipment->get_id(),
//                             'method_title'  => 'Express Versand innerhalb Deutschland',
//                             'method_id'     => 1,
//                             'total'         => 0,
//                             'total_tax'     => 0,
//                         ),
//                     ),
//                     'line-items'        => $line_items_array,
//                 );
//             }
//         }

//         // if result is empty return 404
//         if ( empty( $result ) ) {
//             return new \WP_Error( 'empty_result', 'No order shipments found', array( 'status' => 404 ) );
//         }

//         // Return the order data to DHL express.
//         if ( $is_single_order_request ) {
//             $result = $result[0];
//         }

//         return $result;
//     }

//     /**
//      * Function to get a single order from WooCommerce Germanized and return it to DHL express.
//      * Get a single order that is in the status "ready to ship" and return it to DHL express.
//      */
//     public function getOrder( $request )
//     {
//         return $this->getOrders( $request );
//     }

//     public function updateOrder( $order_id )
//     {
//         return array(
//             'id' => $order_id,
//             'jmd_msg' => 'Working!! Update!!',
//         );
//     }
// }



    /**
     * Function to intercept calls from DHL express. From DHL the express integration 
     * comes a call to the default WC rest api to import orders that need to be shipped.
     * The integration only gets orders. We will send it shipments that look like orders 
     * to process. These shipment objects we get from WooCommerce Germanized.
     */
    // public function InterceptDefaultRestApi()
    // {
    //     register_rest_route('wc/v3', 'orders', array(
    //         'methods' => 'GET',
    //         'callback' => array($this, 'getOrders'),
    //     ));

    //     register_rest_route('wc/v3', 'orders/(?P<id>\d+)', array(
    //         'methods' => 'GET',
    //         'callback' => array($this, 'getOrder'),
    //     ));

    //     register_rest_route('wc/v3', 'orders/(?P<id>\d+)', array(
    //         'methods' => 'PUT',
    //         'callback' => array($this, 'updateOrder'),
    //     ));
    // }