<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Inc\Base\BaseController;

class LanguageControl extends BaseController
{
    public function register()
    {
        // Loads text domain. It is required that admin pages are created via same action hook in order to translate admin menu items      
        add_action( 'plugins_loaded', array( $this, 'loadTabTextDomain' ) );
    }

    public function loadTabTextDomain()
    {
        load_plugin_textdomain( 'tabticketbroker', false, dirname( plugin_basename( __FILE__ ), 2 ) .'/languages/' );
    }

    /**
     * From a given language get the available locale and language that can be used by this plugin. 
     * At the moment it will only be German(de_DE_formal) & English(en_US)[default]
     * @return array locale & language
     */
    public function getLocaleFromLanguage( $language )
    {
        switch ( strtolower( $language ) ) {
            case 'english':
                $locale = 'en_US';
                $language = 'english';
                break;

            case 'german':
                $locale = 'de_DE_formal';
                $language = 'german';
                break;
                    
            default:
                $locale = 'en_US';
                $language = 'english';
                break;
        }

        $locale_array = array(
            'locale'    => $locale,
            'language'  => $language
        );

        return $locale_array;
    }

    public static function getLanguageFromLocale( $locale )
    {
        switch ( strtolower( $locale ) ) {
            case 'en_gb':
                $language = 'English';
                break;
            case 'de_de_formal':
                $language = 'German';
                break;
            default:
                $language = 'Unknown';
                break;
        }
        return $language;
    }
}
