<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Classes;

use Inc\Base\BaseController;
use MyThemeShop\Helpers\Arr;

class TabSupplierEditor extends BaseController

{
    public $reservation = null;
    public $reservations = null;

    public function register()
    {
        $this->init_hooks();
        
        $this->init_filters();
    }

    /**
     * Initiate plugin hooks in Wordpress
     */
    public function init_hooks()
    {
        add_action('save_post_' .$this->tab_supplier_cpt_title, array( $this, 'saveSupplier' ), 10, 1 );

        // Add metaboxes
        add_action('add_meta_boxes', array($this, 'addMetaboxes'));
    }

    /**
     * Initiate plugin filters in Wordpress
     */
    public function init_filters()
    {
        add_filter('enter_title_here', array($this, 'changeEnterTitleHere') );

        add_filter('gettext', array($this, 'changePublishButton'), 10, 2);
    }

    /**
     * Function used when the admin is initiated
     */
    function addMetaboxes()
    {
        global $post;

        // Supplier information
        add_meta_box('tab_supplier_post_information',  __('Supplier information', 'tabticketbroker'), array($this, 'supplierInformationMetaBox'), $this->tab_supplier_cpt_title, 'normal', 'low' );

        // Linked reservations
        if( $post->post_status != "auto-draft" ) // Do not show reservations metabox during adding of new
            add_meta_box('tab_supplier_reservations',  __('Reservations', 'tabticketbroker'), array($this, 'supplierReservationMetabox'), $this->tab_supplier_cpt_title, 'normal', 'low' );
    }

    function changeEnterTitleHere( $title )
    {
        $screen = get_current_screen();

        if ($this->tab_supplier_cpt_title == $screen->post_type) {
            // $title = __('Enter supplier name here', 'tabticketbroker' );
            $title = '<style>#titlediv{display:none;}</style>';
        }

        return $title;
    }

    function changePublishButton( $translation, $text )
    {
        if ( 'Publish' == $text ) return __( 'Save', 'tabticketbroker' );
        return $translation;
    }

    /**
     * Show information metabox in the editor
     */
    function supplierInformationMetaBox( $post )
    {
        $supplier = new TabSupplier($post->ID);

        ?>
        <div class="information-form-fields">
            <div class="form-inline">
                <div class="form-field form-group mr-2">
                    <label for="tab_supplier_name_field"><?php _e( 'Contact Name', 'tabticketbroker' ); ?></label>
                    <input
                    type="text"
                    id="tab_supplier_name_field"
                    name="tab_supplier_name"
                    value="<?php _e( $supplier->getName() ); ?>"
                    placeholder="<?php _e( 'Please enter the supplier\'s name', 'tabticketbroker' ); ?>"
                    required
                    autofocus
                    >
                </div>
                <div class="form-field form-group mr-2">
                    <label for="tab_supplier_email_field"><?php _e( 'Email', 'tabticketbroker' ); ?></label>
                    <input
                    type="text"
                    id="tab_supplier_email_field"
                    name="tab_supplier_email"
                    value="<?php _e( $supplier->getEmail() ); ?>"
                    placeholder="<?php _e( 'Email', 'tabticketbroker' ); ?>"
                    >
                </div>
            </div>
            <div class="form-inline">
                <div class="form-field form-group mr-2">
                    <label for="tab_supplier_company_name_field"><?php _e( 'Company name', 'tabticketbroker' ); ?></label>
                    <input
                    type="text"
                    id="tab_supplier_company_name_field"
                    name="tab_supplier_company_name"
                    value="<?php _e( $supplier->getCompanyName() ); ?>"
                    placeholder="<?php _e( 'Please enter the supplier company name', 'tabticketbroker' ); ?>"
                    >
                </div>
                <div class="form-field form-group mr-2">
                    <label for="tab_supplier_phone_field"><?php _e( 'Phone', 'tabticketbroker' ); ?></label>
                    <input
                    type="text"
                    id="tab_supplier_phone_field"
                    name="tab_supplier_phone"
                    value="<?php _e( $supplier->getPhone() ); ?>"
                    placeholder="<?php _e( 'Phone number', 'tabticketbroker' ); ?>"
                    >
                </div>
            </div>
            <div class="form-field">
                <label for="tab_supplier_notes_field"><?php _e( 'Notes', 'tabticketbroker' ); ?></label>
                <br>
                <textarea 
                id="tab_supplier_notes_field" 
                name="tab_supplier_notes"
                value="<?php echo $supplier->getNotes(); ?>" 
                placeholder="<?php _e( 'Notes', 'tabticketbroker' ); ?>"
                ></textarea>
            </div>
        </div>
        <?php
    }

    /**
     * Generate report that shows reservations for the supplier
     */
    function supplierReservationMetabox( $post ) 
    {
        $gen_filter = new GenericFilters_v2();
        
        $gen_filter->renderDimensionFilters( array( 'include-reservation-status' => true ) );
        
        $gen_filter->printJS();

        ?>
        <div id="report_table_wrap">
        <?php $this->showReservationReport( $post );?>
        </div>
        <?php
    }

    function showReservationReport( $post )
    {
        $supplier = new TabSupplier( $post->ID );
        
        $report_title = 'Supplier reservations';

        $report_columns = array(
            'reservation_id'    => __( 'Reservation', 'tabticketbroker' ),
            'sku'               => __( 'SKU', 'tabticketbroker' ),
            'product_title'     => __( 'Product', 'tabticketbroker' ),
            'display_date'      => __( 'Date', 'tabticketbroker' ),
            'timeslot'          => __( 'Timeslot', 'tabticketbroker' ),
            'start_time'        => __( 'Start Time', 'tabticketbroker' ),
            'date'              => __( 'DateYMD', 'tabticketbroker' ),
            'pax'               => __( 'Pax', 'tabticketbroker' ),
            'pax_assigned'      => __( 'Pax Assigned', 'tabticketbroker' ),
            'area'              => __( 'Area', 'tabticketbroker' ),
            'res_status'        => __( 'Reservation Status', 'tabticketbroker' ),
            'combined_docs'     => __( 'Documents Status', 'tabticketbroker' ),
        );
        
        $report_columns_hidden = array( 'date' );
        
        $report_columns_sortable = array(
            'reservation_id'    => array( 'reservation_id', 'asc' ),
            'sku'               => array( 'sku', 'asc' ),
            'product_title'     => array( 'product_title', 'asc' ),
            'display_date'      => array( 'date', 'asc' ), 
            'timeslot'          => array( 'timeslot', 'asc' ),
            'start_time'        => array( 'start_time', 'asc' ),
            'pax'               => array( 'pax', 'asc' ),
            'pax_assigned'      => array( 'pax_assigned', 'asc' ),
            'area'              => array( 'area', 'asc' ),  
            'res_status'        => array( 'status', 'asc' ),
            'combined_docs'     => array( 'combined_docs', 'asc' ),
        );

        // Check if reservation is set
        if ( ! isset( $this->reservations ) || ! isset( $this->reservations ) || ! is_array( $this->reservations ) ) {
            // Instantiate the class
            $this->reservation = new TabReservation();

            // If event years were selected apply them to the args for setReservations
            $args = array();
            if( isset( $_GET['ttb_event_year'] ) ) {
                $args['ttb_event_year'] = $_GET['ttb_event_year'];
            } else { // Otherwise set the default event year
                $args['ttb_event_year'] = TTB_EVENT_YEAR;
            }

            // Set reservations
            $this->reservation->setReservations( $args );
        }
        
        $supplier_reservations = $supplier->getLinkedReservations( $this->reservation->getReservations() );

        // Import reservation data into array for the report
        $report_data = array();
        foreach( $supplier_reservations as $reservation )
        {
            $check_yes = '<span data-title="{title}" class="dashicons dashicons-yes immediate-title"></span>';
            $check_no = '<span data-title="{title}" class="dashicons dashicons-no immediate-title"></span>';
            $check_fields = array(
                'vouchers_received' => array( __( 'Vouchers Received' ,'tabticketbroker' ),  $reservation->getVouchersReceived() ? 1 : 0),
                'letter_received'   => array( __( 'Letter Received' ,'tabticketbroker' ),    $reservation->getLetterReceived() ? 1 : 0),
                'letter_ready'      => array( __( 'Letter Ready' ,'tabticketbroker' ),       $reservation->getLetterReady() ? 1 : 0),
                'docs_complete'     => array( __( 'Documents Complete' ,'tabticketbroker' ), $reservation->getDocsComplete() ? 1 : 0),

            );

            $combined_docs = '';
            foreach( $check_fields as $check_field ) {
                if ( $check_field[1] == 1 ) {
                    $combined_docs .= str_replace( '{title}', $check_field[0], $check_yes );
                } else {
                    $combined_docs .= str_replace( '{title}', $check_field[0], $check_no );
                }
            }

            $report_data[] = array(
                'reservation_id'    => '<a href="' . get_edit_post_link( $reservation->getId() ) . '">RS' . $reservation->getId() . '</a>',
                'sku'               => $reservation->getSku(),
                'product_title'     => $reservation->getProductTitle(),
                'display_date'      => $reservation->getResDisplayDate(),
                'timeslot'          => $reservation->getTimeslotDisplay(),
                'start_time'        => $reservation->getStartTime(),
                'date'              => $reservation->getDate(),
                'pax'               => $reservation->getPax(),
                'pax_assigned'      => $reservation->getPaxAssigned(),
                'area'              => $reservation->getAreaDisplay(),
                'res_status'        => '<div class="hidden status-slug">' . $reservation->getStatus() . '</div><div class="res-status-' . $reservation->getStatus() . '">' . $reservation->getStatusDisplay() . '</div>',
                'combined_docs'     => $combined_docs,
                'ready_to_ship'     => $reservation->getReadyToShip() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>',
            );
        }

        $per_page = 50;
        
        $list_table = new ListTable;

        $list_table->setupTable( 
            $report_columns, 
            $report_columns_hidden, 
            $report_columns_sortable, 
            $report_data, 
            $per_page
        );
        
        $list_table->prepare_items();
        
        // Show button to add new reservation for this supplier
        echo '<a id="ttb_supplier_res_add_new" href="' . get_admin_url() . 'post-new.php?post_type=tab_reservation&supplier_id=' . $post->ID . '&nav_back_to_supplier=' . $post->ID . '" class="button button-primary">' . __( 'Add new reservation', 'tabticketbroker' ) . '</a>';
        
        // Add button to show reservations for all years
        if ( ! isset( $_GET['ttb_event_year'] ) ) {
            echo '<a id="ttb_supplier_res_yrs" href="' . get_admin_url() . 'post.php?post=' . $post->ID . '&action=edit&ttb_event_year=" class="button button-primary">' . __( 'Show all years', 'tabticketbroker' ) . '</a>';
        } else {
            echo '<a id="ttb_supplier_res_yrs" href="' . get_admin_url() . 'post.php?post=' . $post->ID . '&action=edit" class="button button-primary">' . __( 'Show current year', 'tabticketbroker' ) . '</a>';
        }


        // Hide table nav, it includes a nonce which breaks the updating on the page.
        // AND it is not required 
        $args = array(
            'hide_table_nav' => true,
        );
        
        // Little bit of a hack to keep the top spacing but hide the table nav
        echo '<div class="tablenav top"></div>';

        // Show the table
        $list_table->displayWithoutNonce( $args );
    }

    /**
     * Hook called when a post is saved
     */
    function saveSupplier ( $post_id )
    {
        // If this is just a revision, don't continue
        if ( wp_is_post_revision( $post_id ) || in_array( get_post($post_id)->post_status, array( 'auto-draft', 'trash') ) ) return;

        // Show flash notice if supplier with the same name already exists.
        $suppliers = TabSupplier::getSuppliers();
        $name_check_done = false;
        $email_check_done = ( isset( $_POST['tab_supplier_email'] ) && $_POST['tab_supplier_email'] == '' ? true : false );
        foreach( $suppliers as $supplier ) {
            // If this is the same supplier, skip it.
            if ( $supplier->getId() == $post_id ) { continue; }
            
            // If both checks are already done, quit.
            if ( $name_check_done && $email_check_done ) break;
            
            // If name check is not done, check if name is the same.
            if ( isset( $_POST['tab_supplier_name'] ) && $_POST['tab_supplier_name'] == $supplier->getName() && ! $name_check_done ) {
                $this->addFlashNotice( __( 'WARNING: A supplier with the same <b>name</b> already exists.', 'warning', true ) );
                $name_check_done = true;
            }

            // If email check is not done, check if email is the same.
            if ( isset( $_POST['tab_supplier_email'] ) && $_POST['tab_supplier_email'] == $supplier->getEmail() && ! $email_check_done ) {
                $this->addFlashNotice( __( 'WARNING: A supplier with the same <b>email</b> already exists.', 'warning', true ) );
                $email_check_done = true;
            }
        }

        // Update datas
        $supplier = new TabSupplier( $post_id );

        if ( isset( $_POST['tab_supplier_name'] ) ) {
            $supplier->setName( sanitize_text_field( $_POST['tab_supplier_name'] ) );
        }
        if ( isset( $_POST['tab_supplier_company_name'] ) ) {
            $supplier->setCompanyName( sanitize_text_field( $_POST['tab_supplier_company_name'] ) );
        }
        if ( isset( $_POST['tab_supplier_email'] ) ) {
            $supplier->setEmail( sanitize_text_field( $_POST['tab_supplier_email'] ) );
        }
        if ( isset( $_POST['tab_supplier_phone'] ) ) {
            $supplier->setPhone( sanitize_text_field( $_POST['tab_supplier_phone'] ) );
        }
        if ( isset( $_POST['tab_supplier_notes'] ) ) {
            $supplier->setNotes( sanitize_text_field( $_POST['tab_supplier_notes'] ) );
        }
        
        $supplier->saveSupplier();
    }
}

