<?php
/**
 * @package tabticketbroker
 * Based on example from Paulund here: https://gist.github.com/paulund/7659452
 */
namespace Inc\Classes;

use WP_List_Table; // Import the WP_List_Table class

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class ListTable extends WP_List_Table
{

    public $per_page = 20;

    public $columns = array();

    public $hidden = array();

    public $sortable = array();

    public $table_data = array();

    public function setupTable( array $columns, array $hidden, array $sortable, array $table_data, int $per_page = 20 )
    {
        $this->setColumns( $columns );

        $this->hidden       = $hidden;

        $this->sortable     = $sortable;

        $this->table_data   = $table_data;

        $this->per_page = $per_page;
    }

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns    = $this->columns;

        $hidden     = $this->hidden;

        $sortable   = $this->sortable;

        $table_data = $this->table_data;

        usort( $table_data, array( $this, 'sort_data' ) );

        $per_page = $this->per_page;

        $current_page = $this->get_pagenum();

        $total_items = count($table_data);

        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page
        ) );

        $table_data = array_slice( $table_data,( ( $current_page - 1 ) * $per_page ), $per_page );

        $this->_column_headers = array( $columns, $hidden, $sortable );

        $this->items = $table_data;
    }


    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        return $this->columns;
    }


    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function setColumns( $columns )
    {
        $this->columns = $columns;
    }

    // /**
    //  * Define which columns are hidden
    //  *
    //  * @return Array
    //  */
    // public function get_hidden_columns()
    // {
    //     return array();
    // }

    // /**
    //  * Define the sortable columns
    //  *
    //  * @return Array
    //  */
    // public function get_sortable_columns()
    // {
    //     // return $this->sortable;
    //     return array( 'title' => array( 'title', false ) );
    // }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        // return $item[ $column_name ];
        return ( isset( $item[ $column_name ] ) ? $item[ $column_name ] : 'Column error encountered: `'. $column_name .'`' );
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Order by the value set in the $_GET
        // Otherwise attempt to order by the date
        // Otherwise order by the first column
        if ( isset( $_GET['orderby'] ) ) {
            $orderby = $_GET['orderby'];
            
            // otherwise order by the first key in the array            
            if ( ! isset( $a[$orderby] ) ) {
                if ( isset( $a['date'] ) ) {
                    $orderby = 'date';
                } else {
                    $orderby = array_keys( $a )[0];
                }
            }
        } 
        else {
            if ( isset( $a['date'] ) ) {
                $orderby = 'date';
            } else {
                $orderby = array_keys( $a )[0];
            }
        }

        $order = ( ! empty( $_GET['order'] ) ? $_GET['order'] : 'asc' );
        
        if ( is_int( $a[$orderby] ) ) {
            $result = ( ( $a[$orderby] < $b[$orderby] ) ? -1 : ( ( $a[$orderby] > $b[$orderby] ) ? 1 : 0 ) );
        }
        else {
            $result = strcmp( strtolower( $a[$orderby] ), strtolower( $b[$orderby] ) );
        }

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }
    
    /** 
     * This overrides the parent method "display" which was causing an issue when saving because 
     * it creates a nonce in the display_tablenav function when called with variable "top" here:
     * /wp-content/plugins/wp-mail-logging/src/inc/class-wp-list-table.php:815
     */ 
    public function displayWithoutNonce( $args = array() ) {
		
        if ( ! isset( $args['hide_table_nav'] ) ) {
            $this->display_tablenav( 'top' );  
        }
		?>
        <table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
            <thead>
            <tr>
                <?php $this->print_column_headers(); ?>
            </tr>
            </thead>

            <tbody id="the-list">
                <?php $this->display_rows_or_placeholder(); ?>
            </tbody>

            <tfoot>
            <tr>
                <?php 
                if ( ! isset( $args['hide_footer'] ) ) {
                    $this->print_column_headers( false );
                }
                ?>
            </tr>
            </tfoot>

        </table>
        <?php
        if ( ! isset( $args['hide_bottom_nav'] ) ) {
            $this->display_tablenav( 'bottom' );
        }
    }

    public function single_row( $item ) {
		echo '<tr id="item_' . $item['id'] . '">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}

    public function column_cb( $item ) {
        ?>
            <input id="cb-select-<?php echo $item['id']; ?>" type="checkbox" name="item[]" value="<?php echo $item['id']; ?>">
        <?php
    }

    public function get_bulk_actions()
    {
        // Check that we are busy with the price report table
        // If bulk actions are injected onto other pages it overwrites the action variable and caused issues with 
        // saving custom post types like in the Supplier page where reservations are loaded in a meta box
        if ( isset( $_GET['page'] ) && $_GET['page'] == 'tab_admin_reports' && isset( $_GET['report_slug'] ) && $_GET['report_slug'] == 'price' ) {
            $actions = array(
                'enable'     => 'Enable prices',
                'disable'    => 'Disable prices'
            );
            return $actions;
        }

        return array();
    }
}
?>