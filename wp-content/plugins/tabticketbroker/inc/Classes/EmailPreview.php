<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\EmailController;
use Inc\LanguageControl;
use Inc\Tools\OrderTools;
use Inc\Base\BaseController;

Class EmailPreview extends BaseController
{
    public $orders;

    public $email_class_list;

    public $email_order_actions;

    public $locale_array = array();

    public $email_controller;

    public function initialize()
    {
        // Initialize EmailContoller if class does not exist yet
        if ( ! class_exists( 'EmailController' ) ) {
            $this->email_controller = new EmailController;
        }
        
        // Get the different email types created for the order actions, currently there are no 
        // other emails (other than the order actions) and all emails will be sent from an order
        // page
        $this->email_class_list = $this->email_controller->email_class_list;
        
        // Used in the template to give the user the same options as in the order actions
        $this->email_order_actions = $this->email_controller->customizeWcOrderActions( array() );
        
        // Unset the admin notification email type as it is not needed for the preview
        if ( isset( $this->email_order_actions['send_order_details_admin'] ) ) {
            unset( $this->email_order_actions['send_order_details_admin'] );
        }

        // Get available orders to preview emails for
        $assignment_manager = new AssignmentManager();
        $this->orders = $assignment_manager->getEventOrdersLite();

        // Get all reservations
        $reservation = new TabReservation();
        $this->reservations = $reservation->getReservations();

        //Sort reservations by code 
        usort( $this->reservations, function( $a, $b ) {
            return strcmp( $a->getCode(), $b->getCode() );
        } );
    }
    
    /** 
     * Here we use the different classes to get the content for the preview.
     * @return string $email_preview The HTML to render from the email class that would be sent in a mail.
     */
    public function emailPreviewRender()
    {
        $this->initialize();

        $order_reservation_ids = array();

        // The email to preview selected by the user.
        $email_type_selected = isset ($_GET['email_type'] ) ? $_GET['email_type'] : '';
        $order_id_selected = isset( $_GET['order_id'] ) ? $_GET['order_id'] : '';
        $reservation_id_selected = isset( $_GET['reservation_id'] ) ? $_GET['reservation_id'] : -1;

        if ( ! isset( $_GET['email_type'] ) && ! isset( $_GET['order_id'] ) ) {
            $email_preview = '<p>'. __( 'Please select an email and an order', 'tabticketbroker' ) .'<p>';
        }
        elseif ( isset( $_GET['email_type'] ) && $_GET['email_type'] == 'email_customer_shared_reservation' && isset( $_GET['order_id'] ) && !isset( $_GET['reservation_id'] ) ) {
            // Same as below
            // Get the order reservation ids for display in the preview Reservation selections
            $order_tools = new OrderTools;
            $order_reservations = $order_tools->getOrderReservations( $order_id_selected );
            foreach ( $order_reservations as $reservation_array ) {
                $reservation = $reservation_array['reservation'];
                $order_reservation_ids[] = $reservation->getId();
            }
            
            $email_preview = '<p>'. __( 'Please select a reservation', 'tabticketbroker' ) .'<p>';
        }
        else 
        {
            // Get the order reservation ids for display in the preview Reservation selections
            $order_tools = new OrderTools;
            $order_reservations = $order_tools->getOrderReservations( $order_id_selected );
            foreach ( $order_reservations as $reservation_array ) {
                $reservation = $reservation_array['reservation'];
                $order_reservation_ids[] = $reservation->getId();
            }
            
            // If a reservation is selected, get the reservation from the id
            if ( $reservation_id_selected && $reservation_id_selected !== -1  ) {
                $reservation = new TabReservation( $reservation_id_selected );
                $args = array(
                    'reservation' => $reservation,
                );
            } else {
                $args = array();
            }
            
            if ( class_exists( $this->email_class_list[$email_type_selected] ) ) {

                // Set email class
                $email_class = new $this->email_class_list[$email_type_selected];
                
                // This is to ensure translations are available during email rendering
                // Templates such as order_details did not translate before woocommerce 
                // text domains were loaded here
                $this->email_controller->loadOrderTextDomains( $order_id_selected );
                
                // Set the order in that class
                $email_class->setOrder( $order_id_selected, $args );

                // This variable is echoed in the template
                $email_preview = $email_class->get_content_html();

                $email_preview = $email_class->style_inline( $email_preview );
            }
            else {
                $email_preview = 'Error: Class not found: {'. $email_type_selected. '}' ;
            }
        } 
        
        // Used in the template
        $language = new LanguageControl;
        
        require_once $this->plugin_path .'/templates/email_preview.php';
        
    }
}
