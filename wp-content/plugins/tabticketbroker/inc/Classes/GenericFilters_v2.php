<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

// Create class generic filters
class GenericFilters_v2 extends BaseController {
    /**
     * Show dimensions filter
     */
    public function renderDimensionFilters(array $args = array()) {

        // Default args
        $defaults = array(
            'include-reservation-status' => false,
        );

        // Merge the args with the defaults
        $args = wp_parse_args($args, $defaults);

        // Get the dimension class & initialize it with the display date format
        $dimension = new Dimension();
        $dimension->setDisplayDateFormat('eeeeee.d.M');
        $dimension->initialize();

        // If the include-reservation-status is true, include the reservation status filter
        if ($args['include-reservation-status']) {
            // Get all the reservation statuses
            $res_statuses = $dimension->getReservationStatuses();
        }

        // Sort the codes
        ksort($dimension->sku_prods);
        ksort($dimension->sku_timeslots);
        ksort($dimension->sku_displaydates);        // Sort the dates by key

        // Get the sku from the post data if it exists
        $sku = isset($_GET['sku']) ? $_GET['sku'] : '';

        // Get the sku attributes from the dimension class with the getSkuAttributes method if the sku is set
        $sku_attributes = $sku == '' ? array() : $dimension->getSkuAttributes($sku);

        // Create a panel of divs in html from the product codes stored as the keys in the sku_prods array
?>
        <div id="sku_filter_wrap">

            <div class="panel">
                <div class="panel-body">
                    <?php
                    foreach ($dimension->sku_prods as $code => $name) {
                        // If the sku_attributes array is not empty and the sku_prod is in the array, add the active class to the div
                        $active = !empty($sku_attributes) && $code == $sku_attributes['prod'] ? ' active' : '';
                    ?>
                        <div class="button<?php echo $active; ?>" data-sku-key="sku_prod" onclick="filterBySkuAttribute(this,'sku_prod','<?php echo $code; ?>')"><?php echo $code; ?></div>
                    <?php
                    }
                    ?>
                    <div class="button spacer"></div>
                    <?php
                    foreach ($dimension->sku_timeslots as $code => $name) {
                        $active = !empty($sku_attributes) && $code == $sku_attributes['timeslot'] ? ' active' : '';
                        // capatilize the first letter of the name
                        $name = ucfirst($name);
                    ?>
                        <div class="button<?php echo $active; ?>" data-sku-key="sku_timeslot" onclick="filterBySkuAttribute(this,'sku_timeslot','<?php echo $code; ?>')"><?php echo $code . ':' . $name; ?></div>
                    <?php
                    }
                    ?>
                    <?php
                    if ($args['include-reservation-status']) {
                    ?>
                        <div class="button spacer"></div>
                        <?php
                        foreach ($res_statuses as $code => $name) {
                            $active = !empty($sku_attributes) && $code == $sku_attributes['res_status'] ? ' active' : '';
                        ?>
                            <div class="button<?php echo $active; ?>" data-sku-key="res_status" onclick="filterBySkuAttribute(this,'res_status','<?php echo $code; ?>')"><?php echo $name; ?></div>
                    <?php
                        }
                    }
                    ?>
                    <div class="button spacer"></div>
                    <div id="btn_filter_enabled" class="button" data-sku-key="sku_status" onclick="filterBySkuAttribute(this,'sku_status','1')"><?php _e('Enabled', 'tabticketbroker'); ?></div>
                    <div id="btn_filter_disabled" class="button" data-sku-key="sku_status" onclick="filterBySkuAttribute(this,'sku_status','0')"><?php _e('Disabled', 'tabticketbroker'); ?></div>
                    <div id="btn_filter_reset" class="button" onclick="resetFilters()"><?php _e('Reset', 'tabticketbroker'); ?></div>
                </div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <?php
                    foreach ($dimension->sku_displaydates as $code => $name) {
                        $active = !empty($sku_attributes) && $code == $sku_attributes['day'] ? ' active' : '';
                        // If date is on Sat or Sun, add class weekend
                        $weekend = (date('N', strtotime($dimension->sku_dates[$code])) >= 6) ? ' weekend' : '';
                    ?>
                        <div class="button<?php echo $active . $weekend; ?>" data-sku-key="sku_date" onclick="filterBySkuAttribute(this,'sku_date','<?php echo $code; ?>')"><?php echo $code . ':' . $name; ?></div>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <?php
                    foreach ($dimension->sku_pax as $code => $name) {
                        $active = !empty($sku_attributes) && $code == $sku_attributes['pax'] ? ' active' : '';
                        if ($code == 40) {
                            // Create a range e.g. 40+
                            $name = $code . '+';
                        }
                        // 
                        elseif ($code > 40) {
                            // Skip the rest of the loop
                            continue;
                        }

                    ?>
                        <div class="button<?php echo $active; ?>" data-sku-key="sku_pax" onclick="filterBySkuAttribute(this,'sku_pax','<?php echo $code; ?>')"><?php echo $name; ?></div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>

    <?php
    }


    public function printJS() {
        ob_start();
    ?>
        <script>
            var $ = jQuery;
            var variation_el = null;

            // Set the default filter values
            var sku_filter = {
                'sku_prod': [],
                'sku_timeslot': [],
                'sku_date': [],
                'sku_pax': [],
                'sku_status': [],
                'res_status': [],
            };

            function resetFilters() {
                // Set the loader display = block (you are required to add the loader to the page in the template file)
                jQuery('#loader_dim_page').show();

                // Remove the active class from all the buttons
                jQuery('#sku_filter_wrap div.button').removeClass('active');

                // Reset the sku_filter array
                sku_filter = {
                    'sku_prod': [],
                    'sku_timeslot': [],
                    'sku_date': [],
                    'sku_pax': [],
                    'sku_status': [],
                    'res_status': [],
                };

                // Get the the first table in the div with the id = report_table_wrap
                var table = jQuery('#report_table_wrap table').first();

                // Get all the rows in the table
                var rows = jQuery(table).find('tbody tr');

                // Loop through the rows
                for (var i = 0; i < rows.length; i++) {
                    // Show all the rows
                    jQuery(rows[i]).show();
                }

                // Find the table #report_table_wrap and show it, hide the no results message   
                var table_wrap = jQuery('#report_table_wrap').first();
                jQuery(table_wrap).show();
                jQuery('#no_results').hide();

                // run the set visible rows function
                setVisibleRows();

                // Hide the loader
                jQuery('#loader_dim_page').hide();
            }

            // Create a function to filter the table by sku_prod
            function filterBySkuAttribute(el, sku_key, sku_value) {
                // Set the loader display = block (you are required to add the loader to the page in the template file)
                jQuery('#loader_dim_page').show();

                // Use the key to get the dimension filter we are dealing with
                var filter = sku_filter[sku_key];

                // Before we continue, check if we are dealing with the status filter
                // If so, we want to correctly enable and disable the buttons because 
                // you will never have both enabled and disabled buttons active at the same time
                if (sku_key == 'sku_status') {
                    // Get the enabled and disabled buttons
                    var enabled = jQuery('#btn_filter_enabled');
                    var disabled = jQuery('#btn_filter_disabled');

                    var active_filter = sku_filter[sku_key];
                    var clicked_filter = parseInt(sku_value);
                    var resulting_filter = [];

                    // If active_filter = clicked_filter, reset the filter by setting the resulting filter to null
                    if (active_filter[0] === clicked_filter) {
                        resulting_filter = [];
                    } else {
                        // push the clicked filter to the resulting filter
                        resulting_filter.push(clicked_filter);
                    }

                    // Set the filter to the resulting filter
                    filter = resulting_filter;

                    // Update the buttons
                    if (resulting_filter[0] === 1) { // Enabled
                        enabled.addClass('active');
                        disabled.removeClass('active');
                    } else if (resulting_filter[0] === 0) { // Disabled
                        disabled.addClass('active');
                        enabled.removeClass('active');
                    } else { // Reset
                        enabled.removeClass('active');
                        disabled.removeClass('active');
                    }

                } else {
                    // This is how we handle all other buttons that has to do with the SKU column 
                    // If the div is already active, remove the filter
                    if (jQuery(el).hasClass('active')) {
                        // Remove the active class from the div
                        jQuery(el).removeClass('active');

                        // If the sku_key is sku_pax, convert the sku_value to an integer
                        if (sku_key == 'sku_pax' || sku_key == 'sku_status') {
                            sku_value = parseInt(sku_value);
                        }

                        // Remove the filter value from the sku_filter array
                        filter.splice(filter.indexOf(sku_value), 1);

                    } else { // If the div is not active, add the filter
                        // Add the active class to the clicked div
                        jQuery(el).addClass('active');

                        // If the sku_key is sku_pax, convert the sku_value to an integer
                        if (sku_key == 'sku_pax' || sku_key == 'sku_status') {
                            sku_value = parseInt(sku_value);
                        }

                        // Add the filter value to the sku_filter array
                        filter.push(sku_value);
                    }
                }

                sku_filter[sku_key] = filter;

                // console.log("LOG::" + JSON.stringify(sku_filter));

                // Get the the first table in the div with the id = report_table_wrap
                var table = jQuery('#report_table_wrap table').first();

                // Get all the rows in the table
                var rows = jQuery(table).find('tbody tr');

                // Loop through the rows
                for (var i = 0; i < rows.length; i++) {

                    // If the sku_filter is empty, show all rows
                    if (sku_filter['sku_prod'].length == 0 &&
                        sku_filter['sku_timeslot'].length == 0 &&
                        sku_filter['sku_date'].length == 0 &&
                        sku_filter['sku_pax'].length == 0 &&
                        sku_filter['sku_status'].length == 0 &&
                        sku_filter['res_status'].length == 0
                    ) {
                        jQuery(rows[i]).show();
                        continue;
                    }

                    // Get the row
                    var row = rows[i];

                    // Get the sku_prod from the row, first 3 characters of the product code
                    var row_sku_prod = jQuery(row).find('td[data-colname="SKU"]').html().substring(0, 3);

                    var row_sku_timeslot = jQuery(row).find('td[data-colname="SKU"]').html().substring(7, 8);

                    // Get the sku_date from the row, 5th & 6th characters of the product code
                    var row_sku_date = jQuery(row).find('td[data-colname="SKU"]').html().substring(4, 6);

                    // Get the sku_pax from the row, 10 & 11th characters of the product code
                    var row_sku_pax = jQuery(row).find('td[data-colname="SKU"]').html().substring(9, 11);

                    // Test if row_sku_pax can be converted to an integer
                    row_sku_pax = parseInt(row_sku_pax);

                    // If the row_sku_pax is NaN, log it and clear sku_filter
                    if (isNaN(row_sku_pax)) {
                        console.log("FILETER ERROR:: row_sku_pax is not an integer");
                        sku_filter['sku_pax'] = [];
                    }

                    // Get the variant enabled status from the row. NOTE: This is a different column
                    var row_sku_status = jQuery(row).find('td[data-colname="Enabled"]').html();

                    // If the column does not exist, log it and clear sku_filter
                    if (sku_key == 'sku_status' && row_sku_status === undefined) {
                        console.log("FILETER ERROR:: row_sku_status column not found");
                        sku_filter['sku_status'] = [];
                    } else {
                        // If it exists, parse the row_sku_status to an integer
                        row_sku_status = parseInt(row_sku_status);
                        if (sku_key == 'sku_status') {
                            // If the sku_status is not an integer, log it and clear sku_filter
                            if (isNaN(row_sku_status)) {
                                console.log("FILETER ERROR:: row_sku_status is not an integer");
                                sku_filter['sku_status'] = [];
                            }
                        }
                    }

                    // Find Reservation Status (Different from sku_status which is enabled or disabled)
                    var row_res_status = jQuery(row).find('td[data-colname="Reservation Status"] div.status-slug').html();
                    
                    // Check if the sku_prod matches the filter, ignore if the filter is empty
                    if (sku_filter['sku_prod'].length > 0 && !sku_filter['sku_prod'].includes(row_sku_prod)) {
                        jQuery(row).hide();
                    } else if (sku_filter['sku_timeslot'].length > 0 && !sku_filter['sku_timeslot'].includes(row_sku_timeslot)) {
                        jQuery(row).hide();
                    } else if (sku_filter['sku_date'].length > 0 && !sku_filter['sku_date'].includes(row_sku_date)) {
                        jQuery(row).hide();
                    } else if (sku_filter['sku_pax'].length > 0 && !sku_filter['sku_pax'].includes(row_sku_pax)) {
                        jQuery(row).hide();
                    } else if (sku_filter['sku_pax'].length > 0 && sku_filter['sku_pax'].includes(40) && row_sku_pax >= 40) {
                        jQuery(row).show();
                    } else if (sku_filter['sku_status'].length > 0 && !sku_filter['sku_status'].includes(row_sku_status)) {
                        jQuery(row).hide();
                    } else if ( sku_filter['res_status'] != '' && !sku_filter['res_status'].includes(row_res_status) ) {
                        jQuery(row).hide();
                    } else {
                        jQuery(row).show();
                    }
                }

                // Find the table #report_table_wrap and show it, hide the no results message   
                var table_wrap = jQuery('#report_table_wrap').first();
                jQuery(table_wrap).show();
                jQuery('#no_results').hide();

                // run the set visible rows function
                setVisibleRows();

                // Hide the loader
                jQuery('#loader_dim_page').hide();
            }

            // Function to set the visible rows
            function setVisibleRows() {
                // Get the table
                var table = jQuery('#report_table_wrap table').first();

                // Get the no of visible rows in the rows array
                var visible_rows = jQuery(table).find('tbody tr:visible').length;

                // Set the no of items found (div with classes tablenav-pages one-page and span with class displaying-num)
                jQuery('#report_table_wrap .tablenav-pages .displaying-num').html(visible_rows + ' <?php _e('items', 'tabticketbroker'); ?>');

                // If all the rows are hidden, show the no results message
                if (visible_rows == 0) {
                    jQuery('#no_results').show();
                    jQuery('#report_table_wrap').hide();

                }
            }

            // On load, create a no results table and hide it
            jQuery(document).ready(function() {
                // Get the table
                var table = jQuery('#report_table_wrap table').first();

                // Create the no results html
                var no_results = jQuery('<div id="no_results" style="display: none;"><h2><?php _e('No Results', 'tabticketbroker'); ?></h2><p><?php _e('There are no results that match the selected filters.', 'tabticketbroker'); ?></p></div>');

                // Add the table below the filters
                jQuery('#report_table_wrap').after(no_results);
            });

            // // If a sku is set in the url, filter the table by that sku
            // jQuery(document).ready(function() {
            //     // Get the sku from the url
            //     var url = new URL(window.location.href);
            //     var sku = url.searchParams.get("sku");

            //     // If the sku is set, filter the table by that sku
            //     if ( sku != null ) {
            //         // Get the sku_prod from the sku
            //         var sku_prod = sku.substring(0,3);
            //         // Get the sku_timeslot from the sku
            //         var sku_timeslot = sku.substring(7,8);
            //         // Get the sku_date from the sku
            //         var sku_date = sku.substring(4,6);

            //         // Set the sku filter
            //         sku_filter['sku_prod'] = sku_prod;
            //         sku_filter['sku_timeslot'] = sku_timeslot;
            //         sku_filter['sku_date'] = sku_date;

            //         // Add the active class to the button with the sku_prod
            //         jQuery('#sku_filter_wrap button[data-sku-value="'+sku_prod+'"]').addClass('active');
            //         jQuery('#sku_filter_wrap button[data-sku-value="'+sku_timeslot+'"]').addClass('active');
            //         jQuery('#sku_filter_wrap button[data-sku-value="'+sku_date+'"]').addClass('active');

            //         // Get the the first table in the div with the id = report_table_wrap
            //         var table = jQuery('#report_table_wrap table').first();

            //         // Get all the rows in the table
            //         var rows = jQuery(table).find('tbody tr');

            //         // Loop through the rows
            //         for ( var i = 0; i < rows.length; i++ ) {

            //             // Get the row
            //             var row = rows[i];

            //             // Get the sku_prod from the row, first 3 characters of the product code
            //             var row_sku_prod = jQuery(row).find('td[data-colname="SKU"]').html().substring(0,3);

            //             var row_sku_timeslot = jQuery(row).find('td[data-colname="SKU"]').html().substring(7,8);

            //             // Get the sku_date from the row, 5th & 6th characters of the product code
            //             var row_sku_date = jQuery(row).find('td[data-colname="SKU"]').html().substring(4,6);

            //             // Check if the sku_prod matches the filter, ignore if the filter is empty
            //             if ( sku_filter['sku_prod'] != '' && row_sku_prod != sku_filter['sku_prod'] ) {
            //                 jQuery(row).hide();
            //             } else if ( sku_filter['sku_timeslot'] != '' && row_sku_timeslot != sku_filter['sku_timeslot'] ) {
            //                 jQuery(row).hide();
            //             } else if ( sku_filter['sku_date'] != '' && row_sku_date != sku_filter['sku_date'] ) {
            //                 jQuery(row).hide();
            //             } else {
            //                 jQuery(row).show();
            //             }
            //         }

            //         // run the set visible rows function
            //         setVisibleRows();
            //     }
            // });
        </script>
<?php
        $js = ob_get_clean();
        echo $js;
    }
}
