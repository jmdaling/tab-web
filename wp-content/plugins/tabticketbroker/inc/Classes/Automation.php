<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

class Automation extends BaseController
{

    public function register()
    {
        add_action('init', array( $this, 'scheduleEvents' ) );
        add_action('disableElapsedVariationsAction', array( $this, 'disableElapsedVariations' ), 10, 1 );

        add_action('ttb_emailAdminAction', array( $this, 'sendEmail' ), 10, 1 );
    }

    function scheduleEvents()
    {
        
        if ( ! wp_next_scheduled( 'disableElapsedVariationsAction' ) ) {
            // Get the current time
            $current_time = new \DateTime( 'now', wp_timezone() );

            // Get the next product disable time
            $product_time_cutoff = HardCoded::getProductTimeCutoff();
            $shedule_time = new \DateTime( $product_time_cutoff, wp_timezone() );

            // If the current time is past the schedule time, add 1 day
            if ( $current_time > $shedule_time ) {
                $shedule_time->modify( '+1 day' );
            }

            // Schedule the event
            wp_schedule_event( $shedule_time->getTimestamp(), 'daily', 'disableElapsedVariationsAction', array( array( 'disable_type' => 'auto_date' ) ) );
        }
    }

    public function disableElapsedVariations( $args )
    {
        if ( $args['disable_type'] == 'auto_date' )
        {
            // Disable all product variations with a pa_date attribute in the past and a status of 'publish'
            $args = array(
                'post_type' => 'product_variation',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'attribute_pa_date',
                        'value' => date('Ymd'),
                        'compare' => '<=',
                        'type' => 'DATE'
                    ),
                )
            );
            
            $query = new \WP_Query( $args );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    
                    // Disable the variation by setting it to 'private'
                    $query->the_post();
                    $product = wc_get_product( get_the_ID() );
                    $product->set_status( 'private' );
                    $product->save();

                    JMD_Logger::metalog( get_the_ID(), '_ttb_variation_history', array( 'action' => 'disabled', 'reason' => 'auto_date', 'sku' => $product->get_sku(), 'pa_date' => $product->get_attribute('pa_date')) );

                }
            }

            wp_reset_postdata();
        }
    }

    public function sendEmail( $args ){
        $to = $args['to'];
        $subject = $args['subject'];
        $message = $args['message'];
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $to, $subject, $message, $headers );
    }

    /**
     * Schedule an email to be sent to the admin
     * @param $args array of arguments for the email
     * @param $args['to'] string email address to send the email to
     * @param $args['subject'] string subject of the email
     * @param $args['message'] string message of the email
     * @param $time_interval time interval to send the email, default every 5 minutes
     */
    public static function scheduleEmail( $args, $time_interval = ( 5 * 60)  )
    {
        //Check if the event is scheduled
        if ( ! wp_next_scheduled( 'ttb_emailAdminAction' ) ) {
            //Schedule the event in 5 minutes 
            wp_schedule_single_event( time() + $time_interval, 'ttb_emailAdminAction', array( $args ) );
        }
    }
}