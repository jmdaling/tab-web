<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

class ttbFastbill extends BaseController
{
    /**
     * Custom TabTicketBroker function to include additional item details on the invoices
     * @param $item Item of order
     */
    public function getItemName( $item )
    {
        $item_descripion = $item->get_name().
            ' ('.				
            __( 'Date', 'tabticketbroker' ) .': '. $item->get_meta('pa_date') .'|'.
            __( 'Timeslot', 'tabticketbroker' ) .': '. $item->get_meta('pa_timeslot') .'|'.
            __( 'People', 'tabticketbroker' ) .': '. $item->get_meta('pa_date') .'|'.
            __( 'Area', 'tabticketbroker' ) .': '. $item->get_meta('pa_date').
            ')';

        return $item_descripion;
    }
}