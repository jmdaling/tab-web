<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Classes;

use Inc\Base\BaseController;

class TabSupplier extends BaseController
{
    public $id                    = null;
    public $name                  = null;
    public $email                 = null;
    public $phone                 = null;
    public $notes                 = null;
    public $reservation           = null;
    
    public function __construct( $post_id = null )
    {
        if ( $post_id ) $this->setSupplier( intval( $post_id ) );
    }
    
    public function getPost() { return $this->post; }
    public function setPost( $post ) { $this->post = $post; }
    
    public function getId() { return $this->id; }
    public function setId( $id ) { $this->id = $id; }

    public function getName() { return $this->name; }
    public function setName( $name ) { $this->name = $name; }

    public function getCompanyName() { return $this->company_name; }
    public function setCompanyName( $company_name ) { $this->company_name = $company_name; }
    
    public function getEmail() { return $this->email; }
    public function setEmail( $email ) { $this->email = $email; }

    public function getPhone() { return $this->phone; }
    public function setPhone( $phone ) { $this->phone = $phone; }

    public function getNotes() { return $this->notes; }
    public function setNotes( $notes ) { $this->notes = $notes; }

    public function setSupplier( $post_id )
    {
        $this->setId( $post_id );
        $this->setPost( get_post( $this->getId() ) );

        if ( ! $this->getPost() ) return false; // No post found
        
        $this->setCompanyName( get_post_meta( $this->post->ID,      'tab_supplier_company_name', true ) );
        $this->setName( get_post_meta( $this->post->ID,             'tab_supplier_name', true ) );
        $this->setEmail( get_post_meta( $this->post->ID,            'tab_supplier_email', true ) );
        $this->setPhone( get_post_meta( $this->post->ID,            'tab_supplier_phone', true ) );
        $this->setNotes( get_post_meta( $this->post->ID,            'tab_supplier_notes', true ) );
    }
    
    public function saveSupplier()
    {
        update_post_meta($this->getId(), 'tab_supplier_company_name', $this->getCompanyName());
        update_post_meta($this->getId(), 'tab_supplier_name', $this->getName());
        update_post_meta($this->getId(), 'tab_supplier_email', $this->getEmail());
        update_post_meta($this->getId(), 'tab_supplier_phone', $this->getPhone());
        update_post_meta($this->getId(), 'tab_supplier_notes', $this->getNotes());
    }

    public static function getSuppliers()
    {
        // cannot use base controller in static method, define properly
        $tab_supplier_cpt_title = 'tab_supplier';
        
        $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'post_type'        => $tab_supplier_cpt_title,
            'post_status'      => array( 'publish', 'pending', 'draft', 'future', 'private' ),
        );
        
        $results = get_posts( $args );

        $suppliers = array();
        foreach ( $results as $post ) {
            $suppliers[] = new TabSupplier( $post->ID );
        }

        // Sort by name ignoring case
        usort($suppliers, function($a, $b) {
            return strcmp(strtolower($a->getName()), strtolower($b->getName()));
        });

        return $suppliers;
    }

    /**
     * Get all the reservations linked to this supplier
     * @return array of TabReservation
     */
    public function getLinkedReservations( $reservations )
    {
        $supplier_reservations = array();
        
        foreach( $reservations as $reservation ) {
             if ( $reservation->getSupplierId() == $this->getId() ) {
                 $supplier_reservations[] = $reservation;
             }
        }

        return $supplier_reservations;
    }

    /**
     * Get supplier's list of products
     * @return array
     */
    public function getLowStockProducts( $post_id = null )
    {
        global $wpdb;
        $post = $post_id ? $this->setSupplier($post_id) : $this->post;
        $alert_stock_min = get_option('ft_smfw_alert_stock_min');

        $query = $wpdb->prepare(
            "SELECT post_id
            FROM {$wpdb->postmeta}, {$wpdb->posts}
            WHERE {$wpdb->postmeta}.post_id = {$wpdb->posts}.ID
            AND ({$wpdb->posts}.post_type = 'product' OR {$wpdb->posts}.post_type = 'product_variation')
            AND meta_key='ft_smfw_supplier' AND meta_value=%d"
            , $post->ID);
        $results = $wpdb->get_results($query, ARRAY_A);

        $product_titles = array();
        foreach ($results as $result) {
            $product = wc_get_product($result['post_id']);

            // Product is a variable product ? => We put each variation in the array
            if ($product->is_type('variable')) {
                $variations = $product->get_available_variations();

                foreach ($variations as $variation) {
                    $product = wc_get_product($variation['variation_id']);
                    if ($product->get_manage_stock() && ($alert_stock_min >= $product->get_stock_quantity())) {
                        $product_titles[] = $product;
                    }
                }
            }
            // Product is a simple product ? => We simply put product in the array
            elseif ($product->is_type('simple')) {
                if ($product->get_manage_stock() && ($alert_stock_min >= $product->get_stock_quantity())) {
                    $product_titles[] = $product;
                }
            }
        }

        return $product_titles;
    }
}
    




