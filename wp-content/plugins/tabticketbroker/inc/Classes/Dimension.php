<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Classes;

use Inc\Base\BaseController;

class Dimension extends BaseController
{
    public $sku_prods           = array();
    public $sku_dates           = array();
    public $sku_displaydates    = array();
    public $sku_timeslots       = array();
    public $sku_pax             = array();
    public $sku_areas           = array();
    public $special_pacakge_id  = 0;
    public $init                = false;
    public $displaydate_format  = null;

    /**
     * Initialize the class and set its properties.
     */
    public function initialize()
    {
        $this->sku_prods       = $this->getSkuProds();
        $this->sku_dates       = $this->getSkuDates();
        $this->sku_timeslots   = $this->getSkuTimeslots();
        $this->sku_pax         = $this->getSkuPax();
        $this->sku_areas       = $this->getSkuAreas();
        $this->init            = true; 
    }

    /* Method to set display date format */
    public function setDisplayDateFormat( $format )
    {
        $this->displaydate_format = $format;
    }

    /**
     * Method to check if the class has been initialized
     */
    public function isInitialized()
    {
        return $this->init;
    }

    /** 
     * Get the product code from the title
     */
    public function getProductCode( $title )
    {
        // Lookup product code from sku with title
        $product_code = 'NONE';

        // Loop through $this->sku_prods to find the product code
        foreach ( $this->sku_prods as $key => $value ) {
            if ( $value == $title ) {
                $product_code = $key;
                break;
            }
        }

        return $product_code;
    }

    //Get Beer hall product IDs
    public function getProductIds() {
        // Note the additional tax query used to filter by category
        $product_ids = get_posts( 
            array(
                'post_type'         => 'product',
                'post_status'       => array( 'publish', 'private'),
                'fields'            => 'ids',
                'posts_per_page'    => -1,
                // 'tax_query' => array(
                //     array(
                //     'taxonomy' => 'product_cat',
                //     'field' => 'slug',
                //     'terms' => 'festzelte', // FILTER on Beer hall
                //     'operator' => 'IN',
                //     )
                // ),
            )
        );
        return $product_ids;
    }

    /**
     * Get Product Skus
     * @return array array main skus
     */
    public function getSkuProds()
    {
        $sku_prods = array();
        $product_ids = $this->getProductIds();
        foreach ( $product_ids as $product_id ) {
            $product = wc_get_product( $product_id );
            $sku_prods[$product->get_sku()] = $product->get_title();
        }

        // Add unknown product
        // $sku_prods['UKN'] = 'Unknown';

        return $sku_prods;
    }
    
    public function getSpecialPackageId() 
    {
        return $this->special_pacakge_id;
    }
    
    public function getProductTitles() 
    {
        $product_titles = array();
        $product_ids = $this->getProductIds();

        // Get all the titles from the ids`
        foreach ( $product_ids as $key => $value ) {
            $product_titles[$value] = get_the_title($value);

            if ( $product_titles[$value] == HardCoded::getSpecialPackageProdTitle() ) {
                $this->special_pacakge_id = $value;
            }
        }

        // Cleanup unused array
        unset( $product_ids );

        // Sort product titles if not empty
        if ( ! $product_titles ) {
            $this->throwError( 'Product Error', 'It seems that there are no products in the Beer hall category. Please investigate. This occurred in the getTentProductTitles method in Dimensions.php' );
        }
        else {
            asort( $product_titles );
        }
        
        return $product_titles;
    }

    /**
     * Gets all dates from attributes (terms)
     * @return array array of dates
     */
    public function getProductDates()
    {
        $product_dates = array();

        // Use defined product attributes as source dates
        // $terms = get_terms( array( 'pa_date' ),  array( "hide_empty" => true ) );
        $terms = get_terms( array( 'pa_date' ),  array( "hide_empty" => false ) );

        // Check if date term is set
        if ( is_wp_error( $terms ) ){
            $this->throwError( 'Taxonomy Error', 'It seems that there is no date attribute for products. Please investigate. This occurred in the getProductDates method in Dimensions.php', $terms );
            return $product_dates;
        }

        // Load slug & name in array
        foreach ( $terms as $term ) {
            $product_dates[ $term->slug ] = $term->name;
        }
        
        return $product_dates;
    }

    /**
     * Get SKU lookup for dates
     * @return array array of SKU dates
     */
    public function getSkuDates()
    {
        // Get user locale for display dates
        $user_locale = get_user_locale();
        
        $product_dates = $this->getProductDates();

        // Sort product dates array by key
        ksort( $product_dates );

        $i = 1;
        $sku_dates = array();
        foreach ( $product_dates as $key => $value ) {
            // create code from $i counter always being 2 chars
            $code = str_pad( $i, 2, '0', STR_PAD_LEFT );
            $sku_dates[$code] = $key;

            // Set sku display dates
            $this->sku_displaydates[$code] = $this->getDisplayDateAdvanced( $key, $user_locale, $this->displaydate_format );

            $i++;
        }
        
        return $sku_dates;
    }
    
    /**
     * Get Timeslot attributes
     * @return array array of timeslots
     */
    public function getProductTimeslots()
    {
        $product_timeslots = array();
        
        // Use defined product attributes as source dates
        $terms = get_terms( array( 'pa_timeslot' ),  array( "hide_empty" => true ) );

        foreach ( $terms as $term ) {
            $product_timeslots[$term->slug] = $term->name;
        }
        
        return $product_timeslots;
    }

    /**
     * Get sku timeslots
     * @return array array of timeslots
     */
    public function getSkuTimeslots()
    {
        // If site = ofest, hardcode timeslots
        if ( TTB_SITE == 'ofest' ) {
            return array(
                '1' => 'mittag',
                '2' => 'nachmittag',
                '3' => 'abend',
            );
        }        
        
        $sku_timeslots = array();
        $timeslots = $this->getProductTimeslots();

        // Sort product dates array by key
        ksort( $timeslots );
        
        $i = 1;
        foreach ( $timeslots as $key => $value ) {
            $code = (string) $i;
            $sku_timeslots[$code] = $key;
            $i++;
        }
        
        return $sku_timeslots;
    }

    /**
     * Translate timeslot to english
     * Timeslots (and areas) are in German, so translate to English
     */
    public function translateTimeslot( $timeslot_de )
    {
        // $translations
        switch ( strtolower( $timeslot_de ) ) {
            case 'abend':
                $timeslot = 'evening';
                break;
            case 'mittag':
                $timeslot ='lunch';
                break;
            case 'nachmittag':
                $timeslot ='afternoon';
                break;
            default:
                $timeslot = 'Error:translation for timeslot `'.$timeslot_de.'` not defined';
                break;
        }

        return ucfirst( $timeslot );
    }

    /**
     * Translate area to english
     * Areas (and timeslots) are in German, so translate to English
     */
    public function translateArea( $area_de )
    {
        // input strings:  Kein bestimmter Bereich, Balkon, Box, Mittelschiff 
        // output strings: No specific area, Balcony, Box, Middle aisle
        switch ( strtolower( $area_de ) ) {
            case '':
                $area = '';
                break;
            case 'kein bestimmter bereich':
                $area = 'no specific area';
                break;
            case 'balkon':
                $area ='balcony';
                break;
            case 'box':
                $area ='box';
                break;
            case 'mittelschiff':
                $area ='center/middle area';
                break;
            default:
                $area = 'Error:translation for area `'.$area_de.'` not defined';
                break;
        }

        return ucfirst( $area );
    }


    // Get the order of timeslots 
    static public function getTimeslotOrder()
    {
        // Use defined product attributes as source dates
        $terms = get_terms( array( 'pa_timeslot' ),  array( "hide_empty" => true ) );

        // If the attribute does not exist or has not terms, return empty array
        if ( is_wp_error( $terms ) || empty( $terms ) ){
            return array();
        }

        // Build the order array
        foreach ( $terms as $term ) {
            $product_timeslots_order[] = $term->slug;
        }
        
        return $product_timeslots_order;
    }

    // /** 
    //  * Get pax attributes
    //  */
    // public function getProductPax()
    // {
    //     // Use defined product attributes as source dates
    //     $terms = get_terms( array( 'pa_pax' ),  array( "hide_empty" => true ) );
        
    //     foreach ( $terms as $term ) {
    //         $product_pax[$term->slug] = $term->name;
    //     }
        
    //     return $product_pax;
    // }

    /**
     * Get sku pax
     * @return array array of pax
     */
    public function getSkuPax()
    {
        $sku_pax = array();
        // $pax = $this->getProductPax();

        // Create code from pax (1-99)
        for ($i=1; $i < 99; $i++) { 
            $code = str_pad( $i, 2, '0', STR_PAD_LEFT );
            $sku_pax[$code] = $i;
        }
        
        return $sku_pax;
    }

    /**
     * Get Area attributes
     * @return array array of areas
     */
    public function getProductAreas()
    {
        // Use defined product attributes as source dates
        $terms = get_terms( array( 'pa_area' ),  array( "hide_empty" => true ) );

        foreach ( $terms as $term ) {
            $product_areas[$term->slug] = $term->name;
        }
        
        return $product_areas;
    }

    /**
     * Get sku areas
     * @return array array of areas
     */
    public function getSkuAreas()
    {
        // $sku_areas = array();
        // $areas = $this->getProductAreas();
        // $area_codes = HardCoded::getProductAreaCodes();
        
        // foreach ( $areas as $key => $value ) {   
        //     $sku_areas[$key] = $value;
        // }
        
        return HardCoded::getProductAreaCodes();
    }

    // Get all reservation statuses
    public static function getReservationStatuses()
    {
        $reservation_statuses = array();

        // Use defined product attributes as source dates
        $terms = get_terms( array( 'tab_res_tax_status' ),  array( "hide_empty" => false ) );

        // Check if date term is set
        if ( is_wp_error($terms) ){
            BaseController::throwError( 'Taxonomy Error', 'It seems that there is no reservation status attribute for products. Please investigate. This occurred in the getReservationStatuses method in Dimensions.php', $terms );
        }

        // Sort by name
        usort( $terms, function( $a, $b ) {
            return strcmp( $a->name, $b->name );
        });

        // Load slug & name in array
        foreach ( $terms as $term ) {
            $reservation_statuses[ $term->slug ] = $term->name;
        }
        
        return $reservation_statuses;
    }

    /**
     * This function extracts all the product attributes from the sku
     * Format: [prod]-[day]-[timeslot]-[pax]-[area]
     * example: "FIS-11-3-20"
     */
    public function getSkuAttributes( $sku )
    {
        
        // Split $sku into parts delimited by '-'
        $sku_parts = explode( '-', $sku );

        $sku_attributes = array();
        $sku_attributes['prod'] = $sku_parts[0];
        $sku_attributes['day'] = $sku_parts[1];
        $sku_attributes['timeslot'] = $sku_parts[2];
        $sku_attributes['pax'] = $sku_parts[3];
        $sku_attributes['area'] = isset( $sku_parts[4] ) ? $sku_parts[4] : '';

        return $sku_attributes;
    }

    /**
     * NOT COMPLETE: TODO: Handle incorrect variation data e.g. blank attributes
     * Generate the sku from the attributes
     * Format: [prod]-[day]-[timeslot]-[pax]-[area]
     * example: "FIS-11-3-20"
     */
    public function generateSkuFromVariation( $variation )
    {
        if( $this->isInitialized() == false )
            $this->initialize();

        $sku = 'blank';

        // Search array values in $this->sku_prods for the value of $variation['product_title']
        $sku_prod = array_search( $variation['post_title'], $this->sku_prods );
        $sku_date = array_search( $variation['pa_date'], $this->sku_dates );
        $sku_timeslot = array_search( $variation['pa_timeslot'], $this->sku_timeslots );
        $sku_pax = array_search( $variation['pa_pax'], $this->sku_pax );
        $sku_area = array_search( $variation['pa_area'], $this->sku_areas ) ? array_search( $variation['pa_area'], $this->sku_areas ) : '';

        // Check if all values are found
        if ( ! $sku_prod || ! $sku_date || ! $sku_timeslot || ! $sku_pax || ( $variation['pa_area'] != 'kein-bestimmter-bereich' && ! $sku_area ) ) {

            $sku = 'sku error:';

            // Check which variable is missing
            if ( ! $sku_prod ) {
                $sku .= 'sku_prod error,';
            }
            if ( ! $sku_date ) {
                $sku .= 'sku_date error,';
            }
            if ( ! $sku_timeslot ) {
                $sku .= 'sku_timeslot error,';
            }
            if ( ! $sku_pax ) {
                $sku .= 'sku_pax error,';
            }
            if ( ! $sku_area ) {
                $sku .= 'sku_area error,';
            }

            // Remove trailing comma
            $sku = rtrim( $sku, ',' );

            return $sku;
        //     $this->throwError( 'SKU Error', 'It seems that there is a problem with the SKU. Please investigate. This occurred in the generateSkuFromVariation method in Dimensions.php' );
         }
               
        // Build the sku
        $sku = $sku_prod.'-'.$sku_date.'-'.$sku_timeslot.'-'.$sku_pax.'-'.$sku_area;

        // Trim the sku
        $sku = trim( $sku, '-' );

        return $sku;
    }
}