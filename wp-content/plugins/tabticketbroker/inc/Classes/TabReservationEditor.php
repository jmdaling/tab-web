<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\EmailController;
use Inc\Classes\Dimension;
use Inc\Base\BaseController;


class TabReservationEditor extends BaseController {
    // Set class vars
    public $reservation         = null;
    public $sub_reservations    = array();

    // Register class
    public function register() {
        $this->init_hooks(); // Init hooks in Wordpress

        $this->init_filters(); // Init filters in Wordpress

        $this->setReservationStatusDependancies(); // Set reservation status dependancies
    }

    public function init_hooks() {
        // Saving
        add_action('save_post_' . $this->tab_reservation_cpt_title, array($this, 'saveReservation'), 2, 1);

        // Redirect after saving
        add_filter('redirect_post_location', array($this, 'redirectAfterSaving'), 2, 2);

        // Metaboxes
        add_action('add_meta_boxes', array($this, 'addMetaboxes'));

        // Hidden inputs
        add_action('edit_form_after_title', array($this, 'addHiddenInputs'));

        // Actions
        // This is required to only run after WC has loaded all its post types (as for anything that requires an order)
        add_action('woocommerce_after_register_post_type', array($this, 'maybeDoReservationActions'));
    }

    /**
     * Hidden inputs
     */
    public function addHiddenInputs() {
        if ('tab_reservation' == get_post_type() && isset($_GET['nav_back_to_supplier'])) {
            echo '<input type="hidden" id="nav_back_to_supplier" name="nav_back_to_supplier" value="' . $_GET['nav_back_to_supplier'] . '" />';
        }
    }

    /**
     * Redirect after saving if post nav_from_supplier is set back to supplier
     */
    public function redirectAfterSaving($location, $post_id) {
        if ('tab_reservation' == get_post_type() && isset($_POST['nav_back_to_supplier'])) {
            return admin_url('post.php?post=' . $_POST['nav_back_to_supplier'] . '&action=edit&new_reservation=' . $post_id);
        }

        return $location;
    }

    /**
     * Initiate plugin filters in Wordpress
     */
    public function init_filters() {
        add_filter('enter_title_here', array($this, 'changeEnterTitleHere'));

        add_filter('gettext', array($this, 'changePublishButton'), 10, 2);
    }

    /**
     * Do actions for the reservation
     */
    public function maybeDoReservationActions() {
        // Send emails 
        if (
            isset($_GET['post']) &&
            isset($_GET['action']) &&
            $_GET['action'] == 'edit' &&
            get_post_type($_GET['post']) == $this->tab_reservation_cpt_title &&
            (isset($_GET['ttb_action']) && $_GET['ttb_action'] == 'send_shared_res_email')
        ) {
            // Set the reservation
            $this->getReservation($_GET['post']);

            // Get the email log
            $email_log = JMD_Logger::metalog($this->reservation->getId(), '_email_shared_reservation_log');

            // If the email has already been sent, return
            if (! empty($email_log) && ! isset($_GET['override'])) {
                // Add flash notice
                $this->addFlashNotice(__('Email has already been sent', 'tabticketbroker'), 'error');
                return;
            }

            // Send the email            
            $this->sendSharedReservationEmail();
        }

        // Save voucher owners
        if (
            isset($_GET['post']) &&
            isset($_GET['action']) &&
            $_GET['action'] == 'edit' &&
            get_post_type($_GET['post']) == $this->tab_reservation_cpt_title &&
            (isset($_GET['ttb_action']) && ($_GET['ttb_action'] == 'add_voucher_owner' || $_GET['ttb_action'] == 'remove_voucher_owner'))
        ) {
            // Set the reservation
            $this->getReservation($_GET['post']);

            // Add or remove the voucher owner
            $voucher_owner_order_item_id = intval($_GET['voucher_owner_order_item_id']);

            $voucher_owners = is_array($this->reservation->getVoucherOwners()) ? $this->reservation->getVoucherOwners() : array();

            if ($_GET['ttb_action'] == 'add_voucher_owner') {
                $voucher_owners[$voucher_owner_order_item_id] = array(
                    'order_item_id' => $voucher_owner_order_item_id,
                    'pax_included'  => -1,
                    'date_set'      => date('Y-m-d H:i:s'),
                );
            } else {
                // Remove the voucher owner
                unset($voucher_owners[$voucher_owner_order_item_id]);
            }

            $this->reservation->setVoucherOwners($voucher_owners);

            $this->reservation->saveReservation();
        }
    }

    /**
     * Get reservation
     * @param int $post Post ID
     */
    public function getReservation($post_id = null) {
        // Check if reservation is already set and post_id = reservation post id
        if ($this->reservation && $post_id == $this->reservation->getId()) {
            return $this->reservation;
        }

        if (! $post_id || ! is_numeric($post_id) || $post_id == 1) {
            $this->addFlashNotice('ERROR: TabReservationEditor.php: Reservation not found. Contact your administrator.', 'error');
            return false;
        }

        $this->reservation = new TabReservation($post_id);

        return $this->reservation;
    }

    /**
     * Send the shared reservation email
     */
    public function sendSharedReservationEmail() {
        $email_controller = new EmailController();

        $email_controller->emailSharedReservation($this->reservation);
    }

    /**
     * Function used when the admin is initiated
     */
    function addMetaboxes() {
        global $post;

        // Reservation information, additional detail on the reservation owner etc
        // add_meta_box(
        //     'tab_reservation_post_information',  
        //     __('Reservation information', 'tabticketbroker' ), 
        //     array($this, 'reservationInformationMetaBox'), 
        //     $this->tab_reservation_cpt_title, 
        //     'normal', 
        //     'default'
        // );

        // Reservation detail, event focused details
        add_meta_box(
            'tab_reservation_post_detail',
            __('Reservation detail', 'tabticketbroker'),
            array($this, 'reservationDetailMetaBox'),
            $this->tab_reservation_cpt_title,
            'normal',
            'default'
        );

        // Metaboxes only displayed when the reservation is published
        if ("publish" == $post->post_status) {
            // Show linked orders
            add_meta_box(
                'tab_reservation_wc_orders_linked',
                __('Reservation orders', 'tabticketbroker'),
                array($this, 'generateReservationOrderMetabox'),
                $this->tab_reservation_cpt_title,
                'normal',
                'default'
            );

            // OFEST ONLY: Reservation actions 
            if (TTB_SITE == 'ofest')
                add_meta_box(
                    'tab_reservation_actions',
                    __('Reservation Actions', 'tabticketbroker'),
                    array($this, 'printReservationActions'),
                    $this->tab_reservation_cpt_title,
                    'side',
                    'high'
                );

            add_meta_box(
                'tab_reservation_subreservations',
                __('Sub-Reservations', 'tabticketbroker'),
                array($this, 'subReservationsCallback'),
                $this->tab_reservation_cpt_title,
                'normal',
                'default'
            );

            add_meta_box(
                'tab_reservation_audit_log',
                __('History', 'tabticketbroker'),
                array($this, 'auditlogCallback'),
                $this->tab_reservation_cpt_title,
                'normal',
                'default'
            );
        }
    }

    /** Hides title since we are not using it like a blog post*/
    function changeEnterTitleHere($title) {
        $screen = get_current_screen();

        if ($this->tab_reservation_cpt_title == $screen->post_type) {
            $title = '<style>#titlediv{display:none;}</style>';
        }

        // Hide title field hack
        return $title;
    }

    // Change button text to Save instead of Publish
    function changePublishButton($translation, $text) {
        if ('Publish' == $text) return __('Save', 'tabticketbroker');
        return $translation;
    }

    function reservationDetailMetaBox($post) {
        // Get the reservation
        $this->getReservation($post->ID);

        // Go ahead and set the supplier ID if it has been sent through. (Typically done when clicked on Add Reservation in the Edit Supplier page)
        if (isset($_GET['supplier_id'])) {
            $this->reservation->setSupplierId($_GET['supplier_id']);
        }

        // See if we have orders assigned
        $has_assignments = $this->reservation->getOrderItemIds() ? true : false;

        $disabled = false;
        $editable_statuses = array('available', 'offer');
        // If status not in editable statuses, then disable the fields
        if (! in_array($this->reservation->getStatus(), $editable_statuses) && $post->post_status == 'publish') {
            $disabled = true;
        }

?>
        <!-- Create reservation info box left -->
        <div class="reservation-form-fields" style="display: flex; flex-wrap: wrap; width: 100%;">
            <div class="reservation-form-fields" style="float: left; width: 50%;">
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('product', $this->reservation->getProductTitle(), $disabled); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('pa_date', $this->reservation->getDate(), $disabled); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('pa_reservation_pax', $this->reservation->getPax(), $disabled); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('pa_timeslot', $this->reservation->getTimeslot(), $disabled); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('pa_area', $this->reservation->getArea()); ?>
                </div>
                <div class="form-field" title="<?php echo $has_assignments ? __('There are orders assigned to the reservation. To change the status, first unassign these orders.') : ''; ?>">
                    <?php echo $this->getAttributeSelect($this->tab_reservation_status_taxonomy, $this->reservation->getStatus()); ?>
                </div>
                <div id="res_docs" style="border: 1px solid #ccc; padding: 10px; margin: 10px;">
                    <h4 style="margin:0;"><?php _e('Documents', 'tabticketbroker'); ?></h4>
                    <div class="form-field">
                        <?php echo $this->getAttributeSelect('vouchers_received', $this->reservation->getVouchersReceived()); ?>
                    </div>
                    <div class="form-field">
                        <?php echo $this->getAttributeSelect('letter_received', $this->reservation->getLetterReceived()); ?>
                    </div>
                    <div class="form-field">
                        <?php echo $this->getAttributeSelect('letter_ready', $this->reservation->getLetterReady()); ?>
                    </div>
                    <div class="form-field">
                        <?php echo $this->getAttributeSelect('docs_complete', $this->reservation->getDocsComplete(), true); ?>
                    </div>
                    <div class="form-field">
                        <?php echo $this->getAttributeSelect('ready_to_ship', $this->reservation->getReadyToShip()); ?>
                    </div>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('notes', $this->reservation->getNotes()); ?>
                </div>
            </div>
            <div class="reservation-form-fields" style="float: right; width: 50%;">
                <div style="float: right; font-size:larger;">
                    <label id="tab_reservation_id" title="<?php _e('Reservation ID', 'tabticketbroker'); ?>"><?php echo $this->reservation->getCode() . ' [ ' . $this->reservation->getSku() . ' ] '; ?></label>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('supplier', $this->reservation->getSupplierId()); ?>
                    <?php
                    // Check if the supplier is set, if so, show the link to the supplier
                    if (0 != $this->reservation->getSupplierId()) {
                        echo '<a href="post.php?post=' . $this->reservation->getSupplierId() . '&action=edit">' . __('Go to current supplier', 'tabticketbroker') . '</a> | ';
                    }
                    ?>
                    <a href="post-new.php?post_type=tab_supplier"><?php _e('Add new supplier', 'tabticketbroker') ?>
                    </a>
                </div>
                <div class="form-field">
                    <label for="tab_reservation_owner_name_field"><?php _e('Owner Name', 'tabticketbroker'); ?></label>
                    <input
                        type="text"
                        id="tab_reservation_owner_name_field"
                        name="tab_reservation_owner_name"
                        value="<?php _e($this->reservation->getOwnerName()); ?>"
                        placeholder="<?php _e('Owner', 'tabticketbroker'); ?>" />
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('start_time', $this->reservation->getStartTime()); ?>
                </div>
                <div class="form-field">
                    <?php echo $this->getAttributeSelect('end_time', $this->reservation->getEndTime()); ?>
                </div>
                <div class="form-field" <?php echo (user_can(wp_get_current_user(), 'view_supplier_cost_prices') ? '' : 'style="display:none;"'); ?>>
                    <label for="tab_reservation_cost_price_field"><?php _e('Cost price', 'tabticketbroker'); ?></label>
                    <input
                        type="number"
                        id="tab_reservation_cost_price_field"
                        name="tab_reservation_cost_price"
                        value="<?php echo ($this->reservation->getCostPrice() == 0 ? '' : $this->reservation->getCostPrice()); ?>"
                        placeholder="<?php _e('Original price of ticket / reservation', 'tabticketbroker'); ?>"
                        min="0" />
                </div>
                <div class="form-field">
                    <label for="tab_reservation_source_url_field"><?php _e('Source URL e.g. link to ebay (optional)', 'tabticketbroker'); ?></label>
                    <input
                        type="text"
                        id="tab_reservation_source_url_field"
                        name="tab_reservation_source_url"
                        value="<?php _e($this->reservation->getSourceUrl()); ?>"
                        placeholder="<?php _e('Source URL', 'tabticketbroker'); ?>" />
                </div>
                <div class="form-field">
                    <label for="tab_reservation_table_no_field"><?php _e('Table number', 'tabticketbroker'); ?></label>
                    <input
                        type="text"
                        id="tab_reservation_table_no_field"
                        name="tab_reservation_table_no"
                        value="<?php _e($this->reservation->getTableNo()); ?>"
                        placeholder="<?php _e('Table number', 'tabticketbroker'); ?>" />
                </div>
                <div class="form-field">
                    <label for="tab_reservation_customer_no_field"><?php _e('Customer number', 'tabticketbroker'); ?></label>
                    <input
                        type="text"
                        id="tab_reservation_customer_no_field"
                        name="tab_reservation_customer_no"
                        value="<?php _e($this->reservation->getCustomerNo()); ?>"
                        placeholder="<?php _e('Customer number', 'tabticketbroker'); ?>" />
                </div>
                <div class="form-field">
                    <label for="tab_reservation_booking_no_field"><?php _e('Booking number', 'tabticketbroker'); ?></label>
                    <input
                        type="text"
                        id="tab_reservation_booking_no_field"
                        name="tab_reservation_booking_no"
                        value="<?php _e($this->reservation->getBookingNo()); ?>"
                        placeholder="<?php _e('Booking number', 'tabticketbroker'); ?>" />
                </div>
            </div>
        </div>
    <?php
    }

    public function getAttributeSelect($slug, $current_value, $disabled = false) {
        if (! $slug) return 'ERROR: Slug empty';

        $required = '';

        switch ($slug) {

            case 'product':
                $field_id = 'tab_reservation_product';
                $field_name = 'tab_reservation_product';
                $field_label = __('Product', 'tabticketbroker');
                $field_placeholder = __('Select product', 'tabticketbroker');
                $required = 'required';
                break;

            case 'pa_date':
                $field_id = 'tab_reservation_date';
                $field_name = 'tab_reservation_date';
                $field_label = __('Date', 'tabticketbroker');
                $field_placeholder = __('Select date', 'tabticketbroker');
                $required = 'required';
                break;

            case 'pa_reservation_pax':
                $field_id = 'tab_reservation_pax';
                $field_name = 'tab_reservation_pax';
                $field_label = __("Amount of People", "tabticketbroker");
                $field_placeholder = __('Select amount', 'tabticketbroker');
                $required = 'required';
                break;

            case 'pa_timeslot':
                $field_id = 'tab_reservation_timeslot';
                $field_name = 'tab_reservation_timeslot';
                $field_label = 'Timeslot';
                $field_placeholder = 'Select timeslot';
                $required = 'required';
                break;

            case 'pa_area':
                $field_id = 'tab_reservation_area';
                $field_name = 'tab_reservation_area';
                $field_label = __('Area / Class', 'tabticketbroker');
                $field_placeholder = __('Select area', 'tabticketbroker');
                $required = 'required';
                break;

            case $this->tab_reservation_status_taxonomy:
                $field_id = 'tab_reservation_status';
                $field_name = 'tab_reservation_status';
                $field_label = __('Status', 'tabticketbroker');
                $field_placeholder = __('Select status', 'tabticketbroker');
                $required = 'required';
                break;

            case 'vouchers_received':
                $field_id = 'tab_reservation_vouchers_received';
                $field_name = 'tab_reservation_vouchers_received';
                $field_label = __('Vouchers received', 'tabticketbroker');
                $field_placeholder = __('Select vouchers received', 'tabticketbroker');
                break;

            case 'letter_received':
                $field_id = 'tab_reservation_letter_received';
                $field_name = 'tab_reservation_letter_received';
                $field_label = __('Letter received', 'tabticketbroker');
                $field_placeholder = __('Select letter received', 'tabticketbroker');
                break;

            case 'letter_ready':
                $field_id = 'tab_reservation_letter_ready';
                $field_name = 'tab_reservation_letter_ready';
                $field_label = __('Letter ready', 'tabticketbroker');
                $field_placeholder = __('Select letter ready', 'tabticketbroker');
                break;

            case 'docs_complete':
                $field_id = 'tab_reservation_docs_complete';
                $field_name = 'tab_reservation_docs_complete';
                $field_label = __('Docs complete', 'tabticketbroker');
                $field_placeholder = __('Select docs complete', 'tabticketbroker');
                break;

            case 'ready_to_ship':
                $field_id = 'tab_reservation_ready_to_ship';
                $field_name = 'tab_reservation_ready_to_ship';
                $field_label = __('Ready to ship', 'tabticketbroker');
                $field_placeholder = __('Select ready to ship', 'tabticketbroker');
                break;

            case 'supplier':
                $field_id = 'tab_reservation_supplier';
                $field_name = 'tab_reservation_supplier_id';
                $field_label = 'Supplier';
                $field_placeholder = __('Select supplier', 'tabticketbroker');
                $required = 'required';
                break;

            case 'start_time':
                $field_id = 'tab_reservation_start_time';
                $field_name = 'tab_reservation_start_time';
                $field_label = __('Start Time', 'tabticketbroker');
                $field_placeholder = 'Select start time';
                break;

            case 'end_time':
                $field_id = 'tab_reservation_end_time';
                $field_name = 'tab_reservation_end_time';
                $field_label = __('End Time', 'tabticketbroker');
                $field_placeholder = __('Select end time', 'tabticketbroker');
                break;

            case 'pa_event':
                $field_id = 'tab_reservation_event';
                $field_name = 'tab_reservation_event';
                $field_label = __('Event', 'tabticketbroker');
                $field_placeholder = __('Select event', 'tabticketbroker');
                break;

            case 'pa_city':
                $field_id = 'tab_reservation_city';
                $field_name = 'tab_reservation_city';
                $field_label = __('City', 'tabticketbroker');
                $field_placeholder = __('Select city', 'tabticketbroker');
                break;

            case 'pa_location':
                $field_id = 'tab_reservation_location';
                $field_name = 'tab_reservation_location';
                $field_label = __('Location', 'tabticketbroker');
                $field_placeholder = __('Select location', 'tabticketbroker');
                break;

            case 'pa_opponent':
                $field_id = 'tab_reservation_opponent';
                $field_name = 'tab_reservation_opponent';
                $field_label = __('Opponent', 'tabticketbroker');
                $field_placeholder = __('Select opponent', 'tabticketbroker');
                break;

            case 'notes':
                $field_id = 'tab_reservation_notes_field';
                $field_name = 'tab_reservation_notes';
                $field_label = __('Reservation notes', 'tabticketbroker');
                $field_placeholder = __('Notes which will be displayed in the assignment section to assist the sales delivery team.', 'tabticketbroker');
                break;
        }

        // Set disabled html string
        $disabled_html = $disabled ? 'disabled' : '';

        // Set required html tag
        // This shows a red required star next to the label (copied from WC woocommerce_form_field)
        $required_html = $required ? '<abbr class="required" title="required">*</abbr>' : '';

        // Set check box fields
        $check_box_fields = array(
            'letter_received',
            'letter_ready',
            'vouchers_received',
            'docs_complete',
            'ready_to_ship'
        );

        // Set initial html string
        $html = '';
        $html .= '<label for="' . $field_id . '">' . $field_label . ' ' . $required_html . '</label>';
        $html .= '<select name="' . $field_name . '" id="' . $field_id . '" ' . $required . ' ' . $disabled_html . '>';
        $html .= '<option value="">' . __($field_placeholder, 'tabticketbroker') . '</option>';

        // TAB TICKETS dates
        // Unlike Ofest the dates can be vast and should be selected from a datepicker
        if (TTB_SITE == 'tabtickets' && $slug == 'pa_date') {
            $html = '';
            ob_start();
            woocommerce_form_field(
                $field_id,
                array(
                    'type'          => 'date',
                    'class'         => array('my-field-class form-row-wide'),
                    'label'         => $field_label,
                    'placeholder'   => $field_placeholder,
                    'required'      => $required,
                ),
                $current_value
            );
            $html .= ob_get_clean();
            return $html;
        }

        // Get the product titles, for ofest this is the tents available
        if ($slug == 'product') {

            $dimension = new Dimension();

            $product_titles = $dimension->getProductTitles();

            // HARDCODED HERE: Exclude SPECIAL PACKAGE
            $special_package_key = array_search("Spezial Paket", $product_titles);
            unset($product_titles[$special_package_key]);

            // Include "Unknown" option
            $product_titles[-1] = 'Unknown';

            foreach ($product_titles as $product_id => $product_title) {
                $html .= '<option ' . ($current_value == $product_title ? 'selected="selected"' : '') . ' value="' . $product_id . ',' . htmlspecialchars($product_title) . '">' . htmlspecialchars($product_title) . '</option>';
            }

            $html .= '</select>';
        }
        // Times available
        elseif ($slug == 'start_time' ||  $slug == 'end_time') {
            $hardcoded = new HardCoded;
            $times_available  = $hardcoded->getAvailableTimes();

            foreach ($times_available as $time) {
                $html .= '<option ' . ($current_value == $time ? 'selected' : '') . ' value="' . $time . '">' . $time . '</option>';
            }
            $html .= '</select>';
        }
        // Suppliers
        elseif ($slug == 'supplier') {

            $supplier = new TabSupplier();

            $suppliers = $supplier->getSuppliers();

            foreach ($suppliers as $supplier) {
                $html .= '<option ' . ($current_value == $supplier->getId() ? 'selected' : '') . ' value="' . $supplier->getId() . '">' . $supplier->getName() . '</option>';
            }
            $html .= '</select>';
        }
        // Pax
        elseif ($slug == 'pa_reservation_pax') {
            $html = '';
            $html .= '<label for="' . $field_id . '">' . $field_label . '</label>';
            $html .= '<input type="number" name="' . $field_name . '" id="' . $field_id . '" ' . $required . ' min="1" max="100" ' . $disabled_html . ' value="' . $current_value . '">';
        }

        // Checkbox fields
        elseif (in_array($slug, $check_box_fields)) {
            $html = '';
            $html .= '<label class="checkbox_label" for="' . $field_id . '">' . $field_label . '</label>';
            $html .= '<input type="hidden" name="' . $field_name . '" id="' . $field_id . '" value="0">';
            $html .= '<input class="ttb_checkbox_input" type="checkbox" name="' . $field_name . '" id="' . $field_id . '" ' . $required . ' ' . $disabled_html . ' value="1" ' . ($current_value == 1 ? 'checked' : '') . '>';

            // Add ready to ship date as a text notification if slug is ready to ship and is checked
            if ($slug == 'ready_to_ship' && $current_value == 1) {
                // format date
                $ready_to_ship_date = date('d-m-Y H:i', strtotime($this->reservation->getReadyToShipDate()));
                $html .= '<p class="ttb-ready-to-ship-date">' . __('Flag set on', 'tabticketbroker') . ': ' . $ready_to_ship_date . '</p>';
            }
        }

        // Voucher notes
        elseif ($slug == 'notes') {

            // Get the voucher last log from JMD_Logger
            $last_log_entry = JMD_Logger::getLastLogEntry($this->reservation->getId(), '_tab_reservation_notes_log');
            if (! empty($last_log_entry)) {
                $last_change = '<span id="' . $field_id . '_log">Last change: ' . $last_log_entry['date'] . ' by ' . $last_log_entry['user'] . '</span>';
            } else {
                $last_change = '<span id="' . $field_id . '_log">' . __('Last change not logged', 'tabticketbroker') . '</span>';
            }

            $html = '';
            $html .= '<label for="' . $field_id . '">' . $field_label . $last_change . '</label>';
            $html .= '<textarea name="' . $field_name . '" id="' . $field_id . '" ' . $required . ' rows="4" cols="50" ' . $disabled_html . '>' . $current_value . '</textarea>';
        }

        // Attributes: any attribute that matches the slugname passed through such as for ofest: timeslot/area/date & tabticket: event/city/location/opponent
        else {
            $terms = get_terms(array($slug),  array("hide_empty" => 0));

            // If terms empty or error, we close the select tag and return the html
            if (empty($terms) || is_wp_error($terms)) {
                $html .= '</select>';
                return $html;
            }

            // Set selected flag to false (will be set to true if a term is selected
            $selected_flag = false;

            foreach ($terms as $term) {

                // Default to true for all items (except statuses, which default to false in checkReservationStatus)
                $is_enabled = true;

                if ($slug == 'pa_date') {
                    $date_string = (is_string($term->slug) ? $term->slug : '1990-01-01');
                    $term_name = $this->getDisplayDate($date_string, get_option('ttb_backend_display_date'));
                } elseif ($slug == 'tab_res_tax_status') {

                    // Set status to available if identified as new reservation
                    if ($this->reservation->post->post_status == 'auto-draft' && $current_value == '') {
                        $current_value = 'available';
                    }

                    // Use hardcoded business logic to see if an option should be available
                    $is_enabled = $this->checkReservationStatus($current_value, $term->slug);

                    $term_name = $term->name;
                } else {
                    $term_name = $term->name;
                }

                // Set enabled flag html
                $enabled_html = $is_enabled ? '' : 'disabled';

                // Flag that a value have been selected (used below to show the current value if nothing was found)
                if ($current_value == $term->slug) {
                    $selected_flag = true;
                }

                // Add the option
                $html .= '<option ' . ($current_value == $term->slug ? 'selected' : '') . ' ' . $enabled_html . ' value="' . $term->slug . '">' . $term_name . '</option>';
                // $html .= '<option '. ( $current_value == $term->slug ? 'selected="selected"' : '' ) .' value="'. $term->slug .'">'. $term_name .'</option>';

            }

            // Important: Current value is not blank, and no matching term was found, add the current value and set it as selected.
            // When changing attributes, especially dates for new event years, previous dates will not be found or matched, thus we 
            // want display the current setting ( AND ideally, disable the select box[not implemented])
            if ($current_value != '' && ! $selected_flag) {
                $html .= '<option selected value="' . $current_value . '">' . $current_value . '</option>';
            }

            $html .= '</select>';
        }

        return $html;
    }

    public $status_match = array();

    public function getReservationStatusDependancies() {
        if (empty($this->status_match)) {
            $this->setReservationStatusDependancies();
        }

        return $this->status_match;
    }

    public function setReservationStatusDependancies() {
        // Array to match statuses
        // This is used to determine which statuses are available to change to based on the current status
        // Left = current status, right = available statuses i.e. not disabled
        $this->status_match = array(
            'offer'         => array('soldout', 'on-hold', 'unavailable', 'available'), // Soldout should only be available from offer
            'available'     => array('offer', 'on-hold', 'unavailable', 'booked', 'closed'),
            'booked'        => array('on-hold', 'closed'),
            'closed'        => array('booked'),
            'on-hold'       => array('offer', 'available', 'booked', 'unavailable'),
            'unavailable'   => array('offer', 'on-hold', 'available'),
            'soldout'       => array('offer', 'unavailable', 'available'),
        );
    }

    /**
     * Check whether a reservation status should be enabled based on the current status of the reservation
     * 
     * @param string $current_status
     * @param string $status_to_check
     * @return boolean
     */
    public function checkReservationStatus($current_status, $status_to_check) {
        // Default to disabled
        $is_enabled = false;

        // Ensure dependancies are set
        if (empty($this->status_match)) {
            $this->setReservationStatusDependancies();
        }

        // Check if the current status is in the array of statuses to check
        if (
            (isset($this->status_match[$current_status]) && in_array($status_to_check, $this->status_match[$current_status]))
            || $current_status == $status_to_check
        ) {
            $is_enabled = true;
        }

        return $is_enabled;
    }

    /**
     * Hook called when a post is saved
     */
    function saveReservation($post_id) {

        // Check if the post is first created
        global $post;

        if (! isset($post)) return;

        // If this is just a revision, don't continue
        if (wp_is_post_revision($post_id)) return;

        // Update datas
        $this->reservation = $this->getReservation($post_id);

        if (isset($_POST['tab_reservation_supplier_id'])) {
            $this->reservation->setSupplierId(sanitize_text_field($_POST['tab_reservation_supplier_id']));
        }

        if (isset($_POST['tab_reservation_owner_name'])) {
            $this->reservation->setOwnerName(sanitize_text_field($_POST['tab_reservation_owner_name']));
        }

        if (isset($_POST['tab_reservation_start_time'])) {
            $this->reservation->setStartTime(sanitize_text_field($_POST['tab_reservation_start_time']));
        }

        if (isset($_POST['tab_reservation_end_time'])) {
            $this->reservation->setEndTime(sanitize_text_field($_POST['tab_reservation_end_time']));
        }

        if (isset($_POST['tab_reservation_cost_price'])) {
            $this->reservation->setCostPrice(sanitize_text_field($_POST['tab_reservation_cost_price']));
        }

        if (isset($_POST['tab_reservation_source_url'])) {
            $this->reservation->setSourceUrl(sanitize_text_field($_POST['tab_reservation_source_url']));
        }

        if (isset($_POST['tab_reservation_table_no'])) {
            $this->reservation->setTableNo(sanitize_text_field($_POST['tab_reservation_table_no']));
        }

        if (isset($_POST['tab_reservation_customer_no'])) {
            $this->reservation->setCustomerNo(sanitize_text_field($_POST['tab_reservation_customer_no']));
        }

        if (isset($_POST['tab_reservation_booking_no'])) {
            $this->reservation->setBookingNo(sanitize_text_field($_POST['tab_reservation_booking_no']));
        }

        if (isset($_POST['tab_reservation_code'])) {
            $this->reservation->setCode();
        }

        if (isset($_POST['tab_reservation_product'])) {

            $product = explode(',', $_POST['tab_reservation_product']);

            $product_id = $product[0];

            $product_title = $product[1];

            $this->reservation->setProductId($product_id);

            $this->reservation->setProductTitle(sanitize_text_field($product_title));
        }

        if (isset($_POST['tab_reservation_date'])) {
            $this->reservation->setDate(sanitize_text_field($_POST['tab_reservation_date']));
        }

        if (isset($_POST['tab_reservation_pax'])) {
            $this->reservation->setPax(sanitize_text_field($_POST['tab_reservation_pax']));
        }

        if (isset($_POST['tab_reservation_timeslot'])) {
            $this->reservation->setTimeslot(sanitize_text_field($_POST['tab_reservation_timeslot']));
        }

        if (isset($_POST['tab_reservation_area'])) {
            $this->reservation->setArea(sanitize_text_field($_POST['tab_reservation_area']));
        }

        if (isset($_POST['tab_reservation_status'])) {
            $this->reservation->setStatus(sanitize_text_field($_POST['tab_reservation_status']));
        }

        if (isset($_POST['tab_reservation_vouchers_received'])) {
            $this->reservation->setVouchersReceived($_POST['tab_reservation_vouchers_received']);
        }

        if (isset($_POST['tab_reservation_letter_received'])) {
            $this->reservation->setLetterReceived($_POST['tab_reservation_letter_received']);
        }

        if (isset($_POST['tab_reservation_letter_ready'])) {
            $this->reservation->setLetterReady($_POST['tab_reservation_letter_ready']);
        }

        if (isset($_POST['tab_reservation_docs_complete'])) {
            $this->reservation->setDocsComplete();
        }

        if (isset($_POST['tab_reservation_ready_to_ship'])) {
            $this->reservation->setReadyToShip($_POST['tab_reservation_ready_to_ship']);
        } else
            $this->reservation->setReadyToShip(0);

        if (isset($_POST['tab_reservation_notes'])) {
            $notes = wp_kses_post($_POST['tab_reservation_notes']);

            // check if the notes changed
            if ($notes != $this->reservation->getNotes()) {
                // if so, log the change
                JMD_Logger::metalog(
                    $this->reservation->getId(),
                    '_tab_reservation_notes_log',
                    array(
                        'notes' => $notes
                    )
                );

                // and update the notes
                $this->reservation->setNotes($notes);
            }
        }

        $this->reservation->saveReservation();
    }

    public function printReservationActions($post) {
        // Get reservation owner name from post id
        $this->getReservation($post->ID);

        // Get the owner info
        $reservation_ower_name = $this->reservation->getOwnerName();

        // Get the email log
        $email_log = JMD_Logger::metalog($this->reservation->getId(), '_email_shared_reservation_log');

        // Check the requirements for the action
        if ($reservation_ower_name == '') {
            $disabled_html = 'disabled';
            $title = __('Please fill in the reservation owner name', 'tabticketbroker');
        } elseif (count($this->reservation->getOrderItemIds()) <= 1) {
            $disabled_html = 'disabled';
            $title = __('Only available for reservations with multiple orders that share it', 'tabticketbroker');
        } elseif (! in_array($this->reservation->getStatus(), array('closed'))) {
            $disabled_html = 'disabled';
            $title = __('This email is only enabled if the status is Closed', 'tabticketbroker');
        } elseif ($email_log && ! isset($_GET['override'])) {
            $disabled_html = 'disabled';
            $title = __('This email has already been sent', 'tabticketbroker');
        } else {
            $disabled_html = '';
            $title = __('Click to send each of the orders linked here the shared reservation email', 'tabticketbroker');
        }

        // Generate the HTML
    ?>
        <div class="wrapper">
            <?php echo '<a title="' . $title . '" ' . $disabled_html . ' href="/wp-admin/post.php?post=' . $post->ID . '&action=edit&ttb_action=send_shared_res_email" class="button button-primary">' . __('Send Shared Reservation Email', 'tabticketbroker') . '</a>'; ?>
            <?php echo '<p>' . $title . '</p>'; ?>
        </div>
    <?php
    }

    function printOrdersReport($post_id) {
        // Get reservation
        $this->getReservation($post_id);

        // Get its assigned order items
        $reservation_orders = $this->reservation->getAssignedOrders();

        // If no orders, return html with message
        if (empty($reservation_orders)) {
            echo '<p>' . __('No orders assigned to this reservation', 'tabticketbroker') . '</p>';
            return;
        }

        // Set up report    
        $report_columns = array(
            'order_item_id' => __('Order item ID', 'tabticketbroker'),
            'order_no'      => __('Order number', 'tabticketbroker'),
            'order_status'  => __('Order status', 'tabticketbroker'),
            'customer_name' => __('Customer name', 'tabticketbroker'),
            'pax'           => __('Pax', 'tabticketbroker'),
            'voucher_owner' => __('Brotzeitbrett\'l', 'tabticketbroker'),
            'table_no'      => __('Table number', 'tabticketbroker'),
        );

        $report_columns_hidden = array(); // 'order_item_id' );

        $report_columns_sortable = array(
            'order_item_id' => array('order_item_id', 'asc'),
            'order_no'      => array('order_no', 'asc'),
            'order_status'  => array('product_title', 'asc'),
            'customer_name' => array('customer_name', 'asc'),
            'pax'           => array('pax', 'asc'),
            'voucher_owner' => array('voucher_owner', 'asc'),
            'table_no'      => array('table_no', 'asc'),
        );

        // Populate report data
        $report_data = array();
        $voucher_owners = is_array($this->reservation->getVoucherOwners()) ? $this->reservation->getVoucherOwners() : array();

        // Get reservation status
        $reservation_status = $this->reservation->getStatus();

        // Get reservation order item table numbers
        $reservation_order_item_table_nos = $this->reservation->getOrderItemTableNos();

        $calculated_assigned_pax = 0;
        foreach ($reservation_orders as $order_item_id => $order) {

            if ($reservation_status == 'closed' && array_key_exists($order['order_item_id'], $voucher_owners)) {
                $voucher_field = __('Allocated', 'tabticketbroker');
            } elseif ($reservation_status == 'closed' && ! array_key_exists($order['order_item_id'], $voucher_owners)) {
                $voucher_field = '-';
            } elseif (array_key_exists($order['order_item_id'], $voucher_owners)) {
                $voucher_field = '<a href="/wp-admin/post.php?post=' . $post_id . '&action=edit&ttb_action=remove_voucher_owner&voucher_owner_order_item_id=' . $order['order_item_id'] . '" class="button button-primary">' . __('Remove', 'tabticketbroker') . '</a>';
            } else {
                $voucher_field = '<a href="/wp-admin/post.php?post=' . $post_id . '&action=edit&ttb_action=add_voucher_owner&voucher_owner_order_item_id=' . $order['order_item_id'] . '" class="button button-secondary">' . __('Add', 'tabticketbroker') . '</a>';
            }

            $report_data[] = array(
                'order_item_id' => '<data>' . $order_item_id . '</data>',
                'order_no'      => '<a href="/wp-admin/post.php?post=' . $order['order_id'] . '&action=edit">' . $order['order_no'] . '</a>',
                'order_status'  => $order['status_display'],
                'customer_name' => $order['customer_fullname'],
                'pax'           => $order['pax'],
                'voucher_owner' => $voucher_field,
                'table_no'      => $reservation_order_item_table_nos[$order_item_id],
            );

            $calculated_assigned_pax += $order['pax'];
        }

        $per_page = 50;

        $list_table = new ListTable;

        $list_table->setupTable(
            $report_columns,
            $report_columns_hidden,
            $report_columns_sortable,
            $report_data,
            $per_page
        );

        $list_table->prepare_items();

        // Show the table
        $args = array(
            'hide_table_nav' => true,
            'hide_footer' => true,
            'hide_bottom_nav' => true,
        );

        $this->printPaxStats($calculated_assigned_pax);

        $list_table->displayWithoutNonce($args);
    }

    public function printPaxStats($calculated_assigned_pax) {
        // get total reservation pax
        $res_pax = $this->reservation->getPax();
        $open_pax = $this->reservation->getPaxOpen();
        $assigned_pax = $this->reservation->getPaxAssigned();

        // If there is a mismatch, reset the assigned pax count
        if ($assigned_pax != $calculated_assigned_pax) {
            $this->reservation->resetAssignedPaxCount($this->reservation->getId());
            $assigned_pax = $this->reservation->getPaxAssigned();
        }

        // Print the pax status
    ?>
        <div id="pax_stats">
            <h4><?php echo __('Pax', 'tabticketbroker') . ': ' . $res_pax . ' - ' . $assigned_pax . ' = ' . $open_pax; ?></h4>
        </div>
    <?php
    }

    /**
     * Generate the orders meta box content.
     */
    public function generateReservationOrderMetabox($post) {
        $this->printOrdersReport($post->ID);

        echo '<p><a class="button button-secondary" href="admin.php?page=' . $this->page_names['Assignments'] . '&reservation_id_direct=' . $this->reservation->getId() . '">' . __('Go to assigned orders', 'tabticketbroker') . '</a></p>';
    }

    public function subReservationsCallback() {
        // Initialise the sub reservations
        $this->sub_reservations = $this->reservation->getSubReservations();

        // Print page loader
        $this->printPageLoader();

        // If sub reservations exist, show report and delete options
        if (! empty($this->sub_reservations)) {
            // Just print the sub reservations report
            $this->printSubReservationsReport();

            // Print a confirmation form for deleting the sub reservations
            $this->printDeleteSubReservationsForm();

            return;
        }
        // If no sub reservations exist, show creation action and form
        else {
            // Print the sub reservation actions
            $this->printSubReservationActions();

            // Print the hidden form to create the sub reservations
            $this->printSubReservationCreationForm();

            // Print the script to create the sub reservations
            $this->printCreateSubReservationsScript();
        }
    }

    // print hidden page loader
    public function printPageLoader() {
        echo $this->getLoaderHtml();
    }

    public function printSubReservationCreationForm() {
        // Get the available pax
        $available_pax = $this->reservation->getPaxOpen();

        // Loop through the available pax and create sub reservations
        $this->sub_reservations = array();
        $l = 1;
        $split_pax = 2;
        for ($i = 0; $i < $available_pax; $i += $split_pax) {

            // If the last sub reservation, set the pax to the remainder
            if ($i + $split_pax >= $available_pax) {
                $split_pax = $available_pax - $i;
            }

            $this->sub_reservations[$l] = array(
                'code' => $this->reservation->getCode() . '.' . $l . ' - ' . $this->reservation->getSku(),
                'pax_open' => $split_pax,
                'order_item_ids' => array(),
                'notes' => '',
                'status' => array(
                    'at_head_office' => 1,
                    'shipped' => 0,
                    'at_pickup_office' => 0,
                    'collected' => 0,
                ),
            );

            $l++;
        }

    ?>
        <!-- The Sub reservation form -->
        <!-- Put the form in a wrapper to cover the whole screen and dim the background -->
        <div id="sub_reservations_form_wrapper" style="display:none; position:fixed; top:0; left:0; width:100%; height:100%; background-color:rgba(0,0,0,0.5); z-index:9998;"></div>
        <div id="sub_reservations_form" style="display:none; position:fixed; top:50%; left:50%; transform:translate(-50%, -50%); background-color:white; padding:20px; border:1px solid black; z-index:9999;">
            <h2><?php _e('Sub Reservations', 'tabticketbroker'); ?></h2>
            <p><?php _e('This will create the following sub reservations:', 'tabticketbroker'); ?></p>

            <table class="wp-list-table widefat fixed striped posts">
                <thead>
                    <tr>
                        <th><?php _e('Number', 'tabticketbroker'); ?></th>
                        <th><?php _e('Code', 'tabticketbroker'); ?></th>
                        <th><?php _e('Pax', 'tabticketbroker'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->sub_reservations as $key => $sub_res) : ?>
                        <tr id="sub_res_<?php echo $key; ?>">
                            <td><?php echo $key; ?></td>
                            <td><?php echo $sub_res['code']; ?></td>
                            <td><?php echo $sub_res['pax_open']; ?></td>
                            </t>
                        <?php endforeach; ?>
                </tbody>
            </table>

            <div class="wrapper">
                <a href="#" style="margin-top:5px;" class="button button-primary ttb-sub-reservation-confirm right"><?php _e('Continue', 'tabticketbroker'); ?></a>
                <a href="#" style="margin-top:5px;" class="button button-secondary ttb-sub-reservation-cancel"><?php _e('Cancel', 'tabticketbroker'); ?></a>
            </div>
        </div>
    <?php
    }

    public function printSubReservationsReport() {
        // if the reservation has sub reservations, show the sub reservations
        $sub_reservations = $this->reservation->getSubReservations();

        ob_start();
    ?>
        <div class="wrapper">
            <?php if (empty($sub_reservations)) : ?>
                <p><?php _e('No sub reservations', 'tabticketbroker'); ?></p>
            <?php else : ?>
                <table id="sub_reservations" class="wp-list-table widefat fixed striped posts">
                    <thead>
                        <tr>
                            <th class="number"><?php _e('Number', 'tabticketbroker'); ?></th>
                            <th class="code"><?php _e('Code', 'tabticketbroker'); ?></th>
                            <th class="pax"><?php _e('Pax', 'tabticketbroker'); ?></th>
                            <th class="notes"><?php _e('Notes', 'tabticketbroker'); ?></th>
                            <th class="status"><?php _e('Status', 'tabticketbroker'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sub_reservations as $key => $sub_res) : ?>
                            <tr id="sub_res_<?php echo $key; ?>">
                                <td><?php echo $key; ?></td>
                                <td><?php echo $sub_res['code']; ?></td>
                                <td><?php echo $sub_res['pax_open']; ?></td>
                                <td>
                                    <?php
                                    // show notes in editable textarea
                                    $notes = $sub_res['notes'];
                                    $notes = str_replace('<br>', "\n", $notes);
                                    $notes = str_replace('<br />', "\n", $notes);
                                    $notes = str_replace('<br/>', "\n", $notes);
                                    ?>
                                    <textarea class="ttb-sub-reservation-notes" data-sub-res-id="<?php echo $key; ?>"><?php echo $notes; ?></textarea>
                                </td>
                                <!-- Show all statuses and indicate which ones are set -->
                                <!-- Make each clickable to change the status -->
                                <td>
                                    <?php foreach ($sub_res['status'] as $status => $value) : ?>
                                        <?php
                                        // print clickable status
                                        $status_name = str_replace('_', ' ', $status);
                                        $status_name = ucwords($status_name);
                                        ?>
                                        <?php if ($value == 1) : ?>
                                            <a href="#" class="ttb-sub-reservation-status" data-status="<?php echo $status; ?>" data-sub-res-id="<?php echo $key; ?>"><span class="dashicons dashicons-yes"></span><?php echo $status_name; ?></a>
                                        <?php else : ?>
                                            <a href="#" class="ttb-sub-reservation-status" data-status="<?php echo $status; ?>" data-sub-res-id="<?php echo $key; ?>"><span class="dashicons dashicons-no"></span><?php echo $status_name; ?></a>
                                        <?php endif; ?>
                                        <br>
                                    <?php endforeach; ?>
                                </td>
                                </t>
                            <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>

            <?php $this->printSubReservationActions(); ?>

            <?php $this->printSubReservationStatusChangeScript(); ?>

        </div>
    <?php

        $html = ob_get_clean();

        echo $html;

        // Print the script to delete the sub reservations
        $this->printDeleteSubReservationsScript();
    }


    // Print sub reservation actions
    public function printSubReservationActions() {
        // Create button settings
        if (! empty($this->sub_reservations)) {
            $create_disabled_class = ' disabled hidden';
            $create_tooltip_text = __('There are already sub reservations created for this reservation.', 'tabticketbroker');
        } elseif ($this->reservation->getPaxOpen() == 0) {
            $create_disabled_class = ' disabled';
            $create_tooltip_text = __('There are no pax open to split therefore this function is disabled.', 'tabticketbroker');
        } else {
            $create_tooltip_text = __('Click to split this reservation into separate sub reservations', 'tabticketbroker');
            $create_disabled_class = '';
        }

        // delete button settings
        if (empty($this->sub_reservations)) {
            $delete_disabled_class = ' disabled hidden';
            $delete_tooltip_text = __('There are no sub reservations to delete.', 'tabticketbroker');
        } else {
            $delete_tooltip_text = __('Click to delete the sub reservations', 'tabticketbroker');
            $delete_disabled_class = '';
        }

    ?>
        <div class="sub-res-actions-wrapper">
            <a title="<?php echo $create_tooltip_text; ?>" href="#" class="button button-primary ttb-sub-reservation<?php echo $create_disabled_class; ?>"><?php _e('Create Sub-reservations', 'tabticketbroker'); ?></a>
            <a title="<?php echo $delete_tooltip_text; ?>" href="#" class="button button-primary ttb-sub-reservation-delete<?php echo $delete_disabled_class; ?>"><?php _e('Delete Sub-reservations', 'tabticketbroker'); ?></a>
        </div>
    <?php
    }

    /**
     * Print the form to confirm the deletion of the sub reservations
     */
    public function printDeleteSubReservationsForm() {
    ?>
        <!-- The delete sub reservation form -->
        <!-- Put the form in a wrapper to cover the whole screen and dim the background -->
        <div id="delete_sub_reservations_form_wrapper" style="display:none; position:fixed; top:0; left:0; width:100%; height:100%; background-color:rgba(0,0,0,0.5); z-index:9998;"></div>
        <div id="delete_sub_reservations_form" style="display:none; position:fixed; top:50%; left:50%; transform:translate(-50%, -50%); background-color:white; padding:20px; border:1px solid black; z-index:9999;">
            <h2><?php _e('Delete Sub Reservations', 'tabticketbroker'); ?></h2>
            <p><?php _e('Are you sure you want to delete the sub reservations?', 'tabticketbroker'); ?></p>

            <div class="wrapper">
                <a href="#" style="margin-top:5px;" class="button button-primary ttb-sub-reservation-delete-confirm right"><?php _e('Continue', 'tabticketbroker'); ?></a>
            </div>
        </div>
    <?php
    }

    // print printSubReservationStatusChangeScript
    // This script is used to change the status of the sub reservations
    public function printSubReservationStatusChangeScript() {
    ?>
        <script>
            jQuery(document).ready(function($) {

                // Get the sub reservations
                var sub_reservations = <?php echo json_encode($this->sub_reservations); ?>;

                // When the sub reservation status is clicked
                $('.ttb-sub-reservation-status').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Show the page loader
                    $('#loader_dim_page').css('display', 'block');

                    // Get the sub reservation id
                    var sub_res_id = $(this).data('sub-res-id');

                    // Get the status
                    var status = $(this).data('status');

                    // Get the sub reservation
                    var sub_res = sub_reservations[sub_res_id];

                    // Get the current status
                    var current_status = sub_res['status'][status];

                    // If the status is 1, set to 0, else set to 1
                    var new_status = current_status == 1 ? 0 : 1;

                    // Set all other statuses to 0
                    $.each(sub_res['status'], function(key, value) {
                        if (key != status) {
                            sub_res['status'][key] = 0;
                        }
                    });

                    // Set the new status of the clicked status
                    sub_res['status'][status] = new_status;

                    // Update the sub reservation
                    sub_reservations[sub_res_id] = sub_res;

                    // Update the status dash icons for each status
                    $.each(sub_res['status'], function(key, value) {
                        if (value == 1) {
                            $('#sub_res_' + sub_res_id + ' .ttb-sub-reservation-status[data-status="' + key + '"] .dashicons').removeClass('dashicons-no').addClass('dashicons-yes');
                        } else {
                            $('#sub_res_' + sub_res_id + ' .ttb-sub-reservation-status[data-status="' + key + '"] .dashicons').removeClass('dashicons-yes').addClass('dashicons-no');
                        }
                    });

                    // Save status change
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: {
                            action: 'save_sub_reservations',
                            'sub_reservations': sub_reservations,
                            reservation_id: $('#post_ID').val(),
                        },
                        success: function(response) {
                            $('#loader_dim_page').hide();
                            // If successful, refresh the page
                            console.log('Success: ' + response);
                        },
                        error: function(response) {
                            $('#loader_dim_page').hide();
                            // If error, show the error
                            alert(response);
                        }
                    });

                    // Hide the page loader
                });

                // When the sub reservation notes are changed
                $('.ttb-sub-reservation-notes').change(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Show the page loader
                    $('#loader_dim_page').css('display', 'block');

                    // Get the sub reservation id
                    var sub_res_id = $(this).data('sub-res-id');

                    // Get the notes
                    var notes = $(this).val();

                    // Get the sub reservation
                    var sub_res = sub_reservations[sub_res_id];

                    // Set the notes
                    sub_res['notes'] = notes;

                    // Update the sub reservation
                    sub_reservations[sub_res_id] = sub_res;

                    // Save Notes
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: {
                            action: 'save_sub_reservations',
                            'sub_reservations': sub_reservations,
                            reservation_id: $('#post_ID').val(),
                        },
                        success: function(response) {
                            $('#loader_dim_page').hide();
                            // If successful, refresh the page
                            console.log(response);
                        },
                        error: function(response) {
                            $('#loader_dim_page').hide();
                            // If error, show the error
                            alert(response);
                        }
                    });
                });

            });
        </script>
    <?php

    }

    public function printCreateSubReservationsScript() {
    ?>
        <script>
            // Show the sub reservation form when the button is clicked
            jQuery(document).ready(function($) {
                // When the sub reservation button is clicked
                $('.ttb-sub-reservation').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Unhide the sub reservation form
                    $('#sub_reservations_form_wrapper').show();
                    $('#sub_reservations_form').show();
                });

                // Hide the sub reservation form when the wrapper is clicked
                $('#sub_reservations_form_wrapper').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Hide the sub reservation form
                    $('#sub_reservations_form_wrapper').hide();
                    $('#sub_reservations_form').hide();
                });

                // When the sub reservation cancel button is clicked
                $('.ttb-sub-reservation-cancel').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Hide the sub reservation form
                    $('#sub_reservations_form_wrapper').hide();
                    $('#sub_reservations_form').hide();
                });

                // AJAX call to save the sub reservation array to the reservation
                $('.ttb-sub-reservation-confirm').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Hide the sub reservation form
                    $('#sub_reservations_form_wrapper').hide();
                    $('#sub_reservations_form').hide();

                    // Show the page loader
                    $('#loader_dim_page').css('display', 'block');

                    // Get the sub reservation array
                    var sub_reservations = <?php echo json_encode($this->sub_reservations); ?>;

                    // Save new sub reservations
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: {
                            action: 'save_sub_reservations',
                            'sub_reservations': sub_reservations,
                            reservation_id: $('#post_ID').val(),
                        },
                        success: function(response) {
                            // If successful, refresh the page
                            console.log(response);
                            location.reload();
                        },
                        error: function(response) {
                            // If error, show the error
                            alert(response);
                        }
                    });
                });
            });
        </script>
    <?php
    }

    /**
     * Print the script to delete the sub reservations
     */
    public function printDeleteSubReservationsScript() {
    ?>
        <script>
            // Show the sub reservation form when the button is clicked
            jQuery(document).ready(function($) {
                // When the sub reservation button is clicked
                $('.ttb-sub-reservation-delete').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Unhide the sub reservation form
                    $('#delete_sub_reservations_form_wrapper').show();
                    $('#delete_sub_reservations_form').show();
                });

                // Hide the sub reservation form when the wrapper is clicked
                $('#delete_sub_reservations_form_wrapper').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Hide the sub reservation form
                    $('#delete_sub_reservations_form_wrapper').hide();
                    $('#delete_sub_reservations_form').hide();
                });

                // When the sub reservation confirm button is clicked
                $('.ttb-sub-reservation-delete-confirm').click(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // Hide the sub reservation form
                    $('#delete_sub_reservations_form_wrapper').hide();
                    $('#delete_sub_reservations_form').hide();

                    // Submit the form
                    $('#post').submit();

                });

                // AJAX call to delete the sub reservations
                $('#post').submit(function(e) {
                    // Prevent default
                    e.preventDefault();

                    // AJAX call to delete the sub reservations
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        data: {
                            action: 'delete_sub_reservations',
                            reservation_id: $('#post_ID').val(),
                        },
                        success: function(response) {
                            // If successful, refresh the page
                            location.reload();
                        },
                        error: function(response) {
                            // If error, show the error
                            alert(response);
                        }
                    });
                });
            });
        </script>
    <?php
    }

    /**
     * Retrieve the audit log for the reservation
     * This works from the config set in the reservation class, specifically in the getAuditLogMetaKeys method
     * Note that the audit log meta keys are prepended with audit_log_ automatically, and this is what we look for
     */
    public function auditlogCallback() {
        $audit_log_return = array();
        $audit_log_meta_keys = $this->reservation->getAuditLogMetaKeys();

        // Loop through the meta keys and get the audit log
        foreach ($audit_log_meta_keys as $meta_key => $change_name) {
            $audit_log = JMD_Logger::metalog($this->reservation->getId(), 'audit_log_' . $meta_key);

            // If there is an audit log, add to the audit log array
            if (!empty($audit_log)) {
                foreach ($audit_log as $log_entry) {
                    $log_entry['change_name'] = $change_name;
                    $audit_log_return[] = $log_entry;
                }
            }
        }

        // Sort the audit log array by date
        usort($audit_log_return, function ($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        });

        // Reverse the array so the latest changes are at the top
        $audit_log_return = array_reverse($audit_log_return);

        // Find all change values in the array and add spaces before and after '->' if it exists
        foreach ($audit_log_return as $key => $log) {
            $change = $log['change'];
            $change = str_replace('->', ' -> ', $change);
            $audit_log_return[$key]['change'] = $change;
        }

        // Print the audit log
        $this->printAuditLog($audit_log_return);
    }

    /**
     * Print the audit log
     */
    public function printAuditLog($audit_log) {
        // If no audit log, return
        if (empty($audit_log)) {
            echo '<p>' . __('No history available', 'tabticketbroker') . '</p>';
            return;
        }

        // Print basic HTML table with audit log data
    ?>
        <table id="audit_log" class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th><?php _e('Date', 'tabticketbroker'); ?></th>
                    <th><?php _e('User', 'tabticketbroker'); ?></th>
                    <th><?php _e('Change Type', 'tabticketbroker'); ?></th>
                    <th><?php _e('Change', 'tabticketbroker'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($audit_log as $log) : ?>
                    <tr>
                        <td><?php echo esc_html($log['date']); ?></td>
                        <td><?php echo esc_html($log['user']); ?></td>
                        <td><?php echo esc_html($log['change_name']); ?></td>
                        <td><?php echo esc_html($log['change']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php
    }
}
