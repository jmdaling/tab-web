<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Base;
namespace Inc\Classes;

use Inc\Base\BaseController;
use Inc\Tools\OrderTools;

/*
    Received        = Triggered during order received email
    Confirmed       = Triggered during order confirmed email
    Paid            = Triggered during payment received email OR during checkout if paid. This status is linked to the is_paid value of the order
    Ship Ready      = 
    Shipped         = 
    Complete        = 
*/
Class OrderLifeCycle extends BaseController
{
    public function register() {
        // Add action to flag order lifecycle events
        add_action('woocommerce_order_status_changed', array($this, 'logOrderLifeCycleEvent'), 10, 4);
    }

    public function logOrderLifeCycleEvent($order_id, $old_status, $new_status, $order) {
        $ready_to_ship_done_statuses = array(
            'ready-to-ship',
            'labelled',
            'shipped',
            'completed',
        );

        // If the order is in one of these statuses, mark it as ready to ship
        if ( in_array( $new_status, $ready_to_ship_done_statuses ) ) {
            update_post_meta($order_id, '_ttb_prg_ready_to_ship', 1);
        } else {
            update_post_meta($order_id, '_ttb_prg_ready_to_ship', 0);
        }

        // Set Shipped value
        $shipped_done_statuses = array(
            'shipped',
            'completed',
        );

        // If the order is in one of these statuses, mark it as shipped
        if ( in_array( $new_status, $shipped_done_statuses ) ) {
            update_post_meta($order_id, '_ttb_prg_shipped', 1);
        } else {
            update_post_meta($order_id, '_ttb_prg_shipped', 0);
        }

        // Set completed value
        if ( $new_status == 'completed' ) {
            update_post_meta($order_id, '_ttb_prg_completed', 1);
        } else {
            update_post_meta($order_id, '_ttb_prg_completed', 0);
        } 
    }
    
    /**
     * Retrieves data for the meta box in the backend to display order life cycle process
     */
    public function getOrderProgressData( object $order )
    {
        // Get the order from the post id
        $order_id = $order->get_id();

        $order_meta = array(
            'order_id'   => $order_id,
            'language'   => '',
            'invoice_no' => '',
            'progress'   => array(),
        );

        // Get invoice no, used in the admin_order_metabox.php template
        // use the method in OrderTools which also checks if FakturPro has created an invoice
        $order_meta['invoice_no'] = OrderTools::getAndCopyInvoiceNo( $order_id );

        // Get the language from the post
        $order_meta['language'] = get_post_meta( $order_id, '_ttb_order_language', true ) ? get_post_meta( $order_id, '_ttb_order_language', true ) : '';
        
        $order_meta['language'] = ( $order_meta['language'] == '' ? $this->tab_default_lang_code : $order_meta['language'] );

        
        // Get last first check value
        $first_check_log    = get_post_meta( $order_id, '_ttb_first_check_log', true );
        $last_first_check   = $first_check_log ? end( $first_check_log ) : array();
        // Because this log can be empty, we need to check if the key exists and set the vars here
        $first_check_status = isset( $last_first_check['first_check'] ) ? filter_var( $last_first_check['first_check'], FILTER_VALIDATE_BOOLEAN) : false;
        
        // Get Order progress
        $order_progress = array(
           '_ttb_prg_received'            => array( 'title' => 'Received',    'value' => 1 ),
           '_ttb_prg_invoiced'            => array( 'title' => 'Invoiced',    'value' => 0 ),
           '_ttb_prg_paid'                => array( 'title' => 'Paid',        'value' => $order->is_paid() ),
           '_ttb_prg_ready_to_ship'       => array( 'title' => 'Ship Ready',  'value' => 0 ),
           '_ttb_prg_first_check'         => array( 'title' => 'First Check', 'value' => $first_check_status ),
           '_ttb_prg_shipped'             => array( 'title' => 'Shipped',     'value' => 0 ),
           '_ttb_prg_completed'           => array( 'title' => 'Complete',    'value' => 0 ),
        );

        foreach ( $order_progress as $key => $value ) {
            // Skip settings already handled
            if ( $key == '_ttb_prg_paid' || $key == '_ttb_prg_first_check' || $key == '_ttb_prg_received' ) continue;
            
            // Handle invoicing status differently
            if ( $key == '_ttb_prg_invoiced' ) {
                // Check if invoice no is blank
                $order_progress[$key]['value'] = ( $order_meta['invoice_no'] == '' ? 0 : 1 );

                continue;
            };

            // Set each value according to the current meta value
            $order_progress[$key]['value']  = get_post_meta( $order_id, $key, true ) ? get_post_meta( $order_id, $key, true ) : 0;
        }

        $order_meta['progress'] = $order_progress;

        return $order_meta;
    }

    // public function hideLangVar()
    // {
    //     $query = new \WC_Order_Query( array(
    //         'limit' => -1,
    //         'orderby' => 'date',
    //         'order' => 'DESC',
    //         'return' => 'ids',
    //     ) );
    //     $order_ids = $query->get_orders();

    //     foreach ( $order_ids as $order_id ) {
    //         $order_locale = get_post_meta( $order_id, 'ttb_order_language', true );
    //         update_post_meta( $order_id, '_ttb_order_language', $order_locale );
    //         delete_post_meta( $order_id, 'ttb_order_language' );
    //     }
        
    // }
    

    // public function markInvoiced()
    // {
    //     $query = new \WC_Order_Query( array(
    //         'limit' => -1,
    //         'orderby' => 'date',
    //         'order' => 'DESC',
    //         'return' => 'ids',
    //     ) );
    //     $order_ids = $query->get_orders();

    //     foreach ( $order_ids as $order_id ) {
    //         $inv_no = get_post_meta( $order_id, '_ttb_invoice_no', true );
    //         if ( $inv_no == '' )
    //             update_post_meta( $order_id, '_ttb_prg_invoiced', 1 );
    //         else
    //             update_post_meta( $order_id, '_ttb_prg_invoiced', 0 );   
    //     }
    // }
    
    // public function markAllOrdersAsReceived()
    // {

    //     // Get 10 most recent order ids in date descending order.
    //     $query = new \WC_Order_Query( array(
    //         'limit' => -1,
    //         'orderby' => 'date',
    //         'order' => 'DESC',
    //         'return' => 'ids',
    //     ) );
    //     $order_ids = $query->get_orders();

    //     // echo '<h1>Order Count = '.count( $order_ids ).'</h1>';
        
    //     foreach ( $order_ids as $order_id ) {
    //         update_post_meta( $order_id, '_ttb_prg_received', 1 );
    //     }
    // }

}