<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

// Checks some configuration settings in order to alert developers that settings in the DEV environment are set to production values

class ConfigCheck extends BaseController
{
    public function register()
    {
        add_action('init', array( $this, 'checkConfig' ) );
    }

    public function checkConfig()
    {
        $low_stock_recipent_prod = HardCoded::getLowStockEmailRecipient();
        
        // Get woocommerce inventory email address
        $low_stock_recipent_setting = get_option( 'woocommerce_stock_email_recipient' );       

        // Check if the inventory email is set to the production value
        if ( WP_ENVIRONMENT_TYPE != 'prod' && $low_stock_recipent_setting == $low_stock_recipent_prod ) {
            // Show an admin notice
            $this->addFlashNotice( 'TabTicketBroker: WARNING!!! WooCommerce Inventory Email is set to production value = '. $low_stock_recipent_prod .' and you are on a '. strtoupper(WP_ENVIRONMENT_TYPE) .' environment', 'error', true );
            
        } else if ( WP_ENVIRONMENT_TYPE == 'prod' && $low_stock_recipent_setting != $low_stock_recipent_prod ) {
            // Show an admin notice
            $this->addFlashNotice( 'TabTicketBroker: WARNING!!! WooCommerce Inventory Email is set to development value = ' . $low_stock_recipent_setting . ' and you are on the '. strtoupper(WP_ENVIRONMENT_TYPE) .' environment. It should be '. $low_stock_recipent_prod, 'error', true );
        }
    }
}