<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

class HardCoded extends BaseController
{

    /**
     * Used in WcCustomization for product information (there are CSS on the classes)
     * and also in TabProductList (as info in an array which is not used yet) 
     */
    public function getEventStartTimeInfo()
    {
        ob_start();
        ?>
        <div id="info_tab" class="tabcontent" style="display:block;">
            <h4><strong><?php _e( 'Reservation Times', 'tabticketbroker' ); ?></strong></h4>
            <ul>
                <li><?php _e( 'Lunchtime: Start 10:00 - 13:00; End 16:00', 'tabticketbroker' ); ?></li>
                <li><?php _e( 'Afternoon: Start 14:30 - 15:30; End 18:30', 'tabticketbroker' ); ?></li>
                <li><?php _e( 'Evening: Start 16:45 - 18:00; End 22:30', 'tabticketbroker' ); ?></li>
            </ul>
            <p><?php _e( 'The stated start times of the reservations vary depending on the beer hall area and cannot be changed. You will find out the binding times upon receipt of the official reservation documents.', 'tabticketbroker' ); ?></p>
            <p><?php _e( 'The official reservation time must be strictly adhered to as your table(s) are otherwise reassigned to other guests.', 'tabticketbroker' ); ?></p>
            <p><strong><?php _e( 'Make sure you plan enough time for your arrival !!!', 'tabticketbroker' ); ?></strong></p>

            <p><?php _e( 'For more information, please refer to our ', 'tabticketbroker' ); ?><a href="/agb/" style="color:#1D7EA0;"> <?php esc_html_e( 'Terms and Conditions', 'tabticketbroker' ); ?>.</a></p>

        </div>

        <?php
        
        return trim( ob_get_clean() );
    }

    /**
     * Used in WcCustomization for product information (there are CSS on the classes)
     */
    public function getEventShippingInfo()
    {
        ob_start();
        ?>
            <div id="shipping_tab" class="tabcontent">
                <p><?php _e( 'We will send the reservation documents listed below by express after they have been issued by the beer hall operators and made available by our suppliers.', 'tabticketbroker' ); ?></p>
                              
                <p><?php _e( 'You will receive an envelope from us with the following content (no e-tickets possible):', 'tabticketbroker' ); ?></p>
                <ul>
                    <li> <?php _e( 'Reservation letter', 'tabticketbroker' ); ?></li>
                    <li> <?php _e( 'Meal vouchers', 'tabticketbroker' ); ?></li>
                    <li> <?php _e( 'Wristbands and tickets (optional)', 'tabticketbroker' ); ?></li>
                    <li> <?php _e( 'Customer information', 'tabticketbroker' ); ?></li>
                </ul> 

                <p><?php _e( 'You will receive the documents no later than 5 days before your chosen day of the event.', 'tabticketbroker' ); ?></p>
                <p><?php _e( 'Generally we recommend shipping within Germany (for international guests e.g. to the hotel) to reduce the ever present risks involved with shipping. Alternatively, you can also pick up the documents in Munich / Berlin.', 'tabticketbroker' ); ?></p>
                <p><?php _e( 'For more information, please see our <a href="/agb" style="color:#1D7EA0;">terms and conditions</a>.', 'tabticketbroker' ); ?></p>

            </div>
        <?php
        
        return trim( ob_get_clean() );
    }

    /**
     * Used in WcCustomization for product information (there are CSS on the classes)
     */
    public function getEventPriceInfo()
    {
        ob_start();
        ?>
            <div id="price_tab" class="tabcontent">
                <p>
                    <?php _e( 'All prices include 19% VAT. In addition to the prices quoted, we charge a flat-rate shipping fee for the delivery of the reservation documents. If you have questions about the content of the Oktoberfest and other information about the Oktoberfest, you can visit our FAQ page. The delivery is insured worldwide to all countries and regions.', 'tabticketbroker' ); ?>
                </p>
                <p>
                    <?php _e( 'In accordance with Section 271 of the German Civil Code (BGB), we deliver the documents on the date stated in the offer or in the delivery time stated after the offer, at the latest 5 days before the planned day of the event, but not before payment of the purchase price. With our reservations there is no waiting time at the entrance. You can simply show the reservation, go inside and take your seat right at the table. Your seats must be occupied no later than 15 minutes after the start of the reservation. Otherwise, the owners of the marquees reserve the right to release the tables for other guests. If the right reservation is not available for you or you would like to combine several tables in one beer hall, you can contact us at any time:', 'tabticketbroker' ); ?>
                </p>
                <p><b>info@oktoberfest-tischreservierungen.de |  +49 (0)89 716 718 499</b></p>
            </div>
        <?php
        
        return trim( ob_get_clean() );
    }
    /**
     * Used in WcCustomization for product information
     */
    public function getEventMoreInfo()
    {
        ob_start();
        ?>
        <div id="prod_info_more"> 
            <img src="/wp-content/uploads/2021/05/Background-Press-1440x296-1.png" alt="People at the Oktober Fest">
            <div id="more_info_text">
                <div id="more_info_title">
                    <h4><?php _e( 'Didn\'t find what you were looking for?', 'tabticketbroker' ); ?></h4>
                    <h4><?php _e( 'We are happy to help!', 'tabticketbroker' ); ?></h4>
                </div>
                <div id="more_info_btns">
                    <div class="wp-block-button is-style-fill">
                        <a class="wp-block-button__link has-background tab-button" target="_blank" href="<?php echo $this->getLanguageLink( '/preis-anfrage' ); ?>"><?php _e( 'Price Inquiry', 'tabticketbroker' ); ?></a>
                    </div>
                    <div class="wp-block-button is-style-fill">
                        <a class="wp-block-button__link has-background tab-button" href="<?php echo $this->getLanguageLink( '/tischreservierungen-buchen' ); ?>"><?php _e( 'All Products', 'tabticketbroker' ); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        
        return trim( ob_get_clean() );
    }

    /**
     * Used in WcCustomization for product information
     */
    public function getEventMoreInfo_tt()
    {
        ob_start();
        ?>
        <div id="prod_info_more"> 
            <div id="more_info_container">
                <div id="more_info_text">
                    <div id="more_info_title">
                        <h4><?php _e( 'Didn\'t find what you were looking for?', 'tabticketbroker' ); ?></h4>
                        <h4><?php _e( 'We are here to help!', 'tabticketbroker' ); ?></h4>
                    </div>
                    <div id="more_info_btns">
                        <div class="wp-block-button is-style-fill">
                            <a class="wp-block-button__link has-background tab-button" target="_blank" href="<?php echo $this->getLanguageLink( '/preis-auf-anfrage/' ); ?>"><?php _e( 'Price Inquiries', 'tabticketbroker' );?></a>
                        </div>
                        <div class="wp-block-button is-style-fill">
                            <a class="wp-block-button__link has-background tab-button" href="<?php echo $this->getLanguageLink( '/tickets-kaufen/' ); ?>"><?php _e( 'All Products', 'tabticketbroker' );?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        
        return trim( ob_get_clean() );
    }

    public function getTableLayoutImageArray()
    {
        return array(
            2  => site_url('wp-content/uploads/2021/07/2-Personen.png'),
            3  => site_url('wp-content/uploads/2021/07/3-Personen.png'),
            4  => site_url('wp-content/uploads/2021/07/4-Personen.png'),
            5  => site_url('wp-content/uploads/2021/07/5-Personen.png'),
            6  => site_url('wp-content/uploads/2021/07/6-Personen.png'),
            7  => site_url('wp-content/uploads/2021/07/7-Personen.png'),
            8  => site_url('wp-content/uploads/2021/07/8-Personen.png'),
            10 => site_url('wp-content/uploads/2021/07/10-Personen.png'),
            16 => site_url('wp-content/uploads/2021/07/16-Personen.png'),
            20 => site_url('wp-content/uploads/2021/07/20-Personen.png'),
            24 => site_url('wp-content/uploads/2021/07/24-Personen.png'),
            30 => site_url('wp-content/uploads/2021/07/30-Personen.png'),
            32 => site_url('wp-content/uploads/2021/07/32-Personen.png'),
            40 => site_url('wp-content/uploads/2021/07/40-Personen.png'),
        );
    }

    /**
     * HTML for small box only displayed on top overlaying the page title which 
     * acts as a toggle button for the filter. Hidden by default.
     */
    public function getMobileFilterButton( bool $hide = false )
    {
        $mobile_filter_toggle_html = '';
        ob_start();
        ?>
        <div id="ttb_filter_toggle" class="toggle-filter<?php echo ( $hide ? ' hide-toggle-filter' : '' ); ?>">
            <div class="row">
                <p><?php _e( 'Search Reservations', 'tabticketbroker' ); ?>:</p>
            </div>
            <div class="row">
                <div class="column"><p><?php _e( 'Date', 'tabticketbroker' ); ?></p></div>
                <div class="column"><p><?php _e( 'People', 'tabticketbroker' ); ?></p></div>
                <div class="column"><p><?php _e( 'Timeslot', 'tabticketbroker' ); ?></p></div>
            </div>
        </div>
        <?php

        $mobile_filter_toggle_html = ob_get_clean();

        return $mobile_filter_toggle_html;
    }

    public function getAvailableTimes()
    {
        $times_available  = array(
            '09:00',
            '09:15',
            '09:30',
            '09:45',
            '10:00',
            '10:15',
            '10:30',
            '10:45',
            '11:00',
            '11:15',
            '11:30',
            '11:45',
            '12:00',
            '12:15',
            '12:30',
            '12:45',
            '13:00',
            '13:15',
            '13:30',
            '13:45',
            '14:00',
            '14:15',
            '14:30',
            '14:45',
            '15:00',
            '15:15',
            '15:30',
            '15:45',
            '16:00',
            '16:15',
            '16:30',
            '16:45',
            '17:00',
            '17:15',
            '17:30',
            '17:45',
            '18:00',
            '18:15',
            '18:30',
            '18:45',
            '19:00',
            '19:15',
            '19:30',
            '19:45',
            '20:00',
            '20:15',
            '20:30',
            '20:45',
            '21:00',
            '21:15',
            '21:30',
            '21:45',
            '22:00',
            '22:15',
            '22:30',
            '22:45',
            '23:00',
            '23:15',
            '23:30',
            '23:45',
            '00:00',
        );

        return $times_available;
    }

    public static function getSpecialPackageProdTitle()
    {
        return 'Spezial Paket';
    }

    // public static function getIgnoredReservationStatuses()
    // {
    //     return array( 'soldout', 'unavailable' );
    // }

    // public static function getOfferedStatus()
    // {
    //     return 'offer';
    // }

    /**
     * Return the Reservation slugs of the different type of statuses.
     * This method only exists to be able to put these settings in a proper UI if required.
     * Reservation statuses are currently dynamic and can be changed by admin users. As the 
     * procurement, sales and assignment process evolves these will become more clear and 
     * defined in a more permanent fashion. At the moment this allows for more customization.
     * @param @type The type of status that is required, typically the same as the slug. But 
     *              there are exceptions, see e.g. assigned status
     * @return array The reservation status slug(s)
     */

    public static function getReservationStatus( $type = '' )
    {
        if ( $type = '' ) return 'Error, no reservation status type specified!';
        
        $hardcoded_res_statuses = array(
            'ignored'       => array( 'soldout', 'unavailable' ),
            'offered'       => 'offer',
            'assigned'      => 'booked',
            'available'     => 'available',
        );

        return $hardcoded_res_statuses;
    }

    public static function getBankDetails()
    {
    ?>
        <div id="bank_details" style="float:left;">
            <p>
                <b><?php _e( 'Bank Transfer', 'tabticketbroker' ); ?></b><br>
                Deutsche Bank<br>
                IBAN: DE73100708480343821500<br>
                SWIFT/BIC: DEUTDEDB110
            </p>
        </div>
    <?php
    }

    // Hardcoded legal text
    public static function getLegalShopText()
    {
        return __( '*We start selling our products before delivery from our suppliers and before table reservations and the associated documents are issued by the beer hall operators. We make a binding commitment to our customers to procure the ordered table reservations. If this does not succeed, you are entitled to claims against us, in particular for repayment of the purchase price. As a secondary market provider, we do not receive reservations directly from the beer hall operator, but from people who have purchased them directly from the beer halls. The reservation confirmation, which also contains the name of the first customer, cannot be changed to your name. In addition, the admissibility of transferring table reservations is legally controversial. Should there be any inconvenience when visiting the beer hall, our customer service is always available at short notice.', 'tabticketbroker' );
    }

    public static function getProductAreaCodes()
    {
        $area_codes = array(
            ''  => 'kein-bestimmter-bereich',
            'B' => 'balkon',
            'X' => 'box',
            'M' => 'mittelschiff',
            'F' => 'bereichsbuchung-auf-anfrage',
            'N' => 'unknown',
        );

        return $area_codes;
    }

    /**
     * Centralize company details. This is used in the email footer.
     * To be put into configuation options in the future.
     * @return array
     */
    public static function getOfficeDetails()
    {
        $office_details = array(
            'name'          => 'Tab Ticketbroker GmbH',
            'CEO'           => 'Steffen Kolms',
            'address'       => 'Gustav-Meyer-Allee 25',
            'zip'           => '13355',
            'city'          => 'Berlin',
            'country'       => 'Deutschland',
            
            'email'         => 'keyaccount@tab-ticketbroker.de',
            'mobile_phone'  => '+49 (0) 151 6706 7789',
            'phone'         => '+49 (0) 89 716 718 499',
            'phone_link'    => 'tel:+4989716718499',

            'url_ofest'     => 'www.oktoberfest-tischreservierungen.de',
            'url_tab'       => 'www.tab-ticketbroker.de',

            // OLD PHONE NOs (kept for reference, to be removed later)
            'phone_old_email_footer' => '+49 (0) 30 / 89 75 36 44',
        );

        return $office_details;
    }

    public static function getProductTimeCutoff()
    {
        return '17:00:00';
    }

    public static function getLowStockEmailRecipient()
    {
        return 'keyaccount@tab-ticketbroker.de';
    }

    public static function getViewName()
    {
        if ( TTB_SITE == 'ofest' ) {
            $view_name = 'vw_product_variations_pivot';
        } else {
            $view_name = 'vw_product_variations_pivot_tt';
        }

        return $view_name;
    }
}