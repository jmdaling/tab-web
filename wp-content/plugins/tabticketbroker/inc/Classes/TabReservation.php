<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Tools\ExportTools;
use Inc\Base\BaseController;
use Inc\Classes\AssignmentManager;

class TabReservation extends BaseController {
    public $post                    = null;
    public $reservations            = null;                   // Default set to null, if results are empty, it will be set to an empty array

    public $id                      = null;
    public $sku                     = null;
    public $order_item_ids          = array();
    public $order_item_table_nos    = array();
    public $voucher_owners          = array();
    public $order_count             = 0;

    public $supplier_id             = null;
    public $supplier_name           = null;
    public $owner_name              = null;
    public $table_no                = null;
    public $code                    = null;
    public $start_time              = null;
    public $end_time                = null;
    public $cost_price              = null;
    public $source_url              = null;

    public $product_id              = null;
    public $product_title           = null;
    public $event_year              = null;
    public $date                    = null;
    public $display_date            = null;
    public $pax                     = 0;
    public $timeslot                = null;
    public $timeslot_order          = null;
    public $area                    = null;
    public $status                  = null;
    public $notes                   = null;
    public $voucher_notes           = null;

    public $vouchers_received       = 0;
    public $letter_received         = 0;
    public $letter_ready            = 0;
    public $docs_complete           = 0;
    public $ready_to_ship           = 0;
    public $ready_to_ship_date      = null;

    public $customer_no             = null;
    public $booking_no              = null;

    public $is_locked               = false;
    public $locked_msgs             = array();

    public $dimension               = null;
    public $res_statuses            = null;

    public $sub_res_array         = array();

    // Hidden
    public $pax_assigned            = 0;
    public $pax_open                = 0;

    public $audit_log_meta_keys     = array();

    public function __construct($post_id = null, $reset_dynamic_vars = true) {
        // Return if no post_id
        if (! $post_id) return;

        // Load db properties
        if ($post_id) $this->setReservation(intval($post_id));

        // Load the computed properties
        if ($reset_dynamic_vars)
            $this->resetDynamicVars();

        // Set the meta keys that need to be logged
        $this->setAuditLogConfig();
    }

    public function getPost() {
        return $this->post;
    }
    public function setPost($post) {
        $this->post = $post;
    }

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getSku($dynamic_sku = false) {
        if (! $dynamic_sku) return $this->sku;
        else return $this->getDynamicSku();
    }
    public function setSku($sku) {
        $this->sku = $sku;
    }

    /**
     * Gets the order item ids
     * @return array of order item ids
     */
    public function getOrderItemIds() {
        return $this->order_item_ids;
    }
    public function setOrderItemIds(array $order_item_ids) {
        $this->order_item_ids = $order_item_ids;
    }

    /**
     * Gets the order item table nos
     * @return array of order item table nos, order item ids are the keys, table nos are the values
     */
    public function getOrderItemTableNos() {
        return $this->order_item_table_nos;
    }
    public function setOrderItemTableNos($order_item_table_nos) {
        $order_item_ids = $this->getOrderItemIds();

        // If it is empty, set it to an empty array using the order item ids as the keys
        if (empty($order_item_table_nos)) {
            $order_item_table_nos = array_fill_keys($order_item_ids, '');
        }

        // loop through the order item ids and set the table nos to blank if they are not set
        foreach ($order_item_ids as $order_item_id) {
            $order_item_table_nos[$order_item_id] = isset($order_item_table_nos[$order_item_id]) ? $order_item_table_nos[$order_item_id] : '';
        }

        $this->order_item_table_nos = $order_item_table_nos;
    }

    public function getVoucherOwners() {
        return $this->voucher_owners;
    }
    public function setVoucherOwners($voucher_owners) {
        $this->voucher_owners = $voucher_owners;
    }

    public function getOrderCount() {
        return $this->order_count;
    }
    public function setOrderCount($order_count) {
        $this->order_count = intval($order_count);
    }

    public function getSupplierId() {
        return $this->supplier_id;
    }
    public function getSupplierName() {
        return $this->supplier_name;
    }
    public function setSupplierId($supplier_id) {
        $this->supplier_id = intval($supplier_id);
        $supplier = new TabSupplier($supplier_id);
        $this->supplier_name = $supplier->getName();
    }

    public function getOwnerName() {
        return $this->owner_name;
    }
    public function setOwnerName($owner_name) {
        $this->owner_name = $owner_name;
    }

    public function getTableNo() {
        return $this->table_no;
    }
    public function setTableNo($table_no) {
        $this->table_no = $table_no;
    }

    public function getCode() {
        return "RS" . $this->getId();
    }
    public function setCode() {
        $this->code = $this->getCode();
    }

    public function getStartTime() {
        return $this->start_time;
    }
    public function setStartTime($start_time) {
        $this->start_time = $start_time;
    }

    public function getEndTime() {
        return $this->end_time;
    }
    public function setEndTime($end_time) {
        $this->end_time = $end_time;
    }

    public function getCostPrice() {
        return $this->cost_price;
    }
    public function setCostPrice($cost_price) {
        $this->cost_price = intval($cost_price);
    }

    public function getSourceUrl() {
        return $this->source_url;
    }
    public function setSourceUrl($source_url) {
        $this->source_url = $source_url;
    }

    public function getProductId() {
        return $this->product_id;
    }
    public function setProductId($product_id) {
        $this->product_id = intval($product_id);
    }

    public function getProductTitle() {
        return $this->product_title;
    }
    public function setProductTitle($product_title) {
        $this->product_title = $product_title;
    }

    public function getEventYear() {
        return date('Y', strtotime($this->date));
    }

    public function getDate() {
        return $this->date;
    }
    public function setDate($date) {
        $this->date = $date;
        $this->display_date = $this->getResDisplayDate($date);
    }

    public function getResDisplayDate() {
        return $this->getDisplayDate($this->date, get_option('ttb_backend_display_date'));
    }

    public function getPax() {
        return $this->pax;
    }
    public function setPax($pax) {
        $this->pax = intval($pax);
    }

    public function getTimeslot() {
        return $this->timeslot;
    }
    public function setTimeslot($timeslot) {
        $this->timeslot = $timeslot;
        $timeslots_order = Dimension::getTimeslotOrder();
        $this->timeslot_order = array_search($this->timeslot, $timeslots_order);
    }
    public function getTimeslotDisplay() {
        return get_term_by('slug', $this->timeslot, 'pa_timeslot') ? get_term_by('slug', $this->timeslot, 'pa_timeslot')->name : $this->timeslot;
    }
    public function getTimeslotForCompare() {
        return $this->getTimeslot() == 'none' ? '' : $this->getTimeslotDisplay();
    }

    public function getArea() {
        return $this->area;
    }
    public function setArea($area) {
        $this->area = $area;
    }
    public function getAreaDisplay() {
        return get_term_by('slug', $this->area, 'pa_area') ? get_term_by('slug', $this->area, 'pa_area')->name : $this->area;
    }

    public function getStatus() {
        return $this->status;
    }
    public function setStatus($status) {
        $this->status = $status;
    }
    public function getStatusDisplay() {
        return get_term_by('slug', $this->status, 'tab_res_tax_status')->name;
    }

    public function getNotes() {
        return $this->notes;
    }
    public function setNotes($notes) {
        $this->notes = $notes;
    }

    public function getVouchersReceived() {
        return $this->vouchers_received;
    }
    public function setVouchersReceived($vouchers_received) {
        $this->vouchers_received = $vouchers_received ? 1 : 0;
    }

    public function getLetterReceived() {
        return $this->getLetterReady() ? 1 : $this->letter_received;
    } //if letter ready, then letter received is true
    public function setLetterReceived($letter_received) {
        $this->letter_received = $letter_received ? 1 : 0;
    }

    public function getLetterReady() {
        return $this->letter_ready;
    }
    public function setLetterReady($letter_ready) {
        $this->letter_ready = $letter_ready ? 1 : 0;
    }

    public function getDocsComplete() {
        return $this->docs_complete;
    }
    public function setDocsComplete() {
        // Check if vouchers received, letter received, and letter ready are all true
        if ($this->getVouchersReceived() && $this->getLetterReceived() && $this->getLetterReady()) {
            $this->docs_complete = 1;
        } else {
            $this->docs_complete = 0;
        }
    }

    public function getReadyToShip() {
        return $this->ready_to_ship;
    }
    public function setReadyToShip($ready_to_ship) {
        $this->ready_to_ship = $ready_to_ship ? 1 : 0;
    }

    public function getReadyToShipDate() {
        return $this->ready_to_ship_date;
    }
    public function setReadyToShipDate($ready_to_ship_date) {
        $this->ready_to_ship_date = $ready_to_ship_date;
    }

    public function getCustomerNo() {
        return $this->customer_no;
    }
    public function setCustomerNo($customer_no) {
        $this->customer_no = $customer_no;
    }

    public function getBookingNo() {
        return $this->booking_no;
    }
    public function setBookingNo($booking_no) {
        $this->booking_no = $booking_no;
    }

    // Hidden    
    public function getPaxAssigned() {
        return $this->pax_assigned;
    }
    public function setPaxAssigned($pax_assigned) {
        $this->pax_assigned = intval($pax_assigned);
    }

    public function addPaxAssigned($pax_assigned) {
        $this->pax_assigned = $this->getPaxAssigned() + intval($pax_assigned);
        $this->setStatus($this->getPax() - $this->getPaxAssigned() <= 0 ? HardCoded::getReservationStatus()['assigned'] : HardCoded::getReservationStatus()['available']);
    }

    public function getPaxOpen() {
        return $this->pax_open;
    }
    public function setPaxOpen($pax_open) {
        $this->pax_open = intval($pax_open);
    }

    public function setSubReservations($sub_res_array) {
        $this->sub_res_array = $sub_res_array;
    }
    /**
     * Gets the sub reservations
     * @return array of sub reservations
     */
    public function getSubReservations() {
        return $this->sub_res_array;
    }

    /**
     * If sku matches a product, return the product price
     * Used in the Reservation list for sales to see what our shop price is for reservations we have, 
     * and when no product exists in the shop and should be created.
     * NOTE: This is the current LIVE price which could be a sales price and NOT the Regular price. 
     * For that use getShopRegularPrice()
     */
    public function getShopPrice() {
        $product_id = wc_get_product_id_by_sku($this->sku);

        if ($product_id > 0) {
            // Get an instance of the WC_Product Object
            $product = wc_get_product($product_id);
            $price = $product->get_price();
            // Create link to tab_admin_reports&report_slug=price and add the sku as a parameter
            $link = admin_url('admin.php?page=tab_admin_reports&report_slug=price&sku=' . $this->sku);
            // Return the price with a link to the price report, in new tab
            return '<a href="' . $link . '" target="_blank">' . $price . '</a>';
        } else {
            // Display an error (invalid Sku
            return 'no price';
        }
    }

    /**
     * Same as getShopPrice() but returns the Regular price and no link
     */
    public function getShopRegularPrice() {
        $product_id = wc_get_product_id_by_sku($this->sku);

        if ($product_id > 0) {
            // Get an instance of the WC_Product Object
            $product = wc_get_product($product_id);
            $price = $product->get_regular_price();
            return $price;
        } else {
            return 'no price';
        }
    }

    public function setAuditLogConfig() {
        $this->audit_log_meta_keys = [
            'tab_reservation_status' => 'Status',
            'tab_reservation_vouchers_received' => 'Vouchers Received',
            'tab_reservation_letter_received' => 'Letter Received',
            'tab_reservation_letter_ready' => 'Letter Ready',
            'tab_reservation_docs_complete' => 'Docs Complete',
            'tab_reservation_ready_to_ship' => 'Ready to Ship',
            'tab_reservation_ready_to_ship_date' => 'Ready to Ship Date',
        ];
    }

    public function getAuditLogMetaKeys() {
        return $this->audit_log_meta_keys;
    }

    public function setReservation($post_id) {
        $this->setId($post_id);
        $this->setPost(get_post($this->getId()));

        $order_item_ids = (is_array(get_post_meta($this->post->ID, 'tab_reservation_order_item_ids', true)) ? get_post_meta($this->post->ID, 'tab_reservation_order_item_ids', true) : array());
        $this->setOrderItemIds($order_item_ids);

        $this->setOrderItemTableNos(get_post_meta($this->post->ID, 'tab_reservation_order_item_table_nos', true));

        $this->setOrderCount(count($order_item_ids));

        $this->setVoucherOwners(get_post_meta($this->post->ID, 'tab_reservation_voucher_owners', true));
        $this->setSupplierId(get_post_meta($this->post->ID, 'tab_reservation_supplier_id', true)); // Also sets $this->supplier_name
        $this->setOwnerName(get_post_meta($this->post->ID, 'tab_reservation_owner_name', true));
        $this->setStartTime(get_post_meta($this->post->ID, 'tab_reservation_start_time', true));
        $this->setEndTime(get_post_meta($this->post->ID, 'tab_reservation_end_time', true));
        $this->setCostPrice(get_post_meta($this->post->ID, 'tab_reservation_cost_price', true));
        $this->setSourceUrl(get_post_meta($this->post->ID, 'tab_reservation_source_url', true));
        $this->setTableNo(get_post_meta($this->post->ID, 'tab_reservation_table_no', true));
        $this->setCode($this->getCode()); // always set code after id
        $this->setSku(get_post_meta($this->post->ID, 'tab_reservation_sku', true));
        $this->setProductId(get_post_meta($this->post->ID, 'tab_reservation_product_id', true));
        $this->setProductTitle(get_post_meta($this->post->ID, 'tab_reservation_product_title', true));
        $this->setDate(get_post_meta($this->post->ID, 'tab_reservation_date', true));
        $this->setPax(get_post_meta($this->post->ID, 'tab_reservation_pax', true));
        $this->setTimeslot(get_post_meta($this->post->ID, 'tab_reservation_timeslot', true));
        $this->setArea(get_post_meta($this->post->ID, 'tab_reservation_area', true));
        $this->setStatus(get_post_meta($this->post->ID, 'tab_reservation_status', true));
        $this->setNotes(get_post_meta($this->post->ID, 'tab_reservation_notes', true));
        $this->setVouchersReceived(get_post_meta($this->post->ID, 'tab_reservation_vouchers_received', true));
        $this->setLetterReceived(get_post_meta($this->post->ID, 'tab_reservation_letter_received', true));
        $this->setLetterReady(get_post_meta($this->post->ID, 'tab_reservation_letter_ready', true));
        $this->setDocsComplete(get_post_meta($this->post->ID, 'tab_reservation_docs_complete', true));
        $this->setReadyToShip(get_post_meta($this->post->ID, 'tab_reservation_ready_to_ship', true));
        $this->setReadyToShipDate(get_post_meta($this->post->ID, 'tab_reservation_ready_to_ship_date', true));
        $this->setCustomerNo(get_post_meta($this->post->ID, 'tab_reservation_customer_no', true));
        $this->setBookingNo(get_post_meta($this->post->ID, 'tab_reservation_booking_no', true));

        $this->setPaxAssigned(get_post_meta($this->post->ID, 'tab_reservation_pax_assigned', true));
        $this->setPaxOpen(get_post_meta($this->post->ID, 'tab_reservation_pax_open', true));

        $this->setSubReservations(get_post_meta($this->post->ID, 'tab_reservation_sub_res_array', true));
    }

    public function saveReservation() {
        // Run this to ensure the computed values are set after all updates have been made 
        $this->autosetReadyToShipDate();

        // Retrieve current values from post meta
        $meta_keys = [
            'tab_reservation_order_item_ids' => $this->getOrderItemIds(),
            'tab_reservation_order_item_table_nos' => $this->getOrderItemTableNos(),
            'tab_reservation_voucher_owners' => $this->getVoucherOwners(),
            'tab_reservation_order_count' => $this->getOrdercount(),
            'tab_reservation_supplier_id' => $this->getSupplierId(),
            'tab_reservation_supplier_name' => $this->getSupplierName(),
            'tab_reservation_owner_name' => $this->getOwnerName(),
            'tab_reservation_start_time' => $this->getStartTime(),
            'tab_reservation_end_time' => $this->getEndTime(),
            'tab_reservation_cost_price' => $this->getCostPrice(),
            'tab_reservation_source_url' => $this->getSourceUrl(),
            'tab_reservation_table_no' => $this->getTableNo(),
            'tab_reservation_code' => $this->getCode(),
            'tab_reservation_product_id' => $this->getProductId(),
            'tab_reservation_product_title' => $this->getProductTitle(),
            'tab_reservation_event_year' => $this->getEventYear(),
            'tab_reservation_date' => $this->getDate(),
            'tab_reservation_pax' => $this->getPax(),
            'tab_reservation_timeslot' => $this->getTimeSlot(),
            'tab_reservation_area' => $this->getArea(),
            'tab_reservation_status' => $this->getStatus(),
            'tab_reservation_notes' => $this->getNotes(),
            'tab_reservation_vouchers_received' => $this->getVouchersReceived(),
            'tab_reservation_letter_received' => $this->getLetterReceived(),
            'tab_reservation_letter_ready' => $this->getLetterReady(),
            'tab_reservation_docs_complete' => $this->getDocsComplete(),
            'tab_reservation_ready_to_ship' => $this->getReadyToShip(),
            'tab_reservation_ready_to_ship_date' => $this->getReadyToShipDate(),
            'tab_reservation_customer_no' => $this->getCustomerNo(),
            'tab_reservation_booking_no' => $this->getBookingNo(),
            'tab_reservation_pax_assigned' => $this->getPaxAssigned(),
            'tab_reservation_pax_open' => $this->getPax() - $this->getPaxAssigned(),
            'tab_reservation_sku' => $this->generateResSku(),
            'tab_reservation_sub_res_array' => $this->getSubReservations(),
        ];

        foreach ($meta_keys as $meta_key => $new_value) {
            $current_value = get_post_meta($this->getId(), $meta_key, true);
            if ($new_value != $current_value) {
                // Update if different
                update_post_meta($this->getId(), $meta_key, $new_value);

                // Log changes if it is a key that needs to be logged
                if (array_key_exists($meta_key, $this->audit_log_meta_keys)) {
                    $this->auditLog('audit_log_' . $meta_key, $this->audit_log_meta_keys[$meta_key], $current_value, $new_value);
                }
            }
        }
    }

    /**
     * Audit logs changes to the reservation
     * @param string $audit_meta_key NOTE:  this key should be different to the meta key being audited, prepend 'audit_log_' 
     *                                      to the meta key, if it is not done it will be done automatically
     *                                  
     * @param string $change_name
     * @param string $prev_value
     */
    public function auditLog($audit_meta_key, $change_name, $prev_value, $new_value) {

        // Ensure the audit_meta_key is prepended with 'audit_log_'
        if (strpos($audit_meta_key, 'audit_log_') === false) {
            $audit_meta_key = 'audit_log_' . $audit_meta_key;
        }

        // Log status changes
        JMD_Logger::metalog(
            $this->getId(),
            $audit_meta_key,
            array(
                'change' =>  $prev_value . '->' . $new_value,
            )
        );
    }

    public function autosetReadyToShipDate() {
        // Set the Ready to Ship date
        if ($this->getReadyToShip()) {

            //  Get saved ready_to_ship_date
            $this->setReadyToShipDate(get_post_meta($this->post->ID, 'tab_reservation_ready_to_ship_date', true));

            // check if ready_to_ship_date is a valid date and if not set, set it to today
            if (! $this->getReadyToShipDate() || ! strtotime($this->getReadyToShipDate())) {
                // If not, set ready_to_ship_date to today
                $this->setReadyToShipDate(current_time('Y-m-d H:i'));
            }
        } else {
            $this->setReadyToShipDate(''); // should be blank if not ready to ship
        }
    }

    public function resetDynamicVars() {
        // Newly created reservations will have pax, open-pax & assigned-pax = 0, skip if it is a new reservation
        if ($this->getPax() == 0 && $this->getPaxAssigned() == 0 && $this->getPaxOpen() == 0) return;

        // Set open based on total available and assigned pax
        $this->setPaxOpen($this->getPax() - $this->getPaxAssigned());

        // Set status to booked if nothing is open and it is not in booked status, ignore if on-hold
        if (
            $this->getId()           != null
            && $this->getPaxOpen()      == 0
            && $this->getStatus()       != 'booked'
            && $this->getStatus()       != 'on-hold'
            && $this->getStatus()       != 'closed'
            && $this->post->post_status != 'auto-draft'
        ) {

            $prev_status = $this->getStatus();

            $this->setStatus('booked');
            $this->saveReservation();

            // Display flash message that the reservation is now booked
            $this->addFlashNotice(__('Reservation ' . $this->getCode() . ': Status automatically changed to booked since there are no seats open (previous status = ' . $prev_status . ')', 'tabticketbroker'));
        }
    }

    public function getReservations($args = null) {
        // check if reservations are already loaded
        if (isset($this->reservations)) {
            return $this->reservations;
        } else {
            // Set reservations
            $this->setReservations($args);
        }

        return $this->reservations;
    }

    /** 
     * Gets all the reservations 
     * Typically every reservation should be returned, except when doing assignments, then we filter by the post status = publish
     * @return array of reservations
     */
    // public function setReservations( int $reservation_id = -1, int $offset = 0, int $posts_per_page = -1, $sort_by = '', $product_id_filter = array(), $status = array() )
    public function setReservations($args = null) {

        $reservation_id = -1;
        // $reservation_id = 29671; // debug set to a specific reservation id
        $offset = 0;
        $posts_per_page = -1;

        // cannot use base controller in static method, define properly
        $tab_reservation_cpt_title = 'tab_reservation';

        $status = empty($status) ? array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', /*'trash'*/) : $status;

        // Set event year meta query
        if (! empty($args) && isset($args['ttb_event_year']) && $args['ttb_event_year'] == '') {
            // If all, then don't set the meta query
            $meta_query = array();
        } elseif (! empty($args) && isset($args['ttb_event_year'])) {
            // If a specific year was passed in, set the meta query
            $event_year = $args['ttb_event_year'];
            $meta_query = array(
                array(
                    'key' => 'tab_reservation_event_year',
                    'value' => $event_year,
                    'compare' => '==',
                ),
            );
        } elseif (empty($args)) {
            // If no year was passed in, set the meta query to the current event year
            $meta_query = array(
                array(
                    'key' => 'tab_reservation_event_year',
                    'value' => TTB_EVENT_YEAR,
                    'compare' => '==',
                ),
            );
        }


        // If excluded statuses were passed in, add them to the meta query
        if (! empty($args) && isset($args['exclude_res_statuses'])) {
            $meta_query[] = array(
                'key' => 'tab_reservation_status',
                'value' => $args['exclude_res_statuses'],
                'compare' => 'NOT IN',
            );
        }

        // If there is more than one meta query, set the relation to AND
        if (count($meta_query) > 1) {
            $meta_query['relation'] = 'AND';
        }

        $args = array(
            'p'                 => $reservation_id,
            'fields'            => 'ids',
            'post_type'         => $tab_reservation_cpt_title,
            'post_status'       => $status,
            'offset'            => $offset,
            'posts_per_page'    => $posts_per_page,
            'meta_query'        => $meta_query,
        );

        $results = get_posts($args);

        // If no results, set the class var to empty array
        // This ensures other methods can see it is set, but empty
        // This is also the reason the class var is set to null 
        $reservations = array();
        if (empty($results)) {
            $this->reservations = $reservations;
            return $reservations;
        }

        // Create reservation objects
        foreach ($results as $post_id) {
            $reservations[] = new TabReservation($post_id);
        }

        // Get dimensions for sorting
        $product_title  = array_column($reservations, 'product_title');
        $date           = array_column($reservations, 'date');
        $timeslot_order = array_column($reservations, 'timeslot_order');
        $area           = array_column($reservations, 'area');

        // Sort by product title, date, timeslot, area
        array_multisort(
            $product_title,
            SORT_ASC,
            $date,
            SORT_ASC,
            $timeslot_order,
            SORT_ASC,
            $area,
            SORT_ASC,
            $reservations
        );

        $this->reservations = $reservations;
    }

    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();

        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row->$col;
        }

        array_multisort($sort_col, $dir, $arr);
    }

    /** 
     * Gets all the reservations 
     * Typically every reservation should be returned, except when doing assignments, then we filter by the post status = publish
     * @return array of reservations
     */
    public static function getReservations_v0(int $reservation_id = -1, int $offset = 0, int $posts_per_page = -1, $sort_by = '', $product_id_filter = array(), $status = array()) {
        // cannot use base controller in static method, define properly
        $tab_reservation_cpt_title = 'tab_reservation';

        $status = empty($status) ? array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash') : $status;

        $args = array(
            'p'                 => $reservation_id,
            'fields'            => 'ids',
            'post_type'         => $tab_reservation_cpt_title,
            'post_status'       => $status,
            'offset'            => $offset,
            'posts_per_page'    => $posts_per_page,
            'meta_query' =>
            array(
                array(
                    'key' => 'tab_reservation_event_year',
                    'value' => get_option('ttb_current_event_year'),
                    'compare' => '==',
                ),
            ),
        );

        $results = get_posts($args);

        // If no results, return empty array
        if (empty($results)) return array();

        $reservations = array();

        foreach ($results as $post_id) {
            $reservations[] = new TabReservation($post_id);
        }

        // Get dimensions for sorting
        $product_title  = array_column($reservations, 'product_title');
        $date           = array_column($reservations, 'date');
        $timeslot_order = array_column($reservations, 'timeslot_order');
        $area           = array_column($reservations, 'area');

        // Sort by product title, date, timeslot, area
        array_multisort(
            $product_title,
            SORT_ASC,
            $date,
            SORT_ASC,
            $timeslot_order,
            SORT_ASC,
            $area,
            SORT_ASC,
            $reservations
        );

        return $reservations;
    }

    /**
     * Adds the order item ids to the reservation and updates its pax availability
     * Remember to save the reservation afterwards
     * @param $order_item_id the order item to be added to the reservation
     * @param $pax the amount of people added to the resevation
     * @return void
     */
    public function addOrderItemId(int $order_item_id, int $pax) {
        // Quit if item already assigned (avoids duplicates on page refresh)
        if (in_array($order_item_id, $this->order_item_ids)) return;

        // Add to array
        array_push($this->order_item_ids, $order_item_id);

        // Also add pax to counter
        $this->addPaxAssigned($pax);

        // Save 
        $this->saveReservation();
    }

    public function removeOrderItemId($order_item_id, $pax) {
        // Put the order item in an array
        $remove_array = array($order_item_id);

        // Remove it from the assigned id's array
        $this->setOrderItemIds(array_diff($this->order_item_ids, $remove_array));

        // Remove the pax from the count
        $this->addPaxAssigned(-$pax);

        // Save 
        $this->saveReservation();
    }

    public function removeAllAssignments() {

        // Disable: just in case
        return 'JMD DISABLED';

        // Get all reservations
        $reservations = $this->getReservations();

        // Call clear function on each individually
        foreach ($reservations as $reservation) {
            $reservation->clearOrderItemIds();
        }
    }

    public function clearOrderItemIds() {
        // Disable: just in case
        return 'JMD DISABLED';

        // Clear items assigned
        $this->setOrderItemIds(array());

        // Reset assigned pax to zero
        $this->setPaxAssigned(0);

        // Set open pax to available pax of reservation
        $this->setPaxOpen($this->getPax());

        // Save
        $this->saveReservation();
    }

    public function resetAssignedPaxCount($reservation_id = null) {
        // Get all reservations
        if ($reservation_id == null) {
            $reservations = $this->getReservations();
        } else {
            $reservations = array(new TabReservation($reservation_id));
        }

        // Get all order items
        $assignment_manager = new AssignmentManager;
        $assignment_manager->setAllAvailableOrders();
        $event_order_items = $assignment_manager->getEventOrderItems();

        foreach ($reservations as $reservation) {
            $reservation_order_item_ids = $reservation->getOrderItemIds();

            // Loop through assigned orders to get total pax
            $assigned_pax = 0;
            foreach ($reservation_order_item_ids as $reservation_order_item_id) {
                $assigned_pax += $event_order_items[$reservation_order_item_id]['pax'];
            }

            // This was used to reset the supplier name that is also set in the setSupplierId function
            // It can be safely removed in the future (Date of inclusion: 2022-06-29)
            $reservation->setSupplierId($reservation->getSupplierId());

            // Set the assigned number of pax, calculate the open reserve & save
            $reservation->setPaxAssigned($assigned_pax);
            $reservation->setPaxOpen($reservation->getPax() - $assigned_pax);

            $reservation->saveReservation();
        }
    }

    /**
     * Gets assigned orders for when editing a reservation. Do not loop or avoid using where the method is iterated.
     * @return array All orders for a reservation
     */
    public function getAssignedOrders($reservation = null) {
        if ($reservation == null) $reservation = $this;

        // Get linked order ids from reservation
        $reservation_order_item_ids = $reservation->getOrderItemIds();

        // Instantiate a assignment manager class to get all linked orders with their data
        $assignment_manager = new AssignmentManager;

        // Set all available orders
        $assignment_manager->setAllAvailableOrders();

        $event_order_items = $assignment_manager->getEventOrderItems();

        // Run through all assignments and keep relevant order data
        $reservation_orders = array();
        foreach ($event_order_items as $order_item_id => $order_item_data) {
            if (in_array($order_item_id, $reservation_order_item_ids)) {
                $reservation_orders[$order_item_id] = $order_item_data;
            }
        }

        return $reservation_orders;
    }

    public function getDynamicSku() {
        $sku = $this->getSku();
        $pax_open = $this->getPaxOpen();

        // If reservation is full, return normal sku
        if ($pax_open == 0) return $sku;

        // Create a 2 char string of the pax open padded with 0's
        $pax_open_sku = str_pad($pax_open, 2, '0', STR_PAD_LEFT);

        // replace the PAX with the pax open
        $dynamic_sku = substr_replace($sku, $pax_open_sku, 9, 2);

        return $dynamic_sku;
    }

    /**
     * WIP:
     * Generate the sku from the different dimensions of the reservation
     * @return string The sku
     */
    public function generateResSku() {
        // Default SKU 
        // Format example: SPE-01-1-08-X
        //                 0123456789012
        // SPE = Product Code, 01 = Date, 1 = Timeslot, 08 = Pax, X = Area
        $sku_arr = array(
            'product' => 'AAA',     // 3 char product code
            'date' => '00',         // 2 number date code
            'timeslot' => '0',      // 1 number timeslot code
            'pax' => '00',          // 2 number pax code
            'area' => '',           // 1 char area code
        );

        // If product id is not set, return default sku
        if (! $this->getProductId())
            return $sku_arr['product'] . '-' . $sku_arr['date'] . '-' . $sku_arr['timeslot'] . '-' . $sku_arr['pax'];

        // Get the dimensions of the reservation (product, date, timeslot, pax, area)
        $product_title  = $this->getProductTitle();
        $date           = $this->getDate();
        $timeslot       = $this->getTimeslot();
        $pax            = $this->getPax();
        $area           = $this->getArea();

        // Get all dimensions
        if (! isset($this->dimension)) {
            $this->dimension = new Dimension;
            $this->dimension->initialize();
        }

        // Get product code
        $sku_arr['product'] = array_search($product_title, $this->dimension->sku_prods);

        // Get date code
        $sku_arr['date'] = array_search($date, $this->dimension->sku_dates);

        // pad the date with a 0 if it is only 1 char
        $sku_arr['date'] = str_pad($sku_arr['date'], 2, '0', STR_PAD_LEFT);

        // Get timeslot code
        $sku_arr['timeslot'] = (string)array_search($timeslot, $this->dimension->sku_timeslots);

        // Get pax code (pad with 0's if less than 2 chars)
        $sku_arr['pax'] = str_pad((string)$pax, 2, '0', STR_PAD_LEFT);

        // Get area code
        $sku_arr['area'] = array_search($area, $this->dimension->sku_areas);

        // Build sku string from array leaving out area if not set
        $sku = $sku_arr['product'] . '-' . $sku_arr['date'] . '-' . $sku_arr['timeslot'] . '-' . $sku_arr['pax'];

        // Add the area if set ( Note that the "keine bereich" defaults to blank )
        if ($sku_arr['area'] != '') {
            $sku .= '-' . $sku_arr['area'];
        }

        return $sku;
    }

    /**
     * Get a link for each order item in the reservation
     * @return array All order items in the reservation
     */
    public function getOrderItems() {
        // Get the order items assigned to the reservation
        $reservation_order_item_ids = $this->getOrderItemIds();

        // Run through all assignments and get the id's
        $reservation_orders = array();
        foreach ($reservation_order_item_ids as $i => $order_item_id) {

            // Get the order
            $order_id = wc_get_order_id_by_order_item_id($order_item_id);
            $order = wc_get_order($order_id);

            // If order is not found, send admin email 
            if (! $order) {
                $admin_email = get_option('admin_email');
                $subject = 'OFEST: Assigned order item not found';
                $message = 'An order item was changed. ON reservation ' . $this->getCode() . ' the order item id: ' . $order_item_id . ' was removed. ';
                wp_mail($admin_email, $subject, $message);

                // Remove the order item id from the reservation
                $this->removeOrderItemId($order_item_id, 0);

                // Recalculate the pax
                $this->resetAssignedPaxCount($this->getId());

                continue;
            }

            $reservation_orders[] = $order;
        }

        return $reservation_orders;
    }

    /**
     * Get all the orders from the reservation and build up a list of link buttons to them
     * @return array Link buttons to all order items in the reservation
     */
    public function getOrderItemLinks() {
        // Get all orders
        $reservation_orders = $this->getOrderItems();

        // Build up a list of link buttons to all orders
        $order_item_links = array();
        foreach ($reservation_orders as $order_item) {
            $class = 'order-status status-' . $order_item->get_status();
            $order_item_links[] = '<a href="' . $order_item->get_edit_order_url() . '" target="_blank"><mark class="' . $class . '" onlick="window.open(\'' . $order_item->get_edit_order_url() . '\', \'_blank\');"><span>' . $order_item->get_order_number() . '</span></mark></a>';
        }

        return $order_item_links;
    }

    /**
     * Combine all the locked messages into a single string
     * @return string All locked messages
     */
    public function getLockedMessage() {
        $locked_message = __('Assignments locked for the following reasons', 'tabticketbroker');

        $locked_message .= ': ' . implode(', ', $this->locked_msgs);

        $locked_message = rtrim($locked_message, ', ');

        return $locked_message;
    }

    /**
     * Generate a list of links to change the status of the reservation
     */
    public function getChangeStatusLinks($res_statuses, $reservation_editor) {
        // Get the current status of the reservation
        $current_status = $this->getStatus();

        // cycle through all statuses and generate a link for each
        $status_links = array();
        foreach ($res_statuses as $slug => $status) {
            // ignore the current status
            if ($slug == $current_status) continue;

            // Check if the status should be disabled (business rules apply)
            $is_enabled = $reservation_editor->checkReservationStatus($current_status, $slug) ? '' : 'disabled';

            // Build the class name
            $class = 'status-' . $slug;

            // Build the link
            $status_links[] = '<input type="button" name="' . $slug . '" value="' . $status . '" class="button ' . $class . '" onclick="updateResStatus(' . $this->getId() . ',\'' . $slug . '\',\'' . $status . '\')" ' . $is_enabled . '>';
        }

        return $status_links;
    }

    public function getChangeLinkHtml($res_statuses, $reservation_editor) {
        $status_options = $this->getChangeStatusLinks($res_statuses, $reservation_editor);

        // If no options are available, return an empty string
        if (empty($status_options)) return '';

        // Else, build the html
        $html = '<div id="res_status_' . $this->getId() . '" class="change-status-wrap" style="display:none;">';
        $html .= '<h4>' . __('Change status to', 'tabticketbroker') . ': </h4>';
        $html .= '<div class="change-status-links">' . implode(' ', $status_options) . '</div>';
        $html .= '</div>';

        return $html;
    }


    /**
     * Function to change reservation status
     * Used by ajax call in reservation page
     */
    public static function changeReservationStatus() {
        $reservation_id = (int) $_POST['res_id'];
        $status = $_POST['status_slug'];

        $reservation = new TabReservation($reservation_id);
        $reservation->setStatus($status);
        $reservation->saveReservation();

        // Get the reservation editor from the class if it is not set
        $res_editor = new TabReservationEditor;

        // Get the dependancies for all statuses
        $res_deps = $res_editor->getReservationStatusDependancies();

        // Get the dependancies for the current status
        $res_dep = array();
        $res_dep = $res_deps[$status];

        // Return success with the dependancies
        echo json_encode(array('success' => true, 'res_dep' => $res_dep));

        wp_die();
    }


    /**
     * Function to save the sub reservation array 
     * Used by ajax call in reservation editor page
     */
    public static function saveSubReservations() {
        // First save the reservation
        // self::saveReservation();

        // Get the reservation id
        $reservation_id = (int) $_POST['reservation_id'];

        // Get the sub reservation array
        $sub_res_array = $_POST['sub_reservations'];

        // If the sub reservation array is empty, return error
        if (empty($sub_res_array)) {
            echo json_encode(array('success' => false, 'error' => 'Sub reservation array is empty'));
            wp_die();
        }

        // if reservation id is empty, return error
        if (empty($reservation_id)) {
            echo json_encode(array('success' => false, 'error' => 'Reservation id is empty'));
            wp_die();
        }

        // Get the reservation
        $reservation = new TabReservation($reservation_id);

        // Set the sub reservation array
        $reservation->setSubReservations($sub_res_array);

        // Save the reservation
        $reservation->saveReservation();

        // Return success
        echo json_encode(array('success' => true));

        wp_die();
    }

    /**
     * Function to delete a sub reservation
     * Used by ajax call in reservation editor page
     */
    public static function deleteSubReservations() {
        // First save the reservation
        // self::saveReservation();

        // Get the reservation id
        $reservation_id = (int) $_POST['reservation_id'];

        // if reservation id is empty, return error
        if (empty($reservation_id)) {
            echo json_encode(array('success' => false, 'error' => 'Reservation id is empty'));
            wp_die();
        }

        // Get the reservation
        $reservation = new TabReservation($reservation_id);

        // Set the sub reservation array
        $reservation->setSubReservations(array());

        // Save the reservation
        $reservation->saveReservation();

        // Return success
        echo json_encode(array('success' => true));

        wp_die();
    }

    /**
     * Function to save the table nos
     */
    public static function saveTableNos() {
        // Get the reservation id
        $reservation_id = (int) $_POST['reservation_id'];

        // Get the table nos
        $table_no = $_POST['table_no'];

        // if reservation id is empty, return error
        if (empty($reservation_id)) {
            echo json_encode(array('success' => false, 'error' => 'Reservation id is empty'));
            wp_die();
        }

        // Get the reservation
        $reservation = new TabReservation($reservation_id);

        // Get the current table nos
        $current_table_nos = $reservation->getOrderItemTableNos();

        // Set the incoming table no
        $current_table_nos[$_POST['order_item_id']] = $table_no;

        // Set the table nos
        $reservation->setOrderItemTableNos($current_table_nos);

        // Save the reservation
        $reservation->saveReservation();

        // Return a done success for the ajax call
        echo json_encode(array('success' => true));

        wp_die();
    }

    /**
     * Reservation export used in new year price calculations.
     * All reservations are exported to a csv file. It includes basic data but
     * enables you to pivot the data to see which variations (sku's) was available from
     * suppliers and which had orders. Also cost prices and shop prices are included.
     * @return void
     */
    public function exportReservations() {
        // Get all reservations
        $args = array('ttb_event_year' => '');
        $this->setReservations($args);
        $reservations = $this->getReservations();

        // Data array
        $res_array = array();

        // Loop through reservations
        foreach ($reservations as $res) {

            // Strip value from cost price a tag
            $shop_price = strip_tags($res->getShopRegularPrice());

            // Populate array
            $res_array[$res->getCode()] = array(
                'code'                  => $res->getCode(),
                'sku'                   => $res->getSku(),
                'status'                => $res->getStatus(),
                'event_year'            => $res->getEventYear(),
                'reservation_date'      => $res->getDate(),
                'supplier'              => $res->getSupplierName(),
                'cost_price'            => $res->getCostPrice(),
                'shop_price'            => $shop_price,
                'order_count'           => $res->getOrderCount(),
            );
        }

        // Create the class
        $export_tools = new ExportTools;

        // Export the data to CSV
        $filepath = $export_tools->exportCsv($res_array, 'reservations.csv');

        // Download the file
        $export_tools->downloadSendHeaders($filepath);

        $this->addFlashNotice('Reservations exported', 'success');
    }
}
