<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use WP_Query;
use Inc\Classes\Dimension;
use Inc\Base\BaseController;

class ProductTools {
    public static function createFirstEventDateAllProducts() {
        // Get all variable products 
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'fields' => 'ids',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_type',
                    'field' => 'slug',
                    'terms' => 'variable'
                )
            )
        );

        $products = new WP_Query($args);

        // for each variable product, get first variation id
        foreach ($products->posts as $parent_id) {
            // Get the date attributes
            $terms  = wp_get_post_terms($parent_id, 'pa_date');

            // If no date attribute, set to 9999
            if ($terms) {
                // put all dates in a comma separated string
                $new_event_dates = '';
                foreach ($terms as $term) {
                    $new_event_dates .= $term->name . ',';
                }

                // remove last comma
                $new_event_dates = rtrim($new_event_dates, ',');
            } else {
                $new_event_dates = '9999';
            }

            // set a custom field jm_event_dates
            update_post_meta($parent_id, 'jm_event_dates', $new_event_dates);

            // save the first date in a custom field jm_event_first_date and strip out '-'
            $order_by_date_fmt = str_replace('-', '', explode(',', $new_event_dates)[0]);
            update_post_meta($parent_id, 'jm_event_first_date', $order_by_date_fmt);
        }
    }

    public static function resetVariationSkus($mock_run = true) {
        // Get dimension class
        $dimension = new Dimension();
        $base_controller = new BaseController();

        // Setup changes log
        $changes_log = array(
            'changed' => array(),
            'error' => array(),
            'total_count' => 0,
            'changed_count' => 0,
        );

        // Get all variations
        $operations = new Operations();
        $variations = $operations->getAllProdVariations();

        $error_vars = array();
        $invalid_vars = array();

        // Loop through each variation
        foreach ($variations as $variation_from_view) {
            $variation_id = $variation_from_view['id'];
            $variation = wc_get_product($variation_id);

            // if product is not a variation, skip
            if (!$variation) {
                continue;
            }

            // Get current sku
            $variation_sku = $variation->get_sku();
            $auto_gen_sku = $dimension->generateSkuFromVariation($variation_from_view);

            // If SKU contains 'error', log it and skip
            if (strpos($auto_gen_sku, 'error') !== false) {
                // Push id to error array
                $error_vars[] = $variation_id;
                continue;
            }

            // If the sku is different, update it
            if ($variation_sku != $auto_gen_sku) {
                // If not a mock run, update the sku
                if (!$mock_run) {
                    try {
                        $variation->set_sku($auto_gen_sku);
                        $variation->save();

                        // Log the change
                        $changes_log['changed'][] = array(
                            'id' => $variation_id,
                            'old_sku' => $variation_sku,
                            'new_sku' => $auto_gen_sku
                        );
                    } catch (\Throwable $th) {
                        $invalid_vars[] = array(
                            'id' => $variation_id,
                            'auto-sku' => $auto_gen_sku,
                            'error' => $th->getMessage()
                        );
                    }
                } else {
                    // If mock run, log the changes
                    $changes_log['changed'][] = array(
                        'id' => $variation_id,
                        'old_sku' => $variation_sku,
                        'new_sku' => $auto_gen_sku
                    );
                }
            }
        }

        // Log the results in a file
        $changes_log_file['mock_run'] = $mock_run ? 'true' : 'false';
        $changes_log_file['total_count'] = count($variations);
        $changes_log_file['error_count'] = count($changes_log['error']);
        $changes_log_file['changed_count'] = count($changes_log['changed']);
        $changes_log_file['timestamp'] = date('Y-m-d H:i:s');
        $changes_log_file['changed'] = $changes_log['changed'];
        $changes_log_file['invalid'] = $invalid_vars;
        $changes_log_file['error'] = $error_vars;

        $log_file = $base_controller->plugin_path . 'logs/sku_changes_log_' . date('Y-m-d H:i:s') . '.json';

        // if file does not exist, create it
        if (!file_exists($log_file)) {
            $fh = fopen($log_file, 'w');
            fclose($fh);
        }

        // Append to the file
        $fh = fopen($log_file, 'a');
        fwrite($fh, json_encode($changes_log_file) . PHP_EOL);
        fclose($fh);

        // Return the changes log
        return $changes_log_file;
    }
}
