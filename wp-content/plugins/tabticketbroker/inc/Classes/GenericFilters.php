<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

// Create class generic filters
class GenericFilters extends BaseController
{
    public $post_type_configs = array();

    /**
     * This function configures the filter paramerters used to define the filter dropdowns
     * @param $filters array Specifies the filters available
     *          - The array is indexed by the post_type
     *          - type is either 'char' or 'numeric' (use numeric for dates and format dates as YYYY-MM-DD)
     *          - is_multi_select is a boolean and specifies whether the filter is a single select or multi select
     * 
     *  NOTE: The collective_name & is_multi_select is not currently working for post type SHOP_ORDER. If multiple values are selected 
     *        WooCommerce redirects to the No New Orders yet page for some reason.
     * 
     */
    public function __construct()
    {
        // Set the post type configs
        $this->post_type_configs = array(
            'shop_order'    => array(
                'shop_order_event_year' =>  array(
                    'filter_meta_key'   => '_ttb_event_year',
                    'filter_classes'    => 'ttb-event-year-filter',
                    'collective_name'   => '', // Leave blank to not show the collective name   
                    'type'              => 'numeric',
                    'is_multi_select'   => false,
                )    
            )
        );
        
        // Add the filters to the List Tables
        add_action('restrict_manage_posts', array( $this, 'addPostTypeFilters' ), 10, 1 );
    
        // Add the filters to the query
        add_filter( 'pre_get_posts', array( $this, 'applyPostTypeFilters' ), 10, 1 );
    }

    /**
     * Add extra dropdowns to the List Tables
     *
     * @param required string $post_type    The Post Type that is being displayed
     */
    public function addPostTypeFilters( $post_type )
    {
        // If the config for this post type does not exist, then return
        if ( ! isset( $this->post_type_configs[ $post_type ] ) ) {
            return;
        }

        // Get the config for this post type
        $filter_configs = $this->post_type_configs[ $post_type ];
        
        // Add the filters
        foreach ( $filter_configs as $filter_meta_key => $filter_meta ) {
            $this->addFilter( $post_type, $filter_meta['filter_meta_key'], $filter_meta['filter_classes'], $filter_meta['collective_name'], $filter_meta['type'], $filter_meta['is_multi_select'] );
        }

        // Add a hidden input field container div
        /* This is so that the multiple select filters still produce a parameter and
        on the reload then maintains the selected values */
        echo '<div id="ttb-hidden-input-fields" style="display:none;"></div>';
    }

/**
 * Add a filter to the List Table
 * @param required string $meta_key   The meta key of the dimension to filter on
 */

    public function addFilter( $post_type, $meta_key, $filter_classes = '', $collective_name = 'All', $type = 'numeric', $is_multi_select = false )
    {
        global $wpdb;

        // Get all available options
        // Note that I do not filter by post_status as I want to show all options, this might be required in the future (e.g. if you want to not show trashed posts' options)
        $query = $wpdb->prepare(
        'SELECT DISTINCT pm.meta_value FROM %1$s pm
            LEFT JOIN %2$s p ON p.ID = pm.post_id
            WHERE pm.meta_key = "%3$s"
            AND p.post_type = "%4$s"
            AND pm.meta_value <> ""
            ORDER BY pm.meta_value',
            $wpdb->postmeta,
            $wpdb->posts,
            $meta_key,          // Your meta key - change as required
            $post_type
        );

        $results = $wpdb->get_col( $query );

        /** Ensure there are options to show */
        if( empty( $results ) ) return;

        // Sort the different filters
        if ( $type == 'numeric' ) {
            // Convert array to int
            $results = array_map(
                function( $value ) { return intval( $value );},
                $results
            );

            asort( $results );
        } 
        elseif ( $type == 'char' ) {           
            asort( $results );
        }
        
        // Get selected option if there is one selected
        if ( isset( $_GET[$meta_key] ) && $_GET[$meta_key] != '' ) {
            $selected_name = $_GET[$meta_key];
        } else {
            $selected_name = array( 'all' );
        }

        // Convert selected name to array by splitting on comma
        if ( is_string( $selected_name ) ) {
            $selected_name = explode( ',', $selected_name );
        }

        // Add the collective filter
        if ( $collective_name != '') {
            $options[] = sprintf( '<option value="all">%1$s</option>', $collective_name );
        }

        // Add the filter options from the query
        foreach( $results as $key => $result ) :
            $value = esc_attr( $result );
            $display = $result;

            // Set selected from GET var
            $selected = in_array( $value, $selected_name ) ? 'selected="selected"' : '';

            // Add option to array
            $options[] = sprintf( '<option value="%1$s" %2$s>%3$s</option>', $value, $selected, $display );

        endforeach;

        // Add the filter to the page
        if ( $is_multi_select ) {
            echo '<select class="'. $filter_classes .'" id="'. $meta_key .'" name="'. $meta_key .'" multiple onchange="addHiddenInputFields(\''. $meta_key .'\', Array.from(this.selectedOptions).map(x=>x.value??x.text))" multiselect-hide-x="true">';
        } else {
            echo '<select class="'. $filter_classes .'" id="'. $meta_key .'" name="'. $meta_key .'">';
        }

        echo join( "\n", $options );
        echo '</select>';

        // Add the hidden input field to the container div if it is a multi select filter
        if ( $is_multi_select ) {
            $meta_value = isset( $_GET[$meta_key] ) ? $_GET[$meta_key] : '';
            echo '<input type="hidden" name="' . $meta_key . '" value="' . $meta_value . '">';
        }
    }

    function applyPostTypeFilters( $query )
    {
        if( $query->is_main_query() && is_admin() ) {
            // Loop through the filter configs and match the post type to the filter
            foreach( $this->post_type_configs as $post_type => $filter_configs ) {
                // Check if the post type matches the current post type
                if ( $query->query_vars['post_type'] == $post_type ) {
                    foreach( $filter_configs as $filter_config )
                        $this->applyFilter( $query, $filter_config );
                }
            }
        }
    }

    function applyFilter( $query, $filter_configs )
    {
        // Get the meta key
        $meta_key = $filter_configs['filter_meta_key'];

        // Get the meta query
        $meta_query = $query->get( 'meta_query' ) ? $query->get( 'meta_query' ) : array();

        // If the filter is not set, skip it
        if ( ! isset( $_GET[$meta_key] ) || $_GET[$meta_key] == '' ) return;

        // Get the selected value
        $selected_value = $_GET[$meta_key];

        // If the selected value is 'all', skip it
        if ( $selected_value == 'all' ) return;

        // Convert selected value to array by splitting on comma
        if ( is_string( $selected_value ) ) {
            $selected_value = explode( ',', $selected_value );
        }

        // Add the meta query
        $meta_query[] = array(
            'key'           => $meta_key,
            'value'         => $selected_value,
            'compare'       => 'IN',
            'type'          => $filter_configs['type'],
        );

        // If there is more than one meta query, set the relation to AND
        if ( count( $meta_query ) > 1 ) {
            $meta_query['relation'] = 'AND';
        }
        
        // Set the meta query
        $query->set( 'meta_query', $meta_query );
        $query->meta_query = true;
    }
}
