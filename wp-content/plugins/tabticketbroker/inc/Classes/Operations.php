<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

use Inc\Base\BaseController;

Class Operations extends BaseController
{
    /**
     * Get all product variations in the system
     */
    public function getAllProdVariations()
    {
        global $wpdb;

        $view_name = HardCoded::getViewName();

        // Use the custom view to get all product variation ids
        $sql = 
            "SELECT 
                `id`,
                `post_parent`,
                `sku`, 
                `post_title`, 
                `pa_date`,
                `pa_timeslot`,
                `pa_pax`,
                `pa_area`,
                `price`,
                `stock`,
                `is_active` AS `enabled`
            FROM `$view_name`
            -- WHERE `sku` LIKE 'HAC-14-3-02'
            -- WHERE pa_date >= CURDATE()
            -- WHERE id = 21634
            ORDER BY `sku` ASC
            -- LIMIT 20;
            ";

        $sql_prep = $wpdb->prepare( $sql );

        $variations = $wpdb->get_results( $sql_prep, ARRAY_A );

        // Check for errors
        if ( $wpdb->last_error ) {
            $this->addFlashNotice( 'ERROR: Operations Report: '.$wpdb->last_error, 'error' );
        }

        return $variations;
    }

    /**
     * Generate sku for TabTickets
     * Based on a set of rules, automatically generate a sku for a TabTickets variation product
     */
    // public function generateSku( $product_id )
    // {
    //     $sku = '';

    //     // Get the product
    //     $product = wc_get_product( $product_id );



    //     // Get the parent product
    //     $parent_product = wc_get_product( $product->get_parent_id() );

    //     // Get the parent product sku
    //     $parent_sku = $parent_product->get_sku();



    //     // Set the sku
    //     $sku = $parent_sku.'-'.$date.'-'.$timeslot_code.'-'.$area_code.'-'.$pax;
    // }
       
}