<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Classes;

use Inc\Classes\Dimension;
use Inc\Base\BaseController;
use Inc\Classes\TabReservation;
use Inc\Tools\OrderTools;

class AssignmentManager extends BaseController
{
    
    public $orders_html;
    
    public $page_no = 1;
    
    public $display_page = 1;
    
    public $orders = array();
    public $order_products = array();
    
    public $display_amount = 20;

    public $show_flagged_only = false;
    
    public $order_items = array();

    public $special_pacakge_id = 0;
    
    public $reservation     = null;
    public $reservations    = array();
    public $all_reservations = array();
    
    public $assigned_orders = array();
        
    public $product_dates = array();            // Unique list of dates
    public $product_date_filter = array();      // List of dates filtered on
                
    public $product_titles = array();           // Unique list of product ids with titles (ofest = tents)
    public $product_id_filter = array();        // List of products filtered on

    public $timeslots = array();                // Unique list of timeslots
    public $timeslot_filter = array();          // List of timeslots filtered on

    public $reservation_statuses      = array(); // List of reservation statuses
    public $reservation_status_filter = array(); // List of reservation statuses filtered on

    public $assignment_changed_order_item_id;    // When assignments change the id is logged here to update order statuses if all order items have been assigned
    
    public $locked_statuses = array(); 

    public $displayed_statuses = array( 'available', 'booked', 'closed', 'on-hold', 'unavailable' );

    public $event_year = TTB_EVENT_YEAR;

    public $statuses_to_include = array();

    /**
     * Call this function to initiate the build of the assignment page/form
     */
    public function initialize( $event_year = TTB_EVENT_YEAR ) 
    {
        $this->event_year = $event_year;
        
        $this->maybeDoAssignments();
    
        $this->setProductTitles();
        $this->setProductDates();
        $this->setTimeslots();
        
        $this->setTitleFilter();
        $this->setDateFilter();
        $this->setTimeslotFilter();
        $this->setReservationStatusesFilter();
        $this->setFlaggedFilter();
        $this->setDisplayAmount();
        
        // Set Reservations & Orders
        $this->setAllAvailableOrders();

        // Set locked statuses and their messages (because it contains translations it must be done after the language is set)
        $this->locked_statuses = array(
            'closed'        => __( 'Reservation is Closed', 'tabticketbroker' ),
            'on-hold'       => __( 'Reservation is On Hold', 'tabticketbroker' ),
            'unavailable'   => __( 'Reservation is Unavailable', 'tabticketbroker' ),
            'ready-to-ship' => __( 'Reservation is Ready to ship', 'tabticketbroker' ),
            'labelled'      => __( 'Reservation is Labelled', 'tabticketbroker' ),
        );
    }
    
    public function displayAssignments()
    {
        $this->initialize();

        return require_once( "$this->plugin_path/templates/assignment_mgmt.php" );
    }

    public function setDisplayAmount()
    {
        // Set display amount to the active amount if passed through GET variable when the user changed the page
        if ( isset( $_GET['display_amount'] ) ) {

            $this->display_amount = $_GET['display_amount'];
   
            // Also reset page no
            $_GET['page_no'] = ( isset( $_GET['filter_applied'] ) ? 1 : $_GET['page_no'] );
            
            // Unset filter applied var not to reset to first page above everytime
            unset( $_GET['filter_applied'] );

        }
        // Set filter to default 
        else { 
            $this->display_amount = 50;            
        }
    }

    /** 
     * This sets the main products used. Thus the parent product titles and id's that group different smaller products together.
     * For Ofest for example these are the Tents. This variable is typically used by the filtering system to filter only specific tents.
     */
    public function setProductTitles()
    {
        $dimension = new Dimension;

        $this->product_titles = $dimension->getProductTitles();

        $this->special_pacakge_id = $dimension->getSpecialPackageId();
    }

    public function setOrderProducts()
    {

        $this->order_products = array_combine( array_unique( array_column( $this->orders, 'product_id' ) ), array_unique( array_column( $this->orders, 'product_title' ) ) );
 
    }
    
    /** 
     * This sets the main products used. Thus the parent product titles and id's that group different smaller products together.
     * For Ofest for example these are the Tents. This variable is typically used by the filtering system to filter only specific tents.
     */
    public function setProductDates()
    {
        // //Use date as key and date_de for display value
        $dimension = new Dimension;
 
        $prod_dates = $dimension->getProductDates();

        // Also add all dates from reservations loaded
        // This is required because of the event years switching and the date filter being applied on the assignment form
        // If these dates aren't added and only the dates from the attributes are loaded, reservations falling outside of
        // these attributes are filtered out
        foreach ( $this->reservations as $reservation ) {
            if ( ! array_key_exists( $reservation->getDate(), $prod_dates ) )
                $prod_dates[$reservation->getDate()] = $reservation->getResDisplayDate();
        }

        $this->product_dates = $prod_dates;

    }

    // Set all timeslots
    public function setTimeslots()
    {
        $dimension = new Dimension;
 
        $this->timeslots = $dimension->getProductTimeslots();
    }

    public function getPaginationHtml() 
    {
        $this->page_no = isset( $_GET['page_no'] ) ? $_GET['page_no'] : 1;

        // Get amount of reservations, taking into account the products filter
        $cnt_filtered_reservations = count( array_intersect( array_column( $this->reservations, 'product_id' ), $this->product_id_filter ) );

        $total_pages = ceil( $cnt_filtered_reservations / $this->display_amount );

        // Only show if more than 1 page
        if ( $total_pages == 1 ) return '';
        
        $html = ''; 

        $html .= 
            '<div id="pagination_div">'.
                '<nav>'.
                    '<ul class="pagination justify-content-end">'.
                        '<li class="page-item'. ( $this->page_no == 1 ? ' disabled' : '' ) .'">'.
                            '<a class="page-link" href="admin.php?'.
                            http_build_query( array_merge($_GET, array( "page_no"=>$this->page_no - 1 ), array( "display_amount"=>$this->display_amount ) ) ) .
                            '" tabindex="-1" '. ( $this->page_no == 1 ? ' aria-disabled="true"' : '' ) .'>'. __( 'Previous', 'tabticketbroker' ) .'</a>'.
                        '</li>'; 

        for ( $i = 1; $i <= $total_pages; $i++ ) {  
            $html .= 
                '<li class="page-item'. ( $i == $this->page_no ? ' active' : '' ) .'">'.
                    '<a class="page-link" '.
                        'href="admin.php?'. http_build_query( array_merge($_GET, array( "page_no"=>$i ), array( "display_amount"=>$this->display_amount ) ) ) .'">'. 
                        $i .'
                    </a>'.
                '</li>';
        };  

        $html .= 
                    ' <li class="page-item'. ( $this->page_no == $total_pages ? ' disabled' : '' ) .'">'.
                        '<a class="page-link" href="admin.php?'. 
                            http_build_query( array_merge( $_GET, array( "page_no"=>$this->page_no + 1 ), array( "display_amount"=>$this->display_amount ) ) ).
                        '" >Next</a>'.
                    '</li>'.
                '</ul>'.
            '</nav>'.
        '</div>'; 

        return $html;
    }

    /**
     * Generate HTML for the template file. This includes all the unique products.
     */
    public function getProductFilterHtml()
    {
        $html = 
            '<div id="product_filter">'.
                '<fieldset class="metabox-prefs">';
                    foreach( $this->product_titles as $product_id => $product_title ) {
                        // Mark filter item as checked if posted, also check if first-load-indicator exits, if so, dont check any boxes
                        $checked = ( in_array( -1, $this->product_id_filter ) ? '' :  ( in_array( $product_id, $this->product_id_filter ) ? 'checked' : '' ) );

                        $html .= 
                            '<input class="filter-item-checkbox" type="checkbox" '. $checked .' name="prod_filter[]" id="reservation-id-'. $product_id .'" value='. $product_id .'>'.
                            '<label for="reservation-id-'. $product_id .'">'. $product_title .'</label>';
                    }
            $html .= 
                '</fieldset>'.
            '</div>';
        
        return $html;
    }

    /**
     * Generate HTML for the template file. This includes all the unique dates.
     */
    public function getDateFilterHtml()
    {
        $html = 
            '<div id="date_filter">'.
                '<fieldset class="metabox-prefs">';
                    foreach( $this->product_dates as $date => $display_date ) {
                        // Mark filter item as checked if posted, also check if first-load-indicator exits, if so, dont check any boxes
                        $checked = ( in_array( -1, $this->product_date_filter ) ? '' :  ( in_array( $date, $this->product_date_filter ) ? 'checked' : '' ) );

                        $html .= 
                            '<input class="filter-item-checkbox" type="checkbox" '. $checked .' name="date_filter[]" id="reservation-date-id-'. $date .'" value='. $date .'>'.
                            '<label for="reservation-date-id-'. $date .'">'. $display_date .'</label>';
                    }
            $html .= 
                '</fieldset>'.
            '</div>';
        
        return $html;
    }

    /**
     * Generate HTML for the template file. This includes all the unique reservation statuses.
     */
    public function getReservationStatusFilterHtml()
    {
        $this->reservation_statuses = Dimension::getReservationStatuses();

        $html = 
            '<div id="status_filter"><label for="res_status_filter">'. __( 'Included statuses', 'tabticketbroker' ) .'</label>'.
                '<fieldset id="res_status_filter" class="metabox-prefs">';
                    foreach( $this->reservation_statuses as $status_id => $status_title ) {
                        // Mark filter item as checked if posted, also check if first-load-indicator exits, if so, dont check any boxes
                        $checked = in_array( $status_id, $this->reservation_status_filter ) ? 'checked' : '';

                        $html .= 
                            '<input class="filter-item-checkbox" type="checkbox" '. $checked .' name="reservation_status_filter[]" id="reservation-status-id-'. $status_id .'" value='. $status_id .'>'.
                            '<label for="reservation-status-id-'. $status_id .'">'. $status_title .'</label>';
                    }
            $html .= 
                '</fieldset>'.
            '</div>';
        
        return $html;
    }

    /**
     * Generate HTML for the template file. This includes all the unique timeslots.
     */
    public function getTimeslotFilterHtml()
    {
        $html = 
            '<div id="timeslot_filter">'.
                '<fieldset class="metabox-prefs">';
                    foreach( $this->timeslots as $slug => $timeslot ) {
                        // Mark filter item as checked if posted, also check if first-load-indicator exits, if so, dont check any boxes
                        $checked = ( in_array( -1, $this->timeslot_filter ) ? '' :  ( in_array( $slug, $this->timeslot_filter ) ? 'checked' : '' ) );

                        $html .= 
                            '<input class="filter-item-checkbox" type="checkbox" '. $checked .' name="timeslot_filter[]" id="timeslot-id-'. $slug .'" value='. $slug .'>'.
                            '<label for="timeslot-id-'. $slug .'">'. $timeslot .'</label>';
                    }
            $html .= 
                '</fieldset>'.
            '</div>';
        
        return $html;
    }
    
    /**
     * Generate HTML for the template file. This is for filtering on flagged order items.
     */
    public function getFlaggedFilterHtml()
    {
        $html = 
            '<div id="display_flagged_div">'.
                '<label for="flagged_items">'. __( 'Display only flagged items', 'tabticketbroker' ) .'</label>'.
                '<select  name="flagged_items">'.
                    '<option '. ( $this->show_flagged_only == false ? 'selected' : '' ) .' value=false>'. __( 'No', 'tabticketbroker' ) .'</option>'.
                    '<option '. ( $this->show_flagged_only == True ? 'selected' : '' ) .' value=true>'. __( 'Yes', 'tabticketbroker' ) .'</option>'.
                '</select>'.
            '</div>';

        return $html;
    }

    /**
     * Generate HTML for the template file. This includes preset display amounts.
     */
    public function getDisplayAmountHtml( $page )
    {
        $html = 
            '<div id="display_amount_div">'.
                '<label for="display_amount">'. __( 'Display amount', 'tabticketbroker' ) .'</label>'.
                '<select id="display_amount" name="display_amount">'.
                    '<option '. ( $this->display_amount == 10 ? 'selected' : '' ) .'>10</option>'.
                    '<option '. ( $this->display_amount == 20 ? 'selected' : '' ) .'>20</option>'.
                    '<option '. ( $this->display_amount == 50 ? 'selected' : '' ) .'>50</option>'.
                    '<option '. ( $this->display_amount == 100 ? 'selected' : '' ) .'>100</option>'.
                '</select>'.
                '<input class="filter-apply-button button" type="submit" value="'. __( 'Apply filter', 'tabticketbroker' ) .'">'.
                '<input type="hidden" id="page" name="page" value="'. $page .'">'.
                '<input type="hidden" id="filter_applied" name="filter_applied" value=1>'.
            '</div>';

        return $html;
    }

    /**
     * Sets the filter for following methods to use. The filter is applied to the reservation products
     */
    public function setTitleFilter()
    {
        // If no filter has been sent or it is empty, show all products
        if ( ! isset( $_GET['prod_filter'] ) || empty( $_GET['prod_filter'] ) ) {
            $this->product_id_filter = array_keys( $this->product_titles );
            
            // Add first-load-indicator, used to not display all filter items as checked even if all are loaded
            $this->product_id_filter[-1] = -1;
            
        }
        // Filter by products in list selected
        elseif ( isset( $_GET['prod_filter'] ) ) {
            // Set the id filter and convert to INT
            $this->product_id_filter = array_map( 
                function( $value ) { return intval( $value );},
                $_GET['prod_filter']
            );
        }
    }

    /**
     * Sets the filter for following methods to use. The filter is applied to the reservation product dates
     */
    public function setDateFilter()
    {    
        // If no filter has been sent or it is empty, show all products
        if ( ! isset( $_GET['date_filter'] ) || empty( $_GET['date_filter'] ) ) {
            $this->product_date_filter = array_keys( $this->product_dates );
            
            // Add first-load-indicator, used to not display all filter items as checked even if all are loaded
            $this->product_date_filter[-1] = -1;
            
        }
        // Filter by products in list selected
        elseif ( isset( $_GET['date_filter'] ) ) {
            $this->product_date_filter = $_GET['date_filter'];
            
            // Exactly as above array but convert values to int
            $this->product_date_filter = array_map( 
                function( $value ) { return ( $value );},
                $_GET['date_filter']
            );
        }
    }

    /**
     * Sets the filter for following methods to use. The filter is applied to the reservation product timeslots
     */
    public function setTimeslotFilter()
    {    
        // If no filter has been sent or it is empty, show all products
        if ( ( ! isset( $_GET['timeslot_filter'] ) || empty( $_GET['timeslot_filter'] ) ) && $this->timeslots ) {
            $this->timeslot_filter = array_keys( $this->timeslots );
            
            // Add first-load-indicator, used to not display all filter items as checked even if all are loaded
            $this->timeslot_filter[-1] = -1;
        }
        // Filter by products in list selected
        elseif ( isset( $_GET['timeslot_filter'] ) ) {
            $this->timeslot_filter = $_GET['timeslot_filter'];
            
            // Exactly as above array but convert values to int
            $this->timeslot_filter = array_map( 
                function( $value ) { return ( $value );},
                $_GET['timeslot_filter']
            );
        }
    }

    /**
     * Set the reservation statuses filter (used by the unassigned orders report)
     */
    public function setReservationStatusesFilter()
    {
        // Hardcode default filter
        $default_status_slug = 'available';
      
        // If no filter has been sent or it is empty, set to default
        if ( ! isset( $_GET['reservation_status_filter'] ) || empty( $_GET['reservation_status_filter'] ) ) {
            $this->reservation_status_filter[] = $default_status_slug;
        }

        // Filter by products in list selected
        elseif ( isset( $_GET['reservation_status_filter'] ) ) {
            $this->reservation_status_filter = $_GET['reservation_status_filter'];
            
            // Exactly as above array but convert values to int
            $this->reservation_status_filter = array_map( 
                function( $value ) { return ( $value );},
                $_GET['reservation_status_filter']
            );
        }
    }

    /**
     * Sets the filter for following methods to use. The filter is applied to the order items that have been flagged
     */
    public function setFlaggedFilter()
    {    
        // Set to false by default
        if ( ! isset( $_GET['flagged_items'] ) || empty( $_GET['flagged_items'] ) ) {
            $this->show_flagged_only == false;
        }
        // Filter by products in list selected
        elseif ( isset( $_GET['flagged_items'] ) ) {
            $this->show_flagged_only = filter_var( $_GET['flagged_items'], FILTER_VALIDATE_BOOLEAN);
        }
    }
    
    /**
     * Checks GET methods for any assignment pairs to add or remove from orders
     */
    public function maybeDoAssignments()
    {
        // Check if an assignment has been made and record it first
        if ( isset( $_GET['assignment_pair'] ) ) {
            
            $this->assignOrderItem( $_GET['assignment_pair'] );
            
            unset( $_GET['assignment_pair'] );
        } // or if a removal have been issued
        
        elseif ( isset( $_GET['action'] ) && $_GET['action'] == 'remove_assignment' && isset( $_GET['reservation_id'] ) ) {
            
            // This is required when order items have been deleted
            $pax = ! $_GET['pax'] ? 0 : $_GET['pax'];
            
            $this->removeOrderItem( $_GET['reservation_id'], $_GET['order_item_id'], $pax );
                        
            unset( $_GET['action'] );
        }
    }

    /**
     * Sets the reservation to be displayed
     * All reservations are retrived from the main class OR
     * If a user were on the Reservation Editor and clicked to go to it's assignments, this method is applied
     */
    public function setReservations( $args = array( 'exclude_res_statuses' => array( 'offer' ), 'ttb_event_year' => TTB_EVENT_YEAR ) )
    {
        // Check if the object is already set
        if ( ! isset( $this->reservation ) ) {
            $this->reservation = new TabReservation;
        }
        
        // Set all reservations except aangebot
        $this->reservation->setReservations( $args );
        $this->all_reservations = $this->reservation->getReservations();

        // Quit if no reservations were found
        if ( ! $this->all_reservations ) {
            return;
        }

        // If there was a direct reservation requested, set that too
        if ( isset( $_GET['reservation_id_direct'] ) ) {
            $this->reservation->setReservation( $_GET['reservation_id_direct'] );
            $this->reservations = array( $this->reservation );
        }
        else
        {
            // If product_id_filter containes -1, it means that all reservations were retrieved above and we can use that array
            if ( in_array( -1, $this->product_id_filter ) ) {
                $this->reservations = $this->all_reservations;
            }
            else {
                // Loop through all reservations and see if the product_id_filter matches
                foreach ( $this->all_reservations as $reservation ) {
                    if ( in_array( $reservation->getProductId(), $this->product_id_filter ) ) {
                        $this->reservations[] = $reservation;
                    }
                }
            }
        }
    }


    /**
     * Sets the $orders variable
     */
    public function setAllAvailableOrders()
    {
        // First set reservations, included here 
        //      - because it has to run before setting assigned orders &
        //      - so other methods only have call setAllAvailableOrders()
        $this->setReservations();
        
        $this->setAssignedOrders();
        
        $this->orders = $this->getEventOrders();

        $this->order_items = $this->getEventOrderItems();

        $this->order_products = $this->setOrderProducts();
    }

    /**
     * Returns only order items. Must be run after getEventOrders()
     */
    public function getEventOrderItems() {

        $order_items = array();

        if ( isset( $this->orders['Error'] ) ){
            return array();
        }
        
        foreach ( $this->orders as $order_id => $order_data) {

            // Some orders have not items or variations, thus continue
            if ( !isset( $order_data['variation'] ) || $order_data['variation'] == null ) continue;

            foreach ( $order_data['variation'] as $order_item_id => $order_item_data) {

                // If no product selected, add all products to filter (i.e. display all)
                if ( array_key_exists( -1, $this->product_id_filter ) ) {
                    $local_product_id_filter = array_keys( $this->product_titles );
                } else
                {
                    $local_product_id_filter = $this->product_id_filter;
                }
                
                // If no date selected, add all dates to filter (i.e. display all)
                if ( array_key_exists( -1, $this->product_date_filter ) ) {
                    $local_product_date_filter = array_keys( $this->product_dates );
                } else {
                    $local_product_date_filter = $this->product_date_filter;
                }

                // Apply filter (if product is checked in filter or no )
                $filter_flag = 
                ( 
                    ( 
                        in_array( $order_item_data['product_id'],  $local_product_id_filter ) && 
                        in_array( $order_item_data['pa_date'], $local_product_date_filter )
                    )
                ? true : false ) ;

                // If show only flagged is ON, then only keep the filter status from above if the item is flagged
                if ( $this->show_flagged_only ) 
                    $filter_flag = ( $order_item_data['flagged'] ? $filter_flag : false );
                
                $order_items[$order_item_id]['order_item_id']           = $order_item_id;
                $order_items[$order_item_id]['order_id']                = $order_id;
                $order_items[$order_item_id]['order_no']                = $order_data['order_no'];
                $order_items[$order_item_id]['order_created']           = $order_data['order_created'];
                $order_items[$order_item_id]['status']                  = $order_data['status'];
                $order_items[$order_item_id]['status_display']          = $order_data['status_display'];
                $order_items[$order_item_id]['invoice_no']              = $order_data['invoice_no'];
                $order_items[$order_item_id]['is_paid']                 = $order_data['is_paid'];
                $order_items[$order_item_id]['is_pickup']               = $order_data['is_pickup'];
                $order_items[$order_item_id]['ship_by']                 = $order_data['ship_by'];
                $order_items[$order_item_id]['customer_fullname']       = $order_data['customer_fullname'];
                $order_items[$order_item_id]['customer_lastname']       = $order_data['customer_lastname'];
                $order_items[$order_item_id]['customer_firstname']      = $order_data['customer_firstname'];
                $order_items[$order_item_id]['order_item_id']           = $order_item_data['order_item_id'];
                $order_items[$order_item_id]['variation_id']            = $order_item_data['variation_id'];
                $order_items[$order_item_id]['product_id']              = $order_item_data['product_id'];
                $order_items[$order_item_id]['product_title']           = $order_item_data['product_title'];
                $order_items[$order_item_id]['pa_date']                 = $order_item_data['pa_date'];
                $order_items[$order_item_id]['pa_timeslot']             = $order_item_data['pa_timeslot'];
                // $order_items[$order_item_id]['pa_pax']                  = $order_item_data['pa_pax'];
                $order_items[$order_item_id]['pa_area']                 = $order_item_data['pa_area'];
                $order_items[$order_item_id]['display_date']            = $order_item_data['display_date'];
                $order_items[$order_item_id]['pax']                     = $order_item_data['pax'];
                $order_items[$order_item_id]['total']                   = $order_item_data['total'];
                $order_items[$order_item_id]['sku']                     = $order_item_data['sku'];
                $order_items[$order_item_id]['quantity']                = $order_item_data['quantity'];
                $order_items[$order_item_id]['refunded']                = $order_item_data['refunded'];
                $order_items[$order_item_id]['flagged']                 = $order_item_data['flagged'];
                $order_items[$order_item_id]['reservation_id']          = $order_item_data['reservation_id'];
                $order_items[$order_item_id]['filter_flag']             = $filter_flag;
                $order_items[$order_item_id]['notes']                   = $order_data['notes'];
            }
        }
        
        return $order_items;
    }

    public function setReservationIds( $order_item_id )
    {
        // Set assigned orders if not set
        if ( ! $this->assigned_orders ) $this->setAssignedOrders();

        // Check if order item id exists in assignments and save the reservation id
        foreach( $this->assigned_orders as $reservation_id => $order_items ) {
            foreach ( $order_items as $cnt => $order_item_id_loop ) {
                if ( $order_item_id_loop === $order_item_id ) {
                    return $reservation_id;
                }
            }
        }
        return -1;
    }

    /**
     * Get all orders with limited data
     * Lite version of getEventOrders
     * @return $event_orders array list of orders that have been or should be assigned
     */
    public function getEventOrdersLite() 
    {
        // Get all sold orders and their products
        $orders = wc_get_orders( array(
            'post_type'         => 'shop_order',
            'limit'             => -1,
            'orderby'           => 'date',
            'order'             => 'ASC'
            ) 
        );

        if ( empty( $orders ) ) {
            return array( 'Error' => 'No sale products found ' );
        }

        $event_orders = array();
        foreach ( $orders as $order ) {
            
            if ( $order->has_status('cancelled') ) continue;

            $order_number = $order->get_order_number();

            $event_orders[$order->id] = array( 
                'order_id' => $order->id,
                'order_no' => $order_number,
                'status' => $order->get_status(),
                'customer_fullname' => $order->get_billing_last_name() .', '. $order->get_billing_first_name(),
                'customer_lastname' => $order->get_billing_last_name(),
                'customer_firstname'=> $order->get_billing_first_name(),
                'language' => ( $order->get_meta('_ttb_order_language') == '' ? $this->tab_default_lang_code : $order->get_meta('_ttb_order_language') ),
            );

        }

        // Sort by order id
        krsort( $event_orders );

        return $event_orders;
    }

    /**
     * Get all orders
     * @param $order_ids array list of order ids that should be filtered, gets all by default
     * @return $event_orders array list of orders that have been or should be assigned
     */
    public function getEventOrders( $orders = false ) 
    {
        if ( ! $orders ) {
            // Get all sold orders and their products
            $orders = wc_get_orders( array(
                'type'              => 'shop_order',
                'limit'             => -1,  // -1 for all
                'orderby'           => 'date',
                'order'             => 'ASC',
                'status'            => array(
                                        'pending',    
                                        'processing', 
                                        'on-hold',    
                                        'completed',
                                        'assigned',
                                        'ready-to-ship',
                                        'shipped',
                                        'on-hold',
                                        'labelled',
                                        // 'cancelled',  
                                        // 'refunded',   
                                        // 'failed',
                                    ),
                'meta_key'          => '_ttb_event_year',
                'meta_value'        => $this->event_year,
                'meta_compare'      => '=',
                ) 
            );
        }

        // TEMP: keep test orders during DHL integration testing but hide from assignments
        $test_order_ids = array( 30649, 27498, 29828, 30847 );
        foreach ( $orders as $cnt => $order ) {
            if ( in_array( $order->id, $test_order_ids ) ) {
                unset( $orders[$cnt] );
            }
        }

        if ( empty( $orders ) ) {
            return array( 'Error' => 'No sale products found ' );
        }

        // Get all order statuses
        $statuses = wc_get_order_statuses();

        // Orders that should be checked for status changes
        $order_status_check = array();

        // Loop through all orders and get their items
        $event_orders = array();
        $order_refund_items = array();
        foreach ( $orders as $order ) {
            
            $order_number = $order->get_order_number();
            
            $event_orders[$order->id] = array( 
                'order_id'          => $order->id,
                'order_no'          => $order_number,
                'order_created'     => $order->get_date_created()->date('Y-m-d'),
                'status'            => $order->get_status(),
                'status_display'    => $statuses['wc-'.$order->get_status()],
                'customer_fullname' => $order->get_billing_last_name() .', '. $order->get_billing_first_name(),
                'customer_lastname' => $order->get_billing_last_name(),
                'customer_firstname'=> $order->get_billing_first_name(),
                'language'          => ( $order->get_meta('_ttb_order_language') == '' ? $this->tab_default_lang_code : $order->get_meta('_ttb_order_language') ),
                'is_paid'           => $order->is_paid(),
                'invoice_no'        => $order->get_meta('_ttb_invoice_no' ),
                'notes'             => $order->get_meta('_ttb_order_notes' ),
                'is_pickup'         => $order->get_meta('_ttb_pickup' ),
                'ship_by'           => $order->get_meta('_ttb_ship_by_date' ),
                'event_year'        => $order->get_meta('_ttb_event_year' ),
            );
            
            // Check if order has refunds and note them in the order items below
            $order_refunds = $order->get_refunds(); // This gets all refunds for the order 
            $order_refund_items = array();
            foreach( $order_refunds as $refund ){
                // The $refund->get_items() gets only refunds on the OrderItems, if a refund was added as a 
                // separate line item, it will not be included as we are only interested in the item quantities
                foreach( $refund->get_items() as $refund_id => $refund_item ){

                    // Get order item id (refunded item)
                    $refunded_item_id = $refund_item->get_meta('_refunded_item_id') ? $refund_item->get_meta('_refunded_item_id') : -1;

                    // Combine refund items if they are the same
                    if ( ! isset( $order_refund_items[$refunded_item_id] ) ) {
                        $order_refund_items[$refunded_item_id] = array(
                            'qty' => $refund_item->get_quantity(),
                        );
                    } else {
                        $order_refund_items[$refunded_item_id]['qty'] += $refund_item->get_quantity();
                    }
                }
            }

            // Loop through order items and add them to the event orders array
            foreach ( $order->get_items() as $order_item_id => $item ) {

                // If the order item id had an assignment change, log the order for a status check
                if ( $order_item_id == $this->assignment_changed_order_item_id ) {
                    $order_status_check[] = $order->id;
                }

                // Get parent product (some attributes are only in the parent product)
                $parent_product = wc_get_product($item->get_product_id());

                // Get variation as product
                $var_product = wc_get_product($item->get_variation_id());

                // Get SKU for variation (Handle exception for if a variation has been deleted)
                if ( $var_product ) {
                    $item_sku = $var_product->get_sku();
                } else {
                    $item_sku = 'no-sku';
                }

                if ( isset( $order_refund_items[$order_item_id] ) ) {
                    $refund_qty = $order_refund_items[$order_item_id]['qty'];
                    $is_refund = true;
                } else {
                    $refund_qty = 0;
                    $is_refund = false;
                }

                // Check if the variation has a pax attribute, if not, use the quantity
                if ( $item->get_meta('pa_pax') ) {
                    // Cast pax to integer
                    $pax = intval( preg_replace("/[^0-9]/", "", $item->get_meta('pa_pax') ) );
                } else {
                    $pax = $item->get_quantity();
                }

                // Get product date attribute
                // This is required because some variations have a date attribute and others rely on the parent product's date attribute
                $prod_attr = $parent_product->get_attributes();

                $attr_to_check = array( 'pa_date', 'pa_area', 'pa_timeslot' );

                foreach ( $attr_to_check as $attr ) {
                    if ( isset( $prod_attr[$attr] ) ) {
                        $attribute_object = $prod_attr[$attr];
                        $parent_attr[$attr] = implode( ', ', $attribute_object->get_slugs() );
                    } else {
                        $parent_attr[$attr] = '';
                    }
                }
                
                // If the variation has a date attribute, use it, otherwise use the parent product's date attribute
                $date = $item->get_meta('pa_date') ? $item->get_meta('pa_date') : $parent_attr['pa_date'];
                $area = $item->get_meta('pa_area') ? $item->get_meta('pa_area') : $parent_attr['pa_area'];
                $timeslot = $item->get_meta('pa_timeslot') ? $item->get_meta('pa_timeslot') : $parent_attr['pa_timeslot'];

                // Check if date has multiple values (i.e. 2 dates)
                if ( strpos($date, ',') !== false ) {                    
                    // Use the first date
                    $date = explode(',', $date);
                    $date = $date[0];

                    $product_title = $item->get_name();

                    // Notify admin that the order has multiple dates
                    $this->addFlashNotice( 'WARNING: Order #'. $order_number .' has multiple dates for comparison of product '. $product_title .'. The first date found will be used.', 'warning', true );
                }

                // Check if the area has multiple values (i.e. 2 areas)
                if ( strpos($area, ',') !== false ) {                    
                    // Use the first area
                    $area = explode(',', $area);
                    $area = $area[0];

                    $product_title = $item->get_name();

                    // Notify admin that the order has multiple areas
                    $this->addFlashNotice( 'Order #'. $order_number .' has multiple areas for comparison of product '. $product_title .'. The first area found will be used.', 'warning', true );
                }

                // Check if the timeslot has multiple values (i.e. 2 timeslots)
                if ( strpos($timeslot, ',') !== false ) {                    
                    // Use the first timeslot
                    $timeslot = explode(',', $timeslot);
                    $timeslot = $timeslot[0];

                    $product_title = $item->get_name();

                    // Notify admin that the order has multiple timeslots
                    $this->addFlashNotice( 'Order #'. $order_number .' has multiple timeslots for comparison of product '. $product_title .'. The first timeslot found will be used.', 'warning', true );
                }


                // Set all order items
                $event_orders[$order->id]['variation'][$order_item_id] = array(                    
                    'order_item_id'         => $order_item_id,
                    'variation_id'          => $item->get_variation_id(),
                    'product_id'            => $item->get_product_id(),
                    'product_title'         => $item->get_name(),
                    'pa_date'               => $date,
                    'pa_timeslot'           => $timeslot,
                    // 'pa_pax'                => $item->get_meta('pa_pax'),
                    'pa_area'               => $area,
                    'display_date'          => $this->getDisplayDate( $item->get_meta('pa_date') ),
                    'pax'                   => $pax,
                    'total'                 => intval( $item->get_total() ),
                    'sku'                   => $item_sku,
                    'quantity'              => $item->get_quantity() + $refund_qty/*Refund qty is negative, thats why we add here*/,
                    'refunded'              => $is_refund,
                    'flagged'               => ( OrderTools::getOrderItemMeta( $order )[$order_item_id]['flagged'] ),
                    'reservation_id'        => $this->setReservationIds( $order_item_id ),
                );
            }
        }


        /**
         * Possible Order Status Change
         * From here we see if we need to change the order status after an assignment change
         */

        // Assignment statuses to toggle
        $this->statuses_to_include = array( 'processing', 'assigned');

        // Loop through $event_orders with a status that is in $statues_to_include and check if all their variations have a -1 id
        if ( $order_status_check ) 
        {
            foreach ( $event_orders as $order_id => $order ) {
                
                // Only check an order if one if it's items had an assignment change
                if ( ! in_array( $order_id, $order_status_check ) ) continue;

                // Reset counts
                $assignment_cnt = 0;
                $order_item_cnt = 0;

                // Only check orders with a status that is in $statues_to_include
                if ( in_array( $order['status'], $this->statuses_to_include ) ) {
                    foreach ( $order['variation'] as $order_item_id => $variation ) {
                        if ( $variation['reservation_id'] != -1 ) {
                            $assignment_cnt++;
                        }
                        $order_item_cnt++;
                    }

                    // if the order count and assignment count are equal, then the order is fully assigned and its status should be changed to assigned
                    if ( $order_item_cnt == $assignment_cnt ) {
                        if( $event_orders[$order_id]['status'] == 'processing' ) {
                            $event_orders[$order_id]['status'] = 'assigned';
                            $event_orders[$order_id]['status_display'] = $statuses['wc-assigned'];

                            // Update the order status
                            $order_obj = wc_get_order( $order_id );
                            $order_obj->update_status( 'assigned', __( 'Assignment Manager', 'tabticketbroker' ) .':', false );

                            // Generate html link button to navigate to the order in the admin
                            $order_link = '<a href="' . admin_url( 'post.php?post=' . $order_id . '&action=edit' ) . '" target="_blank">'. $order_obj->order_number .'</a>';
                            
                            // add a flash notice
                            $this->addFlashNotice( __( 'Order status changed from processing to <b>assigned</b>', 'tabticketbroker' ) .' '. $order_link, 'success', false );
                            
                            // save the order
                            $order_obj->save();
                            
                            // unset the order object
                            unset( $order_obj );                            
                        }
                    } 
                    // If not all the order items are assigned then then the status should be changed to processing if it is in assigned status
                    elseif ( $event_orders[$order_id]['status'] == 'assigned' ) 
                    {
                        $event_orders[$order_id]['status'] = 'processing';
                        $event_orders[$order_id]['status_display'] = $statuses['wc-processing'];
                        
                        // Update the order status
                        $order_obj = wc_get_order( $order_id );
                        $order_obj->update_status( 'processing', __( 'Assignment Manager', 'tabticketbroker' ) .':', false ); 

                        // Generate html link button to navigate to the order in the admin
                        $order_link = '<a href="' . admin_url( 'post.php?post=' . $order_id . '&action=edit' ) . '" target="_blank">'. $order_obj->order_number .'</a>';
                            
                        // add a flash notice
                        $this->addFlashNotice( __( 'Order status changed from assigned to <b>processing</b>' ) .' '. $order_link, 'success', false );
                        
                        // save the order
                        $order_obj->save();
                        
                        // unset the order object
                        unset($order_obj);
                    }
                }
            }
        }

        return $event_orders;
    }

    /**
     * Generate the html for each available order for the corresponding reservation where assignment capability exists.
     * Same as the original getAvailableOrdersSelectHtml but retrieves links and not option-select dropdowns. These includes
     * display links and page information as to correctly navigate to the reservation again after assignment.
     * @return string assigned orders html, this includes a form for each reservation 
     */
    public function printAvailableOrdersLinksHtml( $reservation, $reservation_full = false )
    {
        $i = 0;
        
        $assigned_orders = $this->assigned_orders;
        
        $product_id = $reservation->getProductId();

        // Get all values from array 2nd level (https://stackoverflow.com/questions/1455758/find-all-second-level-keys-in-multi-dimensional-array-in-php)
        $assigned_order_ids_only = array_unique( array_reduce( array_map( 'array_values', $assigned_orders ), 'array_merge',[] ) );
        
        $order_items = $this->order_items;

        // Only load variations of orders which fall within the same scope of tent/date/timeslot & have not been assigned yet
        
        // Set prev order no, used to see if there are more than one item for the same reservation
        $previous_order_no = '';
        $order_no_count = 1;

        foreach( $order_items as $order_item_id => $order_item ) {
            $is_alternative = false;

            // Do not include refunded items
            if ( $order_item['refunded'] && $order_item['quantity'] == 0 ) continue;

            // Simulate all special product items as the current reservation product
            // So, if the ordered item is a special package we can assign it to any reservation matching the
            // same criteria as usual exept the product ID, so we impersonate the same product by manually 
            // setting the product id to the same as the reservation.
            $is_special_package = ( $this->special_pacakge_id == $order_item['product_id'] );
            $order_item['product_id'] = ( $is_special_package ? $product_id : $order_item['product_id'] );

            // Check all assigned orders if the reservation has already been assigned
            $order_already_assigned = in_array( $order_item_id, $assigned_order_ids_only );
            $order_pax_exceed_res = ( $order_item['pax'] > $reservation->getPaxOpen() ? true : false );
 
            // Start comparison and show available orders
            if ( 
                       ( $order_item['pa_date'] == $reservation->getDate() ) 
                    //    ( $order_item['product_id'] == $product_id ) 
                    && ( strtolower( $order_item['pa_timeslot'] ) == strtolower( $reservation->getTimeslotForCompare() ) ) 
                    && ( $order_item['pax'] <= $reservation->getPax() )
                    && ( $order_already_assigned == false ) // If order_item id exists in the assigned orders array then it should not be listed
                    // && ( $order_item['is_paid'] == true ) // excludes unpaid orders ( rather show them but disabled and indicated as unpaid)
                )
                {
                    if ( $order_item['product_id'] != $product_id ) 
                        $is_alternative = true;
                    
                    if ( $previous_order_no == $order_item['order_no'] ) {
                        $order_no_count++;
                    }           
                    else {
                        $order_no_count = 1;
                    }
                    
                    $order_vars = $reservation->getId() .','. $order_item['order_item_id'] .',' . $order_item['pax'];

                    // Check if order was not paid
                    $order_is_paid = ( isset( $order_item['is_paid'] ) ? $order_item['is_paid'] : false );

                    // Check if order is on hold
                    $is_on_hold = ( isset( $order_item['status'] ) && $order_item['status'] == 'on-hold' ? true : false );

                    // Item disabled or not?
                    $order_item_disabled = ( $reservation_full || $order_pax_exceed_res || ! $order_is_paid /*Note the not ! */ || $is_alternative || $is_on_hold );
                    
                    // Combine messages
                    $disabled_msgs = array();
                    if ( $reservation_full ) $disabled_msgs[]       = __( 'Reservation fully assigned', 'tabticketbroker' );
                    if ( $order_pax_exceed_res ) $disabled_msgs[]   = __( 'Order pax exceeds reservation availability', 'tabticketbroker' );
                    if ( ! $order_is_paid ) $disabled_msgs[]        = __( 'Order not paid yet', 'tabticketbroker' ); // Note: NOT paid
                    if ( $is_alternative ) $disabled_msgs[]         = __( 'Order is a different beer hall', 'tabticketbroker' );
                    if ( $is_alternative && TTB_SITE == 'tabtickets' ) $disabled_msgs[]         = __( 'Order is a different product', 'tabticketbroker' );
                    if ( $is_on_hold ) $disabled_msgs[]             = __( 'Order is on hold', 'tabticketbroker' );
                    $disabled_msg = implode( ' & ', $disabled_msgs );
                    $disabled_msg = rtrim( $disabled_msg, ' & ' );

                    // Define order notes html if required
                    $order_notes = ( get_post_meta( $order_item['order_id'], '_ttb_order_notes', true ) == '' ? '' : get_post_meta( $order_item['order_id'], '_ttb_order_notes', true ) );

                    // Set button message to default or based on disabled msgs
                    $btn_msg = ( $disabled_msg ? $disabled_msg : __( 'Click to assign order to this reservation', 'tabticketbroker' ) );

                    ?>
                    <div class="row order-assigned 
                        <?php echo ( $order_item_disabled ? 'text-muted' : '' );?>
                        <?php echo ( ! $order_is_paid ? 'disable-unpaid-order' : '' );?>
                        <?php echo ( $is_alternative ? 'alternative-order' : '' );?>">

                        <div class="col-sm-9 page-link order-detail" id="assigned_order_item_<?php echo $order_item_id ?>" data-tooltip="<?php echo $order_items[$order_item_id]['status_display']; ?>">
                            <a data-toggle="tooltip" title="<?php _e( 'Go to order', 'tabticketbroker' ); ?>" href="post.php?post=<?php echo $order_items[$order_item_id]['order_id']; ?>&action=edit">
                                <mark class="<?php echo 'order-status status-'. $order_items[$order_item_id]['status'];?>">
                                    <?php echo $order_items[$order_item_id]['order_no']. ( $order_no_count == 1 ? '' : '.'.$order_no_count ); ?>
                                </mark>
                            </a>
                            <?php echo ' | ' . $order_items[$order_item_id]['customer_fullname']; ?><b><?php echo '( '. $order_items[$order_item_id]['pax'] .' )'; ?></b> 
                            <?php //echo ' ['. $order_item['pa_area'] .']'; ?>
                            <?php echo '[ '. $order_items[$order_item_id]['pa_area'] .' ]'; ?>
                            <?php echo ( $is_special_package ? ' {SPE}': '' ); ?>
                            <?php echo $is_alternative ? '{'. substr( $order_item['sku'], 0, 3 ) .'}' : ''; ?>
                            
                            <?php if ( $order_notes ) {?>
                            <span class="ttb-order-notes" data-toggle="tooltip" title="<?php echo $order_notes;?>">&#9432;</span>
                            <?php } ?>
                        </div>
                        

                        <div class="page-link <?php echo ( $order_item_disabled ? 'page-link-disabled' : '' );?>">
                            <span 
                                data-toggle="tooltip" 
                                title="<?php echo $btn_msg;?>">
                                <a class="btn btn-primary <?php echo ( $order_item_disabled ? 'disabled' : '' );?>" 
                                    <?php echo ( $order_item_disabled ? 'disabled' : '' );?>
                                    href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "assignment_pair" => $order_vars ) ) ); ?>#<?php echo $reservation->getId(); ?>">
                                    <?php echo __( 'Assign', 'tabticketbroker' ); ?>
                                </a>
                            </span>
                        </div>
                    </div>


                    <?php

                    $previous_order_no = $order_item['order_no'];

                    $i++;
                }
        }
        
        if ( $i == 0 ){
            _e( 'No orders available', 'tabticketbroker' );
        }
    }

    /**
     * Sets the assigned order variable
     */
    public function setAssignedOrders()
    {
        $this->assigned_orders = $this->getAssignedOrders();
    }

    /**
     * Loops through all the reservations and gets their assigned order item id's
     * @return array Containing reservation ids and their assigned order item ids
     */
    public function getAssignedOrders()
    {
        if ( ! isset( $this->all_reservations ) ) {
            $this->setReservations();
        }

        foreach( $this->all_reservations as $reservation ){
            $reservation_order_items = $reservation->getOrderItemIds();
            if ( ! empty( $reservation_order_items ) ) {
                $assigned_orders[$reservation->getId()] = $reservation_order_items;
            }
        }
        
        return ( empty ( $assigned_orders ) ? array() : $assigned_orders );
    }

    /**
     * Build the html displaying which order-variations are linked to which reservation-variation
     * @param int $order_item_id The ID linked to the reservation variation
     * @return string The html to form part of the assignment template
     */
    public function printAssignedOrdersHtml( $reservation )
    {
        $order_items = $this->order_items;
        
        $assigned_orders = $this->assigned_orders;

        // Quit if no assigned orders 
        if ( ! $assigned_orders || ! $this->order_items ) { _e( 'No orders assigned in the system.', 'tabticketbroker' ); return; }
        
        // Find all order variations that are assigned to the reservation variation passed in
        $assigned_orders_filtered = ( isset( $assigned_orders[$reservation->getId()] ) ? $assigned_orders[$reservation->getId()] : array() );
        
        if ( ! empty( $assigned_orders_filtered ) ) {

            $pax_assigned = 0;
            
            $pax_available = $reservation->getPax();

            foreach( $assigned_orders_filtered as $order_item_id ) {
                // Reset variables
                $order_notes = '';
                $order_item_deleted = false;
                $is_special_package = false;
                
                // Check if linked order was deleted
                if ( ! isset( $order_items[$order_item_id] ) ) {
                    $order_item_deleted = true;
                }
                else{

                    // Get pax data
                    $pax_assigned += $order_items[$order_item_id]['pax'];
                    $pax_available -= $order_items[$order_item_id]['pax'];
                    
                    // Define order notes html if required
                    $order_notes = ( get_post_meta( $order_items[$order_item_id]['order_id'], '_ttb_order_notes', true ) == '' ? '' : get_post_meta( $order_items[$order_item_id]['order_id'], '_ttb_order_notes', true ) );
                }
                
                // Check if the order item is a special package
                $is_special_package = ( $this->special_pacakge_id == $order_items[$order_item_id]['product_id'] );

                // Generate HTML
                ?>
                <div class="row order-assigned">
                    <div class="col-sm-9 page-link order-detail" id="assigned_order_item_<?php echo $order_item_id ?>" data-tooltip="<?php echo $order_items[$order_item_id]['status_display']; ?>">
                        <?php if ( $order_item_deleted ) { ?>
                            <span class="text-danger">
                                <?php 
                                // If you get here most likely there are orders that are ignored due to their status in the getEventOrders() method
                                printf( __( 'WARNING: Linked order item id = %s deleted', 'tabticketbroker' ), $order_item_id ); 
                                ?>
                            </span>
                        <?php } else { ?>
                        
                            <a data-toggle="tooltip" title="<?php _e( 'Go to order', 'tabticketbroker' ); ?>" href="post.php?post=<?php echo $order_items[$order_item_id]['order_id']; ?>&action=edit">
                                <mark class="<?php echo 'order-status status-'. $order_items[$order_item_id]['status'];?>">
                                    <?php echo $order_items[$order_item_id]['order_no']; ?>
                                </mark>
                            </a>
                            <?php echo ' | '. $order_items[$order_item_id]['customer_fullname'];?>
                            <b><?php echo '( '. $order_items[$order_item_id]['pax'] .' )'; ?></b>
                            <?php echo '[ '. $order_items[$order_item_id]['pa_area'] .' ]'; ?>
                            <?php echo ( $is_special_package ? ' {SPE}': '' ); ?>
                            <?php echo ( $order_items[$order_item_id]['status'] == 'cancelled' ? ' - ORDER CANCELLED' : '' ); ?>

                        
                        <?php if ( $order_notes ) {?>
                            <span class="ttb-order-notes" data-toggle="tooltip" title="<?php echo $order_notes;?>">&#9432;</span>
                        <?php } ?>
                        <?php } ?>
                    </div>

                    <?php
                        // If the reservation's ready-to-ship flag is set, disable the unassign button
                        if ( $reservation->is_locked ) {
                            $order_item_disabled = true;
                            // $title = __( 'This reservation is ready to ship. Remove the ready to ship flag in order to make changes.', 'tabticketbroker' );
                        } else {
                            $order_item_disabled = false;
                        }

                        // If the order is in ready-to-ship status, disable the unassign button
                        // USE CASE: A user assigns an order to a reservation, then the order is marked as ready to ship, then the user 
                        // wants to change the assignment of that order but the shipping team already packed the order and marked it as ready to ship, 
                        // So now we will disable the unassign button to prevent the user from changing the assignment of the order once its packed, 
                        // They will need to speak to the packing team in order to change the assignment.

                        if ( $order_items[$order_item_id]['status'] == 'ready-to-ship' ) {
                            $order_item_disabled = true;
                            $disabled_msg = __( 'This order is ready to ship. Remove the ready to ship flag in order to make changes.', 'tabticketbroker' );
                        }

                        
                    ?>
                    <div class="page-link remove-assigned<?php echo ( $order_item_disabled ? ' page-link-disabled' : '' );?>">
                    <span data-toggle="tooltip" title="<?php echo $disabled_msg;?>">
                        <a class="btn btn-secondary <?php echo ( $order_item_disabled ? 'disabled' : '' );?>" 
                            <?php echo ( $order_item_disabled ? 'disabled' : '' );?>
                            href="admin.php?<?php echo http_build_query( array_merge( $_GET, array( "action"=>'remove_assignment', "order_id"=>$order_items[$order_item_id]['order_id'], "reservation_id"=>$reservation->getId(), "order_item_id"=>$order_item_id, "pax"=> $order_items[$order_item_id]['pax'] ) ) ); ?>#<?php echo $reservation->getId(); ?>">
                            <?php echo __( 'Remove', 'tabticketbroker' ); ?>
                        </a>
                        </span>
                    </div>
                </div>
                <?php
            }
        }
        else{
            _e( 'No orders linked', 'tabticketbroker' );
        }
    } 
    
    /**
     * Compile HTML from relevant information in getReservationProducts()
     * Called in the template after being required in construct(or register) 
     * thus, after all other functions have been called
     * @return string HTML for use in assignment page (mainly)
     */
    public function showAssignmentForm()
    {
        // Add script to be able to scroll to any reservation that had an assignment change
        $this->printScriptCode();

        $html = '';
        $prev_prod = '';

        // Display empty results
        if (  ! isset( $this->reservations ) || $this->reservations == array() ) {
            echo '<div class="notice notice-warning"><p>No reservation products available for filter selection.</p></div>';
            return;
        }
        
        // Apply filters first
        foreach ( $this->reservations as $key => $reservation ) {

            // Control displayed reservations by status
            if ( ! in_array( $reservation->getStatus(), $this->displayed_statuses ) ) {
                unset( $this->reservations[$key] );
                continue; // already unset, doesn't matter if other filters apply
            }

            // Skip if not in product filter
            if ( TTB_SITE == 'ofest' && // JMD ONLY FOR NOW: Dont apply filter to tabtickets
                    ( 
                    ! in_array( $reservation->getProductId(), $this->product_id_filter ) || 
                    ! in_array( $reservation->getDate(), $this->product_date_filter ) ||
                    ! in_array( $reservation->getTimeslot(), $this->timeslot_filter )
                    )
                )
                unset( $this->reservations[$key] );
        }

        // Reset the array keys for the counter below
        $this->reservations = array_values( $this->reservations );

        // Get range from pagination
        $from_no = ( $this->display_amount * ( $this->page_no - 1 ) );

        $to_no = ( $this->display_amount * ( $this->page_no ) ) - 1;

        $to_no = ( count( $this->reservations ) < $to_no ? count( $this->reservations ) - 1 : $to_no );

        // Set display amount msg
        /* translators: Display amount message. 1: from amount, 2: to amount. */
        $display_amount_msg = sprintf( __( 'Displaying %1$s - %2$s of %3$s', 'tabticketbroker' ), $from_no + 1, $to_no + 1, count( $this->reservations ) );
        echo '<div class="display_amount">'.$display_amount_msg.'</div>';

        for ( $i = $from_no ; $i <= $to_no; $i++) {

            // Set local reservation
            $reservation = $this->reservations[$i];

            // Quit if no more reservations exists
            if ( empty( $reservation ) ) break;

            // Track locked reservations
            if ( in_array( $reservation->getStatus(), array_keys( $this->locked_statuses ) ) ) {
                $reservation->is_locked = true;
                $reservation->locked_msgs[] = $this->locked_statuses[$reservation->getStatus()];
            }
            
            // Check if the reservation is locked because of ready to ship flag
            if ( $reservation->getReadyToShip() ) {
                $reservation->is_locked = true;
                $reservation->locked_msgs[] = $this->locked_statuses['ready-to-ship'];                
            }           
        
            // Get supplier
            $supplier = new TabSupplier( $reservation->getSupplierId() );
      
            // Start HTML logic
            // Only once a new product(or tent) have been identified
            if ( $prev_prod != $reservation->getProductTitle() ){
                // Close previous container (but not the first time)
                if ( $prev_prod != '' ) echo '</div> <!-- res-tent-container -->';
                
                // Start new container
                echo 
                '<div id="'. $reservation->getId() .'" class="res-tent-container">'.
                    '<h3>'. $reservation->getProductTitle() . '</h3>';
            }
            
            // Keep track of the previous product for logic above
            $prev_prod = $reservation->getProductTitle();

            ?>
                <div id="<?php echo $reservation->getId();?>" name="<?php echo $reservation->getId();?>" class="reservation-product">
                    <!-- Total Reservation Counter  -->
                    <div class="text-left res-cnt" data-toggle="tooltip" data-placement="top" title="<?php _e( "Reservation Counter", 'tabticketbroker' ); ?>"> 
                        <?php echo $i + 1; ?>
                    </div>
                    <!-- Reservation Top Bar -->
                    <div class="res-top-bar">
                        <!-- Reservation title, id & link -->
                        <div class="res-prod-title" data-toggle="tooltip" data-placement="top" title="<?php _e( "Reservation ID. Click to edit the reservation.", 'tabticketbroker' ); ?>">    
                            <a href="post.php?post=<?php echo $reservation->getId(); ?>&action=edit"><b><?php echo $reservation->getProductTitle(); ?> - <?php echo $reservation->getCode() .' - '. $reservation->getSku();?></b></a>
                        </div>
                        
                        <!-- Pax Data -->
                        <div class="pax-display" data-toggle="tooltip" data-placement="top" title="<?php _e( "Assignment counts", 'tabticketbroker' ); ?>">
                            <div class="pax-avail"><?php echo _x( 'Open', 'Assignments: amount of pax open on reservation', 'tabticketbroker' ); echo ' '. $reservation->getPaxOpen(); ?></div>
                            <div class="pax-assigned"><?php echo _x( 'Assigned', 'Assignments: amount of pax assigned to reservation', 'tabticketbroker' ); echo ' '. $reservation->getPaxAssigned(); ?></div>
                            <div class="res-pax"><?php echo _x( 'Reservation', 'Assignments: total pax available on reservation', 'tabticketbroker' ); echo ' '. $reservation->getPax(); ?></div>
                        </div>

                    </div>
                    <div class="res-detail">
                        <div class="row col-sm-8 res-data-container">
                            <div class="col-sm res-data">
                                <div class="row res-date">
                                    <h5 data-toggle="tooltip" data-placement="top" title="<?php _e( 'Event date', 'tabticketbroker' ); ?>"><?php echo $reservation->getResDisplayDate(); ?>
                                    <h6 data-toggle="tooltip" data-placement="top" title="<?php _e( 'Timeslot', 'tabticketbroker' ); ?>">&nbsp;&nbsp;&nbsp;<?php echo $reservation->getTimeslotDisplay();?><?php echo $reservation->getStartTime() == '' ? '' : ' - '. $reservation->getStartTime();?>
                                    </h6></h5>
                                </div>
                                <!-- <div class="row res-timeslot"></div> -->
                                <div class="row res-area">
                                    <h6 data-toggle="tooltip" data-placement="top" title="<?php _e( 'Area', 'tabticketbroker' ); ?>"><?php echo $reservation->getAreaDisplay(); ?></h6>
                                    <h6 style="padding-left:3px;padding-right:3px;border-radius:5px;margin-left:5px;" class="reservation-status status-<?php echo $reservation->getStatus(); ?>"><?php echo $reservation->getStatusDisplay(); ?></h6>
                                </div>
                                <div class="row res-supplier">
                                    <p><?php echo $reservation->getOwnerName();?> (<?php _e( 'Supplied by', 'tabticketbroker' ); ?> <a data-toggle="tooltip" title="<?php _e( 'Go to supplier', 'tabticketbroker' ); ?>" href="post.php?post=<?php echo $supplier->getId(); ?>&action=edit"><?php echo $supplier->getName(); ?></a>)</p>
                                </div>
                            </div>
                            <!-- Add div displaying the docs_ready and ready_to_ship statuses -->
                            <div class="col-sm res-data" data-toggle="tooltip" data-placement="top" title="<?php _e( 'Reservation status', 'tabticketbroker' ); ?>">
                                <div class="row res-docs"><p><?php _e( 'Documents complete', 'tabticketbroker' ); ?></p><?php echo $reservation->getDocsComplete() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>' ?></div>
                                <div class="row res-docs"><p><?php _e( 'Ready to ship', 'tabticketbroker' ); ?></p><?php echo $reservation->getReadyToShip() ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>' ?></div>
                            </div>
                            <div class="col-sm res-notes <?php echo $reservation->getNotes() == '' ? 'text-muted' : ''; ?>" data-toggle="tooltip" data-placement="top" data-html="true" title="<?php _e( 'Reservation notes', 'tabticketbroker' ); ?>">
                                <p><?php echo ( $reservation->getNotes() == '' ? __( 'No notes', 'tabticketbroker' ) : $reservation->getNotes() ); ?></p>
                            </div>
                        </div>

                    </div>
                    <div class="row res-order-container">
                        <div class="col-sm-6 assignment-reserv">
                            <h6><?php _e( 'Available orders', 'tabticketbroker' ); ?></h6>
                            <div class="available-order-links">
                                <?php echo $this->printAvailableOrdersLinksHtml( $reservation, ( $reservation->getPaxOpen() == 0 ? true : false ) ); ?>
                            </div>
                        </div>
                        <div class="col-sm-6 assignment-orders">
                            <h6><?php _e( 'Assigned orders', 'tabticketbroker' ); ?></h6>
                            <?php 
                            // If locked_is true, show the locked message and disable all assignment buttons
                            if ( $reservation->is_locked ) {
                                echo '<div class="alert alert-warning" role="alert">'. $reservation->getLockedMessage() .'</div>';
                                }?>
                            <?php echo $this->printAssignedOrdersHtml( $reservation ); ?>
                        </div>
                    </div>
                </div>
                <?php
        }
        
        // Close last container
        echo '</div> <!-- res-tent-container -->';

        return $html;
    }

    public function getReservationNotesDiv( $notes )
    {
        if ( $notes == '' ) {
            $html = '<div class="col-sm"data-toggle="tooltip" data-placement="top" data-html="true" title="'. __( 'No reservation notes to display. Edit the reservation to display notes here.', 'tabticketbroker' ) .'"></div>';
        }
        else {
            $html = 
            '<div class="col-sm res-notes" data-toggle="tooltip" data-placement="top" data-html="true" title="'. __( 'Reservation notes', 'tabticketbroker' ) .'">'.
                '<p>'. $notes . '</p>'.
            '</div>';
        }
        return $html;
    }

    /**
     * Assign order items to a reservation 
     * @param $new_assignment_pair_str A comma delimited string containing the reservation ID, Order Item ID & amount of pax
     * @return void
     */
    public function assignOrderItem( string $new_assignment_pair_str )
    {
        // Extract the data from the string
        $new_assignment_pair_exploded = explode( ',', $new_assignment_pair_str );
        $new_assignment_pair = array(
            "reservation_id"    => intval( $new_assignment_pair_exploded[0] ),
            "order_item_id"     => intval( $new_assignment_pair_exploded[1] ),
            "order_pax"         => intval( $new_assignment_pair_exploded[2] ),
        );

        // Get reservation
        $reservation = new TabReservation( $new_assignment_pair['reservation_id'] );

        // If already assigned, do nothing
        if( in_array($new_assignment_pair['order_item_id'],$reservation->order_item_ids) ) return;

        // Get available pax
        $pax_available = $reservation->getPaxOpen();

        // Check if the order pax exceeds the available pax
        if ( $new_assignment_pair['order_pax'] > $pax_available ) {
            $this->addFlashNotice( __( 'Unsuccessful assignment. Order pax exceeds reservation availability' ), 'error', false );
            return;
        }

        // Add order items
        $reservation->addOrderItemId( $new_assignment_pair['order_item_id'], $new_assignment_pair['order_pax'] );

        // Log the order item id that changed to check later if the order have all items assigned and requires a status change (that happens in getEventOrders())
        $this->assignment_changed_order_item_id = $new_assignment_pair['order_item_id'];
        
        // Remove the flag from the order item if it was set
        $order_id = wc_get_order_id_by_order_item_id( $new_assignment_pair['order_item_id'] );
        $flagged_items = ( get_post_meta( $order_id, '_ttb_flagged_items', true ) == '' ? array() : get_post_meta( $order_id, '_ttb_flagged_items', true ) );
        if ( in_array( $new_assignment_pair['order_item_id'], $flagged_items ) ) {
            $flagged_items = array_diff( $flagged_items, array( $new_assignment_pair['order_item_id'] ) );
            update_post_meta( $order_id, '_ttb_flagged_items', $flagged_items );
            $this->addFlashNotice( __( 'Item flag removed' ) , 'warning', false );
        }

        // Clear 
        unset( $reservation );

        // Display addition success message
        $this->addFlashNotice( __( 'Assignment successful' ) . ' <button class="show-res-btn" onclick="scrollToReservation('. $new_assignment_pair['reservation_id'].')">'. __( 'Show Reservation', 'tabticketbroker' ) .'</button>', 'success', false );
    }
    
    /**
     * Remove order items from a reservation 
     * @param $reservation_id The id of the reservation
     * @param $order_item_id The id of the order item to be removed
     * @param $pax The amount of pax to be removed from the reservation
     * @return void
     */
    public function removeOrderItem( string $reservation_id, int $order_item_id, int $pax )
    {
        // Get reservation
        $reservation = new TabReservation( $reservation_id );

         // If already removed, do nothing
         if( ! in_array( $order_item_id, $reservation->order_item_ids) ) return;

        // Remove order items
        $reservation->removeOrderItemId( $order_item_id, $pax );

        // Log the order item id that changed to check later if the order have all items assigned and requires a status change (that happens in getEventOrders())
        $this->assignment_changed_order_item_id = $order_item_id;

        // Reset pax open only if $pax = zero
        // Required when order items have been deleted and unncessary to do
        // for every assignment because it has to loop through all the order items
        if ( $pax == 0 ) {
            $reservation->resetAssignedPaxCount( $reservation_id );
        }

        // Clear 
        unset( $reservation );
        
        // Display removal success message
        $this->addFlashNotice( __( 'Assignment removed', 'tabticketbroker' ). ' <button class="show-res-btn" onclick="scrollToReservation('. $reservation_id.')">'. __( 'Show Reservation', 'tabticketbroker' ) .'</button>', 'success', false );
    }

    public function printScriptCode()
    {
        ?>
        <script>
            function scrollToReservation(reservationID) {
                var el = document.getElementById(reservationID);
                el.scrollIntoView(true);
            }
        </script>
        <?php
    }
}
