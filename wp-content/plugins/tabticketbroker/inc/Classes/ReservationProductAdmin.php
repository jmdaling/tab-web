<?php


// NOTE:
// This was used in the previous approach in which procurement products was created and 
// used as reservations. The functionality was migrated to the Reservation custom post type 

// /**
//  * @package tabticketbroker
//  */
// namespace Inc\Classes;

// use Inc\Base\BaseController;
// use WC_Product;

// class ReservationProductAdmin extends BaseController
// {
//     public $supplier_select = array(); 
    
//     public function register()
//     {
//         // // Hide items on procurement products
//         // $this->hideProcurementProductDetail();

        
//         // Add supplier fields to variations 
//         // $this->showCustomSupplierDetail();
//     }
    
//     // public function hideProcurementProductDetail()
//     // {
//     //     add_action( 'woocommerce_product_options_general_product_data', array( $this, 'hide_stuff' ), 10, 2 );
//     // }
    
//     public function setSuppliers()
//     {
//         $tab_supplier = new TabSupplier();

//         $suppliers = $tab_supplier->getSuppliers();

//         $supplier_select = array( '' => __( 'Select supplier', 'woocommerce' ) );

//         foreach( $suppliers as $supplier ) {
//             $supplier_select[$supplier->id] = __( $supplier->name, 'woocommerce' );
//         }

//         $this->supplier_select = $supplier_select;        
//     }

//     public function showCustomSupplierDetail()
//     {
//         $this->setSuppliers();

//         // Add the custom text field to the variation menu
//         add_action( 'woocommerce_variation_options_pricing', array( $this, 'addReservationName') , 10, 3 );
        
//         // Save the custom field
//         add_action( 'woocommerce_save_product_variation', array( $this, 'saveReservationName' ), 10, 2 );

//         // Store the custom field value into variation data
//         add_filter( 'woocommerce_available_variation', array( $this, 'addReservationNameVariationData' ) );

//         // Add the custom text field to the variation menu
//         add_action( 'woocommerce_variation_options_pricing', array( $this, 'addReservationSupplier') , 10, 3 );

//         // Save the custom field
//         add_action( 'woocommerce_save_product_variation', array( $this, 'saveReservationSupplier' ), 10, 2 );

//         // Store the custom field value into variation data
//         //add_filter( 'woocommerce_available_variation', array( $this, 'addReservationSupplierVariationData' ) );
//     }

//     public function addReservationSupplier( $loop, $variation_data, $variation )
//     {
//         // Your Text Field
//         woocommerce_wp_select(
//             array(
//                 'id'                => "tab_reservation_supplier{$loop}",
//                 'name'              => "tab_reservation_supplier[{$loop}]",
//                 'value'             => get_post_meta( $variation->ID, 'tab_reservation_supplier', true ),
//                 'label'             => __('Reservation Supplier', 'woocommerce'),
//                 'placeholder'       => 'Select supplier',
//                 'wrapper_class'     => 'form-row form-row-full',
//                 'desc_tip'          => 'true',
//                 'description'       => __('Required for all reservations. This is the name on the reservation.', 'woocommerce'),
//                 'options'           => $this->supplier_select
//                 // 'options'     => array(
//                 //     ''        => __( 'Select supplier', 'woocommerce' ),
//                 //     'Nowa'    => __('Nowa', 'woocommerce' ),
//                 //     'Uzywana' => __('Uzywana', 'woocommerce' ),
//                 // )
//             )
//         );
//     }
    

//     public function addReservationName( $loop, $variation_data, $variation )
//     {
//         // Your Text Field
//         woocommerce_wp_text_input(
//             array(
//                 'id'                => "tab_reservation_name{$loop}",
//                 'name'              => "tab_reservation_name[{$loop}]",
//                 'value'             => get_post_meta( $variation->ID, 'tab_reservation_name', true ),
//                 'label'             => __('Reservation Owner Name', 'woocommerce'),
//                 'placeholder'       => 'Name on Reservation',
//                 'wrapper_class'     => 'form-row form-row-full',
//                 'desc_tip'          => 'true',
//                 'description'       => __('Required for all reservations. This is the name on the reservation.', 'woocommerce'),
//             )
//         );
//     }

//     function saveReservationName( $variation_id, $loop ) {
//         $text_field = $_POST['tab_reservation_name'][ $loop ];

//         if ( ! empty( $text_field ) ) {
//             update_post_meta( $variation_id, 'tab_reservation_name', esc_attr( $text_field ));
//         }
//     }
     
//     public function addReservationNameVariationData( $variation )
//     {
//         $variation['tab_reservation_name'] = get_post_meta( $variation[ 'variation_id' ], 'tab_reservation_name', true );

//         return $variation;
//     }


//     public function hide_stuff() {
        
//         global $post;
//         $product = new WC_Product( $post->ID );
//         $sku = $product->get_sku();
       
//         // If not procurement product, quit
//         if ( substr( $sku, 0, 2 ) != 'P-' ) return;

//         $hide_old = 
//             '<style>'.
//                 '.woocommerce_variable_attributes.wc-metabox-content,'.
//                 '.handlediv'.
//                  '{display:none !important;}'.
//                  '.woocommerce_variation.wc-metabox h3'.
//                  '{cursor: unset !important;}'.
//             '</style>';

//         $hide = 
//             '<style>'.
//                 '.postarea.wp-editor-expand'.
//                  '{display:none !important;}'.
//             '</style>';
            

//         echo $hide;
// 	}
// }