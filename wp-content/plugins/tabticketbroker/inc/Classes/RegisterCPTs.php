<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Classes;

use Inc\Tools\ExportTools;
use Inc\Base\BaseController;
use Inc\Classes\TabSupplier;
use Inc\Classes\TabReservation;

class RegisterCPTs extends BaseController
{
    public $supplier                    = null;
    public $res_statuses                = null;
    public $reservation                 = null;
    public $reservation_editor          = null;
    public $reservation_filters         = array();
    public $multi_select_filters        = array();
    public $multi_select_filter_labels  = array();

    public function register()
    {
        $this->registerSupplierCpt();

        $this->registerReservationCpt();

        // Hightlight active menu item
        add_filter( 'parent_file', array( $this, 'highlightActiveMenuItem' ) );
    }  

    public function registerSupplierCpt()
    {
        // Register post type
        add_action( 'init', array( $this, 'addTabSupplierCPT' ) );
        
        // Register columns
        add_filter( 'manage_'. $this->tab_supplier_cpt_title .'_posts_columns', array($this, 'registerSupplierColumns' ) );
        
        // Populate columns
        add_action( 'manage_'. $this->tab_supplier_cpt_title .'_posts_custom_column' , array($this, 'populateSupplierColumns' ), 10, 2 );

        // Add sorting columns
        $screen_id = 'edit-tab_supplier';
        add_filter( 'manage_'. $screen_id .'_sortable_columns', array($this, 'setSortSupplierColumns') );

        // Hide bulk actions
        add_filter( 'bulk_actions-edit-tab_supplier', array( $this, 'removeBulkActions' ) );
        
        // Sorting & Searching for columns
        add_action( 'pre_get_posts', array( $this, 'orderBySupplier' ) );
        add_action( 'pre_get_posts', array( $this, 'searchSupplier' ) );
    }

    public function registerReservationCpt()
    {
        // Register post type
        add_action( 'init', array( $this, 'addTabReservationCPT' ) );

        // Set default array of filters
        add_action('init', array($this, 'setReservationFilters'), 20);

        // Register columns
        add_filter( 'manage_'. $this->tab_reservation_cpt_title .'_posts_columns', array($this, 'registerReservationColumns') );
        
        // Populate columns
        add_action( 'manage_'. $this->tab_reservation_cpt_title .'_posts_custom_column' , array($this, 'populateReservationColumns' ), 10, 2 );
        
        // Add sorting columns
        $screen_id = 'edit-tab_reservation';
        add_filter( 'manage_'. $screen_id .'_sortable_columns', array($this, 'setSortReservationColumns') );
        
        // Register taxonomies
        add_action( 'init', array( $this, 'registerReservationTaxonomy' ) );
        
        // Hides title on reservations CPT 
        add_action( 'load-post.php', array( $this, 'remove_post_type_edit_screen' ), 10 );
        
        // Register action & filter to add new filter for Reservations('Show All') page
        add_action('restrict_manage_posts', array( $this, 'addReservationFilters' ), 10, 1 );
        add_filter( 'parse_query', array( $this, 'applyReservationProductFilter' ) );
        
        // Add button next to title to export reservations
        add_action('admin_head-edit.php', array( $this, 'addExportButton'), 10 );

        // Register action to export reservations if export=true is set in the url
        add_action( 'init', array( $this, 'exportReservations' ) );

        // Remove published date filter
        add_action('admin_head', array( $this, 'removeDateFilter' ) );

        // Hide bulk actions
        add_filter( 'bulk_actions-edit-tab_reservation', array( $this, 'removeBulkActions' ) );

        // Custom notices
        add_filter( 'post_updated_messages', array( $this, 'changeReservationNoticeText' ) );

        // Add search functionality to reservations, mainly to search by ID and SKU
        add_action( 'pre_get_posts', array( $this, 'searchReservations' ) );
        add_action( 'pre_get_posts', array( $this, 'orderByReservation' ) );

        // Add page loader html to reservations page
        add_action( 'admin_footer', array( $this, 'addPageLoaderHtml' ) );
    }

    /** 
     * Add export button to reservations page
     */
    public function addExportButton()
    {
        global $pagenow;
        if ( $pagenow == 'edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] == 'tab_reservation' ) {
            ?>
            <script type="text/javascript">
                jQuery(function () {
                    jQuery('hr.wp-header-end').before('<a id="jmd_export_res" href="edit.php?post_type=tab_reservation&export=true" class="add-new-h2">Export CSV</a>');
                });                 
            </script>
            
            <?php
        }

    }

    /**
     * Export reservations
     */
    public function exportReservations()
    {
        global $pagenow;
        if ( $pagenow == 'edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] == 'tab_reservation' && isset( $_GET['export'] ) && $_GET['export'] == 'true' ) {
            
            $reservation = new TabReservation();

            $reservation->exportReservations();

            
        }
    }

    /**
     * Add page loader html to specific pages
     */
    function addPageLoaderHtml() 
    {
        global $pagenow;
        if ( 
               $pagenow == 'edit.php' 
            && isset( $_GET['post_type'] ) 
            && $_GET['post_type'] == 'tab_reservation' 
            ){                
                echo $this->getLoaderHtml();
            }
    }

    /**
     * Custom post updated messages
     */
    function changeReservationNoticeText( $messages ) {
        $post             = get_post();
        $post_type        = get_post_type( $post );
        $post_type_object = get_post_type_object( $post_type );

        $messages['tab_reservation'] = array(
            0  => '', // Unused. Messages start at index 1.
            1  => __( 'Reservation updated.', 'tabticketbroker' ),
            2  => __( 'Custom field updated.', 'tabticketbroker' ),
            3  => __( 'Custom field deleted.', 'tabticketbroker' ),
            4  => __( 'Reservation updated.', 'tabticketbroker' ),
            5  => isset( $_GET['revision'] ) ? sprintf( __( 'Reservation restored to revision from %s', 'tabticketbroker' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
            6  => __( 'Reservation created.', 'tabticketbroker' ),
            7  => __( 'Reservation saved.', 'tabticketbroker' ),
            8  => __( 'Reservation submitted.', 'tabticketbroker' ),
            9  => sprintf(
                __( 'Reservation scheduled for: <strong>%1$s</strong>.', 'tabticketbroker' ),
                date_i18n( __( 'M j, Y @ G:i', 'tabticketbroker' ), strtotime( $post->post_date ) )
            ),
            10 => __( 'Reservation draft updated.', 'tabticketbroker' )
        );

        $messages['tab_supplier'] = array(
            0  => '', // Unused. Messages start at index 1.
            1  => __( 'Supplier updated.', 'tabticketbroker' ),
            2  => __( 'Custom field updated.', 'tabticketbroker' ),
            3  => __( 'Custom field deleted.', 'tabticketbroker' ),
            4  => __( 'Supplier updated.', 'tabticketbroker' ),
            5  => isset( $_GET['revision'] ) ? sprintf( __( 'Supplier restored to revision from %s', 'tabticketbroker' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
            6  => __( 'Supplier created.', 'tabticketbroker' ),
            7  => __( 'Supplier saved.', 'tabticketbroker' ),
            8  => __( 'Supplier submitted.', 'tabticketbroker' ),
            9  => sprintf(
                __( 'Supplier scheduled for: <strong>%1$s</strong>.', 'tabticketbroker' ),
                date_i18n( __( 'M j, Y @ G:i', 'tabticketbroker' ), strtotime( $post->post_date ) )
            ),
            10 => __( 'Supplier draft updated.', 'tabticketbroker' )
        );

        // if ( ! isset( $messages[ $post_type ] ) ) $messages[ $post_type ] = array();

        if ( $post_type_object->publicly_queryable ) {
            $permalink = get_permalink( $post->ID );

            $view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View reservation', 'tabticketbroker' ) );
            $messages[ $post_type ][1] = $view_link;
            $messages[ $post_type ][6] = $view_link;
            $messages[ $post_type ][9] = $view_link;

            $preview_permalink = add_query_arg( 'preview', 'true', $permalink );
            $preview_link      = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview reservation', 'tabticketbroker' ) );
            $messages[ $post_type ][8] = $preview_link;
            $messages[ $post_type ][10] = $preview_link;
        }

        return $messages;
    }

    /**
     * Highlights the active / current menu item. 
     * Usefull link: https://wordpress.stackexchange.com/questions/160224/show-custom-taxonomy-inside-custom-menu
     */
    function highlightActiveMenuItem ( $parent_file ) {
        
        global $submenu_file, $current_screen, $pagenow;

        # Set the submenu as active/current while anywhere in your Custom Post Type
        if  ( 
                    $current_screen->post_type == 'tab_reservation'
                ||  $current_screen->post_type == 'tab_supplier'
            ) {

            // This sets the Reservation Status editing (custom taxonomy) Menu item active when clicked to manage reservation statuses
            if ( $pagenow == 'edit-tags.php' ) {
                $submenu_file = 'edit-tags.php?taxonomy=tab_res_tax_status&post_type=' . $current_screen->post_type;
            }
            // This sets the Reservation Menu item active when Add new is clicked for reservations
            elseif ( $pagenow == 'post-new.php' ) {
                $submenu_file = 'edit.php?post_type=' . $current_screen->post_type;
            }
            elseif ( $pagenow == 'edit.php' ) {
                $submenu_file = 'edit.php?post_type=' . $current_screen->post_type .'&tab_reservation_event_year='. TTB_EVENT_YEAR;
            }

            $parent_file = 'tab_admin_plugin';
        }

        return $parent_file;
    }

    /**
     * This removes the filter with a dropdown of months applied to the publish dates of the CPT Reservations
     */
    public function removeDateFilter()
    {
        $screen = get_current_screen();
    
        if ( $screen->post_type == $this->tab_reservation_cpt_title ){
            add_filter('months_dropdown_results', '__return_empty_array');
        }
    }
      
    public function setReservationFilters()
    {
        $this->reservation_filters = array( 
            $this->tab_reservation_cpt_title . '_status'            => __( 'All Statuses',  'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_product_title'     => __( 'All Products',  'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_date'              => __( 'All Dates',     'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_pax'               => __( 'All Pax',       'tabticketbroker' ),
            // $this->tab_reservation_cpt_title . '_pax_assigned'      => __( 'All Assigned',  'tabticketbroker' ),
            // $this->tab_reservation_cpt_title . '_pax_open'          => __( 'All Open',      'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_timeslot'          => __( 'All Timesots',  'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_area'              => __( 'All Areas',     'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_supplier_name'     => __( 'All Suppliers', 'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_vouchers_received'  => __( 'All Vouchers',  'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_letter_received'   => __( 'All Letters',   'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_letter_ready'      => __( 'All Ready',     'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_docs_complete'     => __( 'All Docs',      'tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_ready_to_ship'     => __( 'All Ready to Ship','tabticketbroker' ),
            $this->tab_reservation_cpt_title . '_event_year'        => __( 'All Years',     'tabticketbroker' ),
        );

        $this->multi_select_filters = array( 'tab_reservation_status', 'tab_reservation_date', 'tab_reservation_product_title' );
        $this->multi_select_filter_labels = array( 
            'tab_reservation_status' => __( 'Statuses',  'tabticketbroker' ),
            'tab_reservation_date' => __( 'Dates',  'tabticketbroker' ),
            'tab_reservation_product_title' => __( 'Products',  'tabticketbroker' ),
        );
    }

    /**
     * Add extra dropdowns to the List Tables
     *
     * @param required string $post_type    The Post Type that is being displayed
     */
    public function addReservationFilters( $post_type )
    {
        // Only do for reservations
        if (  $post_type == 'tab_reservation' ) {

            echo '<div class="ttb-filter-container">';
            
            $i = 0;
            foreach ( $this->reservation_filters as $filter_meta_key => $filter_collective_name ) {
                $this->addFilter( $post_type, $filter_meta_key, $filter_collective_name );
                // add a break after the first  4 filters
                if ( $i >= count( $this->multi_select_filters ) - 1 ) {
                    echo '<br>';
                    $i = -1000;
                }
                $i++;
            }

            echo '</div>';

            // Add a hidden input field container div
            /* This is so that the multiple select filters still produce a parameter and 
            on the reload then maintains the selected values */
            echo '<div id="ttb-hidden-input-fields" style="display:none;"></div>';
        }
    }

    /**
     * @param required string $meta_key   The meta key of the dimension to filter on
     */
    
    public function addFilter( $post_type, $meta_key, $collective_name = 'All')
    {
        global $wpdb;
        
        $query = $wpdb->prepare(
           'SELECT DISTINCT pm.meta_value FROM %1$s pm
            LEFT JOIN %2$s p ON p.ID = pm.post_id
            WHERE pm.meta_key = "%3$s" 
            AND p.post_status = "%4$s" 
            AND p.post_type = "%5$s"
            AND pm.meta_value <> ""
            ORDER BY pm.meta_value',
            $wpdb->postmeta,
            $wpdb->posts,
            $meta_key,          // Your meta key - change as required
            'publish',          // Post status - change as required
            $post_type
        );

        $results = $wpdb->get_col( $query );
        
        /** Ensure there are options to show */
        if( empty( $results ) ) return;

        // Sort the different filters
        if ( in_array( $meta_key, array( 'tab_reservation_pax', 'tab_reservation_pax_assigned', 'tab_reservation_pax_open' ) ) ) {
            // Convert array to int
            $results = array_map( 
                function( $value ) { return intval( $value );},
                $results
            );

            // Sort by int
            asort( $results );
        } elseif ( $meta_key == 'tab_reservation_supplier_name' ) {
            // foreach ($results as $key => $value) {
            //     $supplier = new TabSupplier( $value );
            //     $display = $supplier->getName();
            //     $results[$value] = $display;
            //     unset($results[$key]);
            // }
            asort( $results );
        }

        // get selected option if there is one selected
        if ( isset( $_GET[$meta_key] ) && $_GET[$meta_key] != '' ) {
            $selected_name = $_GET[$meta_key];
        } else {
            $selected_name = array( 'all' );
        }

        // Convert selected name to array by splitting on comma
        if ( is_string( $selected_name ) ) {
            $selected_name = explode( ',', $selected_name );
        }
        
        // Add the ALL option ( if it is not a multi select filter )
        if ( ! in_array( $meta_key, $this->multi_select_filters ) ) {
            $options[] = sprintf( '<option value="all">%1$s</option>', $collective_name );
        }

        // If a year was filtered on, then filter the results on the year
        if ( $meta_key == 'tab_reservation_date' && isset( $_GET['tab_reservation_event_year'] ) && $_GET['tab_reservation_event_year'] != 'all' ) {
            $year = $_GET['tab_reservation_event_year'];
            $results = array_filter( $results, function( $value ) use ( $year ) {
                return strpos( $value, $year ) !== false;
            });
        }

        // Add all the options for the filter
        foreach( $results as $key => $result ) :
            $value = esc_attr( $result );
            $display = $result;

            // Set custom displays
            if ( $meta_key == 'tab_reservation_supplier_id' ) {
                $value = $key;
            }
            elseif ( $meta_key == 'tab_reservation_timeslot' ) {
                $display = get_term_by('slug', $value, 'pa_timeslot') ? get_term_by('slug', $value, 'pa_timeslot')->name : $result;
            }
            elseif( $meta_key == 'tab_reservation_area' ) {
                $display = get_term_by('slug', $value, 'pa_area') ? get_term_by('slug', $value, 'pa_area')->name : $result;
            }
            elseif( $meta_key == 'tab_reservation_status' ) {
                $display = get_term_by('slug', $value, 'tab_res_tax_status') ? get_term_by('slug', $value, 'tab_res_tax_status')->name : $result;
            }
            elseif( $meta_key == 'tab_reservation_date' ) {
                $display = $this->getDisplayDate( $value, 'D d-m Y' );
            }
            elseif( $meta_key == 'tab_reservation_vouchers_received' ) {
                $display = $value == 1 ? __( 'Yes', 'tabticketbroker' ) : __( 'No', 'tabticketbroker' );
            }
            elseif( $meta_key == 'tab_reservation_letter_received' ) {
                $display = $value == 1 ? __( 'Yes', 'tabticketbroker' ) : __( 'No', 'tabticketbroker' );
            }
            elseif( $meta_key == 'tab_reservation_letter_ready' ) {
                $display = $value == 1 ? __( 'Yes', 'tabticketbroker' ) : __( 'No', 'tabticketbroker' );
            }
            elseif( $meta_key == 'tab_reservation_docs_complete' ) {
                $display = $value == 1 ? __( 'Yes', 'tabticketbroker' ) : __( 'No', 'tabticketbroker' );
            }
            elseif( $meta_key == 'tab_reservation_ready_to_ship' ) {
                $display = $value == 1 ? __( 'Yes', 'tabticketbroker' ) : __( 'No', 'tabticketbroker' );
            }

            // Set selected from GET var
            $selected = in_array( $value, $selected_name ) ? 'selected="selected"' : '';

            // Add option to array
            $options[] = sprintf( '<option value="%1$s" %2$s>%3$s</option>', $value, $selected, $display );

        endforeach;

        // Add the filter to the page
        if ( in_array( $meta_key, $this->multi_select_filters ) ) {
            // echo '<div class="multi-select-filter-subcontainer">'; // open div
            echo '<label>'. $this->multi_select_filter_labels[$meta_key] .'</label>';
            echo '<select class="ttb-res-filter" id="'. $meta_key .'" name="'. $meta_key .'" multiple onchange="addHiddenInputFields(\''. $meta_key .'\', Array.from(this.selectedOptions).map(x=>x.value??x.text))" multiselect-hide-x="true">';

        } else {
            echo '<select class="ttb-res-filter" id="'. $meta_key .'" name="'. $meta_key .'">';
        }
    
        // If options are null
        if ( ! isset( $options ) ) {
            $options[] = sprintf( '<option value="all">%1$s</option>', $collective_name );
        }
        else {
            echo join( "\n", $options );
        }

        echo '</select>';

        // Add the hidden input field to the container div if it is a multi select filter
        if ( in_array( $meta_key, $this->multi_select_filters ) ) {
            // echo '</div>'; // close above div
            $meta_value = isset( $_GET[$meta_key] ) ? $_GET[$meta_key] : '';
            echo '<input type="hidden" name="' . $meta_key . '" value="' . $meta_value . '">';
        }
    }

    function applyReservationProductFilter( $query )
    {
        if( $query->is_main_query() && is_admin() && $query->query_vars['post_type'] == 'tab_reservation' ) {

            // Reset the tax query if it is set 
            // On creation of the reservations, they were not properly linked to the taxonomies but 
            // the taxonomies were saved as post meta data. Thus we need to remove the tax query 
            // and add it to the meta query, which is done below.
            if( isset( $query->tax_query ) && isset( $_GET['tab_res_tax_status'] ) ) {
                // here is the hack,
                unset( $query->query_vars['tab_res_tax_status'] ); 
                // these were unset but had no effect
                // $query->tax_query->queries = array();
                // $query->tax_query->queried_terms = array();
                // $query->is_tax = false;
                $_GET['tab_reservation_status'] = $_GET['tab_res_tax_status'];
            }
            
            //Get original meta query
            $meta_query = $query->get( 'meta_query' ) ? $query->get( 'meta_query' ) : array();
            
            foreach ( $this->reservation_filters as $filter_meta_key => $filter_collective_name ) {

                // If the filter is not set, skip it
                if ( ! isset( $_GET[$filter_meta_key] ) || $_GET[$filter_meta_key] == '' ) continue;


                $numeric_fields = array(
                    'tab_reservation_pax',
                    'tab_reservation_pax_assigned',
                    'tab_reservation_pax_open',
                    'tab_reservation_vouchers_received',
                    'tab_reservation_letter_received',
                    'tab_reservation_letter_ready',
                    'tab_reservation_docs_complete',
                );

                // Set compare operator to equal except when filtering by pax, pax_assigned, pax_open
                if ( in_array( $filter_meta_key, $numeric_fields ) ) {
                    $compare    = '>=';
                    $value      = intval( $_GET[$filter_meta_key] );
                    $type       = 'NUMERIC';
                } elseif ( in_array( $filter_meta_key, array( 'tab_reservation_event_year' ) ) ) {
                    $compare    = '=';
                    $value      = intval( $_GET[$filter_meta_key] );
                    $type       = 'NUMERIC';
                } elseif ( in_array( $filter_meta_key, array( 'tab_reservation_status', 'tab_reservation_product_title', 'tab_reservation_date' ) ) ) {
                    $compare    = 'IN';
                    $value      = explode( ',', $_GET[$filter_meta_key] );
                    $type       = '';
                } else {
                    $compare    = '=';
                    $value      = $_GET[$filter_meta_key];
                    $type       = 'CHAR';
                }

                // Check if the filter is set
                if ( isset( $_GET[$filter_meta_key] ) 
                    && $_GET[$filter_meta_key] != 'all' 
                    && $_GET[$filter_meta_key] != ''
                ) {
                   $meta_query[] = array(
                            'key'       => $filter_meta_key,
                            'value'     => $value,
                            'compare'   => $compare,
                            'type'      => $type
                        );
                }    
            }
            
            if ( count( $meta_query ) > 1 ) {
                $meta_query['relation'] = 'AND';
            }
            
            $query->set( 'meta_query', $meta_query );
            $i = 1;
        }
    }

    function remove_post_type_edit_screen() {

        global $typenow;
    
        if( $typenow && $typenow === $this->tab_reservation_cpt_title ){
            remove_post_type_support( $this->tab_reservation_cpt_title, 'title' );
        }
    }

    function registerReservationTaxonomy() {
        $labels = array(
            'name'              => _x ( 'Reservation Status', 'taxonomy general name' ),
            'singular_name'     => _x( 'Reservation Status', 'taxonomy singular name' ),
            'search_items'      => __( 'Search Reservation Statuses' ),
            'all_items'         => __( 'All Reservation Statuses' ),
            'parent_item'       => __( 'Parent Reservation Status' ),
            'parent_item_colon' => __( 'Parent Reservation Status:' ),
            'edit_item'         => __( 'Edit Reservation Status' ),
            'update_item'       => __( 'Update Reservation Status' ),
            'add_new_item'      => __( 'Add New Reservation Status' ),
            'new_item_name'     => __( 'New Reservation Status Name' ),
            'menu_name'         => __( 'Manage Statuses' ),
            'back_to_items'     => __( 'Back to Statuses' )
        );

        $args   = array(
            'hierarchical'      => false, // make it hierarchical (like categories)
            'orderby'           => 'slug',
            'orderby'           => 'slug',
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'meta_box_cb'       => false,
            // 'rewrite'           => [ 'slug' => $this->tab_reservation_status_taxonomy ],
            // 'capabilities' => array(
            //     'manage_terms'  => 'manage_reservation_status',
            //     'edit_terms'    => 'edit_reservation_status',
            //     'delete_terms'  => 'delete_reservation_status',
            //     'assign_terms'  => 'assign_reservation_status',
            // )
        );

        register_taxonomy( $this->tab_reservation_status_taxonomy, [ $this->tab_reservation_cpt_title ], $args );

    }
   

    public function addTabReservationCPT()
    {
        $labels = array(
            'name'               => __( 'Reservations', 'tabticketbroker' ),
            'singular_name'      => __( 'Reservation', 'tabticketbroker' ),
            'menu_name'          => __( 'Reservations', 'tabticketbroker' ),
            'name_admin_bar'     => __( 'Reservation', 'tabticketbroker' ),
            'add_new'            => __( 'Add New', 'tabticketbroker' ),
            'add_new_item'       => __( 'Add New Reservation', 'tabticketbroker' ),
            'new_item'           => __( 'New Reservation', 'tabticketbroker' ),
            'edit_item'          => __( 'Edit Reservation', 'tabticketbroker' ),
            'view_item'          => __( 'View Reservation', 'tabticketbroker' ),
            'all_items'          => __( 'Reservations', 'tabticketbroker' ),
            'search_items'       => __( 'Search Reservations', 'tabticketbroker' ),
            'parent_item_colon'  => __( 'Parent Reservation', 'tabticketbroker' ),
            'not_found'          => __( 'No Reservations Found', 'tabticketbroker' ),
            'not_found_in_trash' => __( 'No Reservations Found', 'tabticketbroker' )
        );

        $args = array(
            'labels'              => $labels,
            'public'              => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => false,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'show_in_menu'        => false, // Not required: Link added in AdminPages with the year filter as a parameter
            'show_in_admin_bar'   => false,
            'menu_icon'           => 'dashicons-buddicons-pm',
            'hierarchical'        => true,
            'supports'            => array( 'title' ),
            'has_archive'         => true,
            'query_var'           => true,
            'rewrite'           => [ 'slug' => $this->tab_reservation_cpt_title ],

            'capability_type'     => 'reservation',
            'map_meta_cap'        => true,

        );

        register_post_type( $this->tab_reservation_cpt_title, $args);
    }

    public function addTabSupplierCPT()
    {
        $labels = array(
            'name'               => __( 'Suppliers', 'tabticketbroker' ),
            'singular_name'      => __( 'Supplier', 'tabticketbroker' ),
            'menu_name'          => __( 'Suppliers', 'tabticketbroker' ),
            'name_admin_bar'     => __( 'Supplier', 'tabticketbroker' ),
            'add_new'            => __( 'Add New', 'tabticketbroker' ),
            'add_new_item'       => __( 'Add New Supplier', 'tabticketbroker' ),
            'new_item'           => __( 'New Supplier', 'tabticketbroker' ),
            'edit_item'          => __( 'Edit Supplier', 'tabticketbroker' ),
            'view_item'          => __( 'View Supplier', 'tabticketbroker' ),
            'all_items'          => __( 'Suppliers', 'tabticketbroker' ),
            'search_items'       => __( 'Search Suppliers', 'tabticketbroker' ),
            'parent_item_colon'  => __( 'Parent Supplier', 'tabticketbroker' ),
            'not_found'          => __( 'No Suppliers Found', 'tabticketbroker' ),
            'not_found_in_trash' => __( 'No Suppliers Found', 'tabticketbroker' )
        );

        $args = array(
            'labels'              => $labels,
            'public'              => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => false,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'show_in_menu'        => false,
            'show_in_admin_bar'   => false,
            'menu_icon'           => 'dashicons-buddicons-buddypress-logo',
            'hierarchical'        => true,
            'supports'            => array( 'title' ),
            'has_archive'         => true,
            'query_var'           => true,
            'rewrite'             => [ 'slug' => $this->tab_supplier_cpt_title ],

            'capability_type'     => 'supplier',
            'map_meta_cap'        => true,


        );

        register_post_type( $this->tab_supplier_cpt_title, $args);
    }

    public function registerReservationColumns( $columns )
    {
        $columns_new = array();
        
        // $columns_new['cb']      = $columns['cb']; // Checkbox for bulk actions
        
        $columns_new[$this->tab_reservation_cpt_title . '_code']           = __( 'Code'             , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_sku']            = __( 'SKU'              , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_supplier_name']  = __( 'Supplier'         , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_status']         = __( 'Status'           , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_event_year']     = __( 'Year'             , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_product_title']  = __( 'Product'          , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_date']           = __( 'Date'             , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_start_time']     = __( 'Start Time'       , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_timeslot']       = __( 'Timeslot'         , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_area']           = __( 'Area/Class'       , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_pax']            = __( 'Pax'              , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_pax_assigned']   = __( 'Assigned'         , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_pax_open']       = __( 'Open'             , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_owner_name']     = __( 'Owner'            , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_customer_no']    = __( 'Customer No'      , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_booking_no']     = __( 'Booking No'       , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_orders']         = __( 'Orders'           , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_order_count']    = __( 'Order Count'      , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_vouchers_received']   = __( 'Vouchers Received'    , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_letter_received']     = __( 'Letter Received'    , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_letter_ready']        = __( 'Letter Ready'    , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_docs_complete']       = __( 'Docs Complete'    , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_ready_to_ship']       = __( 'Ready to Ship'    , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_ready_to_ship_date']  = __( 'Ready to Ship Date'    , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_source_url']          = __( 'Source URL'       , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_notes']               = __( 'Notes'            , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_shared_email']        = __( 'Shared Email'     , 'tabticketbroker' );
        $columns_new[$this->tab_reservation_cpt_title . '_shop_price']          = __( 'Shop Price'       , 'tabticketbroker' );

        $columns_new['date'] = __( 'Date' , 'tabticketbroker' );
            
        $columns = array_merge( $columns_new, $columns );
        
        $columns['author']=  __( 'Author' , 'tabticketbroker' );
        $columns['date'] =  __( 'Published' , 'tabticketbroker' );

        unset( $columns['title'] );
        unset( $columns['wpsite-show-ids'] ); // Already shown
        unset( $columns['taxonomy-tab_res_tax_status'] );

        return $columns;
    }

    /**
    * Custom columns in suppliers table
    */
    public function populateReservationColumns($column, $post_id)
    {
        // Check if the reservation object is already loaded and if the post id matches
        if ( ! isset( $this->reservation ) || $this->reservation->getId() != $post_id ) {
            $this->reservation = new TabReservation( $post_id );
        }

        // Create a global TabReservationEditor object if not already created
        if ( ! isset( $this->reservation_editor ) ) {
            $this->reservation_editor = new TabReservationEditor();
        }

        // Get res_statuses if not already set
        if (! isset( $this->res_statuses ) ) {
            $dimension = new Dimension;
            $dimension->initialize();
            $this->res_statuses = $dimension->getReservationStatuses();
        }
        
        switch ( $column ) :
            case $this->tab_reservation_cpt_title . '_code' :
                echo '<a href="/wp-admin/post.php?post='.$this->reservation->getId().'&action=edit">RS'.$this->reservation->getId().'</a>';
                break;
            case $this->tab_reservation_cpt_title . '_sku' :
                echo $this->reservation->getSku();
                break;
            case $this->tab_reservation_cpt_title . '_supplier_name' :
                echo '<a href="/wp-admin/post.php?post='.$this->reservation->getSupplierId().'&action=edit">'.$this->reservation->getSupplierName().'</a>';
                break;
            case $this->tab_reservation_cpt_title . '_status' :
                echo '<div class="reservation-status status-'.$this->reservation->getStatus().'">'.$this->reservation->getStatusDisplay().'</div>';
                echo $this->reservation->getChangeLinkHtml( $this->res_statuses, $this->reservation_editor );
                break;
            case $this->tab_reservation_cpt_title . '_product_title' :
                echo $this->reservation->getProductTitle();
                break;
            case $this->tab_reservation_cpt_title . '_event_year' :
                echo $this->reservation->getEventYear();
                break;
            case $this->tab_reservation_cpt_title . '_date' :
                echo $this->reservation->getResDisplayDate();
                break;
            case $this->tab_reservation_cpt_title . '_start_time' :
                echo $this->reservation->getStartTime();
                break;
            case $this->tab_reservation_cpt_title . '_pax' :
                echo $this->reservation->getPax();
                break;
            case $this->tab_reservation_cpt_title . '_pax_assigned' :
                echo $this->reservation->getPaxAssigned();
                break;
            case $this->tab_reservation_cpt_title . '_pax_open' :
                echo $this->reservation->getPaxOpen();
                break;
            case $this->tab_reservation_cpt_title . '_timeslot' :
                echo $this->reservation->getTimeslotDisplay();
                break;
            case $this->tab_reservation_cpt_title . '_area' :
                echo $this->reservation->getAreaDisplay();
                break;
            case $this->tab_reservation_cpt_title . '_owner_name' :
                echo $this->reservation->getOwnerName();
                break;
            // Add table_no
            case $this->tab_reservation_cpt_title . '_table_no' :
                echo $this->reservation->getTableNo();
                break;
            // Add cutomer_no
            case $this->tab_reservation_cpt_title . '_customer_no' :
                echo $this->reservation->getCustomerNo();
                break;
            // Add booking_no
            case $this->tab_reservation_cpt_title . '_booking_no' :
                echo $this->reservation->getBookingNo();
                break;
            case $this->tab_reservation_cpt_title . '_orders' :
                // print each link to the order page in $this->reservation->getOrderItemLinks()
                foreach ( $this->reservation->getOrderItemLinks() as $link ) {
                    echo $link;
                }
                break;
            case $this->tab_reservation_cpt_title . '_order_count' :
                echo $this->reservation->getOrderCount();
                break;
            case $this->tab_reservation_cpt_title . '_vouchers_received' :
                $checked = $this->reservation->getVouchersReceived() ? 'checked' : '';
                echo '<input class="ttb_checkbox_input" disabled type="checkbox" '. $checked .'>'; 
                break;
            case $this->tab_reservation_cpt_title . '_letter_received' :
                $checked = $this->reservation->getLetterReceived() ? 'checked' : '';
                echo '<input class="ttb_checkbox_input" disabled type="checkbox" '. $checked .'>'; 
                break;
            case $this->tab_reservation_cpt_title . '_letter_ready' :
                $checked = $this->reservation->getLetterReady() ? 'checked' : '';
                echo '<input class="ttb_checkbox_input" disabled type="checkbox" '. $checked .'>'; 
                break;
            case $this->tab_reservation_cpt_title . '_docs_complete' :
                $checked = $this->reservation->getDocsComplete() ? 'checked' : '';
                echo '<input class="ttb_checkbox_input" disabled type="checkbox" '. $checked .'>'; 
                break;
            case $this->tab_reservation_cpt_title . '_ready_to_ship' :
                $checked = $this->reservation->getReadyToShip() ? 'checked' : '';
                echo '<input class="ttb_checkbox_input" disabled type="checkbox" '. $checked .'>'; 
                break;
            case $this->tab_reservation_cpt_title . '_ready_to_ship_date' :
                if ( $this->reservation->getReadyToShipDate() ) {
                    echo date( 'd-m-Y H:i', strtotime( $this->reservation->getReadyToShipDate() ) );
                }
                break;
            case $this->tab_reservation_cpt_title . '_notes' :
                echo nl2br( $this->reservation->getNotes() );
                break;
            case $this->tab_reservation_cpt_title . '_source_url' :
                // Create a link to the source URL
                $source_url = $this->reservation->getSourceUrl();
                if ( $source_url ) {
                    echo '<a href="'.$source_url.'" target="_blank">'. __( 'Go to link', 'tabticketbroker' ) .'</a>';
                }
                break;
            case $this->tab_reservation_cpt_title . '_shared_email' :
                // Check if the reservation has more than 1 order item ids
                $order_item_ids = $this->reservation->getOrderItemIds();
                if ( count( $order_item_ids ) > 1 ) {
                    $email_log = JMD_Logger::metalog( $this->reservation->getId(), '_email_shared_reservation_log' );
                    // If there is an email log, show the last email sent
                    if ( $email_log ) {
                        $last_email = end( $email_log );
                        echo __( 'Sent on', 'tabticketbroker' ) . ' ' . $last_email['date'] . ' ' . __( 'by', 'tabticketbroker' ) . ' ' . $last_email['user'];
                    } 
                    else {
                        echo __( 'Not sent', 'tabticketbroker' );
                    }
                } else {
                    echo '';
                }
                break;
            case $this->tab_reservation_cpt_title . '_shop_price' :
                    ?>
                    <?php echo $this->reservation->getShopPrice(); ?>
                    <?php
                    break;
        endswitch;
    }

    function setSortReservationColumns( $columns ) 
    {
        $columns[$this->tab_reservation_cpt_title . '_code']            = $this->tab_reservation_cpt_title . '_code';
        $columns[$this->tab_reservation_cpt_title . '_sku']             = $this->tab_reservation_cpt_title . '_sku';
        $columns[$this->tab_reservation_cpt_title . '_supplier_name']   = $this->tab_reservation_cpt_title . '_supplier_name';
        $columns[$this->tab_reservation_cpt_title . '_status']          = $this->tab_reservation_cpt_title . '_status';
        $columns[$this->tab_reservation_cpt_title . '_product_title']   = $this->tab_reservation_cpt_title . '_product_title';
        $columns[$this->tab_reservation_cpt_title . '_event_year']      = $this->tab_reservation_cpt_title . '_date'; // same as date
        $columns[$this->tab_reservation_cpt_title . '_date']            = $this->tab_reservation_cpt_title . '_date';
        $columns[$this->tab_reservation_cpt_title . '_start_time']      = $this->tab_reservation_cpt_title . '_start_time';
        $columns[$this->tab_reservation_cpt_title . '_pax']             = $this->tab_reservation_cpt_title . '_pax';
        $columns[$this->tab_reservation_cpt_title . '_pax_assigned']    = $this->tab_reservation_cpt_title . '_pax_assigned';
        $columns[$this->tab_reservation_cpt_title . '_pax_open']        = $this->tab_reservation_cpt_title . '_pax_open';
        $columns[$this->tab_reservation_cpt_title . '_timeslot']        = $this->tab_reservation_cpt_title . '_timeslot';
        $columns[$this->tab_reservation_cpt_title . '_area']            = $this->tab_reservation_cpt_title . '_area';
        $columns[$this->tab_reservation_cpt_title . '_owner_name']      = $this->tab_reservation_cpt_title . '_owner_name';
        $columns[$this->tab_reservation_cpt_title . '_customer_no']     = $this->tab_reservation_cpt_title . '_customer_no';
        $columns[$this->tab_reservation_cpt_title . '_booking_no']      = $this->tab_reservation_cpt_title . '_booking_no';
        $columns[$this->tab_reservation_cpt_title . '_order_count']     = $this->tab_reservation_cpt_title . '_order_count';
        $columns[$this->tab_reservation_cpt_title . '_docs_complete']   = $this->tab_reservation_cpt_title . '_docs_complete';
        $columns[$this->tab_reservation_cpt_title . '_ready_to_ship']   = $this->tab_reservation_cpt_title . '_ready_to_ship';
        $columns[$this->tab_reservation_cpt_title . '_ready_to_ship_date'] = $this->tab_reservation_cpt_title . '_ready_to_ship_date';
        // $columns[$this->tab_reservation_cpt_title . '_shared_email']    = $this->tab_reservation_cpt_title . '_email_shared_reservation_log'; // sort the log
        $columns[$this->tab_reservation_cpt_title . '_shop_price']   = $this->tab_reservation_cpt_title . '_shop_price';
     
        return $columns;
    }
    
    /**
     * Sorting and filtering uses both meta query. These can overwrite each other. See this post:
     * NB: https://wordpress.stackexchange.com/questions/20237/using-meta-query-how-can-i-filter-by-a-custom-field-and-order-by-another-one
     */
   function orderByReservation( $query ) 
    {
        global $pagenow;

        if ( 
                $query->is_main_query() 
            && is_admin() 
            && $query->query_vars['post_type'] == 'tab_reservation' 
            && $pagenow == 'edit.php'
            && ! in_array( // skip default sorting
                    $query->query_vars['orderby'],
                    array( 'date', 'title', 'author', 'modified', 'parent', 'ID', 'rand', 'comment_count', 'menu_order title', 'menu_order' ) 
                )
            ) {
            
            // Only apply sorting for reservations page in admin
            // Set the key to the order by
            $query->set( 'meta_key', $query->query_vars['orderby'] );
            
            // Specify which columns should use numeric sorting
            $is_numeric = array (
                'id',
                $this->tab_reservation_cpt_title . '_pax',
                $this->tab_reservation_cpt_title . '_pax_assigned',
                $this->tab_reservation_cpt_title . '_pax_open',
                $this->tab_reservation_cpt_title . '_order_count',
                $this->tab_reservation_cpt_title . '_shop_price',
            );

            // For the ready-to-ship-date 
            // Change sort order around ( otherwise users have to click twice to get the right order, default is ASC )
            if ( $query->query_vars['orderby'] == "tab_reservation_ready_to_ship_date" ) {
                if ( $query->query_vars['order'] == 'asc' ) {
                    $query->set( 'order', 'desc' );
                } else {
                    $query->set( 'order', 'asc' );
                }                
            }

            // Specify and switch between alfa and numeric sorting 
            if ( in_array( $query->query_vars['orderby'], $is_numeric ) ) {
                $query->set( 'orderby', 'meta_value_num' );
            } 
            else {
                $query->set( 'orderby', 'meta_value' );
            }
        }
    }

    /**
     * Remove bulk actions from the reservation list
     */
    function removeBulkActions( $actions )
    {
        global $post_type;
        if ( $post_type == 'tab_reservation' || $post_type == 'tab_supplier' ) {
            return array();
        }
        return $actions;
    }

    public function registerSupplierColumns( $columns )
    {
        $columns_new = array();

        $columns_new['cb']      = $columns['cb']; // Checkbox for bulk actions
        $columns_new[$this->tab_supplier_cpt_title . '_name']  = __( 'Name' , 'tabticketbroker' );
        $columns_new[$this->tab_supplier_cpt_title . '_email']  = __( 'Email' , 'tabticketbroker' );
        $columns_new[$this->tab_supplier_cpt_title . '_phone']  = __( 'Phone' , 'tabticketbroker' );
        $columns_new[$this->tab_supplier_cpt_title . '_res_cnt'] = __( 'Number of reservations', 'tabticketbroker' );
        $columns_new['wpsite-show-ids'] = __( 'ID' , 'tabticketbroker' );
        
        unset( $columns['title'] );
        unset( $columns['author'] );

        $columns = array_merge( $columns_new, $columns );

        return $columns;
    }
    
    /**
    * Custom columns in suppliers table
    */
    public function populateSupplierColumns( $column, $post_id )
    {
        // Check if reservation is set
        if ( ! $this->reservation ) {
            $this->reservation = new TabReservation();
            $args = array( 'ttb_event_year' => TTB_EVENT_YEAR );
            $this->reservation->setReservations( $args );
        }

        // Check if the object is already loaded and if the post id matches
        if ( ! isset( $this->supplier ) || $this->supplier->getId() != $post_id ) {
            $this->supplier = new TabSupplier( $post_id );
        }
        
        switch ( $column ) :
            case $this->tab_supplier_cpt_title . '_name' :
                // Print link to edit supplier name with a title attribute
                echo '<a href="' . get_edit_post_link( $post_id ) . '" title="' . __( 'Edit supplier', 'tabticketbroker' ) . '">' . $this->supplier->getName() . '</a>';
                break;
            case $this->tab_supplier_cpt_title . '_email' :
                echo $this->supplier->getEmail();
                break;
            case $this->tab_supplier_cpt_title . '_phone' :
                echo $this->supplier->getPhone();
                break;
            case $this->tab_supplier_cpt_title . '_res_cnt' :
                // _e( 'CalculateHere', 'tabticketbroker' );
                echo count( $this->supplier->getLinkedReservations( $this->reservation->getReservations()  ) );
                break;
        endswitch;
    }

    
    function setSortSupplierColumns( $columns ) 
    {
        $columns[$this->tab_supplier_cpt_title . '_name']  = $this->tab_supplier_cpt_title . '_name';
        $columns[$this->tab_supplier_cpt_title . '_email'] = $this->tab_supplier_cpt_title . '_email';
        // $columns[$this->tab_supplier_cpt_title . '_res_cnt']    = $this->tab_supplier_cpt_title . '_res_cnt';
     
        return $columns;
    }



    // Source: https://wordpress.stackexchange.com/questions/78649/using-meta-query-meta-query-with-a-search-query-s#answer-242447#answer-373253
    function searchReservations( $query )  {

        global $pagenow;

        if ( 
                isset( $_GET['s'] ) 
            && $_GET['s'] != '' 
            && $query->is_search 
            && $query->is_main_query() 
            && is_admin() 
            && $query->query_vars['post_type'] == 'tab_reservation' 
            && $pagenow == 'edit.php' 
        ) {


            $meta_keys = array(
                $this->tab_reservation_cpt_title . '_code',
            );

            global $wpdb;
            $search = $query->query_vars['s']; // get the search string
            $ids = array(); // initiate array of martching post ids per searched keyword
            foreach (explode(' ',$search) as $term) { // explode keywords and look for matching results for each
                $term = trim($term); // remove unnecessary spaces
                if (!empty($term)) { // check the the keyword is not empty
                    
                    $query_posts = $wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE post_status='publish' AND post_type = 'tab_reservation' AND post_title = '".$search."'"); // prepare query to search for posts 
                    $ids_posts = [];
                    $results = $wpdb->get_results($query_posts);
                    if ($wpdb->last_error)
                        die($wpdb->last_error);
                    foreach ($results as $result)
                        $ids_posts[] = $result->ID; // gather matching post ids

                    $query_meta = [];
                    // foreach($query->query_vars['s_meta_keys'] as $meta_key) // now construct a search query the search in each desired meta key
                    foreach($meta_keys as $meta_key) // now construct a search query the search in each desired meta key
                        $query_meta[] = $wpdb->prepare("meta_key='%s' AND meta_value LIKE '%%%s%%'", $meta_key, $term);
                    $query_metas = $wpdb->prepare("SELECT * FROM {$wpdb->postmeta} WHERE ((".implode(') OR (',$query_meta)."))");
                    $ids_metas = [];
                    $results = $wpdb->get_results($query_metas);
                    if ($wpdb->last_error)
                        die($wpdb->last_error);
                    foreach ($results as $result)
                        $ids_metas[] = $result->post_id; // gather matching post ids
                    $merged = array_merge($ids_posts,$ids_metas); // merge the title, content and meta ids resulting from both queries
                    $unique = array_unique($merged); // remove duplicates
                    if (!$unique)
                        $unique = array(0); // if no result, add a "0" id otherwise all posts wil lbe returned
                    $ids[] = $unique; // add array of matching ids into the main array
                }
            }
            if (count($ids)>1)
                $intersected = call_user_func_array('array_intersect',$ids); // if several keywords keep only ids that are found in all keywords' matching arrays
            else
                $intersected = $ids[0]; // otherwise keep the single matching ids array
            $unique = array_unique($intersected); // remove duplicates
            if (!$unique)
                $unique = array(0); // if no result, add a "0" id otherwise all posts wil lbe returned
            unset($query->query_vars['s']); // unset normal search query
            $query->set('post__in',$unique); // add a filter by post id instead
        }
    }


    // Create search function for reservations to search by id
    function searchReservations_v0( $query ) 
    {
        global $pagenow;
        if ( 
                isset( $_GET['s'] ) 
            && $_GET['s'] != '' 
            && $query->is_search 
            && $query->is_main_query() 
            && is_admin() 
            && $query->query_vars['post_type'] == 'tab_reservation' 
            && $pagenow == 'edit.php' 
        ) {
            
            $search_term = $_GET['s'];
            
            //Get original meta query
            $meta_query = ( array )$query->get( 'meta_query' );

            $meta_query[] = array(
                    'key'     => 'tab_reservation_code',
                    'value'   => $search_term,
                    'compare' => 'LIKE'
            );
        
            if ( count( $meta_query ) > 1 ) {
                $meta_query['relation'] = 'AND';
            }

            $query->set( 'meta_query', $meta_query );

            // This was required, I guess it converts the SQL to sort instead of filtering. Haven't had time to investigate. It works.
            // https://wordpress.stackexchange.com/questions/78649/using-meta-query-meta-query-with-a-search-query-s#answer-242447
            add_filter( 'get_meta_sql', function( $sql )
            {
                global $wpdb;

                static $nr = 0;
                if( 0 != $nr++ ) return $sql;

                $sql['where'] = mb_eregi_replace( '^ AND', ' OR', $sql['where']);

                return $sql;
            });
        }
    }


    
    // Source: https://wordpress.stackexchange.com/questions/78649/using-meta-query-meta-query-with-a-search-query-s#answer-242447#answer-373253
    function searchSupplier( $query )  {

        global $pagenow;

        if ( 
                isset( $_GET['s'] ) 
            && $_GET['s'] != '' 
            && $query->is_search 
            && $query->is_main_query() 
            && is_admin() 
            && $query->query_vars['post_type'] == 'tab_supplier' 
            && $pagenow == 'edit.php' 
        ) {


            $meta_keys = array(
                'tab_supplier_name',
                'tab_supplier_email',
            );

            global $wpdb;
            $search = $query->query_vars['s']; // get the search string
            $ids = array(); // initiate array of martching post ids per searched keyword
            foreach (explode(' ',$search) as $term) { // explode keywords and look for matching results for each
                $term = trim($term); // remove unnecessary spaces
                if (!empty($term)) { // check the the keyword is not empty
                    
                    $query_posts = $wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE post_status='publish' AND post_type = 'tab_supplier' AND post_title = '".$search."'"); // prepare query to search for posts 
                    $ids_posts = [];
                    $results = $wpdb->get_results($query_posts);
                    if ($wpdb->last_error)
                        die($wpdb->last_error);
                    foreach ($results as $result)
                        $ids_posts[] = $result->ID; // gather matching post ids

                    $query_meta = [];
                    // foreach($query->query_vars['s_meta_keys'] as $meta_key) // now construct a search query the search in each desired meta key
                    foreach($meta_keys as $meta_key) // now construct a search query the search in each desired meta key
                        $query_meta[] = $wpdb->prepare("meta_key='%s' AND meta_value LIKE '%%%s%%'", $meta_key, $term);
                    $query_metas = $wpdb->prepare("SELECT * FROM {$wpdb->postmeta} WHERE ((".implode(') OR (',$query_meta)."))");
                    $ids_metas = [];
                    $results = $wpdb->get_results($query_metas);
                    if ($wpdb->last_error)
                        die($wpdb->last_error);
                    foreach ($results as $result)
                        $ids_metas[] = $result->post_id; // gather matching post ids
                    $merged = array_merge($ids_posts,$ids_metas); // merge the title, content and meta ids resulting from both queries
                    $unique = array_unique($merged); // remove duplicates
                    if (!$unique)
                        $unique = array(0); // if no result, add a "0" id otherwise all posts wil lbe returned
                    $ids[] = $unique; // add array of matching ids into the main array
                }
            }
            if (count($ids)>1)
                $intersected = call_user_func_array('array_intersect',$ids); // if several keywords keep only ids that are found in all keywords' matching arrays
            else
                $intersected = $ids[0]; // otherwise keep the single matching ids array
            $unique = array_unique($intersected); // remove duplicates
            if (!$unique)
                $unique = array(0); // if no result, add a "0" id otherwise all posts wil lbe returned
            unset($query->query_vars['s']); // unset normal search query
            $query->set('post__in',$unique); // add a filter by post id instead
        }
    }


   function searchSupplier_v0( $query ) 
   {
        global $pagenow;
        if ( 
               isset( $_GET['s'] ) 
            && $_GET['s'] != '' 
            && $query->is_search 
            && $query->is_main_query() 
            && is_admin() 
            && $query->query_vars['post_type'] == 'tab_supplier' 
            && $pagenow == 'edit.php' 
        ) {
            
            $search_term = $_GET['s'];
            
            //Get original meta query
            $meta_query = ( array )$query->get( 'meta_query' );

            $meta_query[] = array(
                    'key'     => 'tab_supplier_name',
                    'value'   => $search_term,
                    'compare' => 'LIKE'
            );
            $meta_query[] = array(
                    'key'     => 'tab_supplier_email',
                    'value'   => $search_term,
                    'compare' => 'LIKE'
            );
        
            if ( count( $meta_query ) > 1 ) {
                $meta_query['relation'] = 'OR';
            }

            $query->set( 'meta_query', $meta_query );

            // This was required, I guess it converts the SQL to sort instead of filtering. Haven't had time to investigate. It works.
            // https://wordpress.stackexchange.com/questions/78649/using-meta-query-meta-query-with-a-search-query-s#answer-242447
            add_filter( 'get_meta_sql', function( $sql )
            {
                global $wpdb;

                static $nr = 0;
                if( 0 != $nr++ ) return $sql;

                $sql['where'] = mb_eregi_replace( '^ AND', ' OR', $sql['where']);

                return $sql;
            });
        }
   }

    
    /**
     * Sorting and filtering uses both meta query. These can overwrite each other. See this post:
     * NB: https://wordpress.stackexchange.com/questions/20237/using-meta-query-how-can-i-filter-by-a-custom-field-and-order-by-another-one
     */
   function orderBySupplier( $query ) 
   {
       global $pagenow;
       if ( 
               $query->is_main_query() 
            && is_admin() 
            && $query->query_vars['post_type'] == 'tab_supplier' 
            && $pagenow == 'edit.php' 
            && ! in_array( // skip default sorting, but not for menu_order title because it is overridden by the supplier name by default
                $query->query_vars['orderby'],
                array( 'date', 'title', 'author', 'modified', 'parent', 'ID', 'rand', 'comment_count', 'menu_order' ) 
            )
        ) {
           
           // Get order by which was selected by the user
           $orderby = $query->get( 'orderby');

           // Set default order by name
           $orderby = ( $orderby == 'menu_order title' ? 'tab_supplier_name' : $orderby ); 
           
           // Get order
           $order = $query->get( 'order');
           
           // Set default order by name
           $order = ( $order == '' ? 'asc' : $order ); 
           $query->set( 'order', $order );
   
           // Return if default order by is sent
           // This avoids setting the order by to a field that does not exist and caused a black reservation page.
           if ( 
               $orderby == 'menu_order title' ||   // passed when page loaded initially
               $orderby == 'date'                  // passed when selected Published Date
               ) 
               return; 
           
           // Only apply sorting for reservations page in admin
           // Set the key to the order by
           $query->set( 'meta_key', $orderby );
           
           // Specify which columns should use numeric sorting
           $is_numeric = array (
            //    $this->tab_supplier_cpt_title . '_res_cnt',
           );

           // Specify and switch between alfa and numeric sorting 
           if ( in_array( $orderby, $is_numeric ) ) {
               $query->set( 'orderby', 'meta_value_num' );
           } 
           else {
               $query->set( 'orderby', 'meta_value' );
           }
       }
   }
}