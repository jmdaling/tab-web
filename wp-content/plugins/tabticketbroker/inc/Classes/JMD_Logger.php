<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Classes;

Class JMD_Logger
{
    // Set class properties

    /**
     * Generic meta log for posts
     * 
     * This function returns the complete log for a post if no input is given.
     * 
     * Otherwise it will log the new input and return the complete log.
     * 
     * Future: add an additional input to log a custom field with the last log status. 
     * This is currently done after the function call.
     * 
     * @param mixed $post_id the post ID
     * @param string $meta_key the meta key
     * @param array $meta_data the meta data to log
     */
    public static function metalog( $post_id, $meta_key = '', $meta_data = array() )
    {
        $log = get_post_meta( $post_id, $meta_key, true );

        if ( ! $log ) {
            $log = array();
        }

        // If the meta key already exists and is not an array, throw an exception
        if ( ! is_array( $log ) ) {
            throw new \Exception( 'The meta data is not an array' );
        }

        // If input vars are empty, return the log
        if ( empty( $meta_data ) ) {
            return $log;
        }

        // Else add the date and user to the log items
        $current_datetime = new \DateTime( 'now', wp_timezone() );
        $meta_data['date'] = $current_datetime->format( 'Y-m-d H:i:s' );
        
        // Method #1: Save UserName
        $meta_data['user'] = isset( $meta_data['user'] ) ? $meta_data['user'] : ( get_current_user_id() == 0 ? 'system' : get_user_by( 'id', get_current_user_id() )->display_name );
        // Method #2: Save User ID
        // $meta_data['user'] = isset( $meta_data['user'] ) ? $meta_data['user'] : ( get_current_user_id() == 0 ? 0 : get_current_user_id() );
        
        // Add the log items to the log
        $log[] = $meta_data;

        update_post_meta( $post_id, $meta_key, $log );

        return $log;
    }

    /**
     * Get the last log of an item and ensure that the log is in sync with the custom field
     * 
     * There are two meta fields. 
     * 1: One is the flag which is the last log value ( $post_custom_field_key )
     * 2: The other is the log itself which is an array of all log items ( $post_log_key)
     * 
     * Then the $log_field_key is the key of the log item which is the last log value.
     */
    public static function getLastLog( $post_id, $post_log_key = '', $log_field_key = '', $post_custom_field_key = '' )
    {
        // If the value is set, return the value
        if ( get_post_meta( $post_id, $post_custom_field_key, true ) ) {
            return get_post_meta( $post_id, $post_custom_field_key, true );
        }

        // Else get the last log item
        $log = self::metalog( $post_id, $post_log_key );
        
        // If the log is empty, set the last value false, else get the last log field specified
        if ( empty( $log ) ) {
            // Create a log entry
            $last_log = end( self::metalog( $post_id, $post_log_key, array( $log_field_key => false ) ) );
        }
        else {
            $last_log = end( $log );
            
            // check if user is set
            if ( ! isset( $last_log['user'] ) ) {
                $last_log['user'] = 'unknown';
            }

            // check if user is an id from a string
            if ( is_numeric( $last_log['user'] ) ) {
                $last_log['user'] = get_user_by( 'id', $last_log['user'] )->display_name;
            }

            // check if date is set
            if ( ! isset( $last_log['date'] ) ) {
                $last_log['date'] = '0000-00-00 00:00:00';
            }
        }

        // combine last log entry in a comma separated string
        $is_true = $last_log[$log_field_key] ? '1' : '0';
        $last_log_string = $is_true . '|' . $last_log['date'] . '|' . $last_log['user'];
        
        // Update the custom field with the last log value
        update_post_meta( $post_id, $post_custom_field_key, $last_log_string );

        return $last_log_string;
    }

    /**
     * Get the last log entry of an item
     * 
     * @param mixed $post_id the post ID
     * @param string $post_log_key the meta key of the log
     * @param string $log_field_key the key of the log field to return
     * 
     * @return array the last log entry or the last log field if the log field key is set
     */
    public static function getLastLogEntry( $post_id, $post_log_key = '', $log_field_key = '' )
    {
        $log = self::metalog( $post_id, $post_log_key );

        if ( empty( $log ) ) {
            return false;
        }

        $last_log = end( $log );

        // if the log field key is empty, return the last log entry
        if ( empty( $log_field_key ) ) {
            return $last_log;
        }

        // else return the last log field in an array
        return array( $log_field_key => $last_log[$log_field_key] );
    }
}