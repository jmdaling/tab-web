<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Inc\Base\BaseController;
use Inc\Classes\JMD_Logger;

class EmailController extends BaseController
{
    public $order_notes;
    
    public $email_classes;

    public $email_class_list;

    public $order_notices;

    public $admin_notices;

    public function register()
    {
        // Add all email hooks
        $this->addOrderActions();

        // Add email classes to WooCommerce
		add_action( 'woocommerce_email_classes', array( $this, 'registerEmails' ), 100, 1 );

        // Customize the email footer
        add_action( 'ttb_email_footer', array( $this, 'emailFooter'), 10, 1 );
    
        // Add explanatory text before the order actions
        add_action( 'woocommerce_order_actions_start', array( $this, 'orderActionsDescription'), 10, 1 );
        
        // Add actions on order page
        if ( TTB_SITE == 'ofest' ) {
            
            add_action( 'woocommerce_order_actions', array( $this, 'customizeWcOrderActions' ), 11, 1 );
            // Customize Email triggers
            $this->customizeEmailTriggers();
        }
        elseif ( TTB_SITE == 'tabtickets' ) {
            add_action( 'woocommerce_order_actions', array( $this, 'customizeWcOrderActions_tt' ), 11, 1 );
            // Customize Email triggers
            $this->customizeEmailTriggers_tt();
        }

        // Create action ttb_collection_ready_email to trigger the collection ready email
        add_action( 'ttb_collection_ready_email', array( $this, 'emailCollectionReady' ), 10, 1 );
    }

    public function __construct()
    {
        // Set the class list and register all email classes
        $this->setEmailClassList();
        
        $this->setAdminNotices();
        
        $this->setOrderActionOrderNotes();
    }

    /* Add explanatory text before the order actions */
    public function orderActionsDescription( $order )
    {
        echo '<div id="order_action_title" title="'. __( 'All emails are sent to the customer except the admin notification', 'tabticketbroker' ). '">' . __( 'Send emails', 'tabticketbroker' ) . '</div>';
    }

    /**
     * Customize email triggers by removing some automated WooCommerce and Germanized triggers and
     * replacing them with our own custom emails
     */
    public function customizeEmailTriggers()
    {
        // Germanized triggers
        // Avoid germanizer sending its own email also for order confirmation and payment confirmation
        add_filter( 'woocommerce_germanized_send_instant_order_confirmation', '__return_false', 10, 2 ); // Return false to disable
        add_filter( 'woocommerce_gzd_disable_paid_for_order_notification', '__return_true', 10, 2 ); // Return true to disable

        // Ofest triggers
        // Order Received
        add_action( 'woocommerce_checkout_order_processed', array( $this, 'emailOrderReceived'), 10, 1 );
        add_action( 'woocommerce_checkout_order_processed', array( $this, 'sendOrderNotificationEmail'), 10, 1 ); // Admin
        
        // Order Confirmed: Customer
        // This email is manually triggered after an invoice has been created
    
        // Payment Received
        add_action( 'woocommerce_order_status_pending_to_processing_notification', array( $this, 'emailPaymentReceived' ), 10 ); 

        // Shipment Confirmation: REMOVED, Switching to DHL that will send out their own email
        // add_action( 'woocommerce_gzd_updated_shipment_status', array( $this, 'shipmentStatusChanged' ), 10, 1 ); 

        // Instructions Final
        add_action( 'woocommerce_order_status_booked_to_shipped', array( $this, 'emailInstructionsFinal' ), 10, 1 );
        add_action( 'woocommerce_order_status_ready-to-ship_to_shipped', array( $this, 'emailInstructionsFinal' ), 10, 1 );
        add_action( 'woocommerce_order_status_labelled_to_shipped', array( $this, 'emailInstructionsFinal' ), 10, 1 );

        // Collection Ready / Pickup
        add_action( 'woocommerce_order_status_ready-to-ship_to_shipped', array( $this, 'emailCollectionReady' ), 10, 1 );

        // Order Cancelled
        add_action( 'woocommerce_order_status_pending_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_shipped_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_processing_to_cancelled',     array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_pending_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_on-hold_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_assigned_to_cancelled',       array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_ready-to-ship_to_cancelled',  array( $this, 'emailOrderCancelled' ), 10, 1 );

        // Order Refunded
        add_action( 'woocommerce_order_status_pending_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_shipped_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_processing_to_refunded',     array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_pending_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_on-hold_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_assigned_to_refunded',       array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_ready-to-ship_to_refunded',  array( $this, 'emailOrderRefunded' ), 10, 1 );
    }

    /**
     * Tab Specific
     * Customize email triggers by removing some automated WooCommerce and Germanized triggers and
     * replacing them with our own custom emails
     */
    public function customizeEmailTriggers_tt()
    {
        // Germanized triggers
        // Avoid germanizer sending its own email also for order confirmation and payment confirmation
        add_filter( 'woocommerce_germanized_send_instant_order_confirmation', '__return_false', 10, 2 ); // Return false to disable
        add_filter( 'woocommerce_gzd_disable_paid_for_order_notification', '__return_true', 10, 2 ); // Return true to disable

        // Ofest triggers
        // Order Received
        add_action( 'woocommerce_checkout_order_processed', array( $this, 'emailOrderReceived'), 10, 1 );
        add_action( 'woocommerce_checkout_order_processed', array( $this, 'sendOrderNotificationEmail'), 10, 1 ); // Admin
        
        // Order Confirmed: Customer
        // This email is manually triggered after an invoice has been created
    
        // Payment Received
        add_action( 'woocommerce_order_status_pending_to_processing_notification', array( $this, 'emailPaymentReceived' ), 10 ); 

        // Shipment Confirmation: REMOVED, Switching to DHL that will send out their own email
        // NOT Done yet for TabTickets, but will most likely be done in the future, rather remove for now too
        // add_action( 'woocommerce_gzd_updated_shipment_status', array( $this, 'shipmentStatusChanged' ), 10, 1 ); 

        // Instructions Final
        // add_action( 'woocommerce_order_status_booked_to_shipped', array( $this, 'emailInstructionsFinal' ), 10, 1 );
        // add_action( 'woocommerce_order_status_ready-to-ship_to_shipped', array( $this, 'emailInstructionsFinal' ), 10, 1 );

        // Order Cancelled
        add_action( 'woocommerce_order_status_pending_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_shipped_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_processing_to_cancelled',     array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_pending_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_on-hold_to_cancelled',        array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_assigned_to_cancelled',       array( $this, 'emailOrderCancelled' ), 10, 1 );
        add_action( 'woocommerce_order_status_ready-to-ship_to_cancelled',  array( $this, 'emailOrderCancelled' ), 10, 1 );

        // Order Refunded
        add_action( 'woocommerce_order_status_pending_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_shipped_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_processing_to_refunded',     array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_pending_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_on-hold_to_refunded',        array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_assigned_to_refunded',       array( $this, 'emailOrderRefunded' ), 10, 1 );
        add_action( 'woocommerce_order_status_ready-to-ship_to_refunded',  array( $this, 'emailOrderRefunded' ), 10, 1 );
    }

    /**
     * Hook into germanized and stop it from sending the order received email
     * This is handled by tab
     * @return false
     */
    public function useWcGermanizedEmails( $result, $order )
    {
        return false;
    }

    /**
     * Hook into germanized and stop it from sending the payment recieved email
     * This is handled by tab
     * @return true
     */
    public function disableWcGermanizedEmails( $result, $order )
    {
        return true;
    }

    /**
     * Send order notification
     * This is required because the Germanized was sending an additional Order received 
     * email on top of the custom defined one and had to be disabled. Once disabled, it 
     * also disabled the order notification.
     */
    public function sendOrderNotificationEmail( $order_id )
    {
        $order = wc_get_order( $order_id );

        WC()->mailer()->emails['WC_Email_New_Order']->trigger( $order_id, $order, true );
        
        return true;
    }

    public function registerEmails( $email_classes )
    {
        // Unset all emails and add our own
        $this->email_classes = array();
        
        // Un/comment the line below to toggle add/remove of all WC and Germanized emails in the system
        $this->email_classes = $email_classes;

        foreach ( $this->getEmailClassList() as $name => $class ) {
                $this->email_classes[$name] = new $class;
            }
		
        return $this->email_classes;
    }

    function emailFooter( $email ) 
    { 
        // Get the email template from the TTB plugin
        if ( TTB_SITE === 'ofest' ) {
            $template_html  = plugin_dir_path( dirname( __FILE__, 2 ) ) . 'tabticketbroker/templates/emails/email-footer.php';
        } elseif ( TTB_SITE === 'tabtickets' ) {
            $template_html  = plugin_dir_path( dirname( __FILE__, 2 ) ) . 'tabticketbroker/templates/emails_tt/email-footer.php';
        }

        require_once( $template_html );
    }

    public function setEmailClassList()
    {
         $this->email_class_list = array( 
            'email_customer_order_received'             => Emails\OrderReceived::class,
            'email_customer_order_confirmed'            => Emails\OrderConfirmed::class,
            'email_customer_payment_received'           => Emails\PaymentReceived::class,
            'email_customer_payment_reminder'           => Emails\PaymentReminder::class,
            'email_customer_shipping_address_required'  => Emails\ShippingAddressRequired::class,
            'email_customer_shipping_confirmation'      => Emails\ShippingConfirmation::class,
            'email_customer_collection_ready'           => Emails\CollectionReady::class,
            'email_customer_instructions_final'         => Emails\InstructionsFinal::class,
            'email_customer_shared_reservation'         => Emails\SharedReservation::class,
            'email_customer_order_cancelled'            => Emails\OrderCancelled::class,
            'email_customer_order_refunded'             => Emails\OrderRefunded::class,
        );
    }

    public function getEmailClassList() 
    {
        return $this->email_class_list;
    }

    public function setAdminNotices()
    {
        
        $this->admin_notices = array(
            'email_customer_order_received'             => __( 'Order received email sent to customer', 'tabticketbroker' ),
            'email_customer_order_confirmed'            => __( 'Order confirmation sent to customer', 'tabticketbroker' ),
            'email_customer_payment_received'           => __( 'Payment received email sent to customer', 'tabticketbroker' ),
            'email_customer_payment_reminder'           => __( 'Payment reminder sent to customer', 'tabticketbroker' ),
            'email_customer_shipping_address_required'  => __( 'Informing customers that their shipping address is required', 'tabticketbroker' ),
            'email_customer_shipping_confirmation'      => __( 'Shipping confirmation sent to customer', 'tabticketbroker' ),
            'email_customer_collection_ready'           => __( 'Collection ready email sent to customer', 'tabticketbroker' ),
            'email_customer_instructions_final'         => __( 'Final instructions sent to customer', 'tabticketbroker' ),
            'email_customer_shared_reservation'         => __( 'Additional info email for shared reservations', 'tabticketbroker' ),
            'email_customer_order_cancelled'            => __( 'Order cancelled email sent to customer', 'tabticketbroker' ),
            'email_customer_order_refunded'             => __( 'Order refunded email sent to customer', 'tabticketbroker' ),
        );
        
    }

    function customizeWcOrderActions( $actions )
    {
        // Remove default WooCommerce actions
        unset( $actions['send_order_details'] );
        unset( $actions['regenerate_download_permissions'] );

        // Remove WooCommerce Germanized actions
        unset( $actions['order_confirmation'] );
        unset( $actions['paid_for_order_notification'] );

        // Create new instance of the woocommerce new order class and retrieve the email recipients
        $wc_new_order_email_setting = WC()->mailer()->get_emails()['WC_Email_New_Order']->recipient;

        // Rename send order admin action
        $actions['send_order_details_admin']           = __( 'ADMIN NOTIFICATION:', 'tabticketbroker' ). ' '. $wc_new_order_email_setting;

        // Add
        $actions['email_customer_order_received']           = __( 'Order Received', 'tabticketbroker' );
        $actions['email_customer_order_confirmed']          = __( 'Order Confirmed', 'tabticketbroker' );
        $actions['email_customer_payment_received']         = __( 'Payment Received', 'tabticketbroker' );
        $actions['email_customer_payment_reminder']         = __( 'Payment Reminder', 'tabticketbroker' );
        $actions['email_customer_shipping_address_required']= __( 'Shipping Address Required', 'tabticketbroker' );
        // $actions['email_customer_shipping_confirmation']    = __( 'Shipping Confirmation', 'tabticketbroker' ); // Actioned by setting the shipment as shipped
        $actions['email_customer_collection_ready']         = __( 'Collection ready', 'tabticketbroker' );
        $actions['email_customer_instructions_final']       = __( 'Final Instructions', 'tabticketbroker' );
        $actions['email_customer_shared_reservation']       = __( 'Shared Reservation', 'tabticketbroker' );
        $actions['email_customer_order_cancelled']          = __( 'Order Cancelled', 'tabticketbroker' );
        $actions['email_customer_order_refunded']           = __( 'Order Refunded', 'tabticketbroker' );
        
        return $actions;
    }

    function customizeWcOrderActions_tt( $actions )
    {
        // Remove default WooCommerce actions
        unset( $actions['send_order_details'] );
        unset( $actions['regenerate_download_permissions'] );

        // Remove WooCommerce Germanized actions
        unset( $actions['order_confirmation'] );
        unset( $actions['paid_for_order_notification'] );

        // Create new instance of the woocommerce new order class and retrieve the email recipients
        $wc_new_order_email_setting = WC()->mailer()->get_emails()['WC_Email_New_Order']->recipient;

        // Rename send order admin action
        $actions['send_order_details_admin']           = __( 'ADMIN NOTIFICATION:', 'tabticketbroker' ). ' '. $wc_new_order_email_setting;

        // Add
        $actions['email_customer_order_received']           = __( 'Order Received', 'tabticketbroker' );
        $actions['email_customer_order_confirmed']          = __( 'Order Confirmed', 'tabticketbroker' );
        $actions['email_customer_payment_received']         = __( 'Payment Received', 'tabticketbroker' );
        // $actions['email_customer_payment_reminder']         = __( 'Payment Reminder', 'tabticketbroker' );
        // $actions['email_customer_shipping_address_required']= __( 'Shipping Address Required', 'tabticketbroker' );
        // // $actions['email_customer_shipping_confirmation']    = __( 'Shipping Confirmation', 'tabticketbroker' ); // Actioned by setting the shipment as shipped
        // $actions['email_customer_collection_ready']         = __( 'Collection ready', 'tabticketbroker' );
        // $actions['email_customer_instructions_final']       = __( 'Final Instructions', 'tabticketbroker' );
        // $actions['email_customer_shared_reservation']       = __( 'Shared Reservation', 'tabticketbroker' );
        // $actions['email_customer_order_cancelled']          = __( 'Order Cancelled', 'tabticketbroker' );
        // $actions['email_customer_order_refunded']           = __( 'Order Refunded', 'tabticketbroker' );
        
        return $actions;
    }

    public function setOrderActionOrderNotes()
    {
        $this->order_notes = array(
            'email_customer_order_received'                 => __( 'Order received email sent', 'tabticketbroker' ),
            'email_customer_order_confirmed'                => __( 'Order confirmed email sent', 'tabticketbroker' ),
            'email_customer_payment_received'               => __( 'Payment received email sent', 'tabticketbroker' ),
            'email_customer_payment_reminder'               => __( 'Payment Reminder email sent', 'tabticketbroker' ),
            'email_customer_shipping_address_required'      => __( 'Shipping address required email sent', 'tabticketbroker' ),
            'email_customer_shipping_confirmation'          => __( 'Shipping confirmation email sent', 'tabticketbroker' ),
            'email_customer_collection_ready'               => __( 'Collection ready email sent', 'tabticketbroker' ),
            'email_customer_instructions_final'             => __( 'Instructions final email sent', 'tabticketbroker' ),
            'email_customer_shared_reservation'             => __( 'Shared reservation email sent', 'tabticketbroker' ),
            'email_customer_order_cancelled'                => __( 'Order cancelled email sent', 'tabticketbroker' ),
            'email_customer_order_refunded'                 => __( 'Order refunded email sent', 'tabticketbroker' ),
        );
    }

    public function addOrderActions()
    {
        add_action( 'woocommerce_order_action_email_customer_order_received'            , array( $this, 'emailOrderReceived' ) );
        add_action( 'woocommerce_order_action_email_customer_order_confirmed'           , array( $this, 'emailOrderConfirmed' ) );
        add_action( 'woocommerce_order_action_email_customer_payment_received'          , array( $this, 'emailPaymentReceived' ) );
        add_action( 'woocommerce_order_action_email_customer_payment_reminder'          , array( $this, 'emailPaymentReminder' ) );
        add_action( 'woocommerce_order_action_email_customer_shipping_address_required' , array( $this, 'emailShippingAddressRequired' ) );
        add_action( 'woocommerce_order_action_email_customer_shipping_confirmation'     , array( $this, 'emailShippingConfirmation' ) );
        add_action( 'woocommerce_order_action_email_customer_collection_ready'          , array( $this, 'emailCollectionReady' ) );
        add_action( 'woocommerce_order_action_email_customer_instructions_final'        , array( $this, 'emailInstructionsFinal' ) );
        add_action( 'woocommerce_order_action_email_customer_shared_reservation'        , array( $this, 'emailSharedReservation' ) );
        add_action( 'woocommerce_order_action_email_customer_order_cancelled'           , array( $this, 'emailOrderCancelled' ) );
        add_action( 'woocommerce_order_action_email_customer_order_refunded'            , array( $this, 'emailOrderRefunded' ) );
    }

    
    public function emailOrderReceived( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_order_received' );
    }

    public function emailOrderConfirmed( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_order_confirmed' );
    }

    public function emailPaymentReceived( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_payment_received' );
    }
    
    public function emailPaymentReminder( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_payment_reminder' );
    }

    public function emailShippingAddressRequired( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_shipping_address_required' );
    }

    public function emailShippingConfirmation( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_shipping_confirmation' );
    }
   
    public function emailCollectionReady( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_collection_ready' );
    }

    public function emailInstructionsFinal( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_instructions_final' );
    }

    public function emailSharedReservation( $reservation )
    {
        // For each order in the reservation, send the email
        foreach ( $reservation->getOrderItemIds() as $order_item_id ) {
            
            $order_id = wc_get_order_id_by_order_item_id( $order_item_id );
            $order = wc_get_order( $order_id );

            $args = array(
                'order' => $order,
                'reservation' => $reservation,
            );

            $this->sendCustomEmail( $order, 'email_customer_shared_reservation', $args, true );

            // Log the action
            JMD_Logger::metalog( $reservation->getId(), '_email_shared_reservation_log', array( 'order_item_id' => $order_item_id ) );

            $this->addFlashNotice( 'Shared res email sent to Order: '. $order->get_order_number(), 'success' );
        }
    }

    public function emailOrderCancelled( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_order_cancelled' );
    }

    public function emailOrderRefunded( $order )
    {
        $this->sendCustomEmail( $order, 'email_customer_order_refunded' );
    }
    
    /**
     * Used to send emails and manage notices and notes on orders.
     * @param int $order The order in question (accepts the order object or the order id)
     * @param string $email_type Any of the following email types contained in $this->email_classes
     */
    public function sendCustomEmail( $order, $email_type, $args = array(), $disable_notice = false )
    {
        // Get order if only ID supplied
        if ( ! is_object( $order ) && is_int( $order ) ) {
            $order_id = $order;
            $order = wc_get_order( $order_id );
        }
        
        // Initiate parent class
        if ( ! class_exists( 'WC_Email' ) )
            WC()->mailer();

        // Get custom email class (created on register)
        if ( ! $this->email_classes ) $this->registerEmails( array() );

        $email_class = $this->email_classes[$email_type];

        if ( empty( $email_class ) ) {
            $msg = 'ERROR: Class '. $email_type .' not found.';
            $this->addFlashNotice( $msg, 'error', true );
            return $msg;
        }

        // Load text domains
        $this->loadOrderTextDomains( $order->get_id() );
        
        // Trigger email sending (returns true if success)
        $sent_result = $email_class->trigger( $order, $args );

        $sent_success = ( $sent_result === true || $sent_result === null ? true : false );
        
        // Clear up class
        unset( $email_class );
        
        // Check result for errors and setup return message & flash notice
        $sent_msg = ( $sent_success === true ? $this->order_notes[$email_type] : $sent_result );
    
        // Log note to order ( @params are: $is_customer_note = false, $added_by_user = true )
        $order->add_order_note( $sent_msg, false, true );
        
        // Set notice type
        $flash_msg_type = ( $sent_success === true ? 'success' : 'error' );

        // Add notice to queue
        if ( $disable_notice === false ) {
            $this->addFlashNotice( $sent_msg, $flash_msg_type, true );
        }
    }
    
    public function loadOrderTextDomains( $order_id )
    {
        
        // Load wc text domain (previously woocommerce text domain was not loaded and caused failed translation)
        $order_locale = get_post_meta( $order_id, '_ttb_order_language', true );
        
        unload_textdomain( 'woocommerce' );
        load_textdomain( 'woocommerce', WP_LANG_DIR . '/plugins/woocommerce-' . $order_locale . '.mo' );
        
        // Load tab text domain if order lang = german
        $base_controller = new BaseController;
        unload_textdomain( 'tabticketbroker' );
        load_textdomain( 'tabticketbroker', $base_controller->plugin_path . 'languages/tabticketbroker-' . $order_locale . '.mo' );
    }

    /**
     * Handles the sending of shipment confirmation email if a shipment was marked as shipped by woocommerce germanized.
     * @param integer $shipment_id The shipment id.
     * @param string  $status The status to be switched to.
     * 
     * NOTE: Triggers removed due to DHL sending their own email
     * 
     */
    public function shipmentStatusChanged( $shipment_id )
    {
        $shipment = wc_gzd_get_shipment( $shipment_id );
        $status = $shipment->get_status();
        
        if ( $status == 'shipped' ) {

            // Get the shipments parent order
            $order = $shipment->get_order();

            $args = array(
                'shipment' => $shipment,
            );

            // Send email
            $this->sendCustomEmail( $order, 'email_customer_shipping_confirmation', $args );
        }
    }
}