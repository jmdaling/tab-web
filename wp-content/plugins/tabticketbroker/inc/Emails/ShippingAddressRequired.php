<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\EmailTools;
use Inc\Base\BaseController;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (!class_exists('WC_Email')) {
    WC()->mailer();
}

class ShippingAddressRequired extends WC_Email
{    
    public $order;
    public $email_tools;
    public $base_controller;
    public $messagebox_1;
    public $messagebox_1_de;
    public $messagebox_2;
    public $messagebox_2_de;
    public $subject_de;
    public $heading_de;

	function __construct()
    {
        if ( ! is_object( $this->base_controller ) ) {
            $this->base_controller = new BaseController();
        }

        if ( ! is_object( $this->email_tools ) ) {
            $this->email_tools = new EmailTools();
        }
        
        // Email Config vars
        $this->id = 'email_customer_shipping_address_required';
        $this->title = $this->base_controller->plugin_name_short .' - '. __( 'Shipping Address Required', 'tabticketbroker' );
        $this->customer_email = true;
        $this->template_base  = $this->base_controller->plugin_path .'templates/';
        $this->template_html  = 'emails/shipping_address_required.php';
        
        // Set the placeholders
        $this->placeholders = $this->email_tools->getPlaceholders();
        $this->description = __( 'An email informing the customer that their shipping address is required.', 'tabticketbroker' );
        $this->description = $this->email_tools->addPlaceholderDescription( $this->placeholders, $this->description );
        
        // Dynamic content
        $this->subject      = __( 'Shipping address required order number {order_no}', 'tabticketbroker' );
        $this->subject_de   = __( 'Versandadresse erforderlich für Ihre Bestellung {order_no}', 'tabticketbroker' );

        $this->heading      = __( 'Shipping address required order number {order_no}', 'tabticketbroker' );
        $this->heading_de   = __( 'Versandadresse erforderlich für Ihre Bestellung {order_no}', 'tabticketbroker' );

        // Add configurable content boxes
$this->messagebox_1 = 
'<p>Hello {first_name} {last_name}</p>
<p>We noticed you left a delivery address outside the EU with us for your Order <b>{order_no}</b>. We always recommend our international clients provide us with a German delivery address to ensure maximum reliability during the shipping.</p>
</br>
<p>We had some deliveries where the package got stuck at customs and didn`t arrive on time. Therefore, we would suggest having the tickets delivered to a <b>German address (e.g. hotel or AirBnB in Munich)</b>. Please provide us with an address where we could dispatch the documents to instead.</p><br>';

$this->messagebox_1_de = 
'<p>Hallo {first_name} {last_name}</p><p>Wir haben festgestellt, dass Sie für Ihre Bestellung <b>{order_no}</b> eine Lieferadresse außerhalb der EU bei uns hinterlegt haben. Wir empfehlen unseren internationalen Kunden, uns eine deutsche Lieferadresse anzugeben, um maximale Zuverlässigkeit beim Versand zu gewährleisten.</p>
<p>Wir hatten einige Lieferungen, bei denen das Paket beim Zoll hängen blieb und dadurch verspätet zugestellt wurde. Daher empfehlen wir, die Tickets an eine <b>deutsche Adresse (z. B. Hotel in München)</b> liefern zu lassen.</p>
<br>
<p>Bitte teilen Sie uns eine Adresse mit an die wir stattdessen die Unterlagen versenden könnten.</p>';

$this->messagebox_2 = 
'<p>To assure a timely arrival for Hotel deliveries and to avoid the tickets sitting too long at the hotel reception, please provide us with the following details from your hotel booking:</p>
<br>
<p><b>Check-In date:__________________</b></p>
<p><b>Booking confirmation number:____________________</b></p>
<br>
<p>Please provide us with the needed information by replying to this email: <b>{order_admin_email}</b></p>
<br>
<p>If you have any further questions, please let us know.</p>';

$this->messagebox_2_de = 
'<p>Um eine rechtzeitige Ankunft für Hotellieferungen zu gewährleisten und um zu vermeiden, dass die Tickets zu lange an der Hotelrezeption liegen, geben Sie uns bitte die folgenden Details aus Ihrer Hotelbuchung an:</p>
<br>
<p><b>Check-In date:__________________</b></p>
<p><b>Booking confirmation number:____________________</b></p>
<br>
<p>Bitte antworten Sie auf diese E-Mail, um uns die nötigen Informationen zukommen zu lassen: <b>{order_admin_email}</b></p>
<br>
<p>Wenn Sie weitere Fragen haben, melden Sie sich gerne.</p>
';

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // Override the email type after parent constructor
        $this->email_type = 'html'; 
    }

    /**
     * Sets the order.
     * @param  int $order_id the id for the order
     * @return void
     */
    public function setOrder( $order_id )
    {
        // Set the order
        $this->order = wc_get_order( $order_id );

        // Set the placeholders
        $this->placeholders = $this->email_tools->setPlaceholders( $this->order, $this->placeholders );
    }

  	/**
	 * Trigger Function that will send this test email.
	 *
	 * @access public
	 * @return void
	 */
    public function trigger( $order ) 
    {
        // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' );
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' );

        // Setup order object
        $this->setOrder( $order );

        // Attempt to set recipient
        $this->recipient = $order->get_billing_email();
        if ( $this->recipient == '' )
            return __( 'Email not sent: No recipient', 'tabticketbroker' );

        // Send
        $this->send( $this->get_recipient(), $this->email_tools->getSubject( $this ), $this->get_content(), $this->get_headers(), $this->get_attachments() );

        // Send copy to admin DISALBED: Use BCC in email settings of woocommerce
        // $this->send( get_option( 'admin_email' ), __( 'COPY SENT TO ', 'tabticketbroker' ) .$this->get_recipient().': '.$this->email_tools->getSubject( $this ), $this->get_content(), $this->get_headers(), $this->get_attachments() );

    }
    
    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, 
            array(
                'order'         => $this->order,
                'email_heading' => $this->email_tools->getHeading( $this ),
                'email'         => $this->recipient,
                'sent_to_admin' => false,
                'plain_text'    => false,
                'messagebox_1'  => $this->email_tools->getMessageboxContent( $this, '1' ),
                'messagebox_2'  => $this->email_tools->getMessageboxContent( $this, '2' ),
            ), 
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    } 
    
    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject ),
                'placeholder' => $this->subject,
                'default'     => $this->subject,
            ),
            'subject_de'    => array(
                'title'       => 'Subject (German)',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject_de ),
                'placeholder' => $this->subject_de,
                'default'     => $this->subject_de,
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading ),
                'placeholder' => $this->heading,
                'default'     => $this->heading,
            ),
            'heading_de'    => array(
                'title'       => 'Email Heading (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading_de ),
                'placeholder' => $this->heading_de,
                'default'     => $this->heading_de,
            ),
            'messagebox_1'    => array(
                'title'       => 'Messagebox 1 (English)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 10em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1 ).'</pre>',
                'placeholder' => $this->messagebox_1,
                'default'     => $this->messagebox_1
            ),
            'messagebox_1_de' => array(
                'title'       => 'Messagebox 1 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 10em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1_de ).'</pre>',
                'placeholder' => $this->messagebox_1_de,
                'default'     => $this->messagebox_1_de
            ),
            'messagebox_2'    => array(
                'title'       => 'Messagebox 2 (English) styled',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 20em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2 ).'</pre>',
                'placeholder' => $this->messagebox_2,
                'default'     => $this->messagebox_2
            ),
            'messagebox_2_de' => array(
                'title'       => 'Messagebox 2 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 20em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2_de ).'</pre>',
                'placeholder' => $this->messagebox_2_de,
                'default'     => $this->messagebox_2_de
            ),
        );
    }

}