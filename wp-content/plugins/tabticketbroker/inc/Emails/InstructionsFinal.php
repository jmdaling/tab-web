<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\EmailTools;
use Inc\Base\BaseController;
use Inc\Tools\OrderTools;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (!class_exists('WC_Email')) {
    WC()->mailer();
}

class InstructionsFinal extends WC_Email
{
    public $order;
    public $email_tools;
    public $base_controller;
    public $placeholders;
    public $required_placeholders;
    public $golden_rules_shortcode;
    public $golden_rules_shortcode_de;
    public $messagebox_1;
    public $messagebox_1_de;
    public $heading;
    public $heading_de;
    public $subject;
    public $subject_de;
    public $email_type;
    
	function __construct() 
    {
        if ( ! is_object( $this->base_controller ) ) {
            $this->base_controller = new BaseController();
        }

        if ( ! is_object( $this->email_tools ) ) {
            $this->email_tools = new EmailTools();
        }

        // Email Config vars
        $this->id = 'email_customer_instructions_final';
        $this->title = $this->base_controller->plugin_name_short .' - '. __( 'Final Instructions', 'tabticketbroker' );
        $this->customer_email = true;
        $this->template_base  = $this->base_controller->plugin_path .'templates/';
        $this->template_html  = 'emails/instructions_final.php';

        // Set the placeholders
        $this->placeholders = $this->email_tools->getPlaceholders();
        $this->description = __( 'An email to the customer conveying the final important instructions and event information.', 'tabticketbroker' );
        $this->description = $this->email_tools->addPlaceholderDescription( $this->placeholders, $this->description );
        
        // Add email specific placeholders
        $this->placeholders['{rules_download_link}'] = __( 'The link to the rules download with the text of the download title set in download monitor. This is linked to the corresponding shortcode in the settings below.', 'tabticketbroker' );
        $this->placeholders['{reservation_infobox}'] = __( 'An info box for each reservation with Product, Start datetime and owner.', 'tabticketbroker' );

        // Dynamic content
        $this->heading      = __( 'IMPORTANT INFORMATION for your Oktoberfest Reservation', 'tabticketbroker' );
        $this->heading_de   = __( 'WICHTIGE HINWEISE zu Ihrer Oktoberfest Reservierung', 'tabticketbroker' );

        $this->subject      = __( 'IMPORTANT INFORMATION for your Oktoberfest Reservation', 'tabticketbroker' );
        $this->subject_de   = __( 'WICHTIGE HINWEISE zu Ihrer Oktoberfest Reservierung', 'tabticketbroker' );

        // Shortcodes are created in the download monitor plugin at time of writing, this should be changed to a custom shortcode
        $this->golden_rules_shortcode     = '[download id="28161"]';
        $this->golden_rules_shortcode_de  = '[download id="28164"]';

        $this->required_placeholders = array(
            '{first_name}',
            '{last_name}',
            '{rules_download_link}',
            '{reservation_infobox}',
            '{mobile_tel}',
        );
        
        // Add configurable content boxes
         $this->messagebox_1 = 
'<p>Hello {first_name} {last_name},</p>
<p>Your Oktoberfest trip is just around the corner!</p>
<p>To ensure that you have a great and relaxed time at the Oktoberfest München, we are sending you our {rules_download_link} that you should download.<br>
<b>Please read the information carefully, treat them confidentiality and <u>do not</u> take the attached leaflet with you into the beer hall.</b></p>
<p>Below is a summary of the most important points:</p>
<p><b>1. Be on time at your table!</b></p>
<p>The <u>reservation is not in your name</u>. You are the guests of the reservation owner as listed.<br>
You will also find the name on your reservation letter or the admission tickets that will be sent to you by courier in the next few days.</p>
{reservation_infobox}
<p><b>2. Never mention that you bought your reservation!</b></p>
<p><b>3. If you receive wristbands, please attach them before entering the beer hall.</b><br>
Otherwise, have your entry card or Reservation document ready for inspection.</p>
<p><b>3. Please take your seat once you reach your table.</b></p>
<p><b>4. Mandatory service fee and tip</b></p>
<br>
<p><b>For more detailed information, please download our {rules_download_link} info sheet and read it carefully. </b></p>
<p>If you have any issues or questions, feel free to contact us anytime.</p>
<p>Mobile (Call/ WhatsApp) {mobile_tel}</p>
';

$this->messagebox_1_de = 
'<p>Hallo {first_name} {last_name},</p>
<p>Ihr Oktoberfest-Besuch steht kurz bevor.</p>
<p>Für eine entspannte Zeit auf der Wies`n erhalten Sie hier als download die {rules_download_link} zu Ihrer Reservierung.<br>
<b>Bitte lesen Sie die Informationen sorgfältig durch, behandeln diese streng vertraulich und nehmen das Infoblatt <u>nicht</u> mit in das Zelt.</b></p>
<p>Hier schon mal eine Zusammenfassung der wichtigsten Inhalte vorweg:</p>
<p><b>1. Erscheinen Sie dringend pünktlich am Zelt!</b></p>
<p>Die <u>Reservierung läuft nicht auf Ihren Namen</u>. Sie sind die Gäste des Reservierungseigentümers, wie aufgeführt.<br>
Den Namen finden Sie ebenfalls auf Ihrem Reservierungsschreiben oder den Einlasskarten, die Ihnen in den nächsten Tagen per Kurier zugestellt werden.</p>
{reservation_infobox}
<p><b>2. Erwähnen Sie nicht, dass Sie Ihre Reservierung käuflich erworben haben!</b></p>
<p><b>3. Soweit in Ihrer Lieferung enthalten, befestigen Sie die Einlassbändchen am Handgelenk vor Betreten des Zeltes. Andernfalls halten Sie Ihre Einlasskarten oder das Reservierungsschreiben bereit.</b></p>
<p><b>4. Nehmen Sie Ihre Plätze rechtzeitig ein!</b></p>
<p><b>5. Geben Sie Trinkgeld!</b></p>
<br>
<p><b>Für detaillierte Informationen laden Sie sich bitte das Infoblatt mit den {rules_download_link} herunter und lesen Sie es sehr sorgfältig (vertraulich behandeln).</b><br>
Falls Sie darüber hinaus noch Fragen haben sollten, können Sie uns jederzeit telefonisch erreichen.</p>
<br>
<p>Mobil (Call/ WhatsApp) {mobile_tel}</p>
';

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // Override the email type after parent constructor
        $this->email_type = 'html'; 
    }

    /**
     * Sets the order.
     * @param  int $order_id the id for the order
     * @return void
     */
    public function setOrder( $order_id )
    {
        $order = wc_get_order( $order_id );
        
        $this->order = $order;

        $this->placeholders['{rules_download_link}'] = $this->email_tools->getRulesDownloadPlaceholder( $this );

        $this->placeholders = $this->email_tools->setPlaceholders( $this->order, $this->placeholders );
    }

  	/**
	 * Trigger Function that will send this test email.
	 *
	 * @access public
	 * @return void
	 */
    public function trigger( $order ) 
    {
        // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' );
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' );

        // Setup order object
        $this->setOrder( $order );

        // Ensure all required placeholders are set
        $check_required_placeholders = $this->email_tools->checkRequiredPlaceholders( $this->required_placeholders, $this->placeholders );
        if ( $check_required_placeholders !== true ) {
            return $check_required_placeholders;
        }

        // Ensure all Order reservations have an owner name
        // Get all reservations for this order
        $order_tools = new OrderTools();
        $order_reservations = $order_tools->getOrderReservations( $order->get_id() );

        // Loop through reservations and check if each has an owner name
        foreach ( $order_reservations as $reservation_link ) {
            $reservation = $reservation_link['reservation'];
            $owner_name = $reservation->getOwnerName();
            if ( $owner_name == '' ) {
                return __( 'ERROR: Final instructions email NOT sent: One or more reservations do not have an owner name', 'tabticketbroker' );
            }
        }

        // Attempt to set recipient
        $this->recipient = $this->order->get_billing_email();
        if ( $this->recipient == '' )
            return __( 'Email not sent: No recipient', 'tabticketbroker' );

        // Send
        $this->send( $this->get_recipient(), $this->email_tools->getSubject( $this ), $this->get_content(), $this->get_headers(), $this->get_attachments() );
    }
    
    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, 
            array(
                'order'         => $this->object,
                'email_heading' => $this->email_tools->getHeading( $this ),
                'email'         => $this->recipient,
                'sent_to_admin' => false,
                'plain_text'    => false,
                'messagebox_1'  => $this->email_tools->getMessageboxContent( $this, '1' ),
            ), 
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    } 
    
    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject ),
                'placeholder' => $this->subject,
                'default'     => $this->subject,
            ),
            'subject_de'    => array(
                'title'       => 'Subject (German)',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject_de ),
                'placeholder' => $this->subject_de,
                'default'     => $this->subject_de,
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading ),
                'placeholder' => $this->heading,
                'default'     => $this->heading,
            ),
            'heading_de'    => array(
                'title'       => 'Email Heading (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading_de ),
                'placeholder' => $this->heading_de,
                'default'     => $this->heading_de,
            ),
            'golden_rules_shortcode' => array(
                'title'       => 'Golden Rules Shortcode',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the shortcode for the golden rules. The default shortcode is: <code>%s</code>' ), $this->golden_rules_shortcode ),
                'placeholder' => $this->golden_rules_shortcode,
                'default'     => $this->golden_rules_shortcode,
            ),
            'golden_rules_shortcode_de' => array(
                'title'       => 'Golden Rules Shortcode (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the shortcode for the golden rules. The default shortcode is: <code>%s</code>' ), $this->golden_rules_shortcode_de ),
                'placeholder' => $this->golden_rules_shortcode_de,
                'default'     => $this->golden_rules_shortcode_de,
            ),
            'messagebox_1'    => array(
                'title'       => 'Messagebox 1 (English)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 30em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1 ).'</pre>',
                'placeholder' => $this->messagebox_1,
                'default'     => $this->messagebox_1
            ),
            'messagebox_1_de' => array(
                'title'       => 'Messagebox 1 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 30em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1_de ).'</pre>',
                'placeholder' => $this->messagebox_1_de,
                'default'     => $this->messagebox_1_de
            ),
        );
    }

}