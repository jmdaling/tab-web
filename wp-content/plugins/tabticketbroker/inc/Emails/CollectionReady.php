<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\EmailTools;
use Inc\Base\BaseController;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (!class_exists('WC_Email')) {
    WC()->mailer();
}

class CollectionReady extends WC_Email
{
    public $order;
    public $email_tools;
    public $base_controller;
    public $subject_de;
    public $heading_de;
    public $email_type;
    public $messagebox_1;
    public $messagebox_1_de;
    public $messagebox_2;
    public $messagebox_2_de;
    public $required_placeholders;

    function __construct()
    {
        if ( ! is_object( $this->base_controller ) ) {
            $this->base_controller = new BaseController();
        }

        if ( ! is_object( $this->email_tools ) ) {
            $this->email_tools = new EmailTools();
        }

        // Email Config vars
        $this->id = 'email_customer_collection_ready';
        $this->title = $this->base_controller->plugin_name_short . ' - ' . __( 'Collection Ready', 'tabticketbroker' );
        $this->customer_email = true;
        $this->template_base  = $this->base_controller->plugin_path . 'templates/emails/';
        $this->template_html  = 'collection_ready.php';

        // Set the placeholders
        $this->placeholders = $this->email_tools->getPlaceholders( $this->placeholders );
        $this->description = __('An email to the customer confirming that his/her order is ready for collection.', 'tabticketbroker');
        $this->description = $this->email_tools->addPlaceholderDescription( $this->placeholders, $this->description );

        // Dynamic content
        $this->subject      = 'Your order is ready for collection - {order_no}';
        $this->subject_de   = 'Ihre Bestellung liegt zur Abholung bereit - {order_no}';
        
        $this->heading      = 'Your order is ready for collection - {order_no}';
        $this->heading_de   = 'Ihre Bestellung liegt zur Abholung bereit - {order_no}';
        
        // Set required placeholders
        $this->required_placeholders = array(
            '{pickup_code}',
            '{pickup_address}',
            '{ttb_pickup_address_map_link}',
            '{pickup_contact}',
            '{pickup_open_hours}',
            '{hotline}',
            '{contact_email}',
            '{event_year}',
        );
                
        // Add configurable content boxes
        ob_start();
        ?>
        <p>Hello {first_name} {last_name}</p>
        <p>Your order is ready for collection.</p>
        <h2>Pickup details:</h2>
        <p><b>Pickup code</b>: <span style="font-size:19px;border-radius:10px;border:solid 1pt gray;padding:6px;margin:5px;">{pickup_code}</span></p>
        <p>This is your personal pickup code. Please have it ready at the collection point.</p>
        <b><p>Please check the documents immediately for completeness.</p></b>
        <p>If you choose to pick up your tickets on the day of your reservation, please ensure that you arrive at our partner shop no later than 2 hours before the start of your reservation to ensure that you arrive at the beer hall on time. Otherwise, the seats will be given to other guests.</p>
        <address>{pickup_address}</address>
        <p>Google Map Location <a target="blank" href="{ttb_pickup_address_map_link}">click here</a></p>
        <p>Your contact person: <b>{pickup_contact}</b></p>
        <h2>Opening hours:</h2>
        <p>{pickup_open_hours}</p>
        <hr>
        <b><em>We strongly recommend to pick-up your reservation no later than 4 hours before your reservation start time.</em></b>
        <hr>
        <p>If you have questions to your reservation or you need assistance during your stay in the beer hall please solely contact our hotline at {hotline} or send us an e-mail to {contact_email} </p>
        <p>Please DO NOT discuss any problems with the staff of the beer hall !!!</p>
        <p>We wish you a pleasant stay at the Oktoberfest {event_year}!</p>
        <?php
        $this->messagebox_1 = ob_get_clean();

        ob_start();
        ?>
        <p>Hallo {first_name} {last_name}</p>
        <p>Ihre Bestellung liegt zur Abholung bereit.</p>
        <h2>Abholungsdetails:</h2>
        <p><b>Abholcode</b>: <span style="font-size:19px;border-radius:10px;border:solid 1pt gray;padding:6px;margin:5px;">{pickup_code}</span></p>
        <p>Dies ist Ihr persönlicher Abholcode. Bitte halten Sie ihn bei der Abholung bereit.</p>
        <b><p>Bitte überprüfen Sie die Dokumente sofort auf Vollständigkeit.</p></b>
        <p>Wenn Sie sich für die Abholung Ihrer Tickets am Tag Ihrer Reservierung entscheiden, stellen Sie bitte sicher, dass Sie spätestens 2 Stunden vor Beginn Ihrer Reservierung in unserem Partner-Shop eintreffen, um sicherzustellen, dass Sie rechtzeitig im Bierzelt ankommen. Andernfalls werden die Plätze an andere Gäste vergeben.</p>
        <address>{pickup_address}</address>
        <p>Google Map Location <a target="blank" href="{ttb_pickup_address_map_link}">hier klicken</a></p>
        <p>Ihr Ansprechpartner: <b>{pickup_contact}</b></p>
        <h2>Öffnungszeiten:</h2>
        <p>{pickup_open_hours}</p>
        <hr>
        <b><em>Wir empfehlen Ihnen dringend, Ihre Reservierung spätestens 4 Stunden vor Beginn Ihrer Reservierungszeit abzuholen.</em></b>
        <hr>
        <p>Wenn Sie Fragen zu Ihrer Reservierung haben oder während Ihres Aufenthalts im Bierzelt Unterstützung benötigen, wenden Sie sich bitte ausschließlich an unsere Hotline unter {hotline} oder senden Sie uns eine E-Mail an {contact_email} </p>
        <p>Bitte besprechen Sie keine Probleme mit dem Personal des Bierzelt !!!</p>
        <p>Wir wünschen Ihnen einen angenehmen Aufenthalt auf dem Oktoberfest {event_year}!</p>
        <?php
        $this->messagebox_1_de = ob_get_clean();
        
        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // Override the email type after parent constructor
        $this->email_type = 'html'; 
    }

    /**
     * Sets the order 
     * @param  int $order_id the id for the order
     * @return void
     */ 
    public function setOrder( $order_id )
    {
        // Set the order
        $this->order = wc_get_order( $order_id );

        // Set the placeholders
        $this->placeholders = $this->email_tools->setPlaceholders( $this->order, $this->placeholders );
    }

    /**
     * Trigger Function that will send this email.
     *
     * @access public
     * @return void
     */
    public function trigger( $order )
    {
        // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' );
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' );

        // Setup order object
        $this->setOrder( $order );

        // Ensure all required placeholders are set
        $check_required_placeholders = $this->email_tools->checkRequiredPlaceholders( $this->required_placeholders, $this->placeholders );
        if ( $check_required_placeholders !== true ) {
            return $check_required_placeholders;
        }

        // Attempt to set recipient
        $this->recipient = $this->order->get_billing_email();
        if ( $this->recipient == '' )
            return __( 'Email not sent: No recipient', 'tabticketbroker' );

        // Send
        $this->send($this->get_recipient(), $this->email_tools->getSubject( $this ), $this->get_content_html(), $this->get_headers(), $this->get_attachments());

        // Record for progressbar
        update_post_meta( $order->get_id(), '_ttb_prg_collection_ready_sent', 1 );

        return true;
    }

    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html()
    {
        ob_start();

        wc_get_template(
            $this->template_html,
            array(
                'order'                 => $this->order,
                'email_heading'         => $this->email_tools->getHeading( $this ),
                'email'                 => $this->recipient,
                'sent_to_admin'         => false,
                'plain_text'            => false,
                'messagebox_1'          => $this->email_tools->getMessageboxContent( $this, '1' ),
                // 'messagebox_2'          => $this->email_tools->getMessageboxContent( $this, '2' ),
            ),
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    }

    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields()
    {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject ),
                'placeholder' => $this->subject,
                'default'     => $this->subject,
            ),
            'subject_de'    => array(
                'title'       => 'Subject (German)',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject_de ),
                'placeholder' => $this->subject_de,
                'default'     => $this->subject_de,
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading ),
                'placeholder' => $this->heading,
                'default'     => $this->heading,
            ),
            'heading_de'    => array(
                'title'       => 'Email Heading (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading_de ),
                'placeholder' => $this->heading_de,
                'default'     => $this->heading_de,
            ),
            'messagebox_1'    => array(
                'title'       => 'Messagebox 1 (English)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 5em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1 ).'</pre>',
                'placeholder' => $this->messagebox_1,
                'default'     => $this->messagebox_1
            ),
            'messagebox_1_de' => array(
                'title'       => 'Messagebox 1 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 5em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1_de ).'</pre>',
                'placeholder' => $this->messagebox_1_de,
                'default'     => $this->messagebox_1_de
            ),
        );
    }
}
