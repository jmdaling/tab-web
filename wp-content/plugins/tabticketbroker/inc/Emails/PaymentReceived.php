<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\OrderTools;
use Inc\Base\BaseController;
use Inc\EmailController;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_Email' ) ) {
    return;
}

class PaymentReceived extends WC_Email
{
    public $order;

    public $invoice_no;
    
    public $is_invoiced = false;

    public $base_controller;

    public $is_europe = false;

    public $email_type;

	function __construct()
    {
        $this->customer_email = true;
        
        $this->base_controller= new BaseController();

        // set ID, this simply needs to be a unique name
        $this->id = 'email_customer_payment_received';

        // this is the title in WooCommerce Email settings
        $this->title = $this->base_controller->plugin_name_short .' - '. __( 'Payment Received', 'tabticketbroker' );
 
        // this is the description in WooCommerce email settings
        $this->description = __( 'An email to the customer confirming that their payment have been received.', 'tabticketbroker' );

        // these are the default heading and subject lines that can be overridden using the settings
        $this->heading = __( 'Payment received', 'tabticketbroker' );
        $this->subject = __( 'Payment received', 'tabticketbroker' );

        // these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
        $this->template_base  = $this->base_controller->plugin_path .'templates/';
        $this->template_html  = 'emails/payment_received.php';
        
        $this->email_type = 'html';

        // Trigger on new paid orders
        // $email_controller = new EmailController;
        // add_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_controller, 'emailPaymentReceived' ) );

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // this sets the recipient to the settings defined below in init_form_fields()
        $this->recipient = $this->get_option( 'recipient' );

        // if none was entered, just use the WP admin email as a fallback
        if ( ! $this->recipient )
            $this->recipient = get_option( 'admin_email' );
    }

    /**
     * Sets the order. This is only used in the email previews. Typically orders are automatically passed to the class in the trigger function.
     * @param  int $order_id the id for the order
     * @access public
	 * @return object order
     */ 

    public function setOrder( $order_id )
    {
        $order = wc_get_order( $order_id );
        
        $this->object = $order;

        // Set invoice no
        $this->invoice_no = get_post_meta( $order->id, '_ttb_invoice_no', true );
        
        if ( $this->invoice_no == '' ) {

            $this->is_invoiced = true;
            
            $this->invoice_no = __( 'To be created', 'tabticketbroker' );
        }

        // Check if order is in europe
        $order_tools = new OrderTools;

        $this->is_europe = $order_tools->isEuContinent( $order->get_shipping_country() );

        return $order;
    }

  	/**
	 * Trigger Function that will send this test email.
	 *
	 * @access public
	 * @return void
	 */
    public function trigger( $order_id ) 
    {
        // Setup order object
        $order = $this->setOrder( $order_id );

                // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' );
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' );

        // Attempt to set recipient
        if ( ! $this->get_recipient() )
            $this->recipient = $order->get_billing_email();
            if ( $this->recipient == '' )
                return __( 'Email not sent: No recipient', 'tabticketbroker' );

        // Check if order have been paid
        if ( ! $order->is_paid() ) {
            return __( 'Payment received email NOT sent: Order is not marked as paid', 'tabticketbroker' ) .' ('. __( 'This is done by putting it in processing', 'tabticketbroker' ) .')';
        }

        // Add invoice no to subject        
        $subject = sprintf( __( 'Payment received for your order %s', 'tabticketbroker' ), $order->get_order_number() );
        
        // Send
        $this->send( $this->get_recipient(), $subject, $this->get_content(), $this->get_headers(), $this->get_attachments() );

        // Return
        return true;
    }
    
    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, 
            array(
                'order'           => $this->object,
                'email_heading' => __( $this->heading, 'tabticketbroker' ),
                'email'           => $this->recipient,
                'sent_to_admin'   => false,
                'plain_text'      => false,
                'invoice_no'      => $this->invoice_no,
                'is_invoiced'     => $this->is_invoiced,
            ), 
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    } 
    
    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'recipient'  => array(
                'title'       => 'Recipient(s)',
                'type'        => 'text',
                'description' => sprintf( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', esc_attr( get_option( 'admin_email' ) ) ),
                'placeholder' => '',
                'default'     => esc_attr( get_option( 'admin_email' ) )
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>', $this->subject ),
                'placeholder' => '',
                'default'     => $this->subject
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
                'placeholder' => '',
                'default'     => $this->heading
            ),
            'email_type' => array(
                'title'       => 'Email type',
                'type'        => 'select',
                'description' => 'Choose which format of email to send.',
                'default'     => 'html',
                'class'       => 'email_type',
                'options'     => array(
                    'plain'     => 'Plain text',
                    'html'      => 'HTML', 'woocommerce',
                    'multipart' => 'Multipart', 'woocommerce',
                )
            )
        );
    }

}