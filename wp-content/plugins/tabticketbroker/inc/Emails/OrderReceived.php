<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\EmailTools;
use Inc\Base\BaseController;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (!class_exists('WC_Email')) {
    WC()->mailer();
}

class OrderReceived extends WC_Email
{
    public $order;
    public $email_tools;
    public $base_controller;

    function __construct()
    {
        if ( ! is_object( $this->base_controller ) ) {
            $this->base_controller = new BaseController();
        }

        if ( ! is_object( $this->email_tools ) ) {
            $this->email_tools = new EmailTools();
        }

        // Email Config vars
        $this->id = 'email_customer_order_received';
        $this->title = $this->base_controller->plugin_name_short . ' - ' . __( 'Order Received', 'tabticketbroker' );
        $this->customer_email = true;
        $this->template_base  = $this->base_controller->plugin_path . 'templates/emails/';
        $this->template_html  = 'order_received.php';

        // Set the placeholders
        $this->placeholders = $this->email_tools->getPlaceholders( $this->placeholders );
        $this->description = __('An email to the customer confirming that his/her order have been received with displaying the order details.', 'tabticketbroker');
        $this->description = $this->email_tools->addPlaceholderDescription( $this->placeholders, $this->description );

        // Dynamic content
        $this->subject      = 'Your order received - {order_no}';
        $this->subject_de   = 'Ihre Bestellung erhalten - {order_no}';
        
        $this->heading      = 'Your order received - {order_no}';
        $this->heading_de   = 'Ihre Bestellung erhalten - {order_no}';
        
// Add configurable content boxes
$this->messagebox_1 = 
'<p>Hello {first_name} {last_name}</p>
<p>Thank you for your order with {website_url}</p>
<p>Your order is now being processed. All details are shown below for your reference.</p>
';

$this->messagebox_1_de = 
'<p>Hallo {first_name} {last_name}</p>
<p>Vielen Dank für Ihre Bestellung bei {website_url}</p>
<p>Ihre Bestellung wird nun bearbeitet. Alle Details sind unten als Referenz aufgeführt.</p>
';

$this->messagebox_2 = 
'<p><strong>Weekdays, you will receive a booking confirmation with an invoice within 24 hours.</strong></p>
<h2>Information</h2>
<p>The delivery of the documents takes place within 5 - 10 days prior to the event date.</p>
<p>For further enquiries don\'t hesitate to contact us at any time.</p>
<hr><br>
';

$this->messagebox_2_de = 
'<p><strong>Werktags erhalten Sie innerhalb von 24 Stunden eine Buchungsbestätigung mit Rechnung.</strong></p>
<h2>Informationen</h2>
<p>Die Lieferung der Reservierungsunterlagen (keine E-Tickets möglich) erfolgt per Express ca. 5 - 10 Tage vor dem Eventdatum.</p>
<p>Für Rückfragen stehen wir Ihnen gerne jederzeit zur Verfügung.</p>
<hr><br>
';

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // Override the email type after parent constructor
        $this->email_type = 'html'; 
    }

    /**
     * Sets the order 
     * @param  int $order_id the id for the order
     * @return void
     */ 
    public function setOrder( $order_id )
    {
        // Set the order
        $this->order = wc_get_order( $order_id );

        // Set the placeholders
        $this->placeholders = $this->email_tools->setPlaceholders( $this->order, $this->placeholders );
    }

    /**
     * Trigger Function that will send this email.
     *
     * @access public
     * @return void
     */
    public function trigger( $order )
    {
        // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' );
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' );

        // Setup order object
        $this->setOrder( $order );

        // Attempt to set recipient
        $this->recipient = $this->order->get_billing_email();
        if ( $this->recipient == '' )
            return __( 'Email not sent: No recipient', 'tabticketbroker' );

        // Send
        $this->send($this->get_recipient(), $this->email_tools->getSubject( $this ), $this->get_content_html(), $this->get_headers(), $this->get_attachments());

        // Record for progressbar
        update_post_meta( $order->get_id(), '_ttb_prg_received', 1 );

        return true;
    }

    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html()
    {
        ob_start();

        wc_get_template(
            $this->template_html,
            array(
                'order'                 => $this->order,
                'email_heading'         => $this->email_tools->getHeading( $this ),
                'email'                 => $this->recipient,
                'sent_to_admin'         => false,
                'plain_text'            => false,
                'messagebox_1'          => $this->email_tools->getMessageboxContent( $this, '1' ),
                'messagebox_2'          => $this->email_tools->getMessageboxContent( $this, '2' ),
            ),
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    }

    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields()
    {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject ),
                'placeholder' => $this->subject,
                'default'     => $this->subject,
            ),
            'subject_de'    => array(
                'title'       => 'Subject (German)',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject_de ),
                'placeholder' => $this->subject_de,
                'default'     => $this->subject_de,
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading ),
                'placeholder' => $this->heading,
                'default'     => $this->heading,
            ),
            'heading_de'    => array(
                'title'       => 'Email Heading (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading_de ),
                'placeholder' => $this->heading_de,
                'default'     => $this->heading_de,
            ),
            'messagebox_1'    => array(
                'title'       => 'Messagebox 1 (English)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 5em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1 ).'</pre>',
                'placeholder' => $this->messagebox_1,
                'default'     => $this->messagebox_1
            ),
            'messagebox_1_de' => array(
                'title'       => 'Messagebox 1 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 5em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1_de ).'</pre>',
                'placeholder' => $this->messagebox_1_de,
                'default'     => $this->messagebox_1_de
            ),
            'messagebox_2'    => array(
                'title'       => 'Messagebox 2 (English) styled',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 20em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2 ).'</pre>',
                'placeholder' => $this->messagebox_2,
                'default'     => $this->messagebox_2
            ),
            'messagebox_2_de' => array(
                'title'       => 'Messagebox 2 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 20em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2_de ).'</pre>',
                'placeholder' => $this->messagebox_2_de,
                'default'     => $this->messagebox_2_de
            ),
        );
    }
}
