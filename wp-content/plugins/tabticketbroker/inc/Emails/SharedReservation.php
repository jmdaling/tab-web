<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\EmailTools;
use Inc\Base\BaseController;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_Email' ) ) {
    return;
}

class SharedReservation extends WC_Email
{
    public $order;
    public $reservation;
    public $email_tools;
    public $base_controller;

	function __construct()
    {
        if ( ! is_object( $this->base_controller ) ) {
            $this->base_controller = new BaseController();
        }

        if ( ! is_object( $this->email_tools ) ) {
            $this->email_tools = new EmailTools();
        }
        
        // Email Config vars
        $this->id = 'email_customer_shared_reservation';
        $this->title = $this->base_controller->plugin_name_short .' - '. __( 'Shared Reservation', 'tabticketbroker' );
        $this->customer_email = true;
        $this->template_base  = $this->base_controller->plugin_path .'templates/';
        $this->template_html  = 'emails/shared_reservation.php';
        $this->recipient = $this->get_option( 'recipient' ) ? $this->get_option( 'recipient' ) : get_option( 'admin_email' );
        $this->placeholders = $this->email_tools->getPlaceholders();
        $this->placeholders['{shared_reservation_infobox}'] = __( 'An info box that includes all the other orders that are share the original reservation', 'tabticketbroker' );
        $this->placeholders['{reservation_owner}'] = __( 'The name of the reservation owner', 'tabticketbroker' );
        $this->placeholders['{parent_reservation_pax}' ] = __( 'The number of people assigned to the reservation', 'tabticketbroker' );

        $description = __( 'This is the email that is sent to the customer when a reservation is shared with between customers.', 'tabticketbroker' );
        $this->description = $this->email_tools->addPlaceholderDescription( $this->placeholders, $description );
        
        // Dynamic content
        $this->heading      = __( 'Additional Information for your Oktoberfest Reservation', 'tabticketbroker' );
        $this->heading_de   = __( 'Zusätzliche Hinweise zu Ihrer Oktoberfest Reservierung', 'tabticketbroker' );

        $this->subject      = __( "Additional Information for your Oktoberfest Reservation", 'tabticketbroker' );
        $this->subject_de   = __( "Zusätzliche Hinweise zu Ihrer Oktoberfest Reservierung", 'tabticketbroker' );
        
        // Add configurable content boxes
$this->messagebox_1 = 
'<h3 style="color:#fff;height:0;font-size:0.001em;">Order {order_no}</h3>
<p>Hello {first_name} {last_name}</p>
<p>Your reserved seats are part of a table reservation which you will share with other groups who will sit next to you.</p>
{shared_reservation_infobox}
<br>
<p>At the tables, there are 2 benches for 5 people each, with a free choice of seats. Please arrange things so everyone is happy and the individual groups can sit together.</p>
<p>This information is provided to assist you in knowing who has a right to be seated at the table with you.</p>
<br>
<p><b>The entire reservation is still in the name of the original reservation holder:</b></p>
<h2>{reservation_owner}</h2>
<br>
<p>Please also refer to the official reservation documents.</p>
<p>If one of the other groups should be late, please inform the waiter to hold the seats as long as possible to avoid the other group losing their seats to other guests.</p>
<br>
<p style="color:red;">If you need any help please <b>DON`T CONTACT THE BEER HALL STAFF</b> nor ask for information at the reservation office inside the beer hall. <b>Please contact us directly!</b></p>
<br>
<p><b>If problems or questions occur feel free to contact us anytime.</b></p>
<br>
<p>Mobile <b>{mobile_tel}</b><br>
<br>
<p>We wish you a pleasant time at the Wies\'n.</p>
';

$this->messagebox_1_de = 
'<h3 style="color:#fff;height:0;font-size:0.001em;">Order {order_no}</h3>
<p>Hallo {first_name} {last_name}</p>
<p>Ihre reservierten Plätze sind Bestandteil einer größeren Tisch-Reservierung an der folgende Kundengruppen von uns Platz nehmen.</p>
{shared_reservation_infobox}
<br>
<p>An den Tischen gibt es jeweils 2 Bänke á 5 Personen mit freier Platzwahl. Bitte arrangieren Sie sich untereinander so, dass alle zufrieden sind und die einzelnen Gruppen untereinander <u>zusammen sitzen</u> können.</p>
<p>Diese Informationen dienen zur Unterstützung, damit Sie wissen, wer ein Recht hat zusammen mit Ihnen am Tisch Platz zu nehmen.</p>
<p>Die gesamte Reservierung läuft nach wie vor auf den ursprünglichen Reservierungsinhaber:</p>
<h2>{reservation_owner}</h2>
<p>Schauen Sie hierzu bitte auch auf die offiziellen Reservierungsunterlagen.</p>
<p>Falls sich eine der anderen Gruppen verspäten sollte, informieren Sie die KellnerIn bitte entsprechend, dass er/sie die Plätze möglichst lange freihält und nicht anderweitig besetzt.</p>
<p>Sollten Sie Fragen oder Probleme im Zelt haben, <b>kontaktieren Sie bitte ausschließlich unsere Hotline:</b></p>
<br>
<p>Mobile <b>{mobile_tel}</b><br>
<br>
<p style="color:red;"><b>Wir sind für Sie erreichbar. Bitte wenden Sie sich NICHT an das Reservierungsbüro des Zeltes!</b></p>
<br>
<p>Wir wünschen Ihnen viel Spaß auf der Wies\'n.</p>
';

$this->messagebox_2 = 
'
';

$this->messagebox_2_de = 
'
';

        // Triggers
        // This email is triggerd in the EmailController class with the woocommerce_gzd_updated_shipment_status hook and shipmentStatusChanged method

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();
        
        // Override the email type after parent constructor
        $this->email_type = 'html'; 

    }

    /**
     * Sets the order. This is only used in the email previews. Typically orders are automatically passed to the class in the trigger function.
     * @param  int $order_id the id for the order
     * @access public
	 * @return void
     */ 

    public function setOrder( $order_id, $args = array() )
    {
        // Set the order
        $this->order = wc_get_order( $order_id );

        // Quit with message if no reservation is found
        if ( ! isset( $args['reservation'] ) || ! is_object( $args['reservation'] ) ) {
            return __( 'Email not sent: No reservation received', 'tabticketbroker' ) . ' ' . $this->order->get_order_number();
        }
        else {
            $this->reservation = $args['reservation'];
        }

        $this->placeholders = $this->email_tools->setPlaceholders( $this->order, $this->placeholders, $args );

        // If reservation owner is not set in the placeholders, don't send the email
        if ( isset( $this->placeholders['{reservation_owner}'] ) && $this->placeholders['{reservation_owner}'] == '{reservation_owner}' ) {
            return __( 'Email not sent: No reservation owner set', 'tabticketbroker' );
        }
    }

  	/**
	 * Trigger Function that will send this test email.
	 *
	 * @access public
	 * @return void
	 */
    public function trigger( $order, $args ) 
    {
        
        // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' ) . ' ' . $this->order->get_order_number();
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' ) . ' ' . $this->order->get_order_number();

        // Setup order object
        $this->setOrder( $order, $args );

        // Attempt to set recipient
        $this->recipient = $this->order->get_billing_email();
        if ( $this->recipient == '' )
            return __( 'Email not sent: No recipient', 'tabticketbroker' ) . ' ' . $this->order->get_order_number();

        // Send
        $this->send( $this->recipient, $this->email_tools->getSubject( $this ), $this->get_content(), $this->get_headers(), $this->get_attachments() );
        
        return true;
    }
    
    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, 
            array(
                'order'         => $this->order,
                'email_heading' => $this->email_tools->getHeading( $this ),
                'sent_to_admin' => false,
                'plain_text'    => false,
                'messagebox_1'  => $this->email_tools->getMessageboxContent( $this, '1' ),
                'messagebox_2'  => $this->email_tools->getMessageboxContent( $this, '2' ),
            ), 
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    } 
    
    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject ),
                'placeholder' => $this->subject,
                'default'     => $this->subject,
            ),
            'subject_de'    => array(
                'title'       => 'Subject (German)',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject_de ),
                'placeholder' => $this->subject_de,
                'default'     => $this->subject_de,
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading ),
                'placeholder' => $this->heading,
                'default'     => $this->heading,
            ),
            'heading_de'    => array(
                'title'       => 'Email Heading (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading_de ),
                'placeholder' => $this->heading_de,
                'default'     => $this->heading_de,
            ),
            'messagebox_1'    => array(
                'title'       => 'Messagebox 1 (English)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 30em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1 ).'</pre>',
                'placeholder' => $this->messagebox_1,
                'default'     => $this->messagebox_1
            ),
            'messagebox_1_de' => array(
                'title'       => 'Messagebox 1 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 30em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1_de ).'</pre>',
                'placeholder' => $this->messagebox_1_de,
                'default'     => $this->messagebox_1_de
            ),
            'messagebox_2'    => array(
                'title'       => 'Messagebox 2 (English) styled',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 5em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2 ).'</pre>',
                'placeholder' => $this->messagebox_2,
                'default'     => $this->messagebox_2
            ),
            'messagebox_2_de' => array(
                'title'       => 'Messagebox 2 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 5em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2_de ).'</pre>',
                'placeholder' => $this->messagebox_2_de,
                'default'     => $this->messagebox_2_de
            ),            
        );
    }
}