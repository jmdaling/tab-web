<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Emails;

use \WC_Email;
use Inc\Tools\EmailTools;
use Inc\Base\BaseController;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if (!class_exists('WC_Email')) {
    WC()->mailer();
}

class OrderConfirmed extends WC_Email
{        
    public $order;
    public $email_tools;
    public $base_controller;
    
    function __construct() 
    {
        if ( ! is_object( $this->base_controller ) ) {
            $this->base_controller = new BaseController();
        }

        if ( ! is_object( $this->email_tools ) ) {
            $this->email_tools = new EmailTools();
        }

        // Email Config vars
        $this->id = 'email_customer_order_confirmed';
        $this->title = $this->base_controller->plugin_name_short .' - '. __( 'Order Confirmed', 'tabticketbroker' );
        $this->customer_email = true;
        $this->template_base  = $this->base_controller->plugin_path .'templates/';
        $this->template_html  = 'emails/order_confirmed.php';
        
        // Set the placeholders
        $this->placeholders = $this->email_tools->getPlaceholders( $this->placeholders );
        $this->description = __( 'An email to the customer informing him/her that their order have been confirmed.', 'tabticketbroker' );
        $this->description = $this->email_tools->addPlaceholderDescription( $this->placeholders, $this->description );

        // Dynamic content
        $this->subject      = 'Invoice {invoice_no} for the Oktoberfest {event_year} - {order_no}';
        $this->subject_de   = 'Rechnung {invoice_no} für das Oktoberfest {event_year} - {order_no}';

        $this->heading      = 'Order confirmed';
        $this->heading_de   = 'Bestellung bestätigt';

// Add configurable content boxes
$this->messagebox_1 = 
'<p>Hello {first_name} {last_name}</p>
<p>Thank you for your order <b>{order_no}</b> with {website_url}</p>
<p>Invoice number <b>{invoice_no}</b> of <b>{invoice_total}</b> for your Oktoberfest reservation have been generated.</p>
<p>The following payment method was selected: <b>{payment_method_selected}</b></p>
<p>The reservation documents will be shipped about 5 - 10 days prior to the event date. Further details will be provided in September.</p>
<p>To ensure maximum reliability regarding the shipping of the reservation documents we strongly recommend our international customers to supply us with a delivery address within Germany (e.g. hotel). For short-termed orders, we also offer a collecting point in Munich.</p>
<p>Current delivery address:</p>
';

$this->messagebox_1_de = 
'<p>Hallo {first_name} {last_name}</p>
<p>Vielen Dank für Ihre Buchung <b>{order_no}</b> bei {website_url}</p>
<p>Rechnungsnummer <b>{invoice_no}</b> von <b>{invoice_total}</b> für Ihre Oktoberfest-Reservierung wurde generiert.</p>
<p>Die folgende Zahlungsmethode wurde ausgewählt: <b>{payment_method_selected}</b></p>
<p>Der Versand der Reservierungsunterlagen erfolgt ca. 5 - 10 Tage vor dem Veranstaltungstermin. Weitere Einzelheiten werden im September bekannt gegeben.</p>
<p>Um eine größtmögliche Zuverlässigkeit beim Versand der Reservierungsunterlagen zu gewährleisten, empfehlen wir unseren internationalen Kunden dringend, uns eine Lieferadresse innerhalb Deutschlands (z. B. Hotel) anzugeben. Für kurzfristige Bestellungen bieten wir auch eine Abholstelle in München an.</p>
<p>Aktuelle Lieferadresse:</p>
';

$this->messagebox_2 = 
'<p>For hotel reservations, we recommend our 4-star partner hotel "Eurostars Book Hotel" in Munich (only 5 minutes walking distance to the Oktoberfest). For availability and prices, please contact our customer service.</p>
<p>Please do not hesitate to contact us anytime if you need further assistance.</p>
';

$this->messagebox_2_de = 
'<p>Für Hotelreservierungen empfehlen wir unser 4-Sterne-Partnerhotel „Eurostars Book Hotel“ in München (nur 5 Gehminuten vom Oktoberfest entfernt). Für Verfügbarkeit und Preise wenden Sie sich bitte an unseren Kundenservice.</p>
<p>Bitte zögern Sie nicht, uns jederzeit zu kontaktieren, wenn Sie weitere Hilfe benötigen.</p>
';

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // Override the email type after parent constructor
        $this->email_type = 'html'; 
    }

    /**
     * Sets the order 
     * @param  int $order_id the id for the order
     * @return void
     */ 
    public function setOrder( $order_id )
    {
        // Set the order
        $this->order = wc_get_order( $order_id );

        // Set the placeholders
        $this->placeholders = $this->email_tools->setPlaceholders( $this->order, $this->placeholders );
    }

  	/**
	 * Trigger Function that will send this email.
	 *
	 * @access public
	 * @return void
     */
    public function trigger( $order ) 
    {
        // Check if enabled
        if ( ! $this->is_enabled() )
            return __( 'Email not sent: Disabled', 'tabticketbroker' );
        
        // Check for order
        if ( ! $order )
            return __( 'Email not sent: No order', 'tabticketbroker' );

        // Setup order object
        $this->setOrder( $order );

        // Check if invoice number is set, otherwise return error
        $invoice_no = get_post_meta( $this->order->id, '_ttb_invoice_no', true );
        if ( $invoice_no == '' ) {
            return __( 'Email not sent: invoice number empty', 'tabticketbroker' );
        }

        // Attempt to set recipient
        $this->recipient = $this->order->get_billing_email();
        if ( $this->recipient == '' )
            return __( 'Email not sent: No recipient', 'tabticketbroker' );

        // Send
        $this->send( $this->get_recipient(), $this->email_tools->getSubject( $this ), $this->get_content(), $this->get_headers(), $this->get_attachments() );

        // Mark as confirmed
        update_post_meta( $order->get_id(), '_ttb_prg_confirmed', 1 );

        return true;
    }
    
    /** Override the html content. Note that woocommerce by default uses its own paths. Here below we specified the 
     * template_path and default_path for the get template function. Although worked, see here for how to add a filter
     * and modify the default path for your own templates: https://gregbastianelli.com/using-wc_get_template-from-a-plugin
     */
    public function get_content_html() {
        ob_start();
        wc_get_template( $this->template_html, 
            array(
                'order'                     => $this->order,
                'email_heading'             => $this->email_tools->getHeading( $this ),
                'email'                     => $this->recipient,
                'sent_to_admin'             => false,
                'plain_text'                => false,
                'messagebox_1'              => $this->email_tools->getMessageboxContent( $this, '1' ),
                'messagebox_2'              => $this->email_tools->getMessageboxContent( $this, '2' ),
            ), 
            $this->template_base,
            $this->template_base
        );

        return ob_get_clean();
    } 
    
    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject ),
                'placeholder' => $this->subject,
                'default'     => $this->subject,
            ),
            'subject_de'    => array(
                'title'       => 'Subject (German)',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. The default subject is: <code>%s</code>', $this->subject_de ),
                'placeholder' => $this->subject_de,
                'default'     => $this->subject_de,
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading ),
                'placeholder' => $this->heading,
                'default'     => $this->heading,
            ),
            'heading_de'    => array(
                'title'       => 'Email Heading (German)',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. The default heading is: <code>%s</code>' ), $this->heading_de ),
                'placeholder' => $this->heading_de,
                'default'     => $this->heading_de,
            ),
            'messagebox_1'    => array(
                'title'       => 'Messagebox 1 (English)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 20em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1 ).'</pre>',
                'placeholder' => $this->messagebox_1,
                'default'     => $this->messagebox_1
            ),
            'messagebox_1_de' => array(
                'title'       => 'Messagebox 1 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 20em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_1_de ).'</pre>',
                'placeholder' => $this->messagebox_1_de,
                'default'     => $this->messagebox_1_de
            ),
            'messagebox_2'    => array(
                'title'       => 'Messagebox 2 (English) styled',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 10em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2 ).'</pre>',
                'placeholder' => $this->messagebox_2,
                'default'     => $this->messagebox_2
            ),
            'messagebox_2_de' => array(
                'title'       => 'Messagebox 2 (German)',
                'type'        => 'textarea',
                'css'         => 'width: 100%; height: 10em;',
                'description' => '<p>Default:</p><pre>'.esc_html__ ( $this->messagebox_2_de ).'</pre>',
                'placeholder' => $this->messagebox_2_de,
                'default'     => $this->messagebox_2_de
            ),
        );
    }
}