<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Inc\Api\SettingsApi;
use Inc\ReportController;
use Inc\Base\BaseController;
use Inc\Classes\EmailPreview;
use Inc\Classes\AssignmentManager;
use Inc\Api\Callbacks\AdminCallbacks;

class AdminPages extends BaseController
{
    public $settings;

    public $callbacks;

    public $pages = array();
    
    public $sub_pages = array();
    
    public $assignment_manager;
    
    public $email_preview;

    public $report_controller;

    public function register() 
    {
        // Add admin menu creation on plugins_loaded action (required for languaging to work on admin menus (See Languaging class))
        add_action( 'plugins_loaded', array( $this, 'createAdminMenu' ) );

    }
   
    public function createAdminMenu()
    {
        
        $this->report_controller = new ReportController();

        $this->settings = new SettingsApi();

        $this->callbacks = new AdminCallbacks();
        
        $this->email_preview = new EmailPreview();

        $this->assignment_manager = new AssignmentManager();
    
        // Create Admin pages
        $this->setPages();

        $this->setSubPages();

        // Set Settings
        $this->setSettings();
        $this->setSections();
        $this->setFields();
         
        $this->settings->addPages( $this->pages )->withSubPage( __( 'Dashboard', 'tabticketbroker' ) )->addSubPages( $this->sub_pages )->register();

    }
    
    public function setPages()
    {

        $this->pages = array(
            array(
            'page_title' => 'Tab Admin', 
            'menu_title' => 'Tab Admin',  
            'capability' => 'manage_options', 
            'menu_slug'  => 'tab_admin_plugin', 
            'callback'   => array( $this->callbacks, 'adminDashboard' ), 
            'icon_url'   => 'dashicons-admin-site-alt', 
            'position'   => 4.5,
            )
        );
    }
    
    public function setSubPages() 
    {
        $this->sub_pages = array(
            array(
                'parent_slug' => 'tab_admin_plugin',
                'page_title'  => __( 'Suppliers', 'tabticketbroker' ),
                'menu_title'  => __( 'Suppliers', 'tabticketbroker' ),
                'capability'  => 'manage_options',
                'menu_slug'   => 'edit.php?post_type=tab_supplier&tab_reservation_event_year='. TTB_EVENT_YEAR,
                'callback'    => null,
                'position'    => 2,
            ),
            array(
                'parent_slug' => 'tab_admin_plugin',
                'page_title'  => __( 'Reservations', 'tabticketbroker' ),
                'menu_title'  => __( 'Reservations', 'tabticketbroker' ),
                'capability'  => 'manage_options',
                'menu_slug'   => 'edit.php?post_type=tab_reservation&tab_reservation_event_year='. TTB_EVENT_YEAR,
                'callback'    => null,
                'position'    => 3,
            ),
            array(
                'parent_slug' => 'tab_admin_plugin',
                'page_title'  => __( 'Assignments', 'tabticketbroker' ),
                'menu_title'  => __( 'Assignments', 'tabticketbroker' ),
                'capability'  => 'manage_options',
                'menu_slug'   => $this->page_names['Assignments'],
                'callback'    => array( $this->assignment_manager, 'displayAssignments' ), 
                'position'    => 4,
                
            ),
            array(
                'parent_slug' => 'tab_admin_plugin',
                'page_title'  => __( 'Reports', 'tabticketbroker' ),
                'menu_title'  => __( 'Reports', 'tabticketbroker' ),
                'capability'  => 'manage_options',
                'menu_slug'   => $this->page_names['Reports'],
                'callback'    => array( $this->report_controller, 'reportsHome' ),
                'position'    => 5,
            ),
        );

        if ( is_admin() ) {
            $this->sub_pages = array_merge( $this->sub_pages, 
                array(
                    array(
                        'parent_slug' => 'tab_admin_plugin',
                        'page_title'  => __( 'Reservation Statuses', 'tabticketbroker' ),
                        'menu_title'  => __( 'Reservation Statuses', 'tabticketbroker' ),
                        'capability'  => 'manage_terms',
                        'menu_slug'   => 'edit-tags.php?taxonomy=tab_res_tax_status&post_type=tab_reservation',
                        'callback'    => null,
                        'position'    => 6,
                    ),
                    array(
                        'parent_slug' => 'tab_admin_plugin',
                        'page_title'  => __( 'Email Preview', 'tabticketbroker' ),
                        'menu_title'  => __( 'Email Preview', 'tabticketbroker' ),
                        'capability'  => 'manage_options',
                        'menu_slug'   => 'tab_admin_email_preview',
                        'callback'    => array( $this->email_preview, 'emailPreviewRender' ), 
                        'position'    => 10,
                    ),
                    array(
                        'parent_slug' => 'tab_admin_plugin',
                        'page_title'  => __( 'Debug', 'tabticketbroker' ),
                        'menu_title'  => __( 'Debug', 'tabticketbroker' ),
                        'capability'  => 'manage_options',
                        'menu_slug'   => 'tab_admin_debug',
                        'callback'    => function(){ return require_once( "$this->plugin_path/templates/debug.php" ); }, 
                        'position'    => 99,
                    ),
                )
            );
        }
    }

    
    public function setSections()
    {
        $args = array(
            array(
                'id'        => 'ttb_admin_emails_section',
                'title'     => 'Emails',
                'callback'  => array( $this->callbacks, 'ttbEmailSettingsSection' ),
                'page'      => 'tab_admin_plugin'
            ),
            array(
                'id'        => 'ttb_collection_settings_section',
                'title'     => 'Pickup Address',
                'callback'  => array( $this->callbacks, 'ttbPickupAddressSection' ),
                'page'      => 'tab_admin_plugin'
            ),
            // Event settings section
            array(
                'id'        => 'ttb_event_settings_section',
                'title'     => 'Event Settings',
                'callback'  => array( $this->callbacks, 'ttbEventSettingsSection' ),
                'page'      => 'tab_admin_plugin'
            ),
        );

        $this->settings->setSections( $args );
    }

    public function setSettings()
    {
        $args = array(

            // Collection Office Settings
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_address_line_1',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_address_line_2',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_address_city',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_address_postcode',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_address_map_link',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_contact_name',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_pickup_open_hours',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_hotline',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_contact_email',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),

            // Collections Admin Settings
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_collections_admin_username',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_collections_admin_password',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),

            // Email settings
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_show_tent_table_no',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_show_tent_customer_no',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_show_tent_booking_no',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),

            // Event settings
            // Event year
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_event_year',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            // First day of event
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_event_first_day',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
            // Last day of event
            array(
                'option_group'  => 'ttb_ofest_settings',
                'option_name'   => 'ttb_event_last_day',
                'callback'      => array( $this->callbacks, 'tabticketbrokerOptionsGroup' )
            ),
        );

        $this->settings->setSettings( $args );
    }

    public function setFields()
    {
        $args = array(

            // Collection fields
            array(
                'id'        => 'ttb_pickup_address_line_1',
                'title'     => 'Address Line 1',
                'callback'  => array( $this->callbacks, 'pickupAddressLine1' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Address Line 1',
                    'class'     => 'ttb-pickup-address'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_address_line_2',
                'title'     => 'Address Line 2',
                'callback'  => array( $this->callbacks, 'pickupAddressLine2' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Address Line 2',
                    'class'     => 'ttb-pickup-address'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_address_city',
                'title'     => 'City',
                'callback'  => array( $this->callbacks, 'pickupAddressCity' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'City',
                    'class'     => 'ttb-pickup-address'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_address_country',
                'title'     => 'Country',
                'callback'  => array( $this->callbacks, 'pickupAddressCountry' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'City',
                    'class'     => 'ttb-pickup-address'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_address_postcode',
                'title'     => 'Post Code',
                'callback'  => array( $this->callbacks, 'pickupAddressPostCode' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Post Code',
                    'class'     => 'ttb-pickup-address'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_address_map_link',
                'title'     => 'Map Link',
                'callback'  => array( $this->callbacks, 'pickupAddressMapLink' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Map Link',
                    'class'     => 'ttb-pickup-address'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_contact_name',
                'title'     => 'Contact Name',
                'callback'  => array( $this->callbacks, 'pickupContactName' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Contact Name',
                    'class'     => 'ttb-pickup-contact'
                ),
            ),
            array(
                'id'        => 'ttb_hotline',
                'title'     => 'Hotline',
                'callback'  => array( $this->callbacks, 'hotlinePhone' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Hotline Phone Number',
                    'class'     => 'ttb-pickup-contact'
                ),
            ),
            array(
                'id'        => 'ttb_contact_email',
                'title'     => 'Contact Email',
                'callback'  => array( $this->callbacks, 'contactEmail' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Contact Email',
                    'class'     => 'ttb-pickup-contact'
                ),
            ),
            array(
                'id'        => 'ttb_pickup_open_hours',
                'title'     => 'Opening Hours',
                'callback'  => array( $this->callbacks, 'pickupOpeningHours' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
                'args'      => array(
                    'label_for' => 'Opening Hours',
                    'class'     => 'ttb-pickup-contact'
                ),
            ),
            array(
                'id'        => 'ttb_collections_admin_username',
                'title'     => 'Collections Administrator Username',
                'callback'  => array( $this->callbacks, 'collectionAdminUserName' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
            ),
            array(
                'id'        => 'ttb_collections_admin_password',
                'title'     => 'Collections Administrator Password',
                'callback'  => array( $this->callbacks, 'collectionAdminPassword' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_collection_settings_section',
            ),

            // Email fields
            array(
                'id'        => 'ttb_show_tent_table_no',
                'title'     => 'Show Table No',
                'callback'  => array( $this->callbacks, 'getEmailShowTentTableNo' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_admin_emails_section',
                'args'      => array(
                    'label_for' => 'Show Tent Table No',
                    'class'     => 'emails-settings'
                ),
            ),
            // same for tent customer no
            array(
                'id'        => 'ttb_show_tent_customer_no',
                'title'     => 'Show Tent Customer No',
                'callback'  => array( $this->callbacks, 'getEmailShowTentCustomerNo' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_admin_emails_section',
                'args'      => array(
                    'label_for' => 'Show Tent Customer No',
                    'class'     => 'emails-settings'
                ),
            ),
            // same for tent booking no
            array(
                'id'        => 'ttb_show_tent_booking_no',
                'title'     => 'Show Tent Booking No',
                'callback'  => array( $this->callbacks, 'getEmailShowTentBookingNo' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_admin_emails_section',
                'args'      => array(
                    'label_for' => 'Show Tent Booking No',
                    'class'     => 'emails-settings'
                ),
            ),

            // Event fields
            // Event year
            array(
                'id'        => 'ttb_event_year',
                'title'     => 'Event Year',
                'callback'  => array( $this->callbacks, 'eventYear' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_event_settings_section',
                'args'      => array(
                    'label_for' => 'Event Year',
                    'class'     => 'event-settings'
                ),
            ),
            // First day of event
            array(
                'id'        => 'ttb_event_first_day',
                'title'     => 'First Day of Event',
                'callback'  => array( $this->callbacks, 'eventFirstDay' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_event_settings_section',
                'args'      => array(
                    'label_for' => 'First Day of Event',
                    'class'     => 'event-settings'
                ),
            ),
            // Last day of event
            array(
                'id'        => 'ttb_event_last_day',
                'title'     => 'Last Day of Event',
                'callback'  => array( $this->callbacks, 'eventLastDay' ),
                'page'      => 'tab_admin_plugin',
                'section'   => 'ttb_event_settings_section',
                'args'      => array(
                    'label_for' => 'Last Day of Event',
                    'class'     => 'event-settings'
                ),
            ),

            // SENDING ADDRESS
            // "name1": "Test AG",
            // "street": "Nowhere Straße",
            // "houseNumber": "12",
            // "zipCode": "53119",
            // "city": "Berlin",
            // "country": "DE",
            // "phoneNumber": "",
            // "email": ""

            // array(
            //     'id'        => 'ttb_consignor_name',
            //     'title'     => __( 'Consignor Name', 'tabticketbroker' ),
            //     'callback'  => array( $this->callbacks, 'consignorName' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => 'Consignor Name',
            //         'class'     => 'ttb-consignor-name'
            //     ),
            // ),
            // array(
            //     'id'        => 'ttb_consignor_email',
            //     'title'     => __( 'Consignor Email', 'tabticketbroker' ),
            //     'callback'  => array( $this->callbacks, 'consignorEmail' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => __( 'Consignor Email', 'tabticketbroker' ),
            //         'class'     => 'ttb-consignor-email'
            //     ),
            // ),
            // array(
            //     'id'        => 'ttb_consignor_phone',
            //     'title'     => __( 'Phone number', 'tabticketbroker' ),
            //     'callback'  => array( $this->callbacks, 'consignorPhone' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => __( 'Consignor phone number', 'tabticketbroker' ),
            //         'class'     => 'ttb_consignor_phone'
            //     ),
            // ),
            // array(
            //     'id'        => 'ttb_consignor_street',
            //     'title'     => __( 'Street name', 'tabticketbroker' ),
            //     'callback'  => array( $this->callbacks, 'consignorAddressLine1' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => __( 'Street name', 'tabticketbroker' ),
            //         'class'     => 'ttb-consignor-address'
            //     ),
            // ),
            // array(
            //     'id'        => 'ttb_consignor_house_no',
            //     'title'     => __( 'House number', 'tabticketbroker' ),
            //     'callback'  => array( $this->callbacks, 'consignorAddressLine2' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => __( 'House number', 'tabticketbroker' ),
            //         'class'     => 'ttb-consignor-address'
            //     ),
            // ),
            // array(
            //     'id'        => 'ttb_consignor_city',
            //     'title'     => 'City',
            //     'callback'  => array( $this->callbacks, 'consignorAddressCity' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => 'City',
            //         'class'     => 'ttb-consignor-address'
            //     ),
            // ),
            // array(
            //     'id'        => 'ttb_consignor_postcode',
            //     'title'     => 'Post Code',
            //     'callback'  => array( $this->callbacks, 'consignorAddressPostCode' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_consignor_address_section',
            //     'args'      => array(
            //         'label_for' => 'Post Code',
            //         'class'     => 'ttb-consignor-address'
            //     ),
            // ),
            
            // API 
            // array(
            //     'id'        => 'ttb_go_api_debug_mode',
            //     'title'     => 'Go Express Debug Mode',
            //     'callback'  => array( $this->callbacks, 'goApiDebugToggle' ),
            //     'page'      => 'tab_admin_plugin',
            //     'section'   => 'ttb_admin_api_section',
            //     'args'      => array(
            //         'label_for' => 'Go Express Debug Mode',
            //         'class'     => 'ttb-go-api-debug-mode'
            //     ),
            // ),
        );

        $this->settings->setFields( $args );
    }
}
