<?php
/** 
 * @package tabticketbroker
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{
    public function register() {
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueueBackend' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueueFrontend' ) );

        // Add action for updating variation data used in the Price Report
        add_action('wp_ajax_update_variation_jmd', array($this, 'updateVariationData'));
        add_action('wp_ajax_bulk_update_variation_jmd', array($this, 'bulkUpdateVariationData'));
        add_action('wp_ajax_delete_variation_jmd', array($this, 'deleteVariationData')); // Product config report

        // Register ajax action for changing reservation status
        add_action('wp_ajax_update_reservation_status_jmd', array('Inc\Classes\TabReservation', 'changeReservationStatus'));

        // Register ajax action for saving the sub reservation array to the reservation
        add_action('wp_ajax_save_sub_reservations', array('Inc\Classes\TabReservation', 'saveSubReservations'));

        // Register ajax action for saving the sub reservation array to the reservation
        add_action('wp_ajax_delete_sub_reservations', array('Inc\Classes\TabReservation', 'deleteSubReservations'));

        // Register ajax action for saving the table nos to the reservation
        add_action('wp_ajax_save_table_nos', array('Inc\Classes\TabReservation', 'saveTableNos'));

        // Register ajax action for marking a pickup order item as collected
        // add_action('wp_ajax_mark_pickup_item_collected', array('Inc\Tools\OrderTools', 'markPickupItemCollected'));

        // Register ajax action for updating parcel meta
        add_action('wp_ajax_parcel_meta_status_change', array('Inc\Tools\OrderTools', 'updateParcelMeta'));
        add_action('wp_ajax_nopriv_parcel_meta_status_change', array('Inc\Tools\OrderTools', 'updateParcelMeta'));
    }

    public function enqueueBackend() 
    {
        // Enqueue jquery
        wp_enqueue_script( 'jQuery' );

        // Include popper JS distribution version (for tooltips)
        wp_enqueue_script( 'popper_script', $this->plugin_url . 'assets/popper.js' );
        
        // Include bootstrap distribution version
        wp_enqueue_script( 'bootstrap_script', $this->plugin_url . 'assets/bootstrap.js' );
        
        // Bootstrap for admin breaks some tables and layouts, use this hack to wrap required sections in a bootstrap wrapper
        // Source: https://rushfrisby.com/using-bootstrap-in-wordpress-admin-panel
        wp_enqueue_script( 'admin_js_bootstrap_hack', $this->plugin_url . 'assets/bootstrap-hack.js' );
        
        // This is the old way, working to just add styling but breaks other admin styles such as tables in media library:
        // Enqueue only on certain pages
        // if( $post )
        global $pagenow;
    
        // Enqueue backend script for our own pages
        if ( isset( $_GET['page'] ) )
            if ( $pagenow == 'admin.php' && 
                $_GET['page'] == 'tab_admin_plugin' || 
                $_GET['page'] == 'tab_admin_reports' || 
                $_GET['page'] == 'tab_admin_assignment_mgmt' ) 
                    wp_enqueue_style( 'bootstrap_style', $this->plugin_url . 'assets/bootstrap.css' ); 

        // Also enqueue for editing of posts (part of wordpress)
        if ( isset( $_GET['post_type'] ) )
            if ( $pagenow == 'post-new.php' && $_GET['post_type'] == 'tab_supplier' )      // when adding new suppliers
                    wp_enqueue_style( 'bootstrap_style', $this->plugin_url . 'assets/bootstrap.css' ); 
        
        // Enqueue multiselect
        wp_enqueue_script( 'multiselect', $this->plugin_url . 'assets/multiselect-dropdown.js' );
        
        // Enqueue backend
        wp_enqueue_script( 'ttb-backend', $this->plugin_url . 'assets/ttb-backend.js' );
        wp_enqueue_style( 'ttb-backend', $this->plugin_url . 'assets/ttb-backend.css' );


        // ADDING SCRIPT FOR Reservations page
        if ( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'tab_reservation' ) {
            wp_enqueue_script( 'ttb-reservation-ajax', $this->plugin_url . 'assets/ttb-ajax.js' );
        }

        // Load script for editing reservations
        if ( get_post_type() == 'tab_reservation' && $_GET['action'] == 'edit' ) {
            wp_enqueue_script( 'ttb-reservation', $this->plugin_url . 'assets/ttb-reservation-editor.js' );
        }

        // For tab_admin_reports and report_slug=pickup, load collections script
        if ( isset( $_GET['page'] ) && $_GET['page'] == 'tab_admin_reports' && isset( $_GET['report_slug'] ) && $_GET['report_slug'] == 'pickup' ) {
            $this->registerCollectionScript();
        }
    }

    public function enqueueFrontend() 
    {

        // Enqueue site scripts
        if ( TTB_SITE === 'ofest' ) {
            wp_enqueue_style( 'ttb-frontend', $this->plugin_url . 'assets/ttb-frontend.css' );
    
            // wp_enqueue_style( 'bootstrap', $this->plugin_url . 'assets/bootstrap.css' );
    
            // If post is of post_type = product
            if ( is_singular( 'product' ) )
                wp_enqueue_script( 'ttb-singleproduct', $this->plugin_url . 'assets/ttb-singleproduct.js' );
    
            if ( is_front_page() ) {
                wp_enqueue_script( 'jquery' );
                wp_enqueue_script( 'ttb-homepage', $this->plugin_url . 'assets/ttb-homepage.js' );
            }

            // IF page is pickupfrontend add admin styles
            if ( is_page( 'pickupfrontend' ) ) {
                wp_enqueue_style( 'ttb-backend', $this->plugin_url . 'assets/ttb-backend.css' );

                $this->registerCollectionScript();

                // Add the JS
                // wp_enqueue_script( 'ttb-collections', $this->plugin_url . 'assets/ttb-collections.js' );
            }

            // Register product filter script for use when shortcode is called
            wp_register_script( 'ttb-productfilter', $this->plugin_url . 'assets/ttb-productfilter.js' );
        }
        elseif ( TTB_SITE === 'tabtickets' ) {
            wp_enqueue_style( 'ttb-frontend_tt', $this->plugin_url . 'assets/ttb-frontend_tt.css' );

            if ( is_front_page() ) {
                wp_enqueue_script( 'jquery' );
                wp_enqueue_script( 'ttb-homepage', $this->plugin_url . 'assets/ttb-homepage.js' );
            }

            // If post is of post_type = product
            if ( is_singular( 'product' ) ) {
                wp_enqueue_script( 'jquery' );
                wp_enqueue_script( 'ttb-singleproduct_tt', $this->plugin_url . 'assets/ttb-singleproduct_tt.js' );
            }
        }
    }

    /**
     * Register the script for the collection page
     */
    public function registerCollectionScript()
    {
        // Load script with jquery
        wp_enqueue_script( 'ttb-collections', $this->plugin_url . 'assets/ttb-collections.js', array('jquery') );

        // Localize the script with new data and ajax url
        $ajax_obj = array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce('ttb-collections-nonce')
        );
        wp_localize_script( 'ttb-collections', 'ajax_obj', $ajax_obj );
    }

    /**
     * Ajax function to update variation data
     */
    public function updateVariationData()
    {
        $variation_id       = (int) $_POST['variation_id'];
        $stock              = (int) $_POST['stock'];
        $price              = (float) $_POST['price'];
        $enabled            = (int) $_POST['enabled'];
        $sale_price         = (float) $_POST['sale_price'];
        $sale_price         = $sale_price == 0 ? '' : $sale_price;

        // Find the variation and set the new values
        $variation = wc_get_product( $variation_id );

        $variation->set_stock_quantity( $stock );
        $variation->set_regular_price( $price );
        $variation->set_sale_price( $sale_price );
        $variation->set_status( $enabled ? 'publish' : 'private' );
        $variation->save();

        // Return success
        echo json_encode( array( 'success' => true ) );
       
        wp_die();
    }

    /**
     * Ajax function to update variation data in bulk
     */
    public function bulkUpdateVariationData() 
    {
        $bulk_action            = $_POST['bulk_action'];
        $enabled                = $bulk_action == 'enable' ? 1 : 0;
        $var_ids                = $_POST['variation_ids'];
        
        // if not an array, quit
        if ( !is_array($var_ids) ) {
            echo json_encode( array( 'success' => false, 'error' => 'Error: No variation ids found' ) );
            wp_die();
        }

        foreach ($var_ids as $value ) {
            $value = (int) $value;

            // Skip if value is 0
            if ( $value == 0 )
                continue;
            
            // Find the variation and set the new values
            $variation = wc_get_product( $value );

            if ( $variation == false ) {
                echo json_encode( array( 'success' => false, 'error' => 'Error: Variation not found with id: ' . $value ) );
                wp_die();
            }

            // $variation->set_stock_quantity( $stock );
            // $variation->set_regular_price( $price );
            // $variation->set_sale_price( $sale_price );
            $variation->set_status( $enabled ? 'publish' : 'private' );
            $variation->save();
        }

        // Return success
        echo json_encode( array( 'success' => true ) );
        wp_die();
    }

    /**
     * Ajax function to delete variation data
     * @param string $variation_id This is a comma separated string of variation ids
     */
    public function deleteVariationData()
    {
        $variation_id       = $_POST['variation_id'];
        // $delete_type        = $_POST['delete_type'];

        $var_ids = array();
        $var_ids = explode( ",", $variation_id );
        
        foreach ($var_ids as $value ) {
            $value = (int) $value;

            // Skip if value is 0
            if ( $value == 0 )
                continue;
            
            // Find the variation and set the new values
            $variation = wc_get_product( $value );

            if ( $variation == false ) {
                echo json_encode( array( 'success' => false, 'error' => 'Error: Variation not found with id: ' . $value ) );
                wp_die();
            }

            $variation->delete();
        }

        // Return success
        echo json_encode( array( 'success' => true ) );
        wp_die();
    }
}