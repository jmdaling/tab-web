<?php
/** 
 * @package tabticketbroker
 */

namespace Inc\Base;

use DateTime;
use WP_Error;

class BaseController 
{
    public $plugin_url;
    
    public $plugin_path;

    public $plugin_name;

    public $int_formatter;

    public $plugin_name_short;
    
    public $tab_default_lang_code;
    
    public $tab_supplier_cpt_title;

    public $tab_reservation_cpt_title;

    public $tab_reservation_status_taxonomy;
    
    public $page_names = array();
    
    public function __construct()
    {
        // Define constants
        $this->plugin_url = plugin_dir_url( dirname( __FILE__, 2 ) );
        
        $this->plugin_path = plugin_dir_path( dirname( __FILE__, 2 ) );   

        $this->plugin_name = plugin_basename( dirname( __FILE__, 3 ) . '/tabticketbroker.php' );   

        $this->tab_default_lang_code = 'de_DE_formal';
    
        // Name used to distinguish between the two sites. E.g. Prefixed to custom  email titles defined in woocommerce.
        $this->plugin_name_short = get_option( 'sub_company', 'Ofest' );
        // $this->plugin_name_short = ( $_SERVER['SERVER_NAME'] == 'oktoberfest-tischreservierungen.de' ? 'Ofest' : 'Tab' );
        
        // CPT Names
        $this->tab_supplier_cpt_title       = 'tab_supplier';
        
        $this->tab_reservation_cpt_title    = 'tab_reservation';

        $this->tab_reservation_status_taxonomy = 'tab_res_tax_status';

        $this->page_names =  array(
                'MainDashboard'             => 'tab_admin_plugin',
                'Assignments'               => 'tab_admin_assignment_mgmt',
                'Reports'                   => 'tab_admin_reports',
                'Suppliers'                 => 'tab_supplier',
                'Reservations'              => 'tab_reservation',
        );
    }

    /**
     * Sets the order prefix for use in TTB plugin ( Default = 'OF' )
     * Another plugin is used to set the order no prefix hence the option `alg_wc_custom_order_numbers_prefix`
     * Plugin: Custom Order Numbers for WooCommerce By Tyche Softwares
     */
    public function getOrderNumberPrefix()
    {
        return get_option( 'alg_wc_custom_order_numbers_prefix' ) == '' ? 'OF' : get_option( 'alg_wc_custom_order_numbers_prefix' );
    }

    /**
     * Converts a date from format YYYY-MM-DD to a display format stored in the option `ttb_display_date_format`
     * @param string $date_str Date string in format YYYY-MM-DD
     * @param string $format   Format to convert to. Default = 'D d-m'
     * @return string          Date string in format $format
     */
    public function getDisplayDate( string $date_str, $format = '' )
    {
        if ( $format == '' ) {
            $format = get_option( 'ttb_display_date_format' ) ? get_option( 'ttb_display_date_format' ) : 'D d-m';
        }
        
        $d = new DateTime( $date_str );
            
        $formatted_date = $d->format( $format );
        
        return $formatted_date;
    }

    /**
     * Convert a date from format YYYY-MM-DD to a display date (format = "ddd dd-mm") with day of week abbreviation & 
     * day-month no's (e.g. 2023-09-20 = Mi 20-09 in de_DE and Wed 20-09 in en_GB) 
     * in the language of the locale. 
     * For pattern syntax: https://unicode-org.github.io/icu/userguide/format_parse/datetime/#datetime-format-syntax
     * @param string $date_str Date string in format YYYY-MM-DD
     * @param string $locale   Locale to use for the date. Default = 'de_DE'
     */
    public function getDisplayDateAdvanced( ?string $date_str = '', $locale = '', ?string $format = null ) 
    {
        // If no date is passed, return empty string
        if ( $date_str == '' || $date_str == null ) {
            return '';
        }

        // If no format is passed, use the default
        if ( ! isset( $format ) || $format == '' || $format == null ) {
            $format = get_option( 'ttb_display_date_format' ) ? get_option( 'ttb_display_date_format' ) : 'E dd.MM.YY';
        }

        // If no locale is passed, use the default
        if ( $locale == '' ) {
            // get current language setting
            $locale = get_locale();

            // If the locale is not supported, use the default
            if ( !in_array( $locale, array( 'de_DE_formal', 'en_GB' ) ) ) {
                $locale = $this->tab_default_lang_code;
            }
        }

        // check if International Formatter is set
        if ( ! isset( $this->int_formatter ) ) {
            // Create a new IntlDateFormatter object
            $this->int_formatter = new \IntlDateFormatter( $locale, 0, 0, null, null, $format );
        }

        // There is no validation being done on product attributes, so we need to cater for invalid dates
        try {
            // Set the date object from the string
            $date = new \DateTime( $date_str );
            
            // Format it using the IntlDateFormatter
            $display_date = $this->int_formatter->format( $date ) ? $this->int_formatter->format( $date ) : $date_str; // Fallback to original value if formatting fails

        } catch (\Throwable $th) {
            // Catch the error and ensure the user is notified
            $error_msg = sprintf( __( '[TabTicketBroker Plugin] Date Error: Please check that all the product date attributes are formatted correctly. The error message: %s', 'tabticketbroker' ), $th->getMessage() );
            
            $this->addFlashNotice( $error_msg, "error", true );
            
            // If the date is invalid, just display the value
            $display_date = $date_str;
        }

        return $display_date;
    }

    function phplog($log, $force = false)
    {
        // if (get_config('general', 'debug_level') >= 3 || $force == true) {
            $session_id = (!isset($_SESSION) ? '00000000000000000000000000' : session_id());
            // $date = date('Ymd H:i:s') .' UTC';
            
            date_default_timezone_set( 'Africa/Johannesburg' );
            $date = date('Ymd H:i:s');

            $log = $date . ' | ' . $session_id . ' | ' . $log . PHP_EOL;
            file_put_contents( $this->plugin_path.'/log/tab_ofest_log_' . date("Y.m.d D") . '.log', $log, FILE_APPEND );
        // }
    }

    /**
     * Add a flash notice to {prefix}options table until a full page refresh is done
     *
     * @param string $notice our notice message
     * @param string $type This can be "info", "warning", "error" or "success", "warning" as default
     * @param boolean $dismissible set this to TRUE to add is-dismissible functionality to your notice
     * @return void
     */
    function addFlashNotice( $notice = "", $type = "warning", $dismissible = true ) {
        // Here we return the notices saved on our option, if there are not notices, then an empty array is returned
        $notices = get_option( "my_flash_notices", array() );
    
        $dismissible_text = ( $dismissible ) ? "is-dismissible" : "";
    
        // Check if the notice is already on our array, if it is, then we don't add it again.
        foreach ( $notices as $n ) {
            if ( $n['notice'] == $notice ) {
                return;
            }
        }

        // We add our new notice.
        array_push( $notices, array( 
                "notice" => $notice, 
                "type" => $type, 
                "dismissible" => $dismissible_text
            ) );
    
        // Then we update the option with our notices array
        update_option("my_flash_notices", $notices );
    }
    
    /**
     * Function executed when the 'admin_notices' action is called, here we check if there are notices on
     * our database and display them, after that, we remove the option to prevent notices being displayed forever.
     * @return void
     */
    function displayFlashNotices() 
    {
        $notices = get_option( "my_flash_notices", array() );
        
        // Iterate through our notices to be displayed and print them.
        foreach ( $notices as $notice ) {
            printf('<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
                $notice['type'],
                $notice['dismissible'],
                $notice['notice']
            );
        }
    
        // Now we reset our options to prevent notices being displayed forever.
        if( ! empty( $notices ) ) {
            delete_option( "my_flash_notices", array() );
        }
    }

    public function throwError( string $error_title, string $error_msg, WP_Error $error = null )
    {
        echo $error_title .' - '. $error_msg .' - RawMsg:['. json_encode( $error ).']';
    }

    public function getLanguageLink( string $link ) 
    {
        global $TRP_LANGUAGE;

        return ( $TRP_LANGUAGE == 'en_GB' ? '/en/'. $link : $link );
    }

    public function getLoaderHtml() 
    {
        return '<div id="loader_dim_page"><div class="lds-dual-ring"></div></div>';
    }

    public function getScrollToTopHtml() 
    {
        return '<div id="scroll_to_top"><span class="dashicons dashicons-arrow-up-alt2"></span></div>';
        
    }

    public function consoleLog( $data ){
        echo '<script>';
        echo 'console.log('. json_encode( $data ) .')';
        echo '</script>';
    }
}