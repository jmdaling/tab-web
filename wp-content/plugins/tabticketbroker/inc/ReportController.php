<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Inc\Classes\ListTable;
use Inc\Base\BaseController;
use Inc\Reports\PriceReport;
use Inc\Reports\CriticalOrders;
use Inc\Reports\UnassignedOrders;
use Inc\Reports\EmptyReservations;
use Inc\Reports\OperationsReport;
use Inc\Reports\DashboardReport;
use Inc\Reports\PickupReport;
use Inc\Reports\ProductConfig;

// use Inc\Classes\AssignmentManager;

class ReportController extends BaseController
{
    public $report_title = '';

    public $report_columns = array();

    public $report_columns_hidden = array();

    public $report_columns_sortable = array();

    public $report_data = array();

    public $per_page = 20;

    public $reports = array();
     
    public function __construct()
    {
        $this->setReportList();
    }

    public function setReportList()
    {
        $this->reports = array();

        // if current user is me:
        if (        get_current_user_id() == 1  // jmdaling@gmail.com
                ||  get_current_user_id() == 60 // katkosak
            ) { 
            $this->reports = array_merge( $this->reports, array(
                'dashboard' =>  array(
                    'slug' => 'dashboard',
                    'title' => __( 'Dashboard', 'tabticketbroker' ),
                    'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=dashboard',
                    'active' => false,
                    'enabled' => true,
                ),
            ));
        }

        // Add more reports to array
        $this->reports = array_merge( $this->reports, array(

            'operations' =>  array(
                'slug' => 'operations',
                'title' => __( 'Operations', 'tabticketbroker' ),
                'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=operations',
                'active' => true,
                'enabled' => true,
            ),
            'unassigned_orders' =>  array(
                'slug' => 'unassigned_orders',
                'title' => __( 'Unassigned Orders', 'tabticketbroker' ),
                'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=unassigned_orders',
                'active' => false,
                'enabled' => true,
            ),
            // 'product_stock' =>  array(
            //     'slug' => 'product_stock',
            //     'title' => __( 'Product Stock Report', 'tabticketbroker' ),
            //     'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=product_stock',
            //     'active' => false,
            //     'enabled' => true,
            // ),
            // 'empty_reservations' =>  array(
            //     'slug' => 'empty_reservations',
            //     'title' => __( 'Empty Reservations', 'tabticketbroker' ),
            //     'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=empty_reservations',
            //     'active' => false,
            //     'enabled' => false,
            // ),
            // 'critical_orders' =>  array(
            //     'slug' => 'critical_orders',
            //     'title' => __( 'Critical Orders', 'tabticketbroker' ),
            //     'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=critical_orders',
            //     'active' => false,
            //     'enabled' => false,
            // ),
            'price' =>  array(
                'slug' => 'price',
                'title' => __( 'Price', 'tabticketbroker' ),
                'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=price',
                'active' => false,
                'enabled' => true,
            ),
            'pickup' =>  array(
                'slug' => 'pickup',
                'title' => __( 'Pickup', 'tabticketbroker' ),
                'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=pickup',
                'active' => false,
                'enabled' => true,
            ),
            'product_config' =>  array(
                'slug' => 'product_config',
                'title' => __( 'Product Config', 'tabticketbroker' ),
                'link'  => '/wp-admin/admin.php?page=tab_admin_reports&report_slug=product_config',
                'active' => false,
                'enabled' => true,
            ),
        ));
    }

    public function reportsHome()
    {
        
        // If no report slug is set, render default report
        if ( ! isset( $_GET['report_slug'] ) ) {
            
            $unassigned_orders = new UnassignedOrders();

            $unassigned_orders->showReport();
        }
        // Report slug is set, render correct report
        else {
            switch ( $_GET['report_slug'] ) {
                case 'dashboard':
                    $dashboard = new DashboardReport();
                    $dashboard->showReport();
                    break;

                case 'operations':
                    $operations = new OperationsReport();
                    $operations->showReport();
                    break;
                    
                case 'unassigned_orders':
                    $unassigned_orders = new UnassignedOrders();
                    $unassigned_orders->showReport();
                    break;

                case 'empty_reservations':
                    $empty_reservations = new EmptyReservations();
                    $empty_reservations->showReport();
                    break;
                    
                case 'critical_orders':
                    $critical_orders = new CriticalOrders();
                    $critical_orders->showReport();
                    break;
                
                case 'price':
                    $price = new PriceReport();
                    $price->showReport();
                    break;
                    
                case 'pickup':
                    $pickup = new PickupReport();
                    $pickup->showReport();
                    break;
                
                case 'product_config':
                    $product_config = new ProductConfig();
                    $product_config->showReport();
                    break;

                default:
                    $unassigned_orders = new UnassignedOrders();
                    $unassigned_orders->showReport();
                    break;
            }
        }
    }

    /**
     * Prints the report header
     * @param string report_slug_active for the active report
     * @return void
     * 
     */
    public function printReportHeaderHtml( string $report_slug_active = 'unassigned_orders' )
    {
        // Set report list if not set
        if ( ! $this->reports ) $this->setReportList();

        $report_loop = $this->reports;
        
        // Change the active report's status and link
        $report_loop[$report_slug_active]['active'] = true;
        $report_loop[$report_slug_active]['link'] = '#';

        ?>
            <div class="ttb-header">
                <h1><?php echo __( 'Reports', 'tabticketbroker' ); ?></h1>
                <?php //echo $notification; ?>
                <ul class="nav nav-tabs">
                    
                    <?php foreach ( $report_loop as $report_slug => $report_meta ) {
                    ?>
                        <li class="nav-item" <?php echo ( $report_meta['enabled'] == false ? ' data-toggle="tooltip" data-placement="bottom" title="'. __( 'To be developed', 'tabticketbroker' ) .'"' : '' );?>>
                            <a  class="nav-link  <?php echo ( $report_slug == $report_slug_active ? ' active' : '' );?>
                                                <?php echo ( $report_meta['enabled'] == false ? ' disabled' : '' );?>" 
                                href="<?php echo $report_meta['link']; ?>">
                                <?php echo $report_meta['title']; ?>
                            </a>
                         </li>
                    <?php } ?>

                </ul>
            </div>
        <?php 
    }

    /**
     * Display the list table page
     *
     * @return Void
     */
    public function renderReport()
    {
        $list_table = new ListTable;

        $list_table->setupTable( 
            $this->report_columns, 
            $this->report_columns_hidden, 
            $this->report_columns_sortable, 
            $this->report_data, 
            $this->per_page
        );

        $list_table->prepare_items();

        ?>
            <div class="ttb-list-table <?php echo $this->report_title;?>">
                <!-- <div id="icon-users" class="icon32"></div> -->
                <?php $list_table->display(); ?>
            </div>
        <?php
    }
}