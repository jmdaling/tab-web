<?php
/**
 * @package tabticketbroker
 */

namespace Inc\Reports;

use Inc\ReportController;
use Inc\Classes\Dimension;
use Inc\Base\BaseController;

class ProductConfig extends ReportController
{
    public $variations;

    public $report_title;

    public $report_columns;

    public $report_columns_hidden;

    public $report_columns_sortable;

    public $report_data;

    public $per_page;

    public $base_controller;

    // public $gen2_filters;

    public function showReport()
    {
        $this->base_controller = new BaseController();
        
        $this->report_title = 'product_config';

        $this->report_columns = array(
            'id'                    => __( 'ID', 'tabticketbroker' ),
            'sku_generated'         => __( 'Generated SKU', 'tabticketbroker' ),
            'sku_manual'            => __( 'Manual SKU', 'tabticketbroker' ),
            'post_title'            => __( 'Tent Name', 'tabticketbroker' ),
            'prod_title_code'       => __( 'Tent', 'tabticketbroker' ),
            'pa_date'               => __( 'Date', 'tabticketbroker' ),
            'pa_timeslot'           => __( 'Timeslot', 'tabticketbroker' ),
            'pa_pax'                => __( 'Pax', 'tabticketbroker' ),
            'pa_area'               => __( 'Area', 'tabticketbroker' ),
            'price'                 => __( 'Price', 'tabticketbroker' ),
            'sale_price'            => __( 'Sale Price', 'tabticketbroker' ),
            'stock'                 => __( 'Stock', 'tabticketbroker' ),
            'enabled'               => __( 'Enabled', 'tabticketbroker' ),
            'action'                => __( 'Action', 'tabticketbroker' ),
        );

        $this->report_columns_hidden = array();

        $this->report_columns_sortable = array( 
                'id'                    => array( 'id', 'asc' ),
                'sku_generated'         => array( 'sku', 'asc' ),
                'sku_manual'            => array( 'sku', 'asc' ),
                'post_title'            => array( 'post_title', 'asc' ),
                'prod_title_code'       => array( 'prod_title_code', 'asc' ),
                'pa_date'               => array( 'pa_date', 'asc' ),
                'pa_timeslot'           => array( 'pa_timeslot', 'asc' ),
                'pa_area'               => array( 'pa_area', 'asc' ),
                'pa_pax'                => array( 'pa_pax', 'asc' ),
                'price'                 => array( 'price', 'asc' ),
                'sale_price'            => array( 'sale_price', 'asc' ),
                'stock'                 => array( 'stock', 'asc' ),
                'enabled'               => array( 'is_active', 'asc' ),
            );

        $this->getVariationIssues();

        $this->report_data = $this->variations;

        $this->per_page = 3000;
        
        require_once( $this->base_controller->plugin_path ."/templates/reports/product_config.php" );

        $this->printJs();

        $this->printCSS();
    }
        
        
    /**
     * Get all product variations with issues
     * We identify issues by checking if the sku is valid
     * This function is different to the price report as it does not use the view
     * The view ignores variations where attributes are not set ( with the INNER JOIN )
     */
    public function getVariationIssues()
    {
        // Find all products
        $args = array(
            'post_type'         => 'product',
            'posts_per_page'    => -1,
        );

        $products = get_posts( $args );
        
        $dimension = new Dimension();

        // Loop through products & get variations
        $skus = array();
        $all_variations = array();
        foreach ( $products as $product ) {
            $product_id = $product->ID;

            // Get the product
            $product = wc_get_product( $product_id );

            $variations     = wc_get_products(
                array(
                    'status'  => array( 'private', 'publish' ),
                    'type'    => 'variation',
                    'parent'  => $product_id,
                    'limit'   => -1,
                    'orderby' => array(
                        'menu_order' => 'ASC',
                        'ID'         => 'DESC',
                    ),
                    'return'  => 'objects',
                )
            );
    
            // Loop through variations
            foreach ( $variations as $variation ) {
                // $variation = wc_get_product( $variation_id );
                $variation_id = $variation->get_id();
                $title = $variation->get_title();
                $attr = $variation->get_attributes();

                // Get date attribute slug
                $date = $attr['pa_date'];
                $time_slot = $attr['pa_timeslot'];
                $pax = $attr['pa_pax'];
                $area = $attr['pa_area'];

                $my_variation = array(
                    'id'                => $variation_id,
                    'post_title'        => $title,
                    'pa_date'           => $date,
                    'pa_timeslot'       => $time_slot,
                    'pa_pax'            => $pax,
                    'pa_area'           => $area,
                );

                $sku = $dimension->generateSkuFromVariation( $my_variation );
                $all_variations[$variation_id] = array(
                    'id'                => $variation_id,
                    'sku_generated'     => $sku,
                    'sku_manual'        => $variation->get_sku(),
                    'post_title'        => $title,
                    'prod_title_code'   => $dimension->getProductCode( $title ),
                    'pa_date'           => $date,
                    'pa_timeslot'       => $time_slot,
                    'pa_pax'            => $pax,
                    'pa_area'           => $area,
                    'price'             => $variation->get_regular_price(),
                    'sale_price'        => $variation->get_sale_price(),
                    'stock'             => $variation->get_stock_quantity(),
                    'enabled'           => $variation->get_status() == 'publish' ? 1 : 0,
                    'action'            => '<input type="button" value="Delete" class="button button-primary" onclick="deleteVariation('.$variation_id.')">',
                );
            }
        }
        
        // Loop through all_variations and print the id's of the ones with sku="sku error"
        foreach ( $all_variations as $id => $variation ) {
            // unset if sku does not start with 'sku error:'
            if ( substr( $variation['sku_generated'], 0, 10 ) != 'sku error:' ) {
                unset( $all_variations[$id] );
            }
        }

        // sort array by sku
        usort($all_variations, function($a, $b) {
            return $a['sku_generated'] <=> $b['sku'];
        });

        // Finally, set the variations for the report
        $this->variations = $all_variations;
    }


    
    public function printJS(){
        ob_start();
        ?>
        <script>

            // After DOM loaded
            jQuery(document).ready(function() {
                // Get the first table from the report_table_wrap div
                var table = jQuery('#report_table_wrap').find('table').first();

                // Loop through the table and add the id from the first column to the row id as variation_#
                table.find('tr').each(function() {
                    jQuery(this).find('td').first().children('button').remove();

                    // Get the variation id
                    var variation_id = jQuery(this).find('td').first().text();

                    // Add the id to the row
                    jQuery(this).attr('id', 'variation_'+variation_id);
                    
                });
            });

            function deleteAllVariations() {
                // Get the first table from the report_table_wrap div
                var table = jQuery('#report_table_wrap').find('table').first();

                // Get all the IDs from the table
                var ids = [];
                table.find('tr').each(function() {
                    // skip the first row
                    if ( jQuery(this).find('td').first().text() == 'ID' ) return true;

                    // Get the variation id
                    var variation_id = jQuery(this).find('td').first().text();
                    ids.push( variation_id );
                });

                // Add the ids to a comma separated string
                var ids_string = ids.join(',');
                
                console.log( "deleting all variations in report" );
                
                // Show the preloader
                jQuery('#loader_dim_page').show();
                
                var data = {
                    'action': 'delete_variation_jmd',
                    'variation_id': ids_string,
                    'delete_type': 'all'
                };

                jQuery.post(ajaxurl, data, function(response) {
                    console.log('Got this from the server: ' + response);

                    // response array = {"success":true}
                    var response = JSON.parse(response);
                    if ( response.success ) {
                        
                        console.log( "success" );

                        // Hide the preloader
                        jQuery('#loader_dim_page').hide();

                        // Show success modal
                        jQuery('#success_modal').show();
                        setTimeout(function(){ jQuery('#success_modal').hide(); }, 1500);

                        // Move all rows to deleted table
                        var table_del = jQuery('#deleted_variations');

                        // Find deleted_vars_wrap
                        var deleted_div = jQuery('#deleted_vars_wrap');

                        // if deleted_vars_wrap is hidden
                        if ( jQuery('#deleted_vars_wrap').css('display') == 'none' ) {

                            // Show deleted_vars_wrap
                            jQuery('#deleted_vars_wrap').show();

                            // Add the classes from table to deleted table
                            table_del.addClass( jQuery('#report_table_wrap').find('table').first().attr('class') );

                            // Get the headers from the report table
                            var headers = jQuery('#report_table_wrap').find('table').first().find('tr').first().clone();

                            // Add the headers to the table with jQuery
                            table_del.append(headers);

                            // Strip all tags from the headers
                            table_del.find('tr').first().find('th').each(function() {
                                // Remove the text "Sort acending." from the headers
                                jQuery(this).html( jQuery(this).text().replace('Sort ascending.', '') );
                            });

                            // Add a tbody to the table
                            jQuery('#deleted_variations').append('<tbody></tbody>');
                        }

                        // Move all rows to deleted table
                        table.find('tr').each(function() {
                            // Get the variation id
                            var variation_id = jQuery(this).find('td').first().text();

                            // Get the row from the table
                            var row = jQuery('#report_table_wrap').find('table').first().find('#variation_'+variation_id).clone();

                            // Now append the row
                            table_del.append(row);

                            // Remove the actions column
                            row.find('td').last().remove();

                            // Remove the row from the table
                            jQuery('#report_table_wrap').find('table').first().find('#variation_'+variation_id).remove();
                        });
                    }
                    else if ( response.error ) {
                        // Hide the preloader
                        jQuery('#loader_dim_page').hide();

                        // Alert the error
                        alert( response.error );
                    
                        console.log( "error" );
                        console.log( response );
                    }
                });
            }

            function deleteVariation( variation_id ) {
                console.log( "deleting: "+variation_id );

                // Hide the delete all btn delete_all_vars
                jQuery('#delete_all_vars').hide();
                
                // Show the preloader
                jQuery('#loader_dim_page').show();
                
                var data = {
                    'action': 'delete_variation_jmd',
                    'variation_id': variation_id,
                    'delete_type': 'single'
                };

                console.log("DELETE VARIATION: ", data, ajaxurl);

                jQuery.post(ajaxurl, data, function(response) {
                    console.log('Got this from the server: ' + response);

                    // response array = {"success":true}
                    var response = JSON.parse(response);
                    if ( response.success ) {
                        
                        console.log( "success" );

                        // Hide the preloader
                        jQuery('#loader_dim_page').hide();

                        // Show success modal
                        jQuery('#success_modal').show();
                        setTimeout(function(){ jQuery('#success_modal').hide(); }, 1500);

                        // Get the row from the table
                        var row = jQuery('#report_table_wrap').find('table').first().find('#variation_'+variation_id).clone();

                        // find the table and append the row
                        var table_del = jQuery('#deleted_variations');

                        // Find deleted_vars_wrap
                        var deleted_div = jQuery('#deleted_vars_wrap');

                        // if deleted_vars_wrap is hidden
                        if ( jQuery('#deleted_vars_wrap').css('display') == 'none' ) {

                            // Show deleted_vars_wrap
                            jQuery('#deleted_vars_wrap').show();

                            // Add the classes from table to deleted table
                            table_del.addClass( jQuery('#report_table_wrap').find('table').first().attr('class') );

                            // Get the headers from the report table
                            var headers = jQuery('#report_table_wrap').find('table').first().find('tr').first().clone();

                            // Add the headers to the table with jQuery
                            table_del.append(headers);

                            // Strip all tags from the headers
                            table_del.find('tr').first().find('th').each(function() {
                                // Remove the text "Sort acending." from the headers
                                jQuery(this).html( jQuery(this).text().replace('Sort ascending.', '') );
                            });
  

                            // Add a tbody to the table
                            jQuery('#deleted_variations').append('<tbody></tbody>');

                        }
                        
                        // Now append the row
                        table_del.append(row);

                        // Remove the actions column
                        row.find('td').last().remove();
                        
                        // get the table and remove the row
                        jQuery('#report_table_wrap').find('table').first().find('#variation_'+variation_id).remove();

                    }
                });
            }
        </script>
        <?php
        $js = ob_get_clean();
        echo $js;
    }

    public function printCSS(){
        ob_start();
        ?>
        <style>
            #success_modal {
                display: none;
                position: fixed;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: white;
                padding: 20px;
                border: 1px solid black;
                z-index: 1000;
            }
        </style>
        <?php
        $css = ob_get_clean();
        echo $css;
    }
}