<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Reports;

use Inc\ReportController;
use Inc\Classes\Dimension;
use Inc\Classes\Operations;
use Inc\Base\BaseController;
use Inc\Classes\GenericFilters_v2;

class PriceReport extends ReportController
{
    public $variations = array();

    public $report_title = '';

    public $report_columns = array();

    public $report_columns_hidden = array();

    public $report_columns_sortable = array();

    public $report_data = array();

    public $per_page = 20;

    public $base_controller;

    public $gen2_filter;

    public function showReport()
    {
        $this->base_controller = new BaseController();
        
        $this->report_title = 'price';

        $this->report_columns = array(
            'cb'            => '<input type="checkbox" />',
            'id'            => __( 'ID', 'tabticketbroker' ),
            'sku'           => __( 'SKU', 'tabticketbroker' ),
            'post_title'    => __( 'Tent', 'tabticketbroker' ),
            'pa_date'       => __( 'Date', 'tabticketbroker' ),
            'pa_timeslot'   => __( 'Timeslot', 'tabticketbroker' ),
            'pa_pax'        => __( 'Pax', 'tabticketbroker' ),
            'pa_area'       => __( 'Area', 'tabticketbroker' ),
            'price'         => __( 'Price', 'tabticketbroker' ),
            'sale_price'    => __( 'Sale Price', 'tabticketbroker' ),
            'stock'         => __( 'Stock', 'tabticketbroker' ),
            'enabled'       => __( 'Enabled', 'tabticketbroker' ),
            'action'        => __( 'Action', 'tabticketbroker' ),
        );

        $this->report_columns_hidden = array();

        $this->report_columns_sortable = array(
                'id'                    => array( 'id', 'asc' ),
                'sku'                   => array( 'sku', 'asc' ),
                'post_title'            => array( 'post_title', 'asc' ),
                'pa_date'               => array( 'pa_date', 'asc' ),
                'pa_timeslot'           => array( 'pa_timeslot', 'asc' ),
                'pa_area'               => array( 'pa_area', 'asc' ),
                'pa_pax'                => array( 'pa_pax', 'asc' ),
                'price'                 => array( 'price', 'asc' ),
                'sale_price'            => array( 'sale_price', 'asc' ),
                'stock'                 => array( 'stock', 'asc' ),
                'enabled'               => array( 'is_active', 'asc' ),
            );

        $this->setVariations();

        $this->report_data = $this->variations;

        // echo '<pre>';
        // print_r($this->report_data);
        // echo '</pre>';
        // exit;

        $this->per_page = 3000;

        $this->gen2_filter = new GenericFilters_v2();
        
        require_once( $this->base_controller->plugin_path ."/templates/reports/price_report.php" );

        $this->printJs();
    }

    public function setVariations()
    {        
        // Get all variations
        $operations = new Operations();

        $this->variations = $operations->getAllProdVariations();

        // Replace all null values with '--'
        foreach ( $this->variations as $key => $variation ) {

            // Get the variation prod
            $variation_obj = wc_get_product( $variation['id'] );

            // get the sales price from the variation object
            $regular_price = $variation_obj->get_regular_price();
            $regular_price = $regular_price == '' ? null : $regular_price;
            $this->variations[$key]['price'] = $regular_price != null ? $regular_price : '--';
            
            // Add the sale price to the variation array
            $sale_price = $variation_obj->get_sale_price();
            $sale_price = $sale_price == '' ? null : $sale_price;
            $this->variations[$key]['sale_price'] = $sale_price != null ? $sale_price : '--';

            foreach ( $variation as $key2 => $value ) {
                if ( $value == null ) {
                    $this->variations[$key][$key2] = '--';
                }
                // rename is_active to enabled
                if ( $key2 == 'is_active' ) {
                    $this->variations[$key]['enabled'] = $value;
                    unset( $this->variations[$key][$key2] );
                }

                // Convert stock to int
                if ( $key2 == 'stock' ) {
                    $this->variations[$key]['stock'] = intval( $value );
                }

                // Capatilize the first letter of the area and the timeslot
                if ( $key2 == 'pa_area' || $key2 == 'pa_timeslot' ) {
                    $this->variations[$key][$key2] = ucfirst( $value );
                }                
            }

            // Change the post_title in the array to link to the product
            $this->variations[$key]['post_title'] = '<a href="'.get_edit_post_link( $variation['post_parent'] ).'">'.$variation['post_title'].'</a>';

            // Add an action field with a button to update the price
            $this->variations[$key]['action'] = '<button class="button" onclick="displayVariationUpdateForm(this,'.$variation['id'].')">Update</button>';
            // $this->variations[$key]['action'] = '<a href="#" onclick="displayVariationUpdateForm('.$variation['id'].')">'.$variation['sku'].'</a>';

            // Add a checkbox to the cb field
            $this->variations[$key]['cb'] = 1;
        }

        // Set the array keys to the id
        $this->variations = array_combine( array_column( $this->variations, 'id' ), $this->variations );
    }

    
    public function printJS(){
        ob_start();
        ?>
        <script>
            var $ = jQuery;
            var variation_el = null;

            // Save all variations in an array with the id as key
            var variations = <?php echo json_encode( $this->variations ); ?>;

            // Create a function to display the variation update form
            function displayVariationUpdateForm( el, variation_id ) {

                // Show the update modal (id = price_update_table)
                jQuery('#price_update_wrap').show();

                // Fill with the input fields with the variation data
                jQuery('#price_update_table #variation_id').val( variation_id );
                jQuery('#price_update_table #sku').val( variations[variation_id].sku );
                jQuery('#price_update_table #price').val( variations[variation_id].price );
                jQuery('#price_update_table #sale_price').val( variations[variation_id].sale_price );
                jQuery('#price_update_table #stock').val( variations[variation_id].stock );
                
                // set the checkbox to checked if enabled
                if ( variations[variation_id].enabled == 1 ) {
                    jQuery('#price_update_table #enabled').prop('checked', true);
                } else {
                    jQuery('#price_update_table #enabled').prop('checked', false);
                }
                
                // focus on price
                jQuery('#price_update_table #price').focus();

                // Save the source element
                variation_el = el;
            }
            
            function cancelVariationUpdate(){
                jQuery('#price_update_wrap').hide();
            }

            function updateVariation(el){

                // Show page loader
                jQuery('#loader_dim_page').show();
                // Hide the update modal
                jQuery('#price_update_wrap').hide();

                // Get the variation id
                var variation_id = jQuery('#price_update_table #variation_id').val();
                // Get the price
                var price = jQuery('#price_update_table #price').val();
                // Get the sale price
                var sale_price = jQuery('#price_update_table #sale_price').val();
                // Get the stock
                var stock = jQuery('#price_update_table #stock').val();
                // Get the enabled
                var checked = jQuery('#price_update_table #enabled').prop('checked');
                if ( checked ) {
                    var enabled = 1;
                } else {
                    var enabled = 0;
                }

                // Create the data object
                var data = {
                    'action': 'update_variation_jmd',
                    'variation_id': variation_id,
                    'price': price,
                    'sale_price': sale_price,
                    'stock': stock,
                    'enabled': enabled,
                };

                // console.log(data);

                // Send the data to the server
                jQuery.post(ajaxurl, data, function(response) {
                    // Set the new values in the variations array
                    variations[variation_id].price = price;
                    variations[variation_id].sale_price = sale_price;
                    variations[variation_id].stock = stock;
                    variations[variation_id].enabled = enabled;

                    // Update the price in the table
                    tr = jQuery(variation_el).closest('tr');
                    tr.find('td[data-colname="Price"]').html(price);
                    tr.find('td[data-colname="Sale Price"]').html(sale_price);
                    tr.find('td[data-colname="Stock"]').html(stock);
                    tr.find('td[data-colname="Enabled"]').html(enabled);
                    
                    // Show a success message
                    jQuery('#price_update_success').show();

                    // Hide the page loader
                    jQuery('#loader_dim_page').hide();

                    // Hide the success message after 3 seconds
                    setTimeout(function(){ jQuery('#price_update_success').hide(); }, 1500);
                });
            }

            // CUSTOM BULK ACTION FUNCTION
            // Add listner to the bulk action button
            jQuery(document).ready(function() {
                jQuery('#doaction').on('click', function(){
                    jQuery('#loader_dim_page').show();
                    
                    // Get the selected action
                    var action = jQuery('#bulk-action-selector-top').val();

                    // Get the selected variations
                    var selected = [];
                    
                    // Find all the selected checkboxes in tbody#the-list input[type="checkbox"]
                    jQuery('tbody#the-list input[type="checkbox"]:checked').each(function(){
                        // Add the id of the variation to the selected array
                        selected.push( jQuery(this).val() );
                    });

                    // Create the data object
                    var data = {
                        'action': 'bulk_update_variation_jmd',
                        'bulk_action': action,
                        'variation_ids': selected,
                    };

                    // Send the data to the server
                    jQuery.post(ajaxurl, data, function(response) {
                        // Update the enabled field in the table by looping through the selected variations
                        selected.forEach(function(variation_id){
                            // Set the new values in the variations array
                            variations[variation_id].enabled = action == 'enable' ? 1 : 0;

                            // Update the enabled in the table
                            tr = jQuery('tr#item_'+variation_id);
                            tr.find('td[data-colname="Enabled"]').html(action == 'enable' ? 1 : 0);
                        });

                        // Reset the bulk action selector
                        jQuery('#bulk-action-selector-top').val('-1');

                        // Uncheck all the checkboxes
                        jQuery('tbody#the-list input[type="checkbox"]:checked').prop('checked', false);
                        
                        // Show a success message
                        jQuery('#price_update_success').show();

                        // Hide the page loader
                        jQuery('#loader_dim_page').hide();

                        // Hide the success message after 3 seconds
                        setTimeout(function(){ jQuery('#price_update_success').hide(); }, 1500);
                    });
                });
            });

        </script>
        <?php
        $js = ob_get_clean();
        echo $js;
    }
}