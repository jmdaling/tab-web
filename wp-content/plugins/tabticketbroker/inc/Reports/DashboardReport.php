<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Reports;

use Inc\ReportController;
use Inc\Base\BaseController;
use Inc\Classes\AssignmentManager;

class DashboardReport extends ReportController
{
    public $dimension;
    public $base_controller;
    public $assignment_manager;

    public function __construct()
    {

    }
    
    public function showReport() 
    {
        // Initialize required classes
        $this->base_controller = new BaseController;
        $this->assignment_manager = new AssignmentManager();
        $this->assignment_manager->initialize( TTB_EVENT_YEAR );

        // Run through $this->assignment_manager->all_reservations
        // foreach ( $this->assignment_manager->all_reservations as $reservation ) {
        //     $res_stats = array(
        //         'id'                    => $reservation->getId(),
        //         'code'                  => $reservation->getCode(),
        //         'vouchers_received'     => $reservation->getVouchersReceived(),
        //         'letter_received'       => $reservation->getLetterReceived(),
        //         'letter_ready'          => $reservation->getLetterReady(),
        //         'docs_complete'         => $reservation->getDocsComplete(),
        //         'status'                => $reservation->getStatus(),
        //     );
        // }

        // echo '<pre>';
        // print_r( $res_stats );
        // echo '</pre>';

        // Create following KPIs 
        // count $res_stats['vouchers_received'] == true and $res_stats['letter_received'] == false
        // count $res_stats['letter_received'] == true and $res_stats['letter_ready'] == false
        // count ids where status in ( 'available', 'booked' ) and docs_complete == false
        $kpis = array(
            0 => array(
                'title' => 'Letters to request',
                'count' => 0,
                'link'  => '/wp-admin/admin.php?page=tab_admin_reservations&reservation_status=available&letter_received=0',
            ),
            1 => array(
                'title' => 'Letters to clean',
                'count' => 0,
                'link'  => '/wp-admin/admin.php?page=tab_admin_reservations&reservation_status=available&letter_ready=0',
            ),
            2 => array(
                'title' => 'Nothing received yet',
                'count' => 0,
                'link'  => '/wp-admin/admin.php?page=tab_admin_reservations&reservation_status=available&docs_complete=0',
            ),
        );

        // Fill KPIs
        foreach ( $this->assignment_manager->all_reservations as $reservation ) {
            if ( $reservation->getVouchersReceived() && !$reservation->getLetterReceived() ) {
                $kpis[0]['count']++;
                $kpis[0]['reservations'][] = $reservation;
                $kpis[0]['reservation_links'][] = '<a href="/wp-admin//post.php?post='.$reservation->getId().'&action=edit">'.$reservation->getCode().'</a>';
            }
            if ( $reservation->getLetterReceived() && !$reservation->getLetterReady() ) {
                $kpis[1]['count']++;
                $kpis[1]['reservations'][] = $reservation;
                $kpis[1]['reservation_links'][] = '<a href="/wp-admin//post.php?post='.$reservation->getId().'&action=edit">'.$reservation->getCode().'</a>';
            }
            // not one of the following is set (voucher received, letter received, letter ready)
            if ( in_array( $reservation->getStatus(), array( 'available', 'booked' ) ) && ( !$reservation->getVouchersReceived() || !$reservation->getLetterReceived() || !$reservation->getLetterReady() || !$reservation->getDocsComplete() ) ) {
                $kpis[2]['count']++;
                $kpis[2]['reservations'][] = $reservation;
                $kpis[2]['reservation_links'][] = '<a href="/wp-admin//post.php?post='.$reservation->getId().'&action=edit">'.$reservation->getCode().'</a>';
            }
        }

        // Display KPIs
        echo '<div class="ttb-dashboard-kpis">';
        foreach ( $kpis as $kpi ) {
            echo '<div class="ttb-dashboard-kpi">';
            echo '<div class="ttb-dashboard-kpi-title">' . $kpi['title'] . ': '.$kpi['count'] . '</div>';
            // create collapsable list with 4 columns
            echo '<div class="ttb-dashboard-kpi-list">';
            echo '<div class="ttb-dashboard-kpi-list-column">';
            echo '<ul>';
            foreach ( $kpi['reservation_links'] as $reservation_link ) {
                echo '<li>' . $reservation_link . '</li>';
            } 
            echo '</ul>';
            echo '</div>';
            echo '</div>';
        }
        echo '</div>';

    }
}