<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Reports;

use Inc\ReportController;
use Inc\Classes\Dimension;
use Inc\Base\BaseController;
use Inc\Classes\AssignmentManager;
use Inc\Classes\HardCoded;
use Inc\Classes\GenericFilters_v2;

class UnassignedOrders extends ReportController
{
    public $assignment_manager;

    public $base_controller;

    public $gen2_filter;

    public function __construct()
    {
        // // Add the calling of ajax
        add_action( 'admin_footer', array( $this, 'flagItem' ) );
        
        // // Add the serverside function to execute for the ajax
        add_action( 'wp_ajax_ttb_flag_order_item', array ( $this, 'ttb_flag_order_item' ) );

        // Set base controller
        $this->base_controller = new BaseController;
    }

    /**
     * To enable flagging of order items
     * Adds an ajax script to the unassigned orders report
     * Can be improved by notifying user of the flagg add remove success
     */
    public function flagItem(){ ?>
        <script type="text/javascript" >
            function markItemFlagged(order_id, order_item_id) {

                var data = {
                    'action': 'ttb_flag_order_item',
                    'order_id': order_id,
                    'order_item_id': order_item_id,
                };
                
                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                jQuery.post(ajaxurl, data, function(response) {
                    // alert('Got this from the server: ' + response);
                });
            };
        </script> <?php
    }

    /**
     * To enable flagging of order items
     * Called by ajax, Currently only on the unassigned orders report 
     * Adds or removes order items to the flagged list of an order
     *  */ 
    public function ttb_flag_order_item() {
        // Check if the required parameters were sent through
        if ( ! isset( $_POST['order_item_id'] ) || ! isset( $_POST['order_item_id'] ) ) {
            echo ('ERROR: Order ID or Order Item ID not set! Doing nothing');
            wp_die();
        }

        // Set to vars
        $order_id       = $_POST['order_id'];
        $order_item_id  = $_POST['order_item_id'];

        // Get already flagged order items 
        $flagged_items = ( get_post_meta( $order_id, '_ttb_flagged_items', true ) == '' ? array() : get_post_meta( $order_id, '_ttb_flagged_items', true ) );
        
        // If this meta data is not saved as an array reset it
        if ( ! is_array( $flagged_items ) ) $flagged_items = array();
        
        // Add or remove (the flagged item id) depending on whether or not it exists
        if ( in_array( $order_item_id, $flagged_items ) ) // remove if exists
            $flagged_items = array_diff( $flagged_items, array( $order_item_id ) ); 
        else // else add 
            array_push( $flagged_items, $order_item_id );

        // Udate the order's meta data
        update_post_meta( $order_id, '_ttb_flagged_items', $flagged_items );

        // this is required to terminate immediately and return a proper response
        wp_die(); 
    }
    
    public function showReport()
    {
        $this->assignment_manager = new AssignmentManager();
        
        $this->assignment_manager->initialize();
        
        // After initialize, we reset the reservations with blank args
        // This is because for the assignments we dont want to see offers and these 
        // statuses are hardcoded not to show in the assignment manager
        $this->assignment_manager->setReservations( $args = '' );
        
        $this->report_title = 'unassigned_orders';

        $this->report_columns = array(
            'order_no'                  => __( 'Order Number', 'tabticketbroker' ),
            'order_no_link'             => __( 'Order No', 'tabticketbroker' ),
            'sku'                       => __( 'SKU', 'tabticketbroker' ),
            'total'                     => __( 'Sale Price', 'tabticketbroker' ),
            'total_sort'                => __( 'Sale Sort', 'tabticketbroker' ),
            'pa_date'                   => __( 'DateYMD', 'tabticketbroker' ),
            'display_date'              => __( 'Date', 'tabticketbroker' ),
            'product_title'             => __( 'Product Title', 'tabticketbroker' ),
            'timeslot'                  => __( 'Timeslot', 'tabticketbroker' ),
            'area'                      => __( 'Area', 'tabticketbroker' ),
            'pax'                       => __( 'Pax', 'tabticketbroker' ),
            'customer_fullname'         => __( 'Customer', 'tabticketbroker' ),
            'status_display'            => __( 'Status', 'tabticketbroker' ),
            'invoice_no'                => __( 'Invoice No', 'tabticketbroker' ),
            'flagged'                   => __( 'Flagged', 'tabticketbroker' ),
            'notes'                     => __( 'Notes', 'tabticketbroker' ),
            'alt_res'                   => __( 'Alternative', 'tabticketbroker' ),
        );

        $this->report_columns_hidden = array( 'pa_date', 'order_no', 'total_sort' );

        $this->report_columns_sortable = array( 
                'order_no_link'     => array( 'order_no', 'asc' ),
                'sku'               => array( 'sku', 'asc' ),
                'total'             => array( 'total_sort', 'asc' ),
                'product_title'     => array( 'product_title', 'asc' ),
                'display_date'      => array( 'pa_date', 'asc' ),
                'timeslot'          => array( 'timeslot', 'asc' ),
                'area'              => array( 'area', 'asc' ),
                'pax'               => array( 'pax', 'asc' ),
                'customer_fullname' => array( 'customer_fullname', 'asc' ),
                'status_display'    => array( 'status_display', 'asc' ),
                'invoice_no'        => array( 'invoice_no', 'asc' ),
                'flagged'           => array( 'flagged', 'asc' ),
                'notes'             => array( 'notes', 'asc' ),
            );

        // Get the order ID if it is set. The order ID is set in the URL when the report is called from the order admin page
        $order_id = null;
        if ( isset( $_GET['order_id'] ) ) {
            $order_id = $_GET['order_id'];
        }

        $this->report_data = $this->getUnassignedOrders( $order_id );

        $this->per_page = 1000;
        
        $this->base_controller = new BaseController;
        
        $this->gen2_filter = new GenericFilters_v2();

        require_once( $this->base_controller->plugin_path . "/templates/reports/unassigned_orders.php");
    }

    /**
     * Builds the alternative reservation html for the report field using the find method
     * @return String 
     */
    public function getAlternativeHtml( array $order_item )
    {
        $alt_res = $this->findAltReservations( $order_item );

        $html = '';
        foreach ( $alt_res as $cnt => $reservation ) {
            
            $hover_txt = $reservation->getStatusDisplay() .' | ID = '. $reservation->getId() . ' | '. __( 'Click to open reservation', 'tabticketbroker' );
            
            // Show red text for all statuses except available
            $offer_css = $reservation->getStatus() != 'available' ? ' text-danger' : '';

            // Only show date for tabtickets because for ofest the dates are always the same
            $html_date = TTB_SITE == 'tabtickets' ? $reservation->getResDisplayDate() .'<br>' : '';

            $html .=  
            '<a class="page-link table-cell-link'. $offer_css .'" data-toggle="tooltip" title="'. $hover_txt .'" href="admin.php?page='. $this->base_controller->page_names['Assignments'] .'&reservation_id_direct='. $reservation->getId() .'" target="_blank">'.
                $html_date .
                $reservation->getProductTitle() .'('. $reservation->getPax(). ') '. $reservation->getSupplierName() .
            '</a>';
        }

        return $html;
    }
    

    /**
     * Finding the possible alternative reservations based on basic business rules to give an idea what is 
     * currently available. Business rules are:
     * Ofest: Any product, same date, same timeslot & enough available pax
     * Tabtickets: Same product, any date, any timeslot & enough available pax
     * @return Array
     */
    public function findAltReservations( array $order_item )
    {
        $alt_res = array();

        $included_res_statuses = $this->assignment_manager->reservation_status_filter;
        
        foreach ( $this->assignment_manager->all_reservations as $key => $reservation ) {
        
            // Only look at certain statuses
            if ( ! in_array( $reservation->getStatus(), $included_res_statuses ) ) continue;

            // For ofest we compare the date, timeslot & check there is enough pax available
            if ( TTB_SITE == 'ofest' ) {
    
                if ( 
                            $reservation->getDate()                     == $order_item['pa_date']
                        &&  strtolower( $reservation->getTimeslotForCompare() )   == strtolower( $order_item['pa_timeslot'] )
                        &&  $reservation->getPaxOpen()                  >= $order_item['pax']
                    ) {
                    $alt_res[] = $reservation;
                }
            } 
            // For tabtickets we compare the product and check there is enough pax available (date is ignored)
            elseif ( TTB_SITE == 'tabtickets' ) {
                if ( ! in_array( $reservation->getStatus(), $included_res_statuses ) ) continue;
    
                if ( 
                            $reservation->getProductId()                == $order_item['product_id']
                        &&  $reservation->getPaxOpen()                  >= $order_item['pax']
                    ) {
                    $alt_res[] = $reservation;
                }
            }
        }
        
        return $alt_res;
    }

    
    /**
     * Creates an array with all orders and their specific order items which have not been assigned to a reservation yet.
     * @return array array containing report data
     */
    public function getUnassignedOrders( $filter_order_id = null )
    {
        //Default vars
        $order_no_count = 1;

        $previous_order_no = '';

        $report_items = array();

        // Get display dimensions
        $dimension = new Dimension;

        $product_dates = $dimension->getProductDates();

        $product_areas = $dimension->getProductAreas();

        $product_timeslots = $dimension->getProductTimeslots();

        $i = 0;

        // Get item count for each order to distinguish multiple item orders
        $order_item_cnt = array_count_values( array_column( $this->assignment_manager->order_items, 'order_id' )  );
        
        // Loop through orders & transform for report
        foreach ( $this->assignment_manager->order_items as $order_item_id => $order_item ) {

            // If order_id is set, only return the order_item with the matching order_id
            if ( $filter_order_id != null && $order_item['order_id'] != $filter_order_id ) continue;

            // Only show items not assigned (aka that have no reservation id) AND are filtered
            if ( 
                        $order_item['reservation_id'] != -1                             // hide assigned items
                    ||  ! $order_item['filter_flag']                                    // apply filter 
                    || ( $order_item['refunded'] && $order_item['quantity'] == 0 )      // hide refunded items
                )
                continue;
                
            // Apply the filter of timeslots
            if ( $order_item['pa_timeslot'] != '' &&        // If an order item does not have a timeslot, we ignore this filter
                ( ! in_array( $order_item['pa_timeslot'], $this->assignment_manager->timeslot_filter ) ) ) continue;

            // Set order ID
            $order_id = $order_item['order_id'];

            // Set order item ID
            $report_items[$order_item_id] = $this->assignment_manager->order_items[$order_item_id];

            // SKU
            $report_items[$order_item_id]['sku'] = $order_item['sku'];

            // Set display date
            $report_items[$order_item_id]['display_date'] = $product_dates[$order_item['pa_date']];
            
            // Set timeslot

            $report_items[$order_item_id]['timeslot'] = $order_item['pa_timeslot'] == '' ? __( 'No timeslot', 'tabticketbroker' ) : $product_timeslots[$order_item['pa_timeslot']];
            unset( $report_items[$order_item_id]['pa_timeslot'] );
            
            // Set area
            $report_items[$order_item_id]['area'] = $product_areas[$order_item['pa_area']];
            unset( $report_items[$order_item_id]['pa_area'] );
            
            // Show a item count if there are orders with multiple items
            $order_no_count = ( $previous_order_no == $order_item['order_no'] ? $order_no_count + 1 : 1 );

            // Keep track of order number
            $previous_order_no = $order_item['order_no'];
            
            // Create link to order 
            $report_items[$order_item_id]['order_no_link'] = 
            '<div id="order_id_'.$order_id.'" data-toggle="tooltip" data-placement="top" title="'.__( 'Go to order', 'tabticketbroker' ).'">'.
                '<a class="page-link table-cell-link" href="post.php?post='. $order_id .'&action=edit">'.
                    '#'.$order_item['order_no'] . ( $order_item_cnt[$order_id] == 1 ? '' : '.'. $order_no_count ) .// '(' . $order_id .')'.
                '</a>'.
            '</div>';

            // Add total item cost
            $report_items[$order_item_id]['total_sort'] = $order_item['total'];
            $report_items[$order_item_id]['total'] = '€ '. number_format( $order_item['total'], 0, ',', ' ');
            
            // Highlight On Hold statuses
            if ( $report_items[$order_item_id]['status'] == 'on-hold' ) 
                $report_items[$order_item_id]['status_display'] = '<div class="ttb-order-on-hold">'. $report_items[$order_item_id]['status_display'] .'</div>';
            
            // Invoice No
            $report_items[$order_item_id]['invoice_no'] = ( $order_item['invoice_no'] == '' ? __( 'Not invoiced', 'tabticketbroker' ) : $order_item['invoice_no'] );

            // Add checkbox linked to JS to mark/unmark items
            $report_items[$order_item_id]['flagged'] = '<input onclick="markItemFlagged('. $order_id .','. $order_item_id .')" type="checkbox" id="order_item_id_'. $order_item_id .'" name="order_item_id_'. $order_item_id .'" '. ( $order_item['flagged'] ? 'checked' : '' ) .'>';

            
            // Add notes field
            $report_items[$order_item_id]['notes'] = $this->assignment_manager->orders[$order_id]['notes'];
   
            // Find possible alternative reservations
            $report_items[$order_item_id]['alt_res'] = $this->getAlternativeHtml( $order_item );
        }
        
        return $report_items;
    }

    /**
     * Only gets count of orders not assigned for dashboard KPI
     * DO NOT LOOP
     * @return int $order_count Count of orders not assigned to reservations
     */
    public function getUnassignedOrderCount()
    {
        
        $assignment_manager = new AssignmentManager();
        
        $assignment_manager->initialize();
        
        $order_count = 0;
        
        foreach ( $assignment_manager->order_items as $order_item_id => $order_item ) {
            if ( $order_item['reservation_id'] == -1 ) {
                $order_count ++;
            } 
        }

        return $order_count;
    }

}