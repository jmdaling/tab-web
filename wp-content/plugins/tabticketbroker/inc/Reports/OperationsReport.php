<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Reports;

use Inc\ReportController;
use Inc\Base\BaseController;
use Inc\Classes\Operations;
use Inc\Classes\AssignmentManager;
use Inc\Classes\Dimension;
use Inc\Classes\GenericFilters_v2;

class OperationsReport extends ReportController {
    public $dimension;
    public $base_controller;
    public $assignment_manager;

    public $config = array();

    public $order_items = array();

    public $event_years = array();

    public $event_year = '';

    public $gen2_filter;

    // public $operations_filter = OperationsFilter::class;

    public function __construct() {
        // Set base controller
        $this->base_controller = new BaseController;

        $this->setConfig();

        // Set event years from 2022 to current year
        $this->event_years = range(2022, date('Y'));

        // Get event year from GET if not, set to current year
        $this->event_year = isset($_GET['event_year']) ? $_GET['event_year'] : date('Y');

        // Set the filter
        $this->gen2_filter = new GenericFilters_v2();
    }

    public function setConfig() {
        $this->config = [
            'show_offered'              => false,
            'show_soldout'              => false,
            'show_unavailable'          => true,
            'sort_order'                => 'sku',
            'dynamic_reservation_sku'   => false,
        ];
    }

    public function showReport() {
        // Get assigned orders
        $this->assignment_manager = new AssignmentManager();
        $this->assignment_manager->initialize($this->event_year);

        $this->report_title = 'operations';

        $this->report_columns = array(
            'res_nos'               => __('Reservation Number', 'tabticketbroker'),
            'sku'                   => __('SKU', 'tabticketbroker'),
            'pax_open'              => __('Seats Open', 'tabticketbroker'),
            'pax_assigned'          => __('Seats Sold', 'tabticketbroker'),
            'date'                  => __('Date', 'tabticketbroker'),
            'var_price'             => __('Price', 'tabticketbroker'),
            'var_stock'             => __('Stock', 'tabticketbroker'),
            'op_order_nos'          => __('Order Number', 'tabticketbroker'),
        );

        $this->report_columns_hidden = array('pa_date', 'order_no', 'total_sort');

        $this->report_columns_sortable = array(
            'sku'                       => array('sku', false),
        );

        // ADD FILTERS: WIP JMD_HERE
        // $this->operations_filter = new Filters();
        // $this->operations_filter->renderOperationsFilters();
        // return;

        $this->report_data = $this->getOperationsData();

        $this->per_page = 1000;

        $base_controller = new BaseController;

        require_once("$base_controller->plugin_path/templates/reports/operations_report.php");

        $this->printJs();
    }

    public function getShipmentOrderItems() {
        $args = array(
            //     'post_type'         => 'shop_order',
            //     'post_status'       => array('wc-processing', 'wc-completed'),
            'posts_per_page'    => -1,
            //     'orderby'           => 'date',
            //     'order'             => 'ASC',
            //     'meta_query'        => array(
            //         array(
            //             'key'       => 'assigned',
            //             'value'     => 'yes',
            //             'compare'   => 'NOT EXISTS',
            //         ),
            //     ),
        );
        $shipments = wc_gzd_get_shipments($args);

        // Loop through shipments and get order items
        $shipments_order_items = array();
        foreach ($shipments as $shipment) {
            $shipment_items = $shipment->get_items();
            foreach ($shipment_items as $shipment_item) {
                $order_item_id = $shipment_item->get_order_item_id();

                if (! $order_item_id ) {
                    // add flash notice
                    echo "ERROR: Item not found for shipment: " . $shipment->get_shipment_number() . "<br>";
                    $this->base_controller->addFlashNotice(__('Item not found for shipment: ', 'tabticketbroker') . $shipment->get_shipment_number(), 'error', true);
                    $this->base_controller->displayFlashNotices();
                    continue;
                }

                $shipments_order_items[$order_item_id]['shipment'] = array(
                    'id'               => $shipment_item->get_shipment_id(),
                    'no'               => $shipment_item->get_shipment()->get_shipment_number(),
                    'status'           => $shipment_item->get_shipment()->get_status(),
                    'date_created'     => $shipment->get_date_created()->date('Y-m-d'),
                    'date_sent'        => $shipment->get_date_sent() ? $shipment->get_date_sent()->date('Y-m-d') : '',
                    'tracking_id'      => $shipment->get_tracking_id(),
                    'tracking_url'     => $shipment->get_tracking_url(),
                    'shipment_sku'     => $shipment_item->get_sku(),
                    'shipment_qty'     => $shipment_item->get_quantity(),
                );
            }
        }

        return $shipments_order_items;
    }

    /**
     * Creates an array with all orders and their specific order items which have not been assigned to a reservation yet.
     * @return array array containing report data
     */
    public function getOperationsData() {
        $this->dimension = new Dimension();

        // Get all variations
        $operations = new Operations();
        $variations = $operations->getAllProdVariations();

        // Get all orders from the assignment manager
        // Check if $this->assignment_manager is set, if not, initialize it
        if (!isset($this->assignment_manager)) {
            $this->assignment_manager = new AssignmentManager();
            $this->assignment_manager->initialize($this->event_year);
        }

        $this->order_items = $this->assignment_manager->order_items;

        // Get all shipments
        $shipments_order_items = $this->getShipmentOrderItems();

        // Loop through all order items and add shipment data
        foreach ($this->order_items as $order_item_id => $order_item) {
            if (isset($shipments_order_items[$order_item_id])) {
                $this->order_items[$order_item_id]['shipment_no'] = $shipments_order_items[$order_item_id]['shipment']['no'];
                $this->order_items[$order_item_id]['shipment'] = $shipments_order_items[$order_item_id]['shipment'];
            }
            // Also add blank entries for no shipments, the complex array search / array combine below does not work if the arrays differ
            else {
                $this->order_items[$order_item_id]['shipment_no'] = '';
                $this->order_items[$order_item_id]['shipment'] = array(
                    'id'               => '',
                    'no'               => '',
                    'status'           => '',
                    'date_created'     => '',
                    'date_sent'        => '',
                    'tracking_id'      => '',
                    'tracking_url'     => '',
                    'shipment_sku'     => '',
                    'shipment_qty'     => '',
                );
            }
        }

        // Export to CSV if set
        $export_csv = isset($_GET['export_csv']) ? $_GET['export_csv'] : false;

        if ($export_csv) {
            $this->exportCsv($this->order_items);
            $this->base_controller->addFlashNotice(__('CSV exported, click <b>Download CSV</b> in the top right corner', 'tabticketbroker'), 'success', true);
            $this->base_controller->displayFlashNotices();
        }

        // Loop through all variations and build the html sections
        $report_items = array();
        $res_ids_included = array();
        foreach ($variations as $variation) {
            $is_active = false;
            $tot_pax_open = 0;
            $tot_pax_assigned = 0;

            // if sku is empty, generate it
            if (!$variation['sku']) {
                // continue;
                $variation['sku'] = $this->dimension->generateSkuFromVariation($variation);
            }


            // Use array search to see if there is an order item that is linked to the variation
            // Array search will only return the first match, and we also have to first combine the keys and values in another array
            // Limitations of array search are discussed here: https://stackoverflow.com/questions/8102221/php-multidimensional-array-searching-find-key-by-specific-value
            $order_item_index = array_search($variation['id'], array_filter(array_combine(array_keys($this->order_items), array_column($this->order_items, 'variation_id'))));

            // ORDERS
            // Find all order items that matches the variation id
            $variation_orders = array();
            if ($order_item_index) {
                foreach ($this->order_items as $order_item) {
                    if ($order_item['variation_id'] == intval($variation['id'])) {
                        // add link to order
                        $variation_orders[] = $this->getOrderDetailHtml($order_item);

                        // add to the tot_pax from the pa_pax attribute of the order item
                        $tot_pax_assigned += intval($order_item['pax']);

                        $is_active = true;
                    }
                }
            }

            // RESERVATIONS
            // Find all reservations that matches on the sku
            $variation_reservations = array();

            foreach ($this->assignment_manager->all_reservations as $reservation) {
                // Compare sku to reservation sku, ignore the area by only first 11 characters are compared
                if ($reservation->getSku($this->config['dynamic_reservation_sku']) == $variation['sku']) {

                    // REMOVED we want to see all reservations, otherwise some orders linked have no reservations on the report
                    // When we are changing the sku's of reservations we ignore reservations that are full.
                    // if ( $this->config['dynamic_reservation_sku'] && $reservation->getPaxOpen() == 0 ) continue;

                    // Only apply filters if no order items are assigned to the reservation
                    if (count($reservation->getOrderItemIds()) == 0) {
                        // Hide offered reservations based on config
                        if (!$this->config['show_offered'] && $reservation->getStatus() == 'offer') continue;
                        if (!$this->config['show_soldout'] && $reservation->getStatus() == 'soldout') continue;
                        if (!$this->config['show_unavailable'] && $reservation->getStatus() == 'unavailable') continue;
                    }

                    // add link to reservation
                    $variation_reservations[] = $this->getReservationDetailHtml($reservation);
                    $tot_pax_open += $reservation->getPaxOpen();
                    $is_active = true;
                    $res_ids_included[] = $reservation->getId();
                }
            }

            // Add the item to the report data
            $report_items[$variation['id']] = array(
                'res_nos'               => $variation_reservations ? implode(' ', $variation_reservations) : '',
                'sku'                   => $variation['sku'],
                'date'                  => $variation['pa_date'],
                'op_order_nos'             => $variation_orders ? implode(' ', $variation_orders) : '',
                'var_price'             => '€ ' . number_format(intval($variation['price']), 0, ',', ' '),
                'var_stock'             => number_format($variation['stock'], 0, ',', ' '),
                'is_active'             => $is_active,
                'pax_open'              => $tot_pax_open == 0 ? '' : $tot_pax_open,
                'pax_assigned'          => $tot_pax_assigned == 0 ? '' : $tot_pax_assigned,
            );
        }

        // Find all reservations that are not included in the report
        unset($reservation); // just in case
        foreach ($this->assignment_manager->all_reservations as $reservation) {
            if (!in_array($reservation->getId(), $res_ids_included)) {

                // REMOVED we want to see all reservations, otherwise some orders linked have no reservations on the report
                // When we are changing the sku's of reservations we ignore reservations that are full.
                // if ( $this->config['dynamic_reservation_sku'] && $reservation->getPaxOpen() == 0 ) continue;

                // Only apply filters if no order items are assigned to the reservation
                if (count($reservation->getOrderItemIds()) == 0) {
                    // Hide offered reservations based on config
                    if (!$this->config['show_offered'] && $reservation->getStatus() == 'offer') continue;
                    if (!$this->config['show_soldout'] && $reservation->getStatus() == 'soldout') continue;
                    if (!$this->config['show_unavailable'] && $reservation->getStatus() == 'unavailable') continue;
                }

                // Check if the sku exists in the report
                $sku = $reservation->getSku($this->config['dynamic_reservation_sku']);

                $sku_index = array_search($sku, array_filter(array_combine(array_keys($report_items), array_column($report_items, 'sku'))));

                // If the sku does not exist in the report, add it to the report
                if (!$sku_index) {

                    // get the max id of the report items
                    $new_id = max(array_keys($report_items)) + 1;

                    // Manually add them to the report data
                    $report_items[$new_id] = array(
                        'res_nos'               => $this->getReservationDetailHtml($reservation),
                        'sku'                   => $reservation->getSku($this->config['dynamic_reservation_sku']),
                        'date'                  => $reservation->getDate(),
                        'op_order_nos'             => '',
                        'var_price'             => '',
                        'var_stock'             => '',
                        'is_active'             => true,
                        'pax_open'              => $reservation->getPaxOpen(),
                        'pax_assigned'          => '',
                    );
                } else {
                    // If the sku does exist in the report, add the reservation to the report
                    $report_items[$sku_index]['res_nos'] .= ' ' . $this->getReservationDetailHtml($reservation);

                    // if $report_items[$sku_index]['pax_open'] is empty, set it to 0
                    if (!$report_items[$sku_index]['pax_open']) {
                        $report_items[$sku_index]['pax_open'] = 0;
                    } else {
                        // add the open seats to the existing item
                        $report_items[$sku_index]['pax_open'] += $reservation->getPaxOpen();
                    }
                }
            }
        }

        // Sort the report data
        $report_items = $this->sortReportData($report_items);

        // Return only active report items
        return array_filter($report_items, function ($item) {
            return $item['is_active'];
        });
    }

    /**
     * Sorts the report data based on the config
     * @param  array $report_items report data
     * @return array               sorted report data
     */
    public function sortReportData($report_items) {

        // first sort by date then by sku
        usort($report_items, function ($a, $b) {
            $date_a = strtotime($a['date']);
            $date_b = strtotime($b['date']);
            if ($date_a == $date_b) {
                return strcmp($a['sku'], $b['sku']);
            }
            return $date_a - $date_b;
        });

        return $report_items;
    }


    /**
     * Get order detail html
     * @param  array $order_item order item
     * @return string            order detail html
     */
    public function getOrderDetailHtml($order_item) {
        // Get linked reservation
        if (array_search($order_item['reservation_id'], array_column($this->assignment_manager->reservations, 'id'))) {
            $reservation = $this->assignment_manager->reservations[array_search($order_item['reservation_id'], array_column($this->assignment_manager->reservations, 'id'))];
        } else {
            $reservation = false;
        }

        // $reservation = $this->assignment_manager->reservations[];

        ob_start();
        ?>
        <div id="<?php echo $order_item['order_item_id']; ?>" class="item-detail">
            <mark class="order-status status-<?php echo $order_item['status']; ?><?php echo $order_item['is_pickup'] ? ' pickup' : ''; ?>" onclick="showItemDetail(this)"><a><?php echo $order_item['order_no']; ?></a>
                <?php if ($order_item['shipment']['status'] == 'delivered') { ?>
                    <div class="dashicons dashicons-yes"></div>
                <?php } ?>
            </mark>
            <span>
                <div class="inside-detail-box order">
                    <div class="order-box">
                        <p class="order-status status-<?php echo $order_item['status']; ?>">
                            <a title="Go to order" target="_blank" href="<?php echo get_edit_post_link($order_item['order_id']); ?>" style="text-decoration:underline;">
                                <?php echo $order_item['order_no'];?>
                            </a>
                            <?php echo ' - '. $order_item['pax'] .' '. __( 'pax', 'tabticketbroker' ) .' - '. $order_item['status_display'];?>
                        </p>
                        <?php if ($order_item['notes']) { ?>
                        <p><b><?php _e( 'OrderNotes', 'tabticketbroker' ); ?>:</b> <?php 
                            echo wordwrap($order_item['notes'], 50, "<br />\n");?></p>
                        <?php } ?>
                        <p><b><?php _e( 'Status', 'tabticketbroker');?>: </b><?php echo $order_item['status_display']; ?> - <?php echo $order_item['is_paid'] ? __('Paid', 'tabticketbroker') : __('Not paid', 'tabticketbroker'); ?></p>
                        <p><b><?php _e( 'Customer', 'tabticketbroker');?>: </b><?php echo $order_item['customer_fullname']; ?></p>
                        <p><b><?php _e( 'InvoiceNo', 'tabticketbroker');?>: </b><?php echo $order_item['invoice_no']; ?></p>
                        <p><?php echo !$order_item['is_pickup'] ? '<b>'. __('Shipment No', 'tabticketbroker') . ':</b> ' . $order_item['shipment_no'] . ' [' . strtoupper($order_item['shipment']['status']) . ']' : __('Pickup', 'tabticketbroker'); ?></p>
                        <p><?php echo ($order_item['shipment']['tracking_url'] ? '<a href="' . $order_item['shipment']['tracking_url'] . '" class="left" target="_blank" style="text-decoration:underline;">' . __('Track', 'tabticketbroker') . '&nbsp;&crarr;</a>' : ($order_item['shipment']['tracking_id'] ? $order_item['shipment']['tracking_id'] : '<b>'. __('Ship by', 'tabticketbroker') . ':</b> ' . ($order_item['ship_by'] ? $order_item['ship_by'] : __('Date not set', 'tabticketbroker')))); ?></p>
                        <p><?php
                            // check if there are other order items shipped with this order item based on the shipment no
                            if ($order_item['shipment_no']) {
                                // Get other order items with the same shipment no
                                $other_order_items = array_filter($this->order_items, function ($item) use ($order_item) {
                                    return $item['shipment_no'] == $order_item['shipment_no'] && $item['order_item_id'] != $order_item['order_item_id'];
                                });

                                if ($other_order_items) {
                                    echo '<a class="other-order-items" onclick="showOtherOrderItems(this)">' . __('Other order items', 'tabticketbroker') . '</a>';
                                    echo '<div class="other-order-items-box" style="display:none;">';
                                    foreach ($other_order_items as $other_order_item) {
                                        echo '<p><a class="order-status status-' . $other_order_item['status'] . '" onclick="showItemDetail(this)">' . $other_order_item['order_no'] . '</a></p>';
                                    }
                                    echo '</div>';
                                }
                            }

                            ?></p>
                    </div>
                    <?php
                    if ($reservation) {
                        echo '<hr>';
                        echo $this->getReservationDetailHtml_v2($reservation, $order_item);
                    }
                    ?>
                </div>
            </span>
        </div>

    <?php
        $order_detail_html = ob_get_clean();

        return $order_detail_html;
    }

    /**
     * Get reservation detail html
     * @param  TabReservation $reservation reservation
     * @return string            reservation detail html
     */
    public function getReservationDetailHtml($reservation) {
        // Get all linked order items' shipment status
        $shipment_statuses = array();
        foreach ($reservation->getOrderItemIds() as $linked_order_item_id) {
            $shipment_statuses[$linked_order_item_id] = $this->order_items[$linked_order_item_id]['shipment']['status'];
        }

        // if all shipments are delivered, set the reservation status to delivered
        $reservation_all_delivered = false;
        if (count(array_unique($shipment_statuses)) == 1 && reset($shipment_statuses) == 'delivered') {
            $reservation_all_delivered = true;
        }

        ob_start();
    ?>
        <div id="<?php echo $reservation->getId(); ?>" class="item-detail">
            <a class="reservation-status status-<?php echo $reservation->getStatus(); ?>" onclick="showItemDetail(this)"><?php echo $reservation->getCode(); ?>
                <?php if ( $reservation_all_delivered ) { ?>
                    <div class="dashicons dashicons-yes"></div>
                <?php } ?>
            </a>
            <!-- <span style="display:none;" onclick="hideMe(this)"> -->

            <!-- Check the reservation for getDocsComplete and add text "Docs" with a checkmark if true -->
            <div class="res-docs-ship-icons">
                <?php if ($reservation->getDocsComplete()) { ?>
                    <div class="dashicons dashicons-yes"></div> <?php _e('Docs', 'tabticketbroker'); ?>
                <?php } else { ?>
                    <div class="dashicons dashicons-no"></div> <?php _e('Docs', 'tabticketbroker'); ?>
                <?php } ?>

                <!-- Same for getReadyToShip -->
                <?php if ($reservation->getReadyToShip()) { ?>
                    <div class="dashicons dashicons-yes"></div> <?php _e('Ship', 'tabticketbroker'); ?>
                <?php } else { ?>
                    <div class="dashicons dashicons-no"></div> <?php _e('Ship', 'tabticketbroker'); ?>
                <?php } ?>
            </div>

            <span style="display:none;">
                <div class="inside-detail-box reservation">
                    <?php echo $this->getReservationDetailHtml_v2($reservation); ?>
                </div>
            </span>
        </div>

    <?php
        $reservation_detail_html = ob_get_clean();

        return $reservation_detail_html;
    }

    public function getReservationDetailHtml_v2($reservation, $order_item = array('order_item_id' => -1)) 
    {
        ob_start();
    ?>
        <div class="reservation-box">
            <p>
                <a title="<?php echo __('Go to Reservation', 'tabticketbroker');?>" target="_blank" href="<?php echo get_edit_post_link($reservation->getId()); ?>" style="text-decoration:underline;">
                    <?php echo $reservation->getCode(); ?>
                </a>
                <span class="reservation-status-indicator status-<?php echo $reservation->getStatus(); ?>">&#9679;</span>
                <a title="<?php echo __('Go to Assignments', 'tabticketbroker');?>" target="_blank" class="right" href="admin.php?page=<?php echo $this->base_controller->page_names['Assignments']; ?>&reservation_id_direct=<?php echo $reservation->getId();?>">
                    &nbsp;&crarr;
                </a>
            </p>
            <p><?php echo $reservation->getSku(); ?></p>
            <p><b><?php echo $reservation->getStartTime(); ?></b></p>
            <p><?php echo $reservation->getStatusDisplay(); ?></p>
            <p><?php echo $reservation->getPax(); ?> - <?php echo $reservation->getPaxAssigned(); ?> = <?php echo $reservation->getPaxOpen(); ?></p>
            <p><?php _e('Supplier', 'tabticketbroker'); ?>: <?php echo $reservation->getSupplierName() ? '<a href="' . get_edit_post_link($reservation->getSupplierId()) . '" target="_blank">' . $reservation->getSupplierName() . '</a>' : __('empty', 'tabticketbroker'); ?></p>
            <!-- Add reservation notes -->
            <hr>
            <p><b><?php _e('Notes', 'tabticketbroker'); ?>:</b> <?php echo $reservation->getNotes() ? wordwrap($reservation->getNotes(), 50, "<br />\n") : '_____'; ?></p>
            <hr>
            <p><?php _e('Owner', 'tabticketbroker'); ?>: <?php echo $reservation->getOwnerName() ? $reservation->getOwnerName() : '_____'; ?></p>
            <p><?php _e('TableNo', 'tabticketbroker'); ?>: <?php echo $reservation->getTableNo() ? $reservation->getTableNo() : '_____'; ?></p>
            <p><?php _e('CustomerNo', 'tabticketbroker'); ?>: <?php echo $reservation->getCustomerNo() ? $reservation->getCustomerNo() : '_____'; ?></p>
            <p><?php _e('BookingNo', 'tabticketbroker'); ?>: <?php echo $reservation->getBookingNo() ? $reservation->getBookingNo() : '_____'; ?></p>
            
            <?php if (count($reservation->getOrderItemIds()) > 0) { ?>
            <hr>
            <div class="linked-items">
                <?php
                foreach ($reservation->getOrderItemIds() as $linked_order_item_id) {
                    $this_order_text = $linked_order_item_id == $order_item['order_item_id'] ? '<span>&rarr;</span>' : '';
                    $hover_meta = $linked_order_item_id == $order_item['order_item_id'] ? '' : 'data-order-item-id="' . $linked_order_item_id . '"';
                    $linked_order_item = $this->assignment_manager->order_items[$linked_order_item_id];
                    $shipment_status = $this->order_items[$linked_order_item_id]['shipment']['status'];
                ?>
                    <?php echo $this_order_text; ?>
                    <a <?php echo $hover_meta;?> class="order-status status-<?php echo $linked_order_item['status']; ?>" onclick="jumpToOrder(<?php echo $linked_order_item_id; ?>)"><?php echo $linked_order_item['order_no'] .' - '. $shipment_status; ?></a><br>
                <?php }?>
            </div>
            <?php } ?>

        </div>
    <?php
        $reservation_detail_html = ob_get_clean();

        return $reservation_detail_html;
    }

    public function printJS() {
        ob_start();
    ?>
        <script>
            var skip_hide_me = false;
            var global_z_index = 100;

            function hideMe(el) {
                if (skip_hide_me) {
                    skip_hide_me = false;
                    return;
                }
                el.style.display = 'none';
            }

            function showItemDetail(el) {

                // get the span element from the parent
                var span = el.getElementsByTagName('span')[0];

                // if the span is hidden, show it
                if (span.style.display == 'none') {

                    // Set the z-index of the parent to the global z-index
                    global_z_index++;

                    el.parentNode.style.zIndex = global_z_index;
                    span.style.zIndex = global_z_index;

                    span.style.opacity = 0;
                    span.style.display = 'inline';

                    // fade in the span
                    (function fade() {
                        var val = parseFloat(span.style.opacity);
                        if (!((val += .1) > 1)) {
                            span.style.opacity = val;
                            requestAnimationFrame(fade);
                        }
                    })();
                } else {
                    // hide it
                    span.style.opacity = 1;
                    span.style.zIndex = 0;
                    span.style.display = 'none';

                    // display fade out
                    (function fade() {
                        if ((span.style.opacity -= .1) < 0) {
                            span.style.display = 'none';
                        } else {
                            requestAnimationFrame(fade);
                        }
                    })();
                }
            }

            function jumpToOrder(order_item_id) {
                // find element with order_item_id
                var order_detail = document.getElementById(order_item_id);

                // if order_item_id is not found, show popup box notifing user
                if (order_detail == null) {
                    alert('Item not found on report');
                    skip_hide_me = true;
                    return;
                }

                // scroll to element
                order_detail.scrollIntoView({
                    behavior: "smooth",
                    block: "center",
                    inline: "nearest"
                });

                // show order detail after 1 second
                setTimeout(function() {
                    showItemDetail(order_detail);
                }, 400);
            }

            // Find the child itemsof all divs with linked-items
            var linked_items = document.getElementsByClassName('linked-items');

            // // Loop through linked_items and find each a element
            // for (var j = 9000000; j < linked_items.length; j++) {
            //     var linked_item = linked_items[j];

            //     // get all a elements with an data-order-item-id attribute
            //     var linked_items_a = linked_item.querySelectorAll('a[data-order-item-id]');

            //     // Add a listner for hover to all a tags
            //     for (var i = 0; i < linked_items_a.length; i++) {
            //         linked_items_a[i].addEventListener('mouseover', function(){
            //             // get the order_item_id from the a tag
            //             var order_item_id = this.getAttribute('data-order-item-id');

            //             // find element with order_item_id
            //             var order_detail = document.getElementById(order_item_id);

            //             showItemDetail(order_detail);
            //         });

            //         linked_items_a[i].addEventListener('mouseout', function(){
            //             // get the order_item_id from the a tag
            //             var order_item_id = this.getAttribute('data-order-item-id');

            //             // find element with order_item_id
            //             var order_detail = document.getElementById(order_item_id);

            //             showItemDetail(order_detail);
            //         });
            //     }
            // }



            // // Find first a tag elements as children of divs with class item-detail
            // var item_details = document.getElementsByClassName('item-detail');

            // // Add a listner for hover to all a tags
            // for (var i = 0; i < item_details.length; i++) {

            //     var item_id = item_details[i].id;

            //     var item_detail_a = item_details[i].getElementsByTagName('a')[0];

            //     item_detail_a.addEventListener('mouseover', function(){
            //         var item_id = this.parentNode.id;
            //         var item_element = document.getElementById(item_id);
            //         showItemDetail(item_element);
            //     });

            //     item_detail_a.addEventListener('mouseout', function(){
            //         var item_id = this.parentNode.id;
            //         var item_element = document.getElementById(item_id);
            //         showItemDetail(item_element);
            //     });
            // }


            var item_details = document.getElementsByClassName('item-detail');

            // Loop through item_details and add a listner for hover
            for (var i = 0; i < item_details.length; i++) {
                item_details[i].addEventListener('mouseover', function() {
                    // show the child span element
                    var span = this.getElementsByTagName('span')[0];
                    span.style.display = 'inline';
                    global_z_index++;
                    span.style.zIndex = global_z_index;
                });

                item_details[i].addEventListener('mouseout', function() {
                    // hide the child span element
                    var span = this.getElementsByTagName('span')[0];
                    span.style.display = 'none';
                    span.style.zIndex = 0;
                });
            }
        </script>
<?php
    }


    /**
     * Export CSV function
     */
    public function exportCsv($order_items) {

        $base_controller = new BaseController();

        // check if file is writable
        if (!is_writable($base_controller->plugin_path . '/exports/')) {
            return false;
        }

        $out = fopen($base_controller->plugin_path . 'exports/operations.csv', 'w');

        foreach (array_keys(reset($order_items)) as $key) {
            $header[] = $key;
        }

        fputcsv($out, $header);

        foreach ($order_items as $order_item) {
            $row = array();

            foreach ($order_item as $key => $value) {
                // Ignore the shipment
                if ($key == 'shipment') {
                    continue;
                }
                $row[] = $value;
            }

            fputcsv($out, $row);
        }

        fclose($out);
    }
}
