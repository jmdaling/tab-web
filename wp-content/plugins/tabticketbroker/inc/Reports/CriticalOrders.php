<?php
/**
 * @package tabticketbroker
 */
namespace Inc\Reports;

use Inc\ReportController;
use Inc\Base\BaseController;

class CriticalOrders extends ReportController
{
    public function showReport()
    {
        
        $base_controller = new BaseController;
        
        require_once( "$base_controller->plugin_path/templates/reports/critical_orders.php");

    }

}