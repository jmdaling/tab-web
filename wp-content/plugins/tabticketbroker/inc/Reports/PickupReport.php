<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Reports;

use Inc\ReportController;
use Inc\Base\BaseController;
use Inc\Classes\JMD_Logger;
use Inc\Tools\OrderTools;
use Inc\Tools\ExportTools;

class PickupReport extends ReportController
{
    public $base_controller;
    public $order_tools;
    public $default_parcel_statuses;
    public $parcel_status_flows;
    
    public function __construct()
    {
        $this->base_controller = new BaseController;
        $this->base_controller = new BaseController;
        $this->order_tools = new OrderTools;

        $this->default_parcel_statuses = array(
            'at_head_office'    => 1,
            'shipped'           => 0,
            'at_pickup_office'  => 0, 
            'collected'         => 0,
        );

        // Used for the frontend actions and determine the next status
        $this->parcel_status_flows = array(
            'at_head_office'    => 'shipped',
            'shipped'           => 'at_pickup_office',
            'at_pickup_office'  => 'collected'
        );

    }

    public function setReportData()
    {
        $this->report_title = 'pickup';

        $this->report_columns = array(
            'order_no'                  => __( 'Order No', 'tabticketbroker' ),
            'order_status'              => __( 'Order Status', 'tabticketbroker' ),
            'earliest_reservation'      => __( 'Earliest Reservation', 'tabticketbroker' ),
            'pickup_code'               => __( 'Pickup Code', 'tabticketbroker' ),
            'order_skus'                => __( 'Order SKUs', 'tabticketbroker' ),
            'customer'                  => __( 'Customer', 'tabticketbroker' ),
            'order_notes'               => __( 'Order Notes', 'tabticketbroker' ),
            'parcel_history'            => __( 'Parcel History', 'tabticketbroker' ),
            'parcel_status_slug'        => __( 'Parcel Status Slug', 'tabticketbroker' ),
            'parcel_status_display'     => __( 'Parcel Status', 'tabticketbroker' ),
            'parcel_status'             => __( 'Status Actions', 'tabticketbroker' ),
            // 'parcel_notes'        => __( 'Notes', 'tabticketbroker' ),
            'parcel_shipments'          => __( 'Internal Shipments', 'tabticketbroker' ),
        ); 

        $this->report_columns_hidden = array('parcel_status_slug');

        $this->report_columns_sortable = array(
            'order_no'                  => array( 'order_no', false ),
            'order_status'              => array( 'order_status', false ),
            'earliest_reservation'      => array( 'earliest_reservation', false ),
            'pickup_code'               => array( 'pickup_code', false ),
            'order_skus'                => array( 'order_skus', false ),
            'customer'                  => array( 'customer', false ),
            'parcel_status_display'     => array( 'parcel_status_slug', false ),
            'parcel_shipments'          => array( 'parcel_shipments', false ),
        );

        $this->report_data = $this->getPickupData();

        $this->maybeExportCsv();
    }

    public function maybeExportCsv()
    {
        // Export to CSV if set
        $export_csv = isset($_GET['export_csv']) ? $_GET['export_csv'] : false;
        if ($export_csv) {
            // Remove non-export fields
            $data_to_export = array_map( function( $row ) {
                unset( $row['parcel_status'] );
                return $row;
            }, $this->report_data );

            // Export to CSV
            $export_tools = new ExportTools();
            $filename = 'pickup-report.csv';
            $data_to_export_cleaned = $export_tools->stripHtml( $data_to_export );
            $filepath = $export_tools->exportCsv( $data_to_export_cleaned, $filename );
            $export_tools->downloadSendHeaders( $filepath );
            exit;
        }
    }

    public function showReport() 
    {
        $this->setReportData();

        $this->per_page = 3000;
        
        require_once( $this->base_controller->plugin_path ."/templates/reports/pickup_report.php" );
    }

    public function getPickupData()
    {
        // Get all sold orders and their products
        // use wc_get_orders() to get all orders for this year in _ttb_event_year and _ttb_pickup = 1
        $orders = wc_get_orders( array(
            'type'              => 'shop_order',
            'limit'             => -1,  // -1 for all
            'orderby'           => 'date',
            'order'             => 'ASC',
            'meta_key'          => '_ttb_event_year',
            'meta_value'        => TTB_EVENT_YEAR,
            'meta_compare'      => '=',
            'status'            => array(
                'pending',    
                'processing', 
                'on-hold',    
                'completed',
                'assigned',
                'ready-to-ship',
                'shipped',
                'on-hold',
                'labelled',
                // 'cancelled',  
                // 'refunded',   
                // 'failed',
            ),
        ));

        // Filter out orders that are not pickup
        $orders = array_filter( $orders, function( $order ) {
            return $order->get_meta( '_ttb_pickup' ) == 1;
        } );

        // Loop through orders and get earliest reservation date
        $parcel_data = array();
        foreach ( $orders as $order ) {
            $order_id = $order->ID;
            $order_no = $order->get_order_number();
            $order_status = wc_get_order_status_name( $order->get_status() );
            $order_status_slug = $order->get_status();
            $order_pickup = $order->get_meta( '_ttb_pickup' );

            // Get all the order items
            $order_items = $order->get_items();

            // loop through order items and get the sku of each
            $order_skus = array();
            foreach ( $order_items as $order_item ) {
                $order_skus[] = $order_item->get_product()->get_sku();
            }
            
            $parcel_meta = array();
            $parcel_meta = get_post_meta( $order_id, '_ttb_parcel_meta', true );

            $parcel_shipments = '';
            if ( $order_status_slug == 'shipped' ) {
                $order_shipment  = wc_gzd_get_shipment_order( $order );
                
                $shipments = $order_shipment->get_shipments();
                foreach ( $shipments as $shipment ) {
                    // Get shipment status
                    $shipment_status = $shipment->get_status();
                    $shipment_no = $shipment->get_shipment_number();
                    $tracking_url = $shipment->get_tracking_url();

                    // set link to tracking url with shipment no & status
                    $parcel_shipments .= '<a href="' . $tracking_url . '" target="_blank">' . $shipment_no . ' (' . $shipment_status . ')</a><br>';                    
                }
            }

            if ( ! is_array( $parcel_meta ) || empty( $parcel_meta ) || ! array_key_exists( $order_id, $parcel_meta ) ) {
                $parcel_meta = array();
                $parcel_meta[$order_id] = array(
                    'item_type'             => 'order',
                    'item_id'               => $order_id,
                    'status'                => $this->default_parcel_statuses,
                    'shipping_tracking_no'  => '',
                    'notes'                 => '',
                    );
                
                    // Save parcel meta
                    update_post_meta( $order_id, '_ttb_parcel_meta', $parcel_meta );
            }

            // Get order parcels from parcel meta where item type is order and item id is order id
            // This should only return one parcel
            if ( is_array( $parcel_meta ) )
                $order_parcel = array_shift( 
                    array_filter( $parcel_meta, function( $parcel ) use ( $order_id ) {
                        return $parcel['item_type'] == 'order' && $parcel['item_id'] == $order_id;
                    } )
                );
            else {
                $order_parcel = array();
                // Add flash notice
                $this->base_controller->addFlashNotice( 'error', 'Parcel meta is not an array' );
            }

            $parcel_status_log = JMD_Logger::metalog( $order_id, '_ttb_parcel_status_log', '' );
            $parcel_history = $this->getParcelHistoryHtml( $parcel_status_log, $order_id );
            
            $order_pickup_code = $order->get_meta( '_ttb_pickup_code' );
            $order_notes = str_replace( "\n", "<br>", $order->get_meta( '_ttb_order_notes' ) );
            $earliest_reservation = $this->order_tools->getEarliestItemDate( $order );

            // Loop through statuses in order parcel and find the first one that is 1
            // This will be the current status
            $order_parcel_status_slug = '';
            $order_parcel_status_display = '';
            foreach ( $order_parcel['status'] as $status => $value ) {
                if ( $value == 1 ) {
                    // replace underscores with spaces and capitalize
                    $order_parcel_status_slug = $status;
                    $order_parcel_status_display = ucwords( str_replace( '_', ' ', $status ) );
                    break;
                }
            }

            $parcel_data[] = array(
                'order_id' => $order_id,
                'order_no' => /*link to order */ '<a href="' . admin_url( 'post.php?post=' . $order_id . '&action=edit' ) . '" target="_blank">' . $order_no . '</a>',
                'status_slug' => $order_status_slug,
                'order_status' => '<mark class="order-status status-' . $order_status_slug . '"><span>' . $order_status . '</span></mark>',
                'earliest_reservation' => $earliest_reservation,
                'pickup_flag' => $order_pickup,
                'pickup_code' => $order_pickup_code,
                'order_skus' => implode( '<br>', $order_skus ),
                'order_notes' => $order_notes,
                'customer' => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name() . ' (' . $order->get_billing_email() . ')',
                'parcel_history' => $parcel_history,
                'parcel_status' => $this->getParcelStatusHtml( $order_parcel ),
                'parcel_status_slug' => $order_parcel_status_slug,
                'parcel_status_display' => $order_parcel_status_display,
                'parcel_shipments' => $parcel_shipments,
            );
        }

        // Sort orders by earliest reservation date
        usort( $parcel_data, function( $a, $b ) {
            return strtotime( $a['earliest_reservation'] ) - strtotime( $b['earliest_reservation'] );
        } );

        return $parcel_data;
    }

    /**
     * Get html for parcel history
     * 
     * @param array $parcel_status_log - array of parcel status logs
     * @return string $html - html for parcel history
     * 
     */
    function getParcelHistoryHtml( $parcel_status_log, $order_id )
    {
        // If parcel status log is empty, return empty string
        if ( empty( $parcel_status_log ) )
            return '';
        
        // Create a html hover over div with the parcel history
        $html = '';
        ob_start();
        ?>
        <div class="ttb-parcel-history-container">
            <div class="ttb-parcel-history">
                <h6><?php _e( 'Parcel History', 'tabticketbroker' ); ?></h6>
                <ul data-parcel-order-id="<?php echo $order_id; ?>">
                    <?php foreach ( $parcel_status_log as $log ) : ?>
                        <li>
                            <?php echo $log['date'] . ' - ' . $log['user'] . ' - ' . $log['status']; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <span class="hover-trigger">📦</span>
        </div>
        <?php
        $html = ob_get_clean();
        
        return $html;
    }

    /**
     * Get html for parcel status
     * Parcel statuses are stored in an array and are either 1 or 0
     * 
     * @param int $parcel_parent_id - parent id of parcel, can be sub reservation ID or Order ID. Typically
     *                                any item that can be shipped to the pickup office for collection
     * @param array $parcel_statuses - array of parcel statuses
     * @return string $html - html for parcel status
     * 
     */
    function getParcelStatusHtml( $parcel_meta_single )
    {
        // Get id of parent item
        $parcel_parent_id = $parcel_meta_single['item_id'];

        ob_start();
        ?>
        <?php foreach ( $parcel_meta_single['status'] as $status => $value ) : ?>
            <?php 
                // print clickable status
                $status_name = str_replace( '_', ' ', $status );
                $status_name = ucwords( $status_name );
                if ( $value == 1 )
                    $dashicons = '<span style="text-decoration:none;" class="dashicons dashicons-yes"></span>';
                else
                    $dashicons = '<span style="text-decoration:none;" class="dashicons dashicons-no"></span>';
            ?>
        
        <a href="#" class="ttb-parcel-status status-<?php echo $status;?>" data-status="<?php echo $status; ?>" data-parcel-id="<?php echo $parcel_parent_id; ?>">
            <?php echo $dashicons.$status_name;?>
        </a>
        <br>

        <?php endforeach; ?>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    function renderCollections()
    {
        $this->setReportData();

        $this->manipulateCollectionsData();

        $this->printShopCollectionsReport();
    }

    /**
     * Manipulate data for frontend report
     */
    public function manipulateCollectionsData()
    {

        // Columns to hide
        $hide_fields = array(
            'order_id',
            'order_status',
            // 'order_no',
            'parcel_status_slug',
            'pickup_flag',
            'collected',
            'parcel_history',
        );

        // Remove columns
        foreach ( $hide_fields as $field ) {
            unset( $this->report_columns[$field] );
        }

        // Loop through data and replace parcel_status with collected button
        foreach ( $this->report_data as $key => $row ) {
            // Get the next status from the parcel status flows
            $next_status = $this->parcel_status_flows[$row['parcel_status_slug']];
            $pickup_code = $row['pickup_code'];
                
            if ( $row['parcel_status_slug'] == 'shipped' ) {
                $confirm_msg = __( 'Are you sure you received this package?', 'tabticketbroker' ) .'\n\n'. __( 'Pickup Code', 'tabticketbroker' ) . ':\n' . $pickup_code;
                $this->report_data[$key]['parcel_status'] = '<button class="button btn-secondary ttb-collection-actions-btn" data-confirm-msg="'.$confirm_msg.'" data-status-value="'.$next_status.'" data-order-id="' . $row['order_id'] . '">' . __( 'Received from Courier', 'tabticketbroker' ) .'</button>';
            }
            elseif ( $row['parcel_status_slug'] == 'at_pickup_office' ) {
                $confirm_msg = __( 'Are you sure the customer collected this package?', 'tabticketbroker' ) .'\n\n'. __( 'Pickup Code', 'tabticketbroker' ) . ':\n' . $pickup_code;
                $this->report_data[$key]['parcel_status'] = '<button class="button btn-primary ttb-collection-actions-btn" data-confirm-msg="'.$confirm_msg.'" data-status-value="'.$next_status.'" data-order-id="' . $row['order_id'] . '">' . __( 'Collected by Customer', 'tabticketbroker' ) .'</button>';
            }
            else {
                $this->report_data[$key]['parcel_status'] = '';
            }
        }
        
        // Only show parcels that have $order_parcel_status_slug = at_pickup_office
        $this->report_data = array_filter( $this->report_data, function( $row ) {
            return $row['parcel_status_slug'] == 'at_pickup_office' || $row['parcel_status_slug'] == 'shipped';
        } );

        // Sort by earliest reservation date
        usort( $this->report_data, function( $a, $b ) {
            return strtotime( $a['earliest_reservation'] ) - strtotime( $b['earliest_reservation'] );
        } );

    }

    public function printShopCollectionsReport()
    {
        
        // Print the page loader because we are not using the page template
        echo $this->base_controller->getLoaderHtml();
        
        ?>

        <!-- Use bootstrap table classes to build table -->
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <?php foreach ( $this->report_columns as $column_slug => $column_title ) { ?>
                        <th scope="col"><?php echo $column_title; ?></th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ( $this->report_data as $row ) { ?>
                    <tr>
                        <?php foreach ( $this->report_columns as $column_slug => $column_title ) { ?>
                            <td><?php echo $row[$column_slug]; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        

        <?php
    }
}