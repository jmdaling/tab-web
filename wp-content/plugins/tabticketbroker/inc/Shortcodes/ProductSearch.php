<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Shortcodes;

use Inc\Base\BaseController;
use WPForms\Admin\Tools\Importers\Base;

defined('ABSPATH') || exit;

/**
 * Product search shortcode class.
 */
class ProductSearch extends BaseController
{
    public function __construct()
    {
        add_shortcode('tab_prod_search', array( $this, 'PrintProductSearchForm' ) );
    }

    public function PrintProductSearchForm( $args = array() ) 
    {
        // Set parameters from args        
        $args = ( empty( $args ) ? array(
            'shop_url' => '/tischreservierungen-buchen/',
            ) 
            : $args
        );
        
        // Ensure all terms are returned (wordpress applies filters in the background)
        $options = array('hide_empty' => false);

        // Get the attributes
        $attr_datum = get_terms( 'pa_date', $options );

        $attr_pax = get_terms( 'pa_pax', $options );

        $attr_timeslot = get_terms( 'pa_timeslot', $options );

        // Build the form
        ob_start(); // capture the buffer 
        ?>
        <form id="prod_search_form" method="post" action="<?php echo $args['shop_url']; ?>">
            <div id="prod_search_container">
                <div class="row d-flex justify-content-around">
                    <div class="col-sm">
                        <!-- <label for="tab_reservation_date_field"><?php _e( 'Date', 'tabticketbroker' ); ?></label> -->
                        <select class="front-prod-search-select" name="pa_date[]" id="tab_reservation_date_field">
                            <option value=""><?php _e( 'Date', 'tabticketbroker' ); ?></option>
                            <?php
                            foreach( $attr_datum as $key => $term ) {
                                echo '<option value="'. $term->slug .'">'. $term->name .'</option>';
                            }
                            ?>
                        </select> 
                    </div>
                    
                    <div class="col-sm">
                        <!-- <label for="tab_reservation_pax_field"><?php _e( 'People', 'tabticketbroker' ); ?></label> -->
                        <select class="front-prod-search-select" name="pa_pax[]" id="tab_reservation_date_field">
                            <option value=""><?php _e( 'People', 'tabticketbroker' ); ?></option>
                            <?php
                                foreach( $attr_pax as $key => $term ) {
                                    echo '<option value="'. $term->slug .'">'. $term->name .'</option>';
                                }
                                ?>
                        </select> 
                    </div>

                    <div class="col-sm">
                        <!-- <label for="tab_reservation_date_field"><?php _e( 'Timeslot', 'tabticketbroker' ); ?></label> -->
                        <select class="front-prod-search-select" name="pa_timeslot[]" id="tab_reservation_date_field">
                            <option value=""><?php _e( 'Timeslot', 'tabticketbroker' ); ?></option>
                            <?php
                                foreach( $attr_timeslot as $key => $term ) {
                                    echo '<option value="'. $term->slug .'">'. $term->name .'</option>';
                                }
                                ?>
                        </select>
                    </div>
                    
                    <div class="col-sm d-flex justify-content-centre">
                        <input class="tab-button" type="submit" value="<?php _e( 'Search', 'tabticketbroker' );?>">
                    </div>
                    
                </div>
            </div>
        </form>
        <?php
    
        $prod_search_html = ob_get_clean(); // end capture
        
        $loader_html = $this->getLoaderHtml();
        
        $html = trim( $prod_search_html ) . trim( $loader_html );
        
        return $html;

    }
}