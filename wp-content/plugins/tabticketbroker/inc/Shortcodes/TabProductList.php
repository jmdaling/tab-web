<?php

/**
 * @package tabticketbroker
 */

namespace Inc\Shortcodes;

use Inc\Base\BaseController;
use Inc\Classes\HardCoded;
use Inc\Classes\Automation;
use \WC_Query;

defined('ABSPATH') || exit;

/**
 * Product search shortcode class.
 * Relies on enqueue of frontend JS and CSS in Enqueue.php
 * JS = ttb-productfilter
 */
class TabProductList extends BaseController
{ 
    public $terms = array();
    
    public $terms_config = array(); 

    public $terms_selected  = array(); 
    
    public function __construct()
    {
        add_shortcode('tab_prod_list', array($this, 'getProductList') );
    }

    /**
     * The main function for returning to shortcode call. 
     * Using the filtered products here display a list of products which contains
     * variable products that do match the filter criteria
     * 
     * Switch between displaying the filter (sidebar), the products (desktop) or both (mobile)
     * 
     * @param array $args $display_filter Toggle if filter is outputted $display_products Toggle if products is outputted
     * @return string html for the product filter & product result list
     */
    public function getProductList( $args = array() )
    {
        // Enqueue JS 
        $this->enqueueScripts();
        
        // Set parameters from args        
        $args = ( empty( $args ) ? array(
            'display_filter' => true,
            'display_products' => true,
            ) 
            : $args
        );
            
        $display_filter = ( isset( $args['display_filter'] ) ? (bool) $args['display_filter'] : true );

        $display_products = ( isset( $args['display_products'] ) ? (bool) $args['display_products'] : true );

        // Set all term / attributes first
        $this->setTermConfig();

        $this->setTermsSelected();
        
        $this->setTerms();

        // Defaults
        $product_list_html = '';
        
        // Get the filter HTML
        $filter_html = ( $display_filter ? $this->getProductFilterHtml() : '' );
        
        // Set loader HTML (used also when no prods found, required for JS)
        $loader_html = $this->getLoaderHtml();

        // Get product list HTML if display specified
        if ( $display_products ) {
            // Get products that should be included
            $filtered_product_ids = $this->getFilteredProductIds( $this->terms_selected );
            
            // If no products match the filter criteria
            if( $filtered_product_ids[0]  === -1 ) {
                ob_start();
                echo $filter_html;
                ?>
                <div id="ttb_product_list_container">
                    <div class="tab-no-prods-found">
                        <h4><?php _e( 'No products found for your search criteria.', 'tabticketbroker' ); ?></h4>
                        <p><?php _e( 'Try more dates or select a product to see the availability detail within a product.', 'tabticketbroker' ); ?></p>
                    </div>
                    <?php echo $loader_html; ?>
                </div>
                    <?php
                // Grab contents from object and recycle
                return ob_get_clean();
            }
            
            // Add the user filter selection to post to the product and select per default the filter selections from the shortcode view
            // Note that if multiple values selected only the first value will be sent 
            $selected_term_input_html = '';
            foreach ( $this->terms_selected as $attribute_name => $attribute_slug ) {
                $selected_term_input_html .= '<input type="hidden" name="'. $attribute_name .'" value='. $attribute_slug[0] .'>';
            }

            // Build query arg with taxonomy array incorporated
            $args = array(
                'post_type'      => 'product',
                'post_status'    => 'publish',
                'posts_per_page' => -1,
                'include' => array_values( $filtered_product_ids ),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'exclude-from-catalog',
                        'operator' => 'NOT IN',
                    ),
                ),
            );

            // Run query to get products
            $products = wc_get_products( $args );

            // Order products here
            $products  = wc_products_array_orderby( $products, 'title', 'ASC' );
            
            // Build the HTML
            if ( $products ) {
                
                // Start recording html build
                ob_start();
                ?>
                <div id="ttb_product_list_container" class="row">
                    <?php foreach ( $products as $key => $product ) { ?>
                        <div class="column">
                            <form id="product_<?php echo $product->get_id(); ?>" class="tab-prod-list-container" action="<?php echo esc_url( $product->get_permalink()); ?>" method="post">
                                <div class="prod-image">
                                <a id="product_link_<?php echo $product->get_id(); ?>" class="product-link-js"><?php echo $product->get_image(); ?></a>
                                </div>
                                <div id="prod_info">
                                    <!-- <div class="prod-title"><?php echo wp_kses_post($product->get_name()); ?></div> -->
                                    <div class="prod-price"><?php echo $product->get_price_html(); ?></div>
                                    <div class="prod-meta"><?php _e('VAT included.', 'tabticketbroker'); ?> <?php _e('excl.', 'tabticketbroker'); ?> <a href="/versand/" target="_blank"><?php _e('Shipping', 'tabticketbroker'); ?></a></div>
                                    <?php echo $selected_term_input_html; ?>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                    <?php echo $loader_html; ?>
                </div>    
                <?php

                // Grab contents from object and recycle
                $product_list_html = ob_get_clean();
    
            }
            else {
                // This code was hit when an item was in the view but the product was not retrieved because of 
                // its status which was not published. So now we handle no products found properly: the same as above
                // ALSO: the view has been updated not to retrieve products that are not published
                // OLD CODE: $product_list_html = 'Error: No products found. Please refresh to start over.'; // Because of lookup for $filtered_product_ids above this code should theoretically not not be reached 

                ob_start();
                echo $filter_html;
                ?>
                <div id="ttb_product_list_container">
                    <div class="tab-no-prods-found">
                        <h4><?php _e( 'No products found for your search criteria.', 'tabticketbroker' ); ?></h4>
                        <p><?php _e( 'Try more dates or select a product to see the availability detail within a product.', 'tabticketbroker' ); ?></p>
                    </div>
                    <?php echo $loader_html; ?>
                </div>
                    <?php
                // Grab contents from object and recycle
                return ob_get_clean();
            }
        }

        // Combine and display ( depending on display settings one of these may be empty )
        $html =  trim($filter_html) . trim($product_list_html);
        return $html;
    }
    
    /**
     * From the selected terms a filtered list of product ids (parent products, not the variations) is returned
     * to be used in the query and display of the filtered products
     * A pivoted view of the product variations is used to have multiple criteria,
     * 
     * NOTE: There is and imporatant difference in this approach which filters on actual product variations and not
     * only the attribute which the parent product has. This ensures accurate dynamic filtering even if the product
     * data administration is not. E.g. If a product variation is removed for a date yet the attribute for that date is
     * left on the parent product, the "original" filter will still include it in its results. This filter will not.
     * @param array of terms selected for filter criteria
     * @return array of product ids to display in result list
     */
    public function getFilteredProductIds( array $terms_selected_input = null ) 
    {
        // Set default vars
        global $wpdb;

        $where_clause_sql = 'WHERE is_active = 1 ';
        
        $prev_attr_name = '';
        
        $is_first_attr = true;

        // Create where sql clause from selected terms
        foreach ( $terms_selected_input as $attribute_name => $attribute_slug ) {
            
            // Skip if input is not array 
            if ( ! is_array( $attribute_slug ) ) continue;
            
            if ( $where_clause_sql == 'WHERE is_active = 1 ' ) $where_clause_sql .= 'AND ';
            if ( $attribute_name != $prev_attr_name ) {
                $prev_attr_name = $attribute_name;
                if ( $is_first_attr ) {
                    $is_first_attr = false;
                    $where_clause_sql .= $attribute_name. ' IN (\''. implode( '\',\'', $attribute_slug ). '\') ';
                } 
                else
                    $where_clause_sql .= 'AND '. $attribute_name. ' IN (\''. implode( '\',\'', $attribute_slug ). '\') ';
            }
            
        }
        $where_clause_sql .= ';';
        
        // Execute SQL
        $view_name = HardCoded::getViewName();

        $sql  = 'SELECT DISTINCT post_parent
                 FROM '. $view_name .' ' .
                 $where_clause_sql;

        $results = $wpdb->get_results( $sql, ARRAY_A );
        
        // Check if there are any errors
        if ( $wpdb->last_error ) {
            $error_message = $wpdb->last_error;
            $error_code = $wpdb->last_error;
            $error = new \WP_Error( $error_code, $error_message );
            
            // Log error
            error_log( 'Error in TabProductList.php query: '.$error->get_error_message() );

            // Email error to the admin
            $args['to'] = get_option( 'admin_email' );
            $args['subject'] = 'Error in TabProductList.php query';
            $args['message'] = 'DB ERROR message: '.$error->get_error_message(). ' SQL: '. $sql . ' No products will be available in the front end.';
            $args['headers'] = array('Content-Type: text/html; charset=UTF-8');
            $time_interval = 10 * 60; // 10 minutes
            Automation::scheduleEmail ( $args, $time_interval );
            
            return array( -1 );
        }

        // Build list of product ids to display 
        // Note that this is a unique list the parent ids of each variation that matches the criteria
        if ( ! empty( $results ) ) {
            foreach ( $results as $result ) {
                $filtered_product_ids[] = intval( $result['post_parent'] );
            }
        }
        else {
            $filtered_product_ids[] = -1;
        }
        
        return $filtered_product_ids;
    }

    /** 
     * Sets the config used to retrieve terms for the product in order to build the product filter
     * @param array Customized terms config can be specified
     */
    public function setTermConfig( array $terms_config_input = null )
    {
        $hardcoded = new HardCoded;
        $this->terms_config = ( ! empty ( $terms_config_input ) ? $terms_config_input : 
            array(
                'pa_date' => array(
                    'slug' => 'pa_date',
                    'name' =>  wc_attribute_label( 'pa_date' ),
                    'info' => __( 'These date are valid for the year', 'tabticketbroker' ) .' '. TTB_EVENT_YEAR,
                ) ,
                'pa_pax' => array(
                    'slug' => 'pa_pax',
                    'name' =>  wc_attribute_label( 'pa_pax' ),
                    'info' => __( 'The tables seat 8 or 10 people. For group bookings of more than 10 people you sit at two or three tables.', 'tabticketbroker' ),
                ) ,
                'pa_timeslot' => array(
                    'slug' => 'pa_timeslot',
                    'name' => wc_attribute_label( 'pa_timeslot' ),
                    'info' => $hardcoded->getEventStartTimeInfo(),
                ) 
            )
        ); 
    }


    /**
     * Get a list of all the attributes for the products
     * @return array of term objects
     */
    public function setTerms()
    {
        // Get the attributes from the taxonomies & their terms
        $args = array(
            'hide_empty' => false,
            'taxonomy' => array_keys( $this->terms_config ),
        );

        // Query db for source terms
        $terms = get_terms( $args );

        if ( is_wp_error( $terms ) || empty( $terms ) ){
            $this->terms = array();
            return;
        }

        // Add selected flag & short names for mobile & smaller viewports
        foreach ( $terms as $key => $term) {

            // Check if term was selected
            $term->selected = 
                ( 
                    ! isset( $this->terms_selected[$term->taxonomy] ) ? false : 
                    ( $this->terms_selected ? ( in_array( $term->slug, $this->terms_selected[$term->taxonomy] ) ) : false )
                );

            switch ( $term->taxonomy ) {
                case 'pa_date':
                    $term->short_name = substr( $term->name, 0, 8 );
                    break;

                case 'pa_pax':
                    $term->short_name = explode( "-", $term->slug )[0];
                    break;

                default :
                    $term->short_name = $term->name;
                    break;
                    
            }
        }

        // Sort the attributes by taxonomy and then term
        // The taxonomy should be done first & separately in order to do the multifaceted sort function thereafter
        usort( $terms, function($a, $b) {
            return $a->taxonomy <=> $b->taxonomy ? 1 : -1;
        } );

        usort( $terms, function($a, $b): int {
            if ( $a->taxonomy == $b->taxonomy ) {
                return $a->slug <=> $b->slug;
            }
            return $a->taxonomy <=> $b->taxonomy;
        } );

        $this->terms = $terms;
    }

    /**
     * Sets the selected terms variable from the posted vars
     * Updates: The post var started coming through with empty values, so now we remove them to
     * ensure that the filter is not applied with empty values. The symptoms were that when you
     * click on search on the home page without selecting anything, the filter would still apply
     * and no products would be found. Now all products are displayed by default.
     * @return void
     */
    public function setTermsSelected()
    {
        // Check if post is set
        $selected_terms = ( empty( $_POST ) ? array() : $_POST );

        // Loop through the terms and remove any that are not in the config
        foreach ( $selected_terms as $key => $value ) {
            if ( ! array_key_exists( $key, $this->terms_config ) ) {
                unset( $selected_terms[$key] );
            }

            // And loop through each value and remove any that are empty
            foreach ( $value as $k => $v ) {
                if ( empty( $v ) ) {
                    unset( $selected_terms[$key][$k] );
                }
            }
        }

        // Remove any empty arrays
        $selected_terms = array_filter( $selected_terms );
        
        // Set the selected terms
        $this->terms_selected = $selected_terms;
    }



    /**
     * Creates the html for the product filter.
     * @return string html
     */
    public function getProductFilterHtml()
    {
        $prev_tax = '';
        
        $filter_html = '';

        $selected_filter_input_html = '';

        // Get mobile toggle button to inject and hide
        $hardcoded = ( class_exists('HardCoded') ? HardCoded::class : new HardCoded );

        $mobile_filter_toggle_html = $hardcoded->getMobileFilterButton();

        // Start recording html build
        ob_start();
        ?>
        <div id="ttb_filter_container" class="clearfix">
            <div id="ttb_prod_filter">
                <a id="ttb_close_filter">
                    <img src="/wp-content/uploads/2021/06/Close.png" alt="×">
                </a>
                <?php foreach ( $this->terms as $key => $term ) { ?>    
                <?php
                
                $selected_filter_input_html .= ( $term->selected ? '<input type="hidden" id="'. $term->slug .'_input" name="'. $term->taxonomy .'[]" value="'. $term->slug .'">' : '' );
                
                if ( $term->taxonomy == "pa_date") {
                    
                    $timezone = wp_timezone();
                    $product_time_cutoff = HardCoded::getProductTimeCutoff();
                    $current_time = new \DateTime( 'now', $timezone );
                    $attribute_time = new \DateTime( $term->slug . $product_time_cutoff, $timezone );
                    $is_past_date = ( $current_time > $attribute_time  ? true : false );

                    // if in the past, set a param disabled html
                    $disabled_txt = ( $is_past_date ? 'disabled ' : '' );
                }

                // Build taxonomy container: Close container only if loop is NOT a continuation from last taxonomy
                if ( $prev_tax != $term->taxonomy ) { 
                    if ( $prev_tax != '' ) 
                        echo '</div></div>'; // <!-- end ttb_prod_attr_list -->
                        echo '<div class="ttb_prod_spacer"></div>'; //<!-- end attr_type -->
                        echo '<div class="ttb_prod_attr_type" id="'. $term->taxonomy .'">';
                        echo '<div class="ttb_prod_attr_title">'. wc_attribute_label( $term->taxonomy ) . $this->getAttributeInfoHover( $term->taxonomy ) . '</div>';
                        echo '<div class="ttb_prod_attr_list">';
                    $prev_tax = $term->taxonomy;
                }
                ?> 
                <div class="attribute_name <?php echo $disabled_txt . ( $term->selected == 0 ? '' : ' attribute_name_selected' ); ?>" id="<?php echo $term->slug; ?>"> 
                    <p><?php echo $term->short_name; ?></p>
                </div>
                <?php } // End foreach $this->terms ?>
                </div>
            </div>
                <form id="ttb_product_filter_form" method="POST">
                    <div class="wp-block-buttons is-content-justification-center tab-mobile-button">
                        <div>
                            <a id="ttb_view_results" class="wp-block-button__link tab-button"><?php _e( 'View Results', 'tabticketbroker' ); ?></a>
                            <a id="ttb_clear_filter"><?php _e( 'Clear Filter', 'tabticketbroker' ); ?></a>
                        </div>
                    </div>
                    <div id="filter_inputs">
                        <?php
                            // Inject filters selected for default selection on product page 
                            echo $selected_filter_input_html; 
                        ?>
                    </div>
                </form>
            </div>
        </div>
        <?php
        // Grab contents from object and recycle
        $filter_html .= ob_get_clean();

        $html = trim($filter_html) . trim($mobile_filter_toggle_html);

        return $html;
    }

    /**
     * Adds the registered JS scripts to the page for the shortcode to consume
     */
    public function enqueueScripts() 
    {
        // Enqueue JS script
        wp_enqueue_script( 'ttb-productfilter' );
    }

    /**
     * Gets any additional info to be displayed with the title of an attribute
     * @return string Info for filter title
     */
    public function getAttributeInfoHover( string $ttb_prod_attr_type = null )
    {
        switch ($ttb_prod_attr_type) {
            case 'pa_date':
                $info_text = '';
                break;
            
            default:
            $info_text = '';
                break;
        }

        return $info_text;
    }
}
