<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Inc\Base\BaseController;

class WpCustomization extends BaseController
{
    public function register()
    {    
        // Set global vars
        add_action( 'plugins_loaded', array( $this, 'setGlobalVars' ) );
        
        // Set default Options (Move to proper place in future)
        $this->setDefaultOptions();

        // UPDATE 5 AUGUST 2022: 
        // Using User Role Editor by Vladimir Garagulya https://wordpress.org/plugins/user-role-editor/
        // rendering the code below and the related functions redundant. Leaving them in reference only.
        //
        // Only runs if developer sets the get correct. This should be moved to correctly 
        // run on plugin activation (without role removal). Also, on plugin de-activation
        // it should remove the roles and capabilities added. 
        // STATUS: To be done in the future, not priority now as this plugin is TTB specific
        if ( is_admin() && isset( $_GET['reload_caps'] ) && $_GET['reload_caps'] == '1'  ) {
            
            // Remove roles not required
            add_action( 'admin_init', array( $this, 'removeUserRoles' ) );
            
            // Add new TTB roles
            add_action( 'admin_init', array( $this, 'addUserRoles' ) );

            // Add the set capabilities for the CPTs of this plugin
            add_action( 'admin_init', array( $this, 'addRoleCapabilies' ) );

            var_dump( 'ROLES & CAPS RELOADED' );
        }

        // Add dynamic content to menu (Add the year to the TT top menu link for oktoberfest)
        add_filter( 'wp_nav_menu_items', array( $this, 'addYearToMenu' ), 10, 2 );

        // Customize the password form
        add_filter( 'the_password_form', array( $this, 'jmCustomPostPasswordForm' ), 10, 2 );
    }

    function jmCustomPostPasswordForm( $output, $post = 0 ) {
        $post   = get_post( $post );
        $label  = 'pwbox-' . ( empty( $post->ID ) ? wp_rand() : $post->ID );
        $output = '<main class="post-password-container"><h1>'. __( 'Tab Price List', 'tabticketbroker' ).'</h1><p class="post-password-message">'.__( "This content is password protected. To view it, please enter your password below", 'tabticketbroker' ).'</p>
        <form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
        <label class="post-password-form__label" for="' . esc_attr( $label ) . '">'.__( "Password", 'tabticketbroker' ).'</label><input class="post-password-form__input" name="post_password" id="' . esc_attr( $label ) . '" type="password" spellcheck="false" size="20" /><input type="submit" class="post-password-form__submit" name="' . esc_attr_x( 'Submit', 'Post password form', 'tabticketbroker' ) . '" value="'.__( "Submit", 'tabticketbroker' ).'" /></form><main>
        ';
        return $output;
    }
    

    /**
     * Set global vars
     */
    public function setGlobalVars()
    {
        // Get the year from the ttb_event_first_day setting
        $first_day = get_option( 'ttb_event_first_day' );
        $event_year = $first_day ? date( 'Y', strtotime( $first_day ) ) : false;

        // If no event year is set
        if ( ! $event_year ) {
            // if current date is before 10th of October, use current year, otherwise use next year
            $event_year = ( date( 'm' ) < 10 || ( date( 'm' ) == 10 && date( 'd' ) < 10 ) ) ? date( 'Y' ) : date( 'Y' ) + 1;
        }

        define( 'TTB_EVENT_YEAR', $event_year );
    }

    /**
     * Add the year to the TT top menu link for oktoberfest
     */
    public function addYearToMenu( $items, $args ) {
        if ( $args->theme_location == 'topbar_menu' ) {
            // If current month is after november, add 1 to the year
            $items = str_replace( 'YYYY', date( 'Y' ) + ( date( 'm' ) > 11 ? 1 : 0 ), $items );
        }
        return $items;
    }

    /**
     * Set default options if they do not exist
     */
    private function setDefaultOptions()
    {
        // Default format for displaying dates on the backend (German format)
        if ( ! get_option( 'ttb_backend_display_date' ) ) update_option( 'ttb_backend_display_date', 'D d-m y' );
    }

    /**
     * Add user roles
     * This function is only executed manually if required by the developer
     */
    public function removeUserRoles() {
        remove_role( 'sales_manager' );
        remove_role( 'tab_admin' );
        remove_role( 'tab_staff' );
        remove_role( 'editor' );
        remove_role( 'author' );
        remove_role( 'contributor' );
        remove_role( 'subscriber' );
    }

    /**
     * Add user roles
     * This function should only run once on plugin activation
     */
    public function addUserRoles() 
    {
        // Role used for administrators of Tab, should be able to do all functions regarding the plugin and its CPTs
        add_role( 'tab_admin', 'Tab Administrator' );
        
        // Role used for staff with minimal difference to the role above (most obvious differences are that sensitive data is hidden)
        add_role( 'tab_staff', 'Tab Staff' );

    }
    /**
     * Standard capabilities for the new roles
     * @return array ( capability => role ) If role = all apply to all else only to role specified
     */
    private static $std_caps = array(
        
        // WP Core
        'read'                              => 'all',
        'manage_options'                    => 'all',
        'edit_posts'                        => 'all',
        
        // Posts & Pages
        'edit_pages'                        => 'all',
        'edit_others_pages'                 => 'all',

        // Users
        'edit_users'                        => 'tab_admin',
        'list_users'                        => 'tab_admin',
        'remove_users'                      => 'tab_admin',
        'delete_users'                      => 'tab_admin',
        'create_users'                      => 'tab_admin',

        // TabTicketBroker
        'view_supplier_cost_prices'         => 'administrator',
        'view_supplier_cost_prices'         => 'tab_admin',
        'tab_admin_functions'               => 'administrator',
        'tab_admin_functions'               => 'tab_admin',

        //Products
        'edit_products'                     => 'all',
        'edit_others_products'              => 'all',
        'publish_products'                  => 'all',
        'read_private_products'             => 'all',
        'delete_products'                   => 'all',
        'delete_private_products'           => 'all',
        'delete_published_products'         => 'all',
        'delete_others_products'            => 'all',
        'edit_private_products'             => 'all',
        'edit_published_products'           => 'all',

        // Woocommerce
        'manage_woocommerce'                => 'tab_admin',
        'view_woocommerce_reports'          => 'tab_admin',
        'edit_shop_order'                   => 'all',
        'read_shop_order'                   => 'all',
        'delete_shop_order'                 => 'all',
        'edit_shop_orders'                  => 'all',
        'edit_others_shop_orders'           => 'all',
        'publish_shop_orders'               => 'all',
        'read_private_shop_orders'          => 'all',
        'delete_shop_orders'                => 'all',
        'delete_private_shop_orders'        => 'all',
        'delete_published_shop_orders'      => 'all',
        'delete_others_shop_orders'         => 'all',
        'edit_private_shop_orders'          => 'all',
        'edit_published_shop_orders'        => 'all',
        // 'manage_shop_order_terms'           => 'all',
        // 'edit_shop_order_terms'             => 'all',
        // 'delete_shop_order_terms'           => 'all',
        // 'assign_shop_order_terms'           => 'all',
        // 'edit_shop_coupon'                  => 'all',
        // 'read_shop_coupon'                  => 'all',
        // 'delete_shop_coupon'                => 'all',
        // 'edit_shop_coupons'                 => 'all',
        // 'edit_others_shop_coupons'          => 'all',
        // 'publish_shop_coupons'              => 'all',
        // 'read_private_shop_coupons'         => 'all',
        // 'delete_shop_coupons'               => 'all',
        // 'delete_private_shop_coupons'       => 'all',
        // 'delete_published_shop_coupons'     => 'all',
        // 'delete_others_shop_coupons'        => 'all',
        // 'edit_private_shop_coupons'         => 'all',
        // 'edit_published_shop_coupons'       => 'all',
        // 'manage_shop_coupon_terms'          => 'all',
        // 'edit_shop_coupon_terms'            => 'all',
        // 'delete_shop_coupon_terms'          => 'all',
        // 'assign_shop_coupon_terms'          => 'all',
    );
    
    /**
     * Custom capabilities of custom post types
     */
    private static $custom_caps = array(
        array(
            'singular' => 'supplier', 
            'plural' => 'suppliers',
        ),
        array(
            'singular' => 'reservation', 
            'plural' => 'reservations',
        ),
    );

    /**
     * Add custom capabilities for admin
     */
    public static function addRoleCapabilies() 
    {
        $roles = array(
            'administrator',
            'tab_admin',
            'tab_staff',
        );

        // Loop above roles
        foreach( $roles as $role_name ) {
            $role = get_role( $role_name );

            // Add standard caps applicable to each role
            foreach( self::$std_caps as  $cap => $role_cap ) {
                // Caps only assigned if it is for all or specified for the role in the loop 
                if ( $role_cap == 'all' || $role_cap == $role_name ) {
                    $role->add_cap( $cap );
                }
            }

            // Add custom caps applicable to each post type defined
            foreach( self::$custom_caps as $cap ){
                
                $singular = $cap['singular'];
                $plural = $cap['plural'];

                $role->add_cap( "edit_{$singular}" ); 
                $role->add_cap( "edit_{$plural}" ); 
                $role->add_cap( "edit_others_{$plural}" ); 
                $role->add_cap( "publish_{$plural}" ); 
                $role->add_cap( "read_{$singular}" ); 
                $role->add_cap( "read_private_{$plural}" ); 
                $role->add_cap( "edit_published_{$plural}" );
                $role->add_cap( "edit_private_{$plural}" );
                
                // Only Admins get delete rights 
                if ( $role_name !== 'tab_staff' ) {
                    $role->add_cap( "delete_{$singular}" ); 
                    $role->add_cap( "delete_{$plural}" );
                    $role->add_cap( "delete_private_{$plural}" );
                    $role->add_cap( "delete_others_{$plural}" );
                    $role->add_cap( "delete_published_{$plural}" );
                }
                
            }
        }
    }
}