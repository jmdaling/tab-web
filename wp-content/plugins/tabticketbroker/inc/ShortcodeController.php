<?php
/**
 * @package tabticketbroker
 */
namespace Inc;

use Inc\Shortcodes\TabProductList;
use Inc\Shortcodes\ProductSearch;

class ShortcodeController
{
    public function register() 
    {
    
        new ProductSearch();
        
        new TabProductList();
        
    }

}