
# Woocommerce Config Translations

## Description

There are some configurations within woocommerce which have titles that are not easily translated. We currenly are using TranslatePress for the frontend and LocoTranslate to translate the TabTicketBroker plugin. These titles however, are settings within woocommerce and is the crossover between frontend & backend. Fortunately even if TranslatePress is used to translate strings suchs as these titles it also affects the backend processes that for example generates the emails and so the translations are done correctly. Even if the WC shipping titles are in German and translated on the checkout page to english they work in the emails too. TranslatePress sees the configured titles as the German strings and are therefore uneditable, thus they were set to german and translated to english, rather than the otherway around.

## Fields

Type | English (TranslatePress value) | German (WC Config value)
-|-|-
Shipping: Pickup | Pickup (Berlin / Munich): 0,00€ | Abholung (Berlin / München): 0,00€
Shipping: Germany | Express shipping within Germany | Express Versand innerhalb Deutschland
Shipping: EU | Shipping within the EU zone | Versand innerhalb der EU-Zone
Shipping: International|International Shipping|Internationaler VersandAbholung
Payment: Paypal Description | Pay via PayPal. You can pay with your credit card if you don't have a PayPal account. | Bezahlen Sie über PayPal; Sie können mit Ihrer Kreditkarte bezahlen, wenn Sie kein PayPal-Konto haben.
Payment: Bank transfer title | Payment via bank transfer | Vorkasse per Banküberweisung
Payment: Bank transfer descripiton | You will receive an invoice after your order | Sie erhalten nach Ihrer Bestellung eine Rechnung.

## Method & Filter
```php
// Extra translation filters
    // add_filter( 'woocommerce_order_get_items', array( $this, 'translateOrderDetailsTitles' ) );
    add_filter( 'woocommerce_get_order_item_totals', array( $this, 'translateOrderDetailsTitles' ) );

    /**
     * In the order details table (on most emails) the footer line items take the title directly
     * from the configuration (e.g. Shipping titles & Payment method titles) and this filter 
     * translate specific titles which is required to be exact matches to what is configured in
     * the system.
     */
    public function translateOrderDetailsTitles( $items )
    {
        foreach ( $items as $item ) {
            
            // Change shipping title for pickup
            if ( isset( $item["name"] ) && $item instanceof WC_Order_Item_Shipping ) {
                if ( $item["name"] = 'Pickup (Berlin / Munich): 0,00€' )
                    $item["name"] = __( 'Pickup (Berlin / Munich): 0,00€', 'tabticketbroker' );
            }
        }

        return $items;
    }
```