<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInit449f947fbf8b702a94a473461c4c741c
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(array('ComposerAutoloaderInit449f947fbf8b702a94a473461c4c741c', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInit449f947fbf8b702a94a473461c4c741c', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        call_user_func(\Composer\Autoload\ComposerStaticInit449f947fbf8b702a94a473461c4c741c::getInitializer($loader));

        $loader->register(true);

        return $loader;
    }
}
