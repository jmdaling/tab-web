


SELECT
    order_number.`Order Number`,
    order_date.`Order Date`,
    pm.*,
    oi.*,
    om.meta_key,
    om.meta_value
FROM
    wp_shcga_woocommerce_order_items oi
    LEFT JOIN wp_shcga_woocommerce_order_itemmeta om ON oi.order_item_id = om.order_item_id
    LEFT JOIN 
    (
        -- Order Number
        SELECT
            p.ID,
            pm.meta_value AS `Order Number`
        FROM
                        wp_shcga_posts    p
            INNER JOIN  wp_shcga_postmeta pm ON p.ID        = pm.post_id
        WHERE
            pm.meta_key IN ('_alg_wc_full_custom_order_number')
    ) AS order_number ON oi.order_id = order_number.ID
    LEFT JOIN 
    (
        
    ) AS order_date ON oi.order_id = order_date.ID
WHERE

    AND om.meta_key IN (
        '_variation_id',
        '_line_total',
        'pa_date',
        'pa_pax',
        'pa_timeslot',
        'pa_area',
        '_qty'
    )
ORDER BY
    order_id








-- Order Data
SELECT
    p.ID,
    pm.meta_value
FROM
                wp_shcga_posts    p
    INNER JOIN  wp_shcga_postmeta pm ON p.ID        = pm.post_id
WHERE
    pm.meta_key IN (
        '_alg_wc_full_custom_order_number',
        '_paid_date',
        '_ttb_invoice_no',
        '_ttb_order_language',
        '_billing_first_name',
        '_billing_last_name',
        '_billing_email',
        '_billing_phone',
        '_payment_method',
        '_order_shipping',
        '_order_tax',
        '_order_total'
    )
AND p.post_type = 'shop_order';