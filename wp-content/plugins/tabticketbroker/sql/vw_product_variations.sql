DROP VIEW vw_product_variations;
CREATE VIEW vw_product_variations
AS
(
    SELECT DISTINCT
        p.post_parent `post_parent`,
        p2.post_status `post_parent_status`,
        p.post_status,
        CASE WHEN p.post_status = 'publish' THEN 1 ELSE 0 END `is_active`,
        pm.post_id, -- variation id
        pp.post_title,
        pm.meta_key,
        pm.meta_value
    FROM
        wp_shcga_posts p
            INNER JOIN
         wp_shcga_posts p2 ON p.post_parent = p2.ID
            INNER JOIN
        wp_shcga_postmeta pm ON p.ID = pm.post_id
            INNER JOIN
        wp_shcga_posts pp ON p.post_parent = pp.ID
    WHERE
        p.post_type = 'product_variation'
    AND pm.meta_key IN ('_sku','attribute_pa_date','attribute_pa_pax','attribute_pa_timeslot','attribute_pa_area', '_price', '_stock' )
    ORDER BY
        pm.post_id,
        pm.meta_id,
        pm.meta_key,
        pm.meta_value
);