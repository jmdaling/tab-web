DROP VIEW vw_product_variations_pivot;
CREATE VIEW vw_product_variations_pivot
AS
(
    SELECT 
        t.post_id AS id, -- variation id
        MAX(CASE WHEN t.meta_key = '_sku' THEN t.meta_value ELSE NULL END) AS `sku`,
        t.post_title, 
        t.is_active,
        MAX(CASE WHEN t.meta_key = 'attribute_pa_date' THEN t.meta_value ELSE NULL END) AS `pa_date`,
        MAX(CASE WHEN t.meta_key = 'attribute_pa_timeslot' THEN t.meta_value ELSE NULL END) AS `pa_timeslot`,
        MAX(CASE WHEN t.meta_key = 'attribute_pa_pax' THEN t.meta_value ELSE NULL END) AS `pa_pax`,
        MAX(CASE WHEN t.meta_key = 'attribute_pa_area' THEN t.meta_value ELSE NULL END) AS `pa_area`,
        MAX(CASE WHEN t.meta_key = '_price' THEN t.meta_value ELSE NULL END) AS `price`,
        MAX(CASE WHEN t.meta_key = '_stock' THEN t.meta_value ELSE NULL END) AS `stock`,
        t.post_parent
    FROM vw_product_variations t
    GROUP BY t.post_parent, t.post_title, t.post_id
);



