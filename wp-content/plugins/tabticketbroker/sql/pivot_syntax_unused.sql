SET @sql = NULL;
SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(
      'SUM(CASE WHEN `Product` = ''',
      REPLACE( Product, '''', '''''' ),
      ''' THEN `Product Quantity` ELSE 0 END) AS `',
      REPLACE( Product, '''', '''''' ), '`'
    )
  ) INTO @sql
FROM
(
  SELECT DISTINCT `Product` AS Product
  FROM vwBookingDetailList
  ORDER BY Product
) pr;

SET @sql 
  = CONCAT(
      'SELECT 
                `Order Number`,
                `Booking Start`,
                `Venue`, 
                ', @sql, ' 
            FROM vwBookingDetailList
            GROUP BY `Order Number`;');

PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;



SELECT 
    `Order Number`,
    `Booking Start`,
    `Venue`,
    CONCAT(`Category`, ' - ', `Product`) AS `Product`,
    `Product Quantity`
FROM vwBookingDetailList
WHERE `Order Number` = 7252
GROUP BY  
    `Order Number`,
    `Booking Start`,
    `Venue`,
order by `order number`;
