
# Documentation: REPORTS

## Pickup Report
It consists of 2 parts, our backend report called the Pickup Report and the Frontend password protected page PickupFrontend.

The Pickup report only includes orders marked as Pickup. It has a status column. This status pertains to the order and defaults to At Head Office for new pickup orders. Clicking on these statuses will put the order pickup status in the clicked status. The statuses are:

    At Head Office
    Shipped
    At Pickup Office
    Collected

Important not to confuse these with the Reservation Document statuses. They are not linked. These are to track reservations and their physical documents. These Document Statuses are:

    Documents
    Vouchers received
    Letter received
    Letter ready
    Docs complete
    Ready to ship

The Frontend page allows an agent called the Collections Administrator to log in to a password protected pickup report and view all orders that were sent from the main office to the collections office. Each order shipped is a row on the report and includes an action button for the collections admin to mark the parcel as "Received" which will put the Order pickup status into At Pickup Office and once a client collects it, they can mark the order as Collected. Thus providing the Head Office with a view on what has been collected and the Collection Admin with inbound shipments.

Links & usefull info:
- The Username & Password can be found/updated on the settings page [here](https://oktoberfest-tischreservierungen.de/wp-admin/admin.php?page=tab_admin_plugin)
- [Pickup report](https://oktoberfest-tischreservierungen.de/wp-admin/admin.php?page=tab_admin_reports&report_slug=pickup)
- Front end page: https://oktoberfest-tischreservierungen.de/pickupfrontend/

