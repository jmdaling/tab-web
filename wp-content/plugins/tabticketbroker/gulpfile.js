// // Load Gulp...of course
const { src, dest, task, series, watch, parallel } = require('gulp');

const sass = require("gulp-sass")(require("sass"));

// // CSS related plugins
// var sass         = require( 'gulp-sass' );
var autoprefixer = require( 'gulp-autoprefixer' );

// // JS related plugins
var uglify       = require( 'gulp-uglify' );
var babelify     = require( 'babelify' );
var browserify   = require( 'browserify' );
var source       = require( 'vinyl-source-stream' );
var buffer       = require( 'vinyl-buffer' );
var stripDebug   = require( 'gulp-strip-debug' );

// // Utility plugins
var rename       = require( 'gulp-rename' );
var sourcemaps   = require( 'gulp-sourcemaps' );
var notify       = require( 'gulp-notify' );
var options      = require( 'gulp-options' );
var gulpif       = require( 'gulp-if' );

// // Browers related plugins
var browserSync  	= require( 'browser-sync' ).create();

// Project related variables
var projectURL   	= 'http://dev.oktoberfest-tischreservierungen.de';

// Ofest
var styleSRC     			= 'src/scss/ttb-backend.scss';
var styleFrontend   		= 'src/scss/ttb-frontend.scss';
var jsAdmin      			= 'ttb-backend.js';
var jsHomepage   			= 'ttb-homepage.js';
var jsSingleProduct 		= 'ttb-singleproduct.js';
var jsSingleProduct_tt 		= 'ttb-singleproduct_tt.js';
var jsProductFilter 		= 'ttb-productfilter.js';
var jsReservations 			= 'ttb-reservation-editor.js';
var jsCollections 			= 'ttb-collections.js';
var jsShop_tt 				= 'ttb-shop_tt.js';
var jsAjax 					= 'ttb-ajax.js';

// TabTickets
var styleFrontend_tt   		= 'src/scss/ttb-frontend_tt.scss';


// BOTH
var styleURL     	= './assets/';
var mapURL       	= './';
var jsSRC        	= 'src/js/';

var jsFiles      = [jsAdmin, jsHomepage, jsSingleProduct, jsSingleProduct_tt, jsProductFilter, jsReservations, jsCollections ];
var jsURL        = './assets/';

var styleWatch   = 'src/scss/**/*.scss';
var jsWatch      = 'src/js/**/*.js';
var phpWatch     = './**/*.php';

function js(done) {
	jsFiles.map(function (entry) {
		return browserify({
			entries: [jsSRC + entry]
		})
		.transform( babelify, { presets: [ '@babel/preset-env' ] } )
		.bundle()
		.pipe( source( entry ) )
		.pipe( buffer() )
		.pipe( gulpif( options.has( 'production' ), stripDebug() ) )
		.pipe( sourcemaps.init({ loadMaps: true }) )
		.pipe( uglify() )
		.pipe( sourcemaps.write( '.' ) )
		.pipe( dest( jsURL ) )
		.pipe( browserSync.stream() );
	});
	done();
}

function css(done) {
	src([styleSRC, styleFrontend, styleFrontend_tt])
		.pipe( sourcemaps.init() )
		.pipe( sass({
			errLogToConsole: true,
			outputStyle: 'compressed'
		}) )
		.on( 'error', console.error.bind( console ) )
		.pipe( autoprefixer({ overrideBrowserslist: [ 'last 2 versions', '> 5%', 'Firefox ESR' ] }) )
		.pipe( sourcemaps.write( mapURL ) )
		.pipe( dest( styleURL ) )
		.pipe( browserSync.stream() );
	done();
}

function css_bootstrap(done) {
	return src(
		['node_modules/bootstrap/dist/css/bootstrap.css*'],
		{base:'node_modules/bootstrap/dist/css'}
	  	)
		.pipe(dest(styleURL))
	  	.pipe(browserSync.stream());
};

function css_bootstrap2(done) {
	return src(
		['node_modules/bootstrap/dist/css/bootstrap.min.*'],
		{base:'node_modules/bootstrap/dist/css'}
	  )
	  .pipe(dest(styleURL))
	  .pipe(browserSync.stream());
};


function reload(done) {
	browserSync.reload();
	done();
}

function browser_sync() {
	browserSync.init({
		proxy: projectURL,
		// https: {
		// 	key: '/home/alecaddd/.valet/Certificates/wp.dev.key',
		// 	cert: '/home/alecaddd/.valet/Certificates/wp.dev.crt'
		// },
		injectChanges: true,
		open: false
	});
}

function watch_files() {
	watch( phpWatch, reload );
	watch( styleWatch, css, css_bootstrap );
	watch( jsWatch, series( js, js2, js3, js4, js5, js6, js_less, reload ) );
	src( jsURL + jsAdmin )
		.pipe( notify({ message: 'Gulp is Watching, Happy Coding!' }) );
};

function js_less(done) {
	return src(
		['node_modules/less/dist/*.js*'],
		{base:'node_modules/less/dist'}
	  	)
		.pipe(dest(jsURL))
	  	.pipe(browserSync.stream());
};

function js2(done) {
	return src(
		['node_modules/popper.js/dist/umd/*.js'],
		{base:'node_modules/popper.js/dist/umd'}
		)
	  .pipe(dest(jsURL))
};
function js3(done) {
	return src(
		['node_modules/popper.js/dist/umd/*.map'],
		{base:'node_modules/popper.js/dist/umd'}
		)
	  .pipe(dest(jsURL))
};

function js4(done) {
	return src(
		['node_modules/bootstrap/dist/js/*.js'],
		{base:'node_modules/bootstrap/dist/js'}
		)
	  .pipe(dest(jsURL))
	  .pipe(browserSync.stream());
};

function js5(done) {
	return src(
		['node_modules/bootstrap/dist/js/*.map'],
		{base:'node_modules/bootstrap/dist/js'}
		)
	  .pipe(dest(jsURL))
	  .pipe(browserSync.stream());
};

function js6(done) {
	return src(
		['src/js/ttb-ajax.js'],
		{base:'src/js/'}
		)
	  .pipe(dest(jsURL))
	  .pipe(browserSync.stream());
};

function multiSelectJS(done) {
	return src(
		['src/multiselect-dropdown-main/multiselect-dropdown.js'],
		{base:'src/multiselect-dropdown-main'}
		)
	  .pipe(dest(jsURL))
	  .pipe(browserSync.stream());
};

task("css", css);
task("css_bootstrap", css_bootstrap);
task("css_bootstrap2", css_bootstrap2);
task("js", js);
task("js2", js2);
task("js3", js3);
task("js4", js4);
task("js5", js5);
task("js6", js6);
task("js_less", js_less);
task("deploy", series(css, css_bootstrap, css_bootstrap2, js, js2, js3, js4, js5, js6, multiSelectJS, js_less));
task("watch", parallel(browser_sync, watch_files));
