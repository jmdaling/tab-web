<?php

/**
 * Trigger this on uninstall
 * 
 * @package tabticketbroker
 */


// If uninstall not called from WordPress, then exit.
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit;
}

// Clear data
global $wpdb;
$prefix = $wpdb->prefix;
$wpdb->query( "DELETE FROM ".$prefix."posts WHERE post_type = 'tab_supplier'" );
$wpdb->query( "DELETE FROM ".$prefix."3_posts WHERE post_type = 'tab_supplier'" );

$wpdb->query( "DELETE FROM ".$prefix."postmeta WHERE post_id NOT IN (SELECT id from ".$prefix."posts)" );
$wpdb->query( "DELETE FROM ".$prefix."3_postmeta WHERE post_id NOT IN (SELECT id from ".$prefix."3_posts)" );

$wpdb->query( "DELETE FROM ".$prefix."term_relationships WHERE object_id NOT IN (SELECT id from ".$prefix."posts)" );
$wpdb->query( "DELETE FROM ".$prefix."3_term_relationships WHERE object_id NOT IN (SELECT id from ".$prefix."posts)" );



