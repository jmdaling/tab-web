var new_dates = SpreadsheetApp.getActive().getRange("Dimensions!G3:H30").getValues();
var post_titles = SpreadsheetApp.getActive().getRange("Dimensions!B3:C17").getValues();

  /**
   * Strips the SKU into any part specified. Used instead of doing complex lookups.
   * @constructor
   * @param {string} sku The product SKU
   * @param {string} part Any of the following parts: tent, date, date_code, timeslot, pax, area. (Also accepted: post_title, pa_date, pa_timeslot, pa_pax & pa_area)
   * @return The part required
   * @customfunction
   */
  function stripSku( sku, part ) {

  // DEBUG
  // sku = "ARM-16-3-04";
  // part = "post_title";

    // return 0;

  switch (part) {
    case 'post_title':
        code = sku.substring(0, 3);
        break;
    case 'tent':
        code = sku.substring(0, 3);
        break;
    case 'date_code':
        code = sku.substring(4,6)
        break;
    
    case 'date': // same as date code, but then the code gets looked up below to return the actual date
        code = sku.substring(4,6)
        break;
    case 'timeslot':
        code = sku.substring(7, 8);
        break;
    case 'pax':
        code = sku.substring(9, 11);
        break;
    case 'area':
        code = sku.substring(12, 13);
        break;
    
    case 'pa_code':
        code = sku.substring(4,6)
        break;
    case 'pa_timeslot':
        code = sku.substring(7, 8);
        break;
    case 'pa_pax':
        code = sku.substring(9, 11);
        break;
    case 'pa_area':
        code = sku.substring(12, 13);
        break;
    
    default:
        code = "";
  }

  if ( part == "post_title" ) {
    for ( var key in post_titles ) {
      var value = post_titles[key];
      if ( value[1] == "" ) break;

      if ( value[1] == code ) {
        return value[0];
      }
    }
  }

  if ( part == "date" ) {
    for ( var key in new_dates ) {
      var value = new_dates[key];
      if ( value[1] == "" ) break;

      if ( value[1] == code ) {
        return value[0];
      }
    }
  }

  // Logger.log( part );
  // Logger.log( code );

  SpreadsheetApp.flush();

  return code;
}
