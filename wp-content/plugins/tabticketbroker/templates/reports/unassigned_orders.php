<body>
    <div id="screen-meta" class="metabox-prefs" style="display: block;">
        <div id="screen-options-wrap" class="hidden" tabindex="-1" aria-label="Screen Options Tab" style="display: none;">
            <form method="get" id="reservation_filter_form">
                <div id="filter_options_wrap">
                    <?php echo $this->assignment_manager->getProductFilterHtml(); ?>
                    <?php echo $this->assignment_manager->getDateFilterHtml(); ?>
                    <?php echo $this->assignment_manager->getFlaggedFilterHtml(); ?>
                    <?php echo $this->assignment_manager->getTimeslotFilterHtml(); ?>
                    <?php echo $this->assignment_manager->getReservationStatusFilterHtml(); ?>
                    <?php echo $this->assignment_manager->getDisplayAmountHtml( $base_controller->page_names['Reports'] ); ?>
                </div>
            </form>
        </div>
    </div>
    <div id="screen-meta-links">
        <div id="screen-options-link-wrap" class="hide-if-no-js screen-meta-toggle">
            <button type="button" id="show-settings-link" class="button show-settings screen-meta-active" aria-controls="screen-options-wrap" aria-expanded="true">Screen Options</button>
        </div>
    </div>
    
    
    <div class="wrap">

        <?php echo $this->printReportHeaderHtml( 'unassigned_orders' ); ?>

        <?php echo $this->gen2_filter->renderDimensionFilters(); ?>
        <?php $this->gen2_filter->printJS(); ?>

        <div class="filter-notice">
        <?php
        // If an order_id is set, display a the order number
        if ( isset( $_GET['order_id'] ) ) {

            // Get the order number
            $order_id = $_GET['order_id'];
            $order = wc_get_order( $order_id );
            $order_number = $order->get_order_number();
            
            // Show that report is filtered
            echo '<span class="subtitle">'. __( 'Filtered by order no ', 'tabticketbroker' ) .': '. $order_number .'</span>';
            
            // Show button linking to unassigned orders report
            echo '<a href="'. admin_url( 'admin.php?page=tab_admin_reports' ) .'" class="button">'. __( 'Reset filter', 'tabticketbroker' ) .'</a>';
        }
        ?>
        </div>

        <div id="report_table_wrap">
            <?php $this->renderReport(); ?>
        </div>

    </div>

    <?php echo $this->base_controller->getLoaderHtml();?>

    <?php echo $this->base_controller->getScrollToTopHtml();?>

</body>
    