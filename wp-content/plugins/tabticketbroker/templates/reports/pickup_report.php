<?php
$username = get_option('ttb_collections_admin_username');
$password = get_option('ttb_collections_admin_password');
?>



<body>
    <div class="wrap">

        <?php echo $this->printReportHeaderHtml('pickup'); ?>
        <!-- Print button to export CSV -->
        <div class="tabticketbroker-print-button">
            <a href="<?php echo admin_url('admin.php?page=tab_admin_reports&report_slug=pickup&export_csv=1'); ?>" class="button button-primary">Export CSV</a>
        </div>

        <!-- add admin notice to show un and pwd -->
        <div class="notice notice-info">
            <p>
                <b>Front End Pickup Login: </b><?php echo __('Username: ', 'tabticketbroker') . $username; ?> & <?php echo __('Password: ', 'tabticketbroker') . "******"; ?>
                <span style="display:none;"><?php echo $password; ?></span>
            </p>
        </div>

        <div id="collections_filter">
        </div>

        <div id="report_table_wrap">
            <h1><?php echo __('Pickup Report', 'tabticketbroker'); ?></h1>

            <?php $this->renderReport(); ?>

        </div>
    </div>

    <?php echo $this->base_controller->getLoaderHtml(); ?>

    <?php echo $this->base_controller->getScrollToTopHtml(); ?>

</body>