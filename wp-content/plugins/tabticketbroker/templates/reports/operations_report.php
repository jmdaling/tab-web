<body>
    <div class="wrap">
        <!-- Print button to export CSV -->
        <div class="tabticketbroker-print-button">
            <a href="<?php echo admin_url('admin.php?page=tab_admin_reports&report_slug=operations&export_csv=1'); ?>" class="button button-primary">Export CSV</a>
            <a href="<?php echo $base_controller->plugin_url.'exports/operations.csv'; ?>" class="button button-primary">Download CSV</a>
        </div>

        <?php echo $this->printReportHeaderHtml( 'operations' ); ?>
        <!-- Options to select event year -->
        <div class="tabticketbroker-report-options">
            <form method="get" action="<?php echo admin_url('admin.php'); ?>">
                <input type="hidden" name="page" value="tab_admin_reports" />
                <input type="hidden" name="report_slug" value="operations" />
                <label for="event_year" title="Under development" >Event Year</label>
                <select name="event_year" onchange="this.form.submit()">
                    <?php foreach ( $this->event_years as $year ) : ?>
                        <option value="<?php echo $year; ?>" <?php echo ( $year == $this->event_year ) ? 'selected' : ''; ?>><?php echo $year; ?></option>
                    <?php endforeach; ?>
                </select>
            </form>
        </div>
    
        <!-- Print report filter -->
        <?php echo $this->gen2_filter->renderDimensionFilters(); ?>

        <div id="report_table_wrap">
                <?php $this->renderReport(); ?>
        </div>
    </div>    

    <?php echo $this->base_controller->getLoaderHtml();?>

    <?php echo $this->base_controller->getScrollToTopHtml();?>

</body>

<?php $this->gen2_filter->printJS(); ?>

    