<h1>Product Config Report</h1>

<body>    
    <div class="wrap">
        <?php echo $this->printReportHeaderHtml( 'product_config' ); ?>

        <div>
            <input type="button" id="delete_all_vars" class="button button-primary" name="delete_all_vars" value="Delete All" onclick="deleteAllVariations();">
        </div>

        <div id="report_table_wrap">
            <?php $this->renderReport(); ?>
        </div>

        <div id="deleted_vars_wrap" style="display: none;">
            <h4>Deleted Variations</h4>
            <p>These variations have been deleted in this session.</p>
            <table id="deleted_variations">
                
            </table>
        </div>
        
         <!-- Build a success modal with id=price_update_success -->
            <div id="success_modal" style="display: none;">
                <h2>Success!</h2>
                <p>The variation has been deleted.</p>
                <note>See bottom of the page for deleted variations.</note>
            </div>
    </div>

    <?php echo $this->base_controller->getLoaderHtml();?>

    <?php echo $this->base_controller->getScrollToTopHtml();?>

</body>
    