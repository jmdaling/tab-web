
<!-- Styles for input table -->
<style>
#price_update_wrap {
    display: none;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    padding: 20px;
    border: 1px solid black;
    z-index: 1000;
}
#price_update_table td {
    padding: 10px;
}

#price_update_success {
    display: none;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    padding: 20px;
    border: 1px solid black;
    z-index: 1000;
}
/* Styles for filters */
#sku_filter_wrap {
    margin-top: 5px;
}
#sku_filter_wrap .panel {
    margin-bottom: 10px;
}
#sku_filter_wrap .panel-body {
    display: flex;
    flex-wrap: wrap;
}
#sku_filter_wrap .panel-body button {
    margin-right: 5px;
    margin-bottom: 5px;
}
/* Add the active class for the filter button */
#sku_filter_wrap .panel-body button.active {
    background-color: #0073aa;
    color: white;
}

</style>

<body>    
    <div class="wrap">
        <?php echo $this->printReportHeaderHtml( 'price' ); ?>

        
        <?php echo $this->gen2_filter->renderDimensionFilters(); ?>
        <?php $this->gen2_filter->printJS(); ?>

        <div id="report_table_wrap">
            <!-- <form id="price_report" method="get"> -->
                <?php $this->renderReport(); ?>
            <!-- </form> -->
        </div>

        <!-- Create a no results div -->
        <div id="no_results" style="display: none;">
            <h2><?php _e( 'No Results', 'tabticketbroker' ); ?></h2>
            <p><?php _e( 'There are no results that match the selected filters.', 'tabticketbroker' ); ?></p>
        </div>
        
        <!-- Create a div where a variation price, stock and enabled status can be updated -->
        <div id="price_update_wrap">
            <table id="price_update_table">
                <tr>
                    <td>Variation ID</td>
                    <td><input type="text" name="variation_id" id="variation_id" value="" readonly></td>
                </tr>
                <tr>
                    <td>SKU</td>
                    <td><input type="text" name="sku" id="sku" value="" readonly></td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td><input type="text" name="price" id="price" value=""></td>
                </tr>
                <tr>
                    <td>Sale Price</td>
                    <td><input type="text" name="sale_price" id="sale_price" value=""></td>
                </tr>
                <tr>
                    <td>Stock</td>
                    <td><input type="numeric" name="stock" id="stock" value=""></td>
                </tr>
                <tr>
                    <td>Enabled</td>
                    <td><input type="checkbox" name="enabled" id="enabled" value="1"></td>
                </tr>
                <tr>
                    <td><input type="button" value="Update" class="button button-primary" onclick="updateVariation()"></td>
                    <td><input type="button" value="Cancel" class="button button-primary" onclick="cancelVariationUpdate()"></td>
                </tr>
            </table>
         </div>

         <!-- Build a success modal with id=price_update_success -->
            <div id="price_update_success" style="display: none;">
                <h2>Success!</h2>
                <p>The variation has been updated.</p>
            </div>
    </div>

    <?php echo $this->base_controller->getLoaderHtml();?>

    <?php echo $this->base_controller->getScrollToTopHtml();?>

</body>
    