<?php
/** 
 * @package tabticketbroker
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: 1: Customer first name 2: Customer last name */ ?>
<p><?php printf( esc_html__( 'Hello %1$s %2$s', 'tabticketbroker' ), esc_html( $order->get_billing_first_name() ), esc_html( $order->get_billing_last_name() ) ); ?></p>
<?php /* translators: %s: Order number */ ?>
<p><?php printf( __( 'Thank you. We received you payment for order: <b>#%s</b> ( Invoice no: <b>%2$s</b> )', 'tabticketbroker' ), $order->get_order_number(), $invoice_no ); ?></p>

<?php if ( ! $is_invoiced == false ) : ?>
<h4><?php _e( 'Orders will be confirmed with invoice within 24 hours', 'tabticketbroker' ); ?></h4>
<?php endif; ?>

<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

?>



<?php 
	// If order will be picked up
	if( $order->has_shipping_method( 'local_pickup' ) ) {

		?><p><?php _e( 'The reservation documents are available for pickup about 5 - 10 days prior to the event date.', 'tabticketbroker' ); ?></p><?php 
		?><p><?php _e( 'Further details will be provided later this year.', 'tabticketbroker' ); ?></p><?php 		
		?><p><?php _e( 'Pickup address:', 'tabticketbroker' ); ?></p><?php 
		?>
		<address>
			<?php _e( 'TO BE CONFIRMED', 'tabticketbroker' ); ?>
		</address>
		
	<?php
		?><p><?php _e( 'Opening hours:', 'tabticketbroker' ); ?></p><?php
		?><p><?php _e( 'Monday - Saturday, 10:00am - 8:00pm', 'tabticketbroker' ); ?></p><?php
	}
	// All other emails will assume shipment is included ( orders that does not have `local_pickup`) 
	else {

		?><p><?php _e( 'The reservation documents will be shipped about 5 - 10 days prior to the event date.', 'tabticketbroker' );?></p><?php 
		?><p><?php _e( 'Further details will be provided later this year.', 'tabticketbroker' ); ?></p><?php
	
		if ( ! $is_europe ) {
			?><p><?php _e( 'To ensure maximum reliability regarding the shipping of the reservation documents we strongly recommend our international customers to supply us with a delivery address within Germany (e.g. hotel). For short-termed orders, we also offer a collecting point in Munich.', 'tabticketbroker' ); ?></p><?php 
		}

		?><p><?php _e( 'Current delivery address:', 'tabticketbroker' ); ?></p><?php 

		do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
 	}

?>

<p><?php _e( 'Please do not hesitate to contact us anytime if you need further assistance.', 'tabticketbroker' ); ?></p>

<?php 

do_action( 'ttb_email_footer', $email );
