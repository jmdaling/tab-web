<?php
/** 
 * @package tabticketbroker
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php

// print the messagebox 1
echo $messagebox_1;

// action customer details from woocommerce
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

// print the messagebox 2
echo $messagebox_2;

do_action( 'ttb_email_footer', $email );