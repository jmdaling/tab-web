<?php
/** 
 * @package tabticketbroker
 * @version 1.0
 */

use Inc\Classes\HardCoded;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: 1: Customer first name 2: Customer last name */ ?>
<p><?php printf( esc_html__( 'Hello %1$s %2$s', 'tabticketbroker' ), esc_html( $order->get_billing_first_name() ), esc_html( $order->get_billing_last_name() ) ); ?></p>


<p><?php //printf( esc_html__( 'This is a payment reminder for order: #%s. Here is the current balance for your account:', 'tabticketbroker' ), esc_html( $order->get_order_number() ) ); ?></p>
<?php /* translators: %1$s: Order number %2$s: Invoice number */ ?>
<p><?php printf( __( 'Regarding your booking for the Oktoberfest with order number <b>#%1$s</b>, we have not received any payment for invoice <b>%2$s</b>', 'tabticketbroker' ), esc_html( $order->get_order_number() ), $invoice_no ); ?></p>
<p><?php _e( 'To ensure your reservation, please pay the full amount as soon as possible. If you already paid, kindly forward us the proof of payment so that we can process your order.', 'tabticketbroker' ); ?></p>
<?php 
// Warn if not invoiced yet.
if ( $invoice_no == '' || ! $invoice_no ) {
    echo '<h4>' . __( 'Your order has not yet been invoiced. Please note that all orders are confirmed with an invoice within 24 hours.', 'tabticketbroker' ) .'</h4>';
    echo '<hr>';
} 
// Or show invoice detail
else {
    printf( __( 'Please use your invoice number for all payments. ', 'tabticketbroker' ), esc_html( $invoice_no ) ); 
    ?>
    <hr style="margin-top:30px;">
    <div id="invoice_box_wrapper" style="border:1px solid black;background-color:#e5e5e5ff;padding:0px 50px 0px 50px;width:30%;margin:20px auto 30px auto;"> 
        <div id="invoice_box_inner" style="width:80%;margin:10px auto;">
            <h6 style="margin:5px 5px;"><?php _e( 'Invoice Number', 'tabticketbroker' ); ?></h6>
            <p><?php echo $invoice_no; ?><p>
        </div>
    </div>
    <?php
}
?>

<div id="payment_wrap" style="margin-bottom:20px;width:40%;margin:0 auto;">
    <div id="title"><h4><?php _e( 'Payment Options', 'tabticketbroker' ); ?></h4></div>
        <?php
            Hardcoded::getBankDetails();
        ?>
    <div id="paypal_link" style="float:right;">
        <b><?php _e( 'PayPal', 'tabticketbroker' ); ?></b><br>
        <p><?php _e( 'Log in or create an account', 'tabticketbroker' ); ?></p>
        <a href="<?php echo site_url( 'account' ); ?>"><?php _e( 'My account', 'tabticketbroker' ); ?></a>
    </div>
</div>
<div style="clear:both;"></div>
<hr style="margin-bottom:30px;">


<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
// do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
// do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

do_action( 'ttb_email_footer', $email );

