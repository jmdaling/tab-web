<?php
/** 
 * @package tabticketbroker
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Remove the logo from the header
ob_start();
do_action( 'woocommerce_email_header', $email_heading, $email ); 
$header = ob_get_clean();
$header = preg_replace( '/<img[^>]+>/i', '', $header );
echo $header;


// print the messagebox 1
echo $messagebox_1;

// Don't include the footer