<?php
/** 
 * @package tabticketbroker
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Print woocommerce email header
do_action( 'woocommerce_email_header', $email_heading, $email );

// Print the messagebox 1
echo $messagebox_1;

// Print customer details
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

// Print the order details
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

// Print the messagebox 2
echo $messagebox_2;

// Print tabticketbroker footer
do_action( 'ttb_email_footer', $email );

