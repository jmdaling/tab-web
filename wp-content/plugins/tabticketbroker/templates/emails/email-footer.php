<?php
/**
 * Email Footer
 *
 * This email footer was copied from the woocommerce footer templated 
 * 
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top">
						<!-- Footer -->
						<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
							<tr>
								<td valign="top">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td colspan="2" valign="middle" id="credit" style="padding-bottom:unset;">
												<h4><?php _e( 'Regards', 'tabticketbroker' ); ?>,</h4>
												<p><?php _e( 'Your team from Oktoberfest-Tischreservierungen.de', 'tabticketbroker' ); ?></p>
											</td>
										</tr>
									</table>
									<div id="company_detail">
										<hr>
										<div style="margin-top:30px;margin-bottom:5px;">
											<?php _e( 'Oktoberfest-Tischreservierungen.de is a brand of', 'tabticketbroker' ); ?>:
										</div>
										<div>
											<div style="float:left;">

												<?php
													// get the Hardcoded class if not already loaded
													use Inc\Classes\HardCoded;
													echo Hardcoded::getOfficeDetails()['name'] .'<br>'; 
													echo 'CEO: '. Hardcoded::getOfficeDetails()['CEO'] .'<br>'; 
													echo Hardcoded::getOfficeDetails()['address'] .'<br>'; 
													echo Hardcoded::getOfficeDetails()['zip'] .' '. Hardcoded::getOfficeDetails()['city'] .'<br>';
													echo Hardcoded::getOfficeDetails()['country'] .'<br>';
												?>
											</div>
											<div style="float:right;">
												<table cellpadding="10" cellspacing="5">
													<tr>
														<td><?php _e( 'Phone', 'tabticketbroker' ); ?></td>
														<td><a href="<?php echo Hardcoded::getOfficeDetails()['phone_link']; ?>"><?php echo Hardcoded::getOfficeDetails()['phone']; ?></a></td>
													</tr>
													<tr>
														<td><?php _e( 'E-Mail', 'tabticketbroker' ); ?></td>
														<td><a href="mailto:<?php echo Hardcoded::getOfficeDetails()['email'];?>"><?php echo Hardcoded::getOfficeDetails()['email'];?></a></td>
													</tr>
													<tr>
														<td><?php _e( 'Online Shop', 'tabticketbroker' ); ?></td>
														<td><a href="<?php echo Hardcoded::getOfficeDetails()['url_ofest'];?>"><?php echo Hardcoded::getOfficeDetails()['url_ofest'];?></a></td>
													</tr>
													<tr>
														<td><?php _e( 'Online Shop', 'tabticketbroker' ); ?></td>
														<td><a href="<?php echo Hardcoded::getOfficeDetails()['url_tab'];?>"><?php echo Hardcoded::getOfficeDetails()['url_tab'];?></a></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</table>
						<!-- End Footer -->
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
