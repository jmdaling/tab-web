<?php
/** 
 * @package tabticketbroker
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Print woocommerce email header
do_action( 'woocommerce_email_header', $email_heading, $email );

// Print the messagebox 1
echo $messagebox_1;

// Print tabticketbroker footer
do_action( 'ttb_email_footer', $email );
