<div>
    
<?php

use Inc\Api\GoExpress\GoApiData;
use Inc\Api\GoExpress\GoApiTools;
use Inc\Classes\JMD_Logger;
use Inc\Classes\GenericFilters;
use Inc\Tools\OrderTools;
use Inc\Base\BaseController;
use Inc\Classes\Dimension;
use Inc\WcCustomization;
use Inc\Classes\TabReservation;
use Inc\Tools\ExportTools;
use Inc\Classes\AssignmentManager;
use Inc\Tools\Utils;
use Inc\Reports\PickupReport;


$update_all_orders                   = true;
$delete_custom_variations       = false;
$delete_invalid_prods           = false;
$delete_invalid_variations      = false;
$reservations_year_changover    = false;
$docs_complete_reservations     = false;
$holgy                          = false;
$newGenerateSkuTabTickets       = false;
$get_all_orders                 = false;
$test_fakturpro                 = false;
$test_date_formater             = false;
$delete_redundant_reservations  = false;
$remove_current_event_year      = false;
$transform_txt_to_datepicker    = false;
$do_set_orders_event_year       = false;
$do_generic_filters_test        = false;
$do_order_test                  = false;
$temp_function                  = false;
$do_api                         = false;
$show_phpinfo                   = false;


if ( $update_all_orders ) {
    $update = false;
  
    if ( $update ) {
        echo '<pre>';
        echo '---UPDATING---';
        echo '</pre>';
    } else {
        echo '<pre>';
        echo '---CHECKING---';
        echo '</pre>';
    }

    $order_tools = new OrderTools();

    // Get all order id
    $args = array(
        'post_type'         => 'shop_order',
        'posts_per_page'    => -1,
        'fields'            => 'ids',
        'post_status'       => array(
            'wc-pending',
            'wc-processing',
            'wc-on-hold',
            'wc-completed',
            'wc-cancelled',
            'wc-refunded',
            'wc-failed',
            'wc-shipped',
            'wc-ready-to-ship',
            'wc-assigned',
            'wc-labelled', 
        ),
    );
    $order_ids = get_posts( $args );

    // Loop through orders
    $orders_not_updated = array();
    foreach ( $order_ids as $order_id ) {
        
        // Check if the meta field _ttb_earliest_date is set
        $earliest_date = get_post_meta( $order_id, '_ttb_earliest_date', true );

        // If it is not set, get the earliest date from the order
        if ( $earliest_date == '' ) {
            $order = wc_get_order( $order_id );
            $earliest_date = $order_tools->getEarliestItemDate( $order );
            $orders_not_updated[$order_id] = $earliest_date;
        }
    }

    // print count of orders
    echo 'Orders: '. count( $order_ids );
    echo '<br>';

    // print count of orders not updated
    echo 'Orders not updated: '. count( $orders_not_updated );
    echo '<br>';
    
    // Output orders that were not updated
    echo '<pre>';
    print_r( $orders_not_updated );
    echo '</pre>';

    // If update is true, update the orders
    if ( $update ) {
        foreach ( $orders_not_updated as $order_id => $earliest_date ) {
            update_post_meta( $order_id, '_ttb_earliest_date', $earliest_date );
        }
    }
    
    echo '<pre>';
    echo '---DONE---';
    echo '</pre>';
}


if ( $delete_custom_variations ){

    echo '<pre>';
    echo '---Deleting custom variations---';
    echo '</pre>';
    
    // $variation_ids = array(
    //     20894,20895,20901,20902,20903,20929,20930,20931,20932,20933,21004,21005,30250,
    //     20895,20901,20902,20903,20929,20930,20931,20932,20933,21004,21005,25866,25867,25868,25869,
    //     25870,25871,25872,25873,25875,25876,25877,25878,25879,25880,25881,25882,25883,25884,25885,
    //     25907,25909,25911,25913,25914,25919,25920,25922,26519,27372,27373,27374,27375,27528,27633,
    //     27734,27940,28114,28115,28116,28117,28118,28119,28617,29066,29116,29117,29118,29143,29144,
    //     29145,29146,29147,29148,29149,29150,29151,29191,29192,29193,29206,29207,29208,29209,29210,
    //     29215,29261,29262,29263,29264,29265,29277,29278,29279,29280,29281,29282,29283,29308,29309,
    //     29310,29344,29345,29346,29347,29348,29349,29350,29388,29389,29390,29391,29392,29394,29395,
    //     29396,29397,29494,29532,29533,29537,29538,29548,29552,29559,29560,29561,29562,29563,29858,
    //     29974,30095,30096,30097,30098,30099,30250,30589,30883,30884,30885,30886,30887,30888,30889,
    //     30890,30892,30893,31215,31216,    );

    $variation_ids = array(
        30946
    );
    

    // given FIS-15-1-24-B & FIS-15-1-24, use regex to get the 24
    $regex = '/FIS-15-1-([0-9]+)/';




    // Loop through ids and delete variations
    foreach ( $variation_ids as $variation_id ) {
        wp_delete_post( $variation_id, true );
    }
    
    echo '<pre>';
    echo '---DONE---';
    echo '</pre>';
}

if ( $delete_invalid_variations )
{
    // Find all products
    $args = array(
        'post_type'         => 'product',
        'posts_per_page'    => -1,
        ''
    );

    $products = get_posts( $args );

    // echo '<pre>';
    // var_dump( $dates );
    // echo '</pre>';
    // exit;

    
    $dimension = new Dimension();
    $dates = $dimension->getProductDates();

    // Loop through products & get variations
    $skus = array();
    foreach ( $products as $product ) {
        $product_id = $product->ID;

        // if( $product_id != 926 ) continue;

        // Get the product
        $product = wc_get_product( $product_id );

        $variations = $product->get_children();

        // Loop through variations
        foreach ( $variations as $variation_id ) {
            $variation = wc_get_product( $variation_id );
            $title = $variation->get_title();
            $attr = $variation->get_attributes();

            // Get date attribute slug
            $date = $attr['pa_date'];
            $time_slot = $attr['pa_timeslot'];
            $pax = $attr['pa_pax'];
            $area = $attr['pa_area'];

            $my_variation = array(
                'id'                => $variation_id,
                'post_title'        => $title,
                'pa_date'           => $date,
                'pa_timeslot'       => $time_slot,
                'pa_pax'            => $pax,
                'pa_area'           => $area,
            );

            $sku = $dimension->generateSkuFromVariation( $my_variation );
            $all_variations[] = array(
                'id'                => $variation_id,
                'post_title'        => $title,
                'prod_title_code'   => $dimension->getProductCode( $title ),
                'pa_date'           => $date,
                'pa_timeslot'       => $time_slot,
                'pa_pax'            => $pax,
                'pa_area'           => $area,
                'sku'               => $sku,
            );
        }
        break;
    }

    // sort array by sku
    usort($all_variations, function($a, $b) {
        return $a['sku'] <=> $b['sku'];
    });

    // Loop through all_variations and print the id's of the ones with sku="sku error"
    foreach ( $all_variations as $variation ) {
        if ( $variation['sku'] == 'sku error' ) {
            echo '<pre>';
            echo $variation['id'] .' - '. $variation['prod_title_code'] .' - '. $variation['sku'] .' - '. $variation['pa_date'] .' - '. $variation['pa_timeslot'] .' - '. $variation['pa_pax'] .' - '. $variation['pa_area'];
            echo '</pre>';
        }
    }

    echo '---DONE---';

    // echo '<pre>';
    // var_dump( $all_variations );
    // echo '</pre>';

}

if ( $reservations_year_changover )
{
    // Get all reservations
    $reservation = new TabReservation();

    // Create an array with reservation code, sku, event year, status, supplier and cost price
    $args = array( 'ttb_event_year' => '' );
    $reservation->setReservations( $args );
    $reservations = $reservation->getReservations();

    $res_array = array();

    // Loop through reservations
    foreach ( $reservations as $reservation ) {
        
        // Strip value from cost price a tag
        $shop_price = strip_tags( $reservation->getShopPrice() );
        
        // Populate array
        $res_array[$reservation->getCode()] = array(
            'code'                  => $reservation->getCode(),
            'sku'                   => $reservation->getSku(),
            'status'                => $reservation->getStatus(),
            'event_year'            => $reservation->getEventYear(),
            'reservation_date'      => $reservation->getDate(),
            'supplier'              => $reservation->getSupplierName(),
            'cost_price'            => $reservation->getCostPrice(),
            'shop_price'            => $shop_price,
            'order_count'           => $reservation->getOrderCount(),
        );
    }

    $export_tools = new ExportTools;

    $filepath = $export_tools->exportCsv( $res_array, 'reservations.csv' );
    // $filepath = '/home/jmd/libraries/development/websites/oktoberfest-tischreservierungen.de/wp-content/plugins/tabticketbroker/exports/reservations_1697270147.csv';

    $export_tools->downloadSendHeaders($filepath);
}

if ( $docs_complete_reservations )
{
    // Get all reservations
    $reservation = new TabReservation();
    $reservations = $reservation->getReservations();

    $i = 0;

    // Loop through reservations
    foreach ( $reservations as $reservation ) {
        // If docs complete is true, set voucher received, letter received and letter read to true
        if ( get_post_meta( $reservation->getId(), 'tab_reservation_docs_complete', true ) == '1' ) {
            $reservation->setVouchersReceived( true );
            $reservation->setLetterReceived( true );
            $reservation->setLetterReady( true );
            
            // Save the reservation
            $reservation->saveReservation();

            $i++;
        }
    }

    echo '<pre>';
    echo 'Updated '.$i.' reservations';
    echo '</pre>';
}

if ( $holgy ) {

    function jmdRoundUpCurrentTime( $precision = 15 ) {
    
        // Get local Berlin time
        $date = new DateTime('now', new DateTimeZone('Europe/Berlin'));
        $cur_date = $date;
    
        // // Round time to the nearest $precision minutes
        $date->setTime( $date->format('H'), ceil( $date->format('i') / $precision ) * $precision );
    
        // Get difference between current time and rounded time
        $diff = $cur_date->diff( $date );
    
        // If difference is less than 5 minutes, add precision minutes to the rounded time
        if ( $diff->format('%i') < 5 ) {
            // add $precision in minutes
            $date->add( new DateInterval('PT15M') ); 
        }
    
        // // Get the datetime in string format
        // $date = getDisplayDateAdvanced( $date->format('Y-m-d H:i:s'));
    
        return $date->format('Y-m-d H:i:s');
    }

}


if ( $newGenerateSkuTabTickets ) {

    // TEST vars
    $product_id = 926;

    // Initialize the Dimension class
    $dimension = new Dimension();

    $dimension->initialize();

    $sku = '';

    // Get the product
    $product = wc_get_product( $product_id );

    // Get the parent product
    $parent_product = wc_get_product( $product->get_parent_id() );

    // Get the parent product sku
    $parent_sku = $parent_product->get_sku();

    // Get date attribute from product
    $date = $product->get_attribute( 'pa_date' );

    $date_code = '';

    // Get year, month, day from date
    $date_arr = explode( '-', $date );
     
    // Get the year, last 2 digits
    $year = substr( $date_arr[0], -2 );

    // Get the month, 2 digits
    $month = str_pad( $date_arr[1], 2, '0', STR_PAD_LEFT );

    // Get the day, 2 digits
    $day = str_pad( $date_arr[2], 2, '0', STR_PAD_LEFT );
    
    // Set the date code
    $date_code = $year.$month.$day;

    // Get timeslot attribute from product
    $timeslot = $product->get_attribute( 'pa_timeslot' );

    // Get the timeslots from the dimension class
    $sku_timeslots = $dimension->sku_timeslots;


    // Set the sku
    $sku = $parent_sku.'-'.$date_code.'-'.$timeslot_code.'-'.$area_code.'-'.$pax;

    echo '<pre>';
    print_r( $sku );
    echo '</pre>';




}



if ( $get_all_orders ) {

    // get all orders for the current event year
    $event_year = TTB_EVENT_YEAR;
    $orders = wc_get_orders( array(
        'type'              => 'shop_order',
        'limit'             => -1,
        'orderby'           => 'date',
        'order'             => 'ASC',
        'status'            => array(
                                    'pending',    
                                    'processing', 
                                    'on-hold',    
                                    'completed',
                                    'assigned',
                                    'ready-to-ship',
                                    'shipped',
                                    'on-hold',
                                ),
        'meta_key'          => 'ttb_event_year',
        'meta_value'        => $event_year,
        'meta_compare'      => '=',
        ) 
    );

    echo '<pre>';
    print_r( $orders );
    echo '</pre>';
}

if ( $test_fakturpro ) {
################################################################################################
## Get variation mini desc
// Get mini description from product id 21966

    // $product_id = 35830;
    // $product = wc_get_product( $product_id );
    // $mini_desc = $product->get_meta('_mini_desc');
    // echo 'MiniDesc:<br>';
    // echo '<pre>';
    // echo $mini_desc;
    // echo '</pre>';

################################################################################################
## Check all main prods mini description templates
    // // Get all products
    // $args = array(
    //     'post_type'         => 'product',
    //     'posts_per_page'    => -1,
    //     ''
    // );

    // $products = get_posts( $args );
    
    // // Print product title and cart description
    // foreach ( $products as $product ) {
    //     $product_id = $product->ID;
    //     $product = wc_get_product( $product_id );
    //     $title = $product->get_title();
    //     $cart_desc = $product->get_meta('_mini_desc');
    //     echo '<pre>';
    //     echo $product_id .' - '. $title .' - '. $cart_desc;
    //     echo '</pre>';
    // }

################################################################################################

// $order_id = 35830;   //35828;
// $fp_order = new FP_Order_Adapter( $order_id );

// $fp_plugin = new FP_Plugin(
//     "/home/jmd/libraries/development/websites/oktoberfest-tischreservierungen.de/wp-content/plugins/woore", 
// "/home/jmd/libraries/development/websites/oktoberfest-tischreservierungen.de/wp-content/plugins/woore",
// "https://dev.oktoberfest-tischreservierungen.de/wp-content/plugins/woorechnung");
// $fp_class = new FP_Factory($fp_plugin);

// $fp_class->create_invoice( $fp_order );



################################################################################################
    // $pax = isset($_GET['pax']) ? $_GET['pax'] : 2;
    // $wc = new WcCustomization();
    // $txt = $wc->getTableArrangementTxt($pax);
    // echo '<pre>';
    // print_r( $txt );
    // echo '</pre>';
    // return;

    // set wp_debug to true
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);


    $order_id = isset($_GET['order_id']) ? $_GET['order_id'] : 35830;

    $order = wc_get_order( $order_id );

    if ( ! $order ) {
        echo 'Order not found';
        return;
    }

    // Get all order items
    $order_items = $order->get_items();
    
    // Loop through order items
    $desc_array = array();

    foreach( $order_items as $product_item ) {
        
        $product = $product_item->get_product();
        $description = $product->get_meta('_mini_desc');
        $description = strip_tags(strval($description));
        // return $description;
        
        // get the product sku
        $sku = $product->get_sku();
        $desc_array[$sku] = explode( "\n", $description );
    }

    // output array
    echo '<pre>';
    var_dump( $order_id, $desc_array );
    echo '</pre>';
################################################################################################
}

if ( $test_date_formater ) {

    $locale = "en_GB";
    $format = "E d.M.yyyy";
    $value = "so-21-01-2021";

    $fmt = new \IntlDateFormatter( $locale, 0, 0, null, null, $format );

                // There is no validation being done on product attributes, so we need to cater for invalid dates
                try {
                    // Set the date object from the string
                    $date = new \DateTime( $value );
                    
                    // Format it using the IntlDateFormatter
                    $display_date = $fmt->format( $date ) ? $fmt->format( $date ) : $value; // Fallback to original value if formatting fails

                } catch (\Throwable $th) {
                    // Catch the error and ensure the user is notified
                    $error_msg = sprintf( __( '[TabTicketBroker Plugin] Date Error: Please check that all the product date attributes are formatted correctly. The error message: %s', 'tabticketbroker' ), $th->getMessage() );
                    
                    // sprintf(
                    //     __( 'Briefing status changed to <b>%s</b> by %s', 'tabticketbroker' ),
                    //     $new_brief_value ? __( 'briefed', 'tabticketbroker' ) : __( 'unbriefed', 'tabticketbroker' ),
                    //     get_user_by( 'id', get_current_user_id() )->display_name
                    // )

                    $this->addFlashNotice( $error_msg, "error", true );
                    
                    // If the date is invalid, just display the value
                    $display_date = $value;
                }
    

    echo $display_date;
    echo '<br>END';

    
}


if ( $delete_redundant_reservations ) {
    // Get all reservations
    $assignment_manager = new AssignmentManager();
        
    $assignment_manager->initialize();

    $reservations = $assignment_manager->getReservations();
    foreach ( $assignment_manager->reservations as $key => $reservation ) {

        if ( $reservation->post->post_status == 'trash' || $reservation->post->post_status == 'auto-draft' ) {
            // delete post
            $post = get_post( $reservation->getId() );
            wp_delete_post( $post->ID, true );
        }
    }
}

if ( $remove_current_event_year ) {
    // Delete option ttb_current_event_year if it exists
    // NOTE: Removed the option, it is now automatically set to the current year in wp-config.php
    //       Use TTB_EVENT_YEAR constant instead
    if ( get_option( 'ttb_current_event_year' ) ) delete_option( 'ttb_current_event_year' );
}

// TEST Transform text to datepicker
if ( $transform_txt_to_datepicker ) {
    ?>
   
    <input type="text" class="short" style="" name="jmd_field" id="jmd_field" value="1234" placeholder="">

    <script>

        jQuery(document).ready(function () {
            jQuery("#jmd_field").datepicker();
        });

            // jQuery("#jmd_field").datetimepicker({
            //     dateFormat: "yy-mm-dd"
            // });
            
            // set the value of ttb_var_event_date[0] to 'TESTING' 
            // jQuery("#ttb_var_event_date[0]").val('TESTING');
            
            // get the value of ttb_var_event_date[0] and print it to the console with jQuery
            console.log( 'that' );
            console.log( jQuery("#jmd_field").val() );
            console.log( 'this' );

            // Output the value of ttb_var_event_date[0] to the page using jQuery
            // jQuery("#ttb_var_event_date[0]").after( jQuery("#ttb_var_event_date[0]").val() );



    </script>

    <?php


}


// Once-off utility to set the event year for all orders
if ( $do_set_orders_event_year )
{
    // Get all orders from 2020-01-01 to 2022-11-01
    $all_orders = wc_get_orders( array(
        'limit' => -1,
        'date_after'  => '2020-01-01',
        'date_before' => '2023-01-25',
    ) );
    
    $order_meta = array();

    $order_tools = new OrderTools();

    // Loop through orders and get the meta data
    foreach ( $all_orders as $order ) {
        $order_id = $order->get_id();

        // Get event year from order meta data
        $event_year = get_post_meta( $order_id, '_ttb_event_year', true );

        if ( $event_year == '' ) {
            $event_year = $order_tools->calculateEventYear( $order );
            update_post_meta( $order_id, '_ttb_event_year', $event_year );
            
            echo '<pre>';
            echo $order_id .' = '. $event_year;
            echo '</pre>';
        }
    }
}

// TEST Generic filters

if ( $do_generic_filters_test )
{
    $gen_filters = new GenericFilters();

    echo '<div id="filter_test">';
    echo '<h2>Filter Test</h2>';

    $gen_filters->addPostTypeFilters( 'shop_order' );


    echo 'done';
    echo '</div>';

    // echo '<pre>';
    // print_r( $gen_filters->getFilters() );
    // echo '</pre>';
    

}

// TEST Brief log
if ( $do_order_test ) {

    // Get all orders from 2020-01-01 to 2022-11-01
    $all_orders = wc_get_orders( array(
        'limit' => -1,
        'date_after'  => '2022-09-01',
        'date_before' => '2022-10-01',
    ) );
    
    $order_meta = array();

    // Loop through orders and get the meta data
    foreach ( $all_orders as $order ) {
        $order_id = $order->get_id();

        // if ( $order_id != 29000 ) {
        //     continue;
        // }

        // $last_log = JMD_Logger::getLastLog( $order_id, '_ttb_brief_log' );
        $last_log_string = JMD_Logger::getLastLog( $order->get_id(), '_ttb_brief_log', 'briefed', '_ttb_briefed' );

        if ( $last_log_string == '1' ) {
            update_post_meta( $order->get_id(), '_ttb_briefed', 0 );
            $last_log_string = JMD_Logger::getLastLog( $order->get_id(), '_ttb_brief_log', 'briefed', '_ttb_briefed' );
        }

        echo '<pre>';
        echo $order_id .' = '. $last_log_string;
        echo '</pre>';


        
    }

}




// API TEST
if ( $do_api ) {

        echo 'API';

        $debug = true;

        $api_tools = new GoApiTools( $debug );

        $api_data = new GoApiData();

        // Get geramanized shipments
        $order = wc_get_order( 27629 );
        $order_shipment  = wc_gzd_get_shipment_order( $order );
        $order_needs_shipping = $order_shipment->needs_shipping(); // This is requried to add the shipments to the Germanized shipment object
        $shipments = $order_shipment->get_shipments();

        foreach ( $shipments as $shipment ) {
            $shipment_status = $shipment->get_status();
            if ( $shipment_status == 'shipped' ) {
                continue;
            }
            
            $shipment_data[] = $api_data->getAllShipmentData( $shipment );
        }

        foreach( $shipment_data as $shipment ) {
            $shipment_orders[] = $api_data->getCreateOrderData( $shipment );
        }

        echo 'exit';
        exit;

        foreach( $shipment_orders as $shipment_order ) {
            $result = $api_tools->createOrder( $shipment_order );
            var_dump( $result );
            return;
        }

        // $result = $api_tools->callApi( 'create_order', $api_tools->create_order_data );

        // echo '<pre>'; var_dump( $result ); echo '</pre>';       
        
    }

// DELETE VARIATIONS
if( $temp_function ){

    echo "Running function...";
    echo "<br>";

    $var_ids = array(
        29047,29053,29059,29060,29061,29123,29129,29142,29155,29158,29161,29167,29168,29169,29171,29177,29181,29187,
        29190,28497,29211,21601,25865,27829,29221,29232,29233,29235,29241,29245,29248,29252,29255,29260,29266,29289,
        29291,29292,29294,29295,29296,29305,29307,21920,26920,27741,29353,29354,29355,29361,29375,29378,29382,29386,
        29387,29393,29398,29078,29082,29084,29085,29088,29089,29090,29094,29105,29106,29115,
    );
    
    foreach ( $var_ids as $var_id ) {
        
        // $variation = get_post( $var_id );
        // $regular_price = get_post_meta( $var_id, '_regular_price', true );
        // echo $variation->post_title . ' - ' . $regular_price;
        // update_post_meta( $var_id, '_regular_price', 0 );

        wp_delete_post( $var_id, true );

        echo $var_id . ' - Deleted<br>';

        // echo "<pre>";
        // var_dump( $variation );
        // echo "/<pre>";

        // break;        

    }
    
    echo "<br>";
    echo "Done";
    exit; 
}
        
    
// Show PHP Info
if ( ! $show_phpinfo ) {
    exit;
}
?>
<div>
    <br>
    <hr>
    <div id="phpinfo">
        <h1>WP Config Settings</h1>
        
        <table>
            <tr>
                <td>Wordpress Current Time</td>
                <td><?php echo current_time( 'Y-m-d H:i' ); ?></td>
            </tr>
            <tr>
                <td>Server Name - Fixed</td>
                <td><?php echo $_SERVER['SERVER_NAME']; ?></td>
            </tr>
            <tr>
                <td>WP_HOME</td>
                <td><?php echo get_option('home'); ?></td>
            </tr>
            <tr>
                <td>WP_SITEURL</td>
                <td><?php echo get_option('siteurl'); ?></td>
            </tr>
            <tr>
                <td>Environment</td>
                <td><?php echo ( WP_ENVIRONMENT_TYPE == 'WP_ENVIRONMENT_TYPE' ? '<b>!!!!   WARNING  !!!! CONFIG NOT LOADED<b>' :  WP_ENVIRONMENT_TYPE ); ?></td>
            </tr>
            <tr>
                <td>Domain Current Site</td>
                <td><?php echo DOMAIN_CURRENT_SITE; ?></td>
            </tr>
            <tr>
                <td>Path Current Site</td>
                <td><?php echo PATH_CURRENT_SITE; ?></td>
            </tr>
            <tr>
                <td>Allow Multisite</td>
                <td><?php echo ( WP_ALLOW_MULTISITE ? 'True' : 'False' ); ?></td>
            </tr>
            <tr>
                <td>Subdomain Install</td>
                <td><?php echo ( SUBDOMAIN_INSTALL ? 'True' : 'False' ); ?></td>
            </tr>
            <tr>
                <td>WP_DEBUG</td>
                <td><?php echo ( WP_DEBUG ? 'True' : 'False' ); ?></td>
            </tr>
            <tr>
                <td>WP_DEBUG_DISPLAY</td>
                <td><?php echo ( WP_DEBUG_DISPLAY ? 'True' : 'False' ); ?></td>
            </tr>
            <tr>
                <td>WP_DEBUG_LOG</td>
                <td><?php echo ( WP_DEBUG_LOG ? 'True' : 'False' ); ?></td>
            </tr>
        </table>
    </div>
    <div id="phpinfo">
        <?php
        ob_start();
        phpinfo();
        $pinfo = ob_get_contents();
        ob_end_clean();

        $pinfo = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $pinfo);
        echo $pinfo;

        ?>
    </div>
</div>