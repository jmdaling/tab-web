<?php
// Process any functions called from the config page
if ( isset($_GET['admin_function'] ) ) {
    $args = $_GET;
    $return_msg = $this->processAdminFunction( $_GET['admin_function'], $args );
}

// Save active tab if set to use in display of template
if (isset($_GET['active_tab'])) {
    $active_tab = $_GET['active_tab'];
} else {
    $active_tab = 1;
}
?>
<div class="wrap bootstrap-wrapper">
    <?php $this->displayFlashNotices(); ?>
    <div class="header">
        <h1><?php echo __( 'Tab Administration', 'tabticketbroker' ); ?></h1>
        
        <ul id="myTab" class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a data-toggle="tab" class="nav-link<?php echo ($active_tab == 1 ? ' active' : ''); ?>" href="#tab-1"><?php _e( 'Dashboard', 'tabticketbroker' ); ?></a></li>
            <li class="nav-item"><a data-toggle="tab" class="nav-link<?php echo ($active_tab == 2 ? ' active' : ''); ?>" href="#tab-2"><?php _e( 'Functions', 'tabticketbroker' ); ?></a></li>
            <li class="nav-item"><a data-toggle="tab" class="nav-link<?php echo ($active_tab == 3 ? ' active' : ''); ?>" href="#tab-3"><?php _e( 'Settings', 'tabticketbroker' ); ?></a></li>
        </ul>
    </div>
    <div class="tab-content">
        <div id="tab-1" class="tab-pane<?php echo ( $active_tab == 1 ? ' active' : ''); ?>">
            <?php echo $this->getDashboardHtml(); ?>
        </div>
        <div id="tab-2" class="tab-pane<?php echo ($active_tab == 2 ? ' active' : ''); ?>">
            <?php echo $this->getAdminFunctionsHtml(); ?>
        </div>
        <div id="tab-3" class="tab-pane<?php echo ($active_tab == 3 ? ' active' : ''); ?>">
            <?php settings_errors(); ?>

            <form method="post" action="options.php">
                <?php
                    settings_fields( 'ttb_ofest_settings' );
                    do_settings_sections( 'tab_admin_plugin' );
                    submit_button('Save Settings');
                ?>
            </form>
        </div>
    </div>
</div>
