<?php
/* Template for displaying additional (& quick overview) order information which includes:
    - Order process flow status (emails sent)
    - Invoice data
    - Order language data
    - Order status
    - Order reservation actions? WIP
*/
?>

<?php
// Echo the order number in a div left floating
echo '<div id="order_number">' . $order->get_order_number() . '</div>';
?>

<div id="order_progress" class="<?php echo $order->has_status('cancelled') ? 'order-cancelled' : ''; ?>">
    <?php
    // Put progress array into variable
    $order_progress = $order_meta['progress'];


    foreach ($order_progress as $key => $meta) {
        $class = $meta['value'] == 1 ? ' class="status-completed"' : '';

        // Add divder, except for last item
        $divider = $key == array_key_last($order_progress) ? '' : '&nbsp;&rarr;&nbsp;';

        // Print status
        echo '<div' . $class . '><p>' . $meta['title'] . $divider . '</p></div>';
    }
    ?>
</div>

<mark class="order-status status-<?php echo $order->get_status(); ?>">
    <p><?php echo wc_get_order_statuses()['wc-' . $order->get_status()]; ?></p>
</mark>

<input type="hidden" name="ttb_order_meta_nonce" value="<?php echo wp_create_nonce(); ?>">

<div class="order-meta metabox-wrapper">
    <div class="order-meta custom-data">

        <div class="form-field form-field-wide">
            <label for="ttb_event_year"><?php _e('Event Year', 'tabticketbroker') ?></label>
            <input required type="number" style="width:250px;" name="ttb_event_year" placeholder="<?php _e('Enter event year', 'tabticketbroker') ?>" value="<?php echo $event_year; ?>">
        </div>

        <div class="form-field form-field-wide">
            <label for="ttb_invoice_no"><?php _e('Invoice Number', 'tabticketbroker') ?>
            <span data-toggle="tooltip" title="<?php _e( 'This number will sync with the FakturPro generated number. To overwrite it, set this to \'-\' and to reset it change this field to be blank.', 'tabticketbroker' ); ?>">&#9432;</span>
            </label>
            <input type="text" style="width:250px;" name="ttb_invoice_no" placeholder="<?php _e('Enter invoice number here', 'tabticketbroker') ?>" value="<?php echo $order_meta['invoice_no']; ?>">
        </div>

        <div class="form-field">
            <label for="ttb_order_language"><?php _e('Order Language', 'tabticketbroker') ?></label>
            <select name="ttb_order_language">
                <option value="de_DE_formal" <?php echo ($order_meta['language'] == 'de_DE_formal' ? 'selected' : ''); ?>><?php _e( 'German', 'tabticketbroker' ); ?></option>
                <option value="en_GB" <?php echo ($order_meta['language'] == 'en_GB' ? 'selected' : ''); ?>><?php _e( 'English', 'tabticketbroker' ); ?></option>
            </select>
        </div>

        <div class="form-field ttb-checkbox" title="<?php _e('Has first check been done after the documents have been packed?', 'tabticketbroker'); ?>">
            <label for="ttb_first_check"><?php _e('First check done', 'tabticketbroker') ?></label>
            <input id="ttb_first_check" name="ttb_first_check" type="checkbox" <?php checked( $first_check_status ); ?> />
            <?php if ( $first_check_status ) { ?> 
            <p class="log-note"><?php printf(__('First check done on %s by %s', 'tabticketbroker'), date( 'Y-m-d H:i', strtotime( $first_check_date ) ), $first_check_user );?></p>
            <?php } ?>
        </div>

        <div class="form-field ttb-checkbox" title="<?php _e('Check this item if the customer has received the final briefing.', 'tabticketbroker'); ?>">
            <label for="ttb_briefed"><?php _e('Customer has been briefed', 'tabticketbroker') ?></label>
            <input id="ttb_briefed" name="ttb_briefed" type="checkbox" <?php checked( $brief_status ); ?> />
            <?php if ( $brief_status ) { ?> 
            <p class="log-note"><?php printf(__('Customer has been briefed on %s by %s', 'tabticketbroker'), date( 'Y-m-d H:i', strtotime( $brief_date ) ), $brief_user );?></p>
            <?php } ?>            
        </div>



        <div class="form-field ttb-checkbox">
            <input type="hidden" name="ttb_order_is_pickup" value="false">
            <label for="ttb_pickup"><?php _e('Order is a pickup', 'tabticketbroker') ?></label>
            <input id="ttb_pickup" name="ttb_pickup" type="checkbox" value="1" <?php checked( $last_pickup_log['pickup'], 1 ); ?>/>
            <?php if ( $last_pickup_log['pickup'] ) { ?> 
            <p class="log-note"><?php printf(__('Order was set to pickup by on %s by %s', 'tabticketbroker'), $last_pickup_log['date'], $last_pickup_log['user']);?></p>
            <?php } else { ?>            
                <p class="log-note"><?php printf(__('Order is <b>not</b> a pickup set on %s by %s', 'tabticketbroker'), date( 'Y-m-d H:i', strtotime( $last_pickup_log['date'] ) ), $last_pickup_log['user']);?></p>
            <?php } ?>            
        </div>

        <?php if ( $last_pickup_log['pickup'] ) { // only show code if it is a pickup ?>
        <div class="order-meta custom-data">
            <div class="form-field">
                <label for="ttb_pickup_code"><?php _e('Pickup Code', 'tabticketbroker') ?>
                    <span data-toggle="tooltip" title="<?php _e( 'Leave blank to auto generate the code. NOTE: The order must be assigned to one or more reservations. Pickup codes are generated by combining the reservation numbers and codes.', 'tabticketbroker' ); ?>">&#9432;</span>
                </label>
                <input type="text" name="ttb_pickup_code" placeholder="<?php _e('Enter pickup code here', 'tabticketbroker') ?>" value="<?php echo $pickup_code; ?>">
            </div>
        </div>
        <?php } ?>

        <div class="form-field ttb-date">
            <label for="ttb_ship_by_date"><?php _e('Ship order by', 'tabticketbroker') ?></label>
            <input id="ttb_ship_by_date" name="ttb_ship_by_date" type="date" value="<?php echo $ship_by_date ? $ship_by_date : ''; ?>" title="<?php _e('Set a date by which the order should be shipped.', 'tabticketbroker') ?>" />
        </div>

        <div class="form-field">
            <label for="ttb_order_notes"><?php _e('Order Notes', 'tabticketbroker') ?></label>
            <textarea name="ttb_order_notes" rows="4" cols="10" placeholder="<?php _e('Add notes specific to the order. To be used internally only.', 'tabticketbroker') ?>"><?php echo $order_notes; ?></textarea>
        </div>

        <div id="ttb_linked_orders_header">
            <h3><?php _e('Linked Orders', 'tabticketbroker') ?></h3>
            <button id="ttb_linked_order_ids_btn" class="button" type="button"><?php _e('Link an order', 'tabticketbroker') ?></button>
        </div>
        <div id="ttb_linked_orders" class="order-items" title="<?php _e('Click to remove link', 'tabticketbroker') ?>">
            <?php if ($linked_orders) : ?>
                <?php foreach ($linked_orders as $order_id) : ?>
                    <?php $order_number = $this->getOrderNumberPrefix() . get_post_meta($order_id, '_alg_wc_custom_order_number', true); ?>
                    <a data-order-id="<?php echo $order_id; ?>"><?php echo $order_number; ?></a>
                <?php endforeach; ?>
            <?php else : ?>
                <p id="ttb_no_orders_found"><?php _e('No linked orders found.', 'tabticketbroker') ?></p>
            <?php endif; ?>

            <div id="ttb_search_orders_wrap"></div>
            <div id="ttb_search_background"></div>
            <?php echo $this->getLoaderHtml(); ?>
        </div>

        <?php if (isset($order_items_meta) && in_array(true, array_column($order_items_meta, 'flagged'))) { ?>
            <div class=flagged-items-wrap>
                <label><?php _e('Flagged Items', 'tabticketbroker') ?></label>
                <?php foreach ($order_items_meta as $order_item_meta) { ?>
                    <?php if ($order_item_meta['flagged']) { ?>

                        <div class="flagged-item">
                            <?php echo ($order_item_meta['flagged'] ? $order_item_meta['sku'] : ''); ?>
                        </div>

                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>