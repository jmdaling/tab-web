<body>
    <div id="screen-meta" class="metabox-prefs" style="display: block;">
        <div id="screen-options-wrap" class="hidden" tabindex="-1" aria-label="Screen Options Tab" style="display: none;">
            <form method="get" id="reservation_filter_form">
                <div id="filter_options_wrap">
                    <?php echo $this->getProductFilterHtml(); ?>
                    <?php echo $this->getDateFilterHtml(); ?>
                    <?php echo $this->getTimeslotFilterHtml(); ?>
                    <?php echo $this->getDisplayAmountHtml( $this->page_names['Assignments'] ); ?>
                </div>
            </form>
        </div>
    </div>
    <div id="screen-meta-links">
		<div id="screen-options-link-wrap" class="hide-if-no-js screen-meta-toggle">
			<button type="button" id="show-settings-link" class="button show-settings screen-meta-active" aria-controls="screen-options-wrap" aria-expanded="true">Screen Options</button>
		</div>
	</div>
                    
    <div class="bootstrap-wrapper">
        <div id="header">
            <h1><?php echo __( 'Assignment Management', 'tabticketbroker' ); ?></h1> <?php echo $this->getPaginationHtml(); ?>
        </div>
        
        <?php echo $this->displayFlashNotices(); ?>

        <div id="reservations_wrap">
            <?php echo $this->showAssignmentForm(); ?>
        </div>

    </div>
    <?php echo $this->getScrollToTopHtml();?>
</body>
<?php 
// It is required to exit the script here in order for the tooltips via bootstrap to work.
exit;