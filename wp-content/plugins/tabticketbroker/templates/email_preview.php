<h1><?php echo __( 'Email Preview', 'tabticketbroker' ); ?></h1>
<div id="email_preview_config">
    <form class="" action="" method="GET">
        <select name="email_type" id="email_type" data-toggle="tooltip" title="<?php _e( 'Email type', 'tabticketbroker' ) ?>">
            <?php 
            foreach ( $this->email_order_actions as $email_type => $email_action_desc ) {
                echo ' <option value="'. $email_type .'" '. ( $email_type == $email_type_selected ? 'selected' : '' ).'>'. __( $email_action_desc, 'tabticketbroker' ) .'</option>';
            }
            ?>
        </select>

        <select name="order_id" id="order_id" data-toggle="tooltip" title="<?php _e( 'Order number', 'tabticketbroker' ) ?>">
            <?php 
            foreach ( $this->orders as $order_id => $order_meta ) {
                echo ' <option value="'. $order_id .'" '. ( $order_id == $order_id_selected ? 'selected' : '' ).'>'. $order_meta['order_no'] .' - '. $order_meta['customer_fullname'] . ' ('. $language->getLanguageFromLocale( $order_meta['language'] ).')</option>';

                if ( $order_id == $order_id_selected )
                {
                    $order_lang = $order_meta['language'];
                }
            }
            ?>
        </select>

        <?php // Only display reservation selection when the shared reservations email is selected
            if ( isset( $_GET['email_type'] ) && $_GET['email_type'] == 'email_customer_shared_reservation' && isset( $_GET['order_id'] ) ) { ?>
        <select name="reservation_id" id="reservation_id" data-toggle="tooltip" title="<?php echo __( 'Reservation', 'tabticketbroker' ) .' ('. __( 'optional', 'tabticketbroker' ). ')';?>">
            <?php
                // If no reservation is selected, display a default option
                if ( ! isset( $_GET['reservation_id'] ) ) {
                    echo ' <option value="-1" selected>'. __( 'Select a reservation', 'tabticketbroker' ) .'</option>';
                }
                
                $i = 0;
                $reservation_options = array();
                foreach ( $this->reservations as $reservation ) {
                    $reservation_id = $reservation->getId();
                    // Check if reservation is in order reservations
                    if ( in_array( $reservation_id, $order_reservation_ids ) ) {
                        $reservation_options[-$i] = ' <option value="'. $reservation_id .'" '. ( $reservation_id == $reservation_id_selected ? 'selected' : '' ).'>'. $reservation->getCode().' - ' . __( 'linked', 'tabticketbroker' ) .'</option>';
                    } else {
                        $reservation_options[$i] = ' <option value="'. $reservation_id .'" '. ( $reservation_id == $reservation_id_selected ? 'selected' : '' ).'>'. $reservation->getCode() .'</option>';
                    }
                    $i++;
                }

                // Sort reservation options by key
                ksort( $reservation_options );
                foreach ( $reservation_options as $reservation_option ) {
                    echo $reservation_option;
                }
            ?>
        </select>
        <?php } ?>

        <input class="button" type="submit" value="<?php _e( 'Render', 'tabticketbroker' ); ?>">
        <input type="hidden" name="page" value="tab_admin_email_preview">
    </form>
</div>
<p><i><?php _e('Note that browsers and email clients render emails differently. Looks might differ but the content will be the same.', 'tabticketbroker' ); ?></i></p>
<div id="order_details">
    <?php 
    echo '<h4 style="display:inline-block;">Order Language: '. $order_lang .'</h4>&nbsp;'; 
    // create a link to the order
    $order_link = get_admin_url( null, 'post.php?post='. $order_id_selected .'&action=edit' );
    echo '<a href="'. $order_link .'" target="_blank">'. __( 'Go to order', 'tabticketbroker' ) .'</a>';
    ?>

</div>
<div id="email_preview_container">
    <?php echo $email_preview; ?>
</div>
