<?php
/**
 * Custom Template: Customer processing order email
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$current_language = get_user_locale();

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
	
	printf( esc_html__( 'Hallo %s %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ), esc_html( $order->get_billing_last_name() ) );

	if( $current_language == 'en_GB' ){
		echo '
			<br>
			<br>
			Thank you for your order with <a href=https://"www.oktoberfest-tischreservierungen.de">oktoberfest-tischreservierungen.de</a>.
			<br> 
			Your order is now being processed. All details are shown below for your reference.
			<br><br>';
	}
	else {
		echo '
			<br>
			<br>
			vielen Dank für Ihre Bestellung bei <a href="https://www.oktoberfest-tischreservierungen.de">oktoberfest-tischreservierungen.de</a>. 
			<br>
			Ihre Bestellung wird nun bearbeitet. Nachfolgend finden Sie alle Einzelheiten.
			<br><br>';
	}
?>
</p>

<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );


// Notification for processing time		
if( $current_language == 'en_GB' ){
	echo '<strong>Weekdays, you will receive a booking confirmation with an invoice within 24 hours.</strong><br><br>';
}
else{
	echo '<strong>Sie erhalten werktags eine Buchungsbestätigung mit Ihrer Rechnung innerhalb von 24 Stunden. </strong><br><br>';			
}
		

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
// do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>

<p>
	<?php 
		if( $current_language == 'en_GB' ){
			echo '
			<h2> Information </h2>
			The delivery of the documents takes place within 5 - 10 days prior to the event date.
			<br>
			<br>
			You can choose to pick up your reservation documents at our office in Berlin or Munich. This service is free of charge. 
			<br>
			AT THE EARLIEST 10 DAYS PRIOR TO YOUR RESERVATION.
			<br>
			<br>
			For further enquiries don\'t hesitate to contact us at any time.
			';
		}
		
		else{			
			echo
			'
			<h2> Informationen </h2>
			Die Lieferung der Reservierungsunterlagen (keine E-Tickets möglich) erfolgt per Express ca. 5 - 10 Tage vor dem Eventdatum.
			<br>
			<br>
			Sie können Ihre Reservierungsunterlagen auch in unserem Büro in Berlin oder München abholen. Dieser Service ist kostenlos. 
			<br> 
			FRÜHESTEN JEDOCH 10 TAGEN VOR IHRER RESERVIERUNG.
			<br>
			<br>
			Für Rückfragen stehen wir Ihnen gerne jederzeit zur Verfügung.
			';			
		}
		
	?>
</p>

<?php

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
// if ( $additional_content ) {
// 	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
// }

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );


