<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$current_language = get_user_locale();

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php 
	printf( esc_html__( 'Hallo %s,', 'woocommerce' ),  esc_html( $user_login ) );
?></p>


<p>
	<?php 
		if( $current_language == 'en_GB' ){
			printf( esc_html__( 'Thanks for creating an account on %1$s. Your username is %2$s. You can access your account area to view orders at: %3$s.' , 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); 
			echo '
				<br>
				<br>
				If you have any questions dont hesitate to contact us.
				';
		}
		else {
			printf( esc_html__( 'Vielen Dank für die Erstellung eines Kontos auf %1$s. Ihr Benutzername lautet %2$s. Unter folgendem Link könnst Sie auf Ihr Konto zugreifen und Ihre Bestellungen einsehen: %3$s. ', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); 
			echo '
				<br>
				<br>
				Wir freuen uns darauf, bald von Ihnen zu hören.
				';
		}
	?>
</p>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s: Auto generated password */ ?>
	<p><?php printf( esc_html__( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>

<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 */
// if ( $additional_content ) {
// 	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
// }

do_action( 'woocommerce_email_footer', $email );
