<?php

// TAB TICKETS
// Enqueue parent theme
function my_theme_enqueue_styles() {
    $parent_style = 'parent-style'; 

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_new_fonts( $array ) {
    $array[] = 'Cookie';
    return $array;
}
add_filter( 'ocean_google_fonts_array', 'my_new_fonts' );

// Attempting to hide variation that have no products
// https://stackoverflow.com/questions/23239003/wordpress-woocommerce-hide-unavailable-variations-when-there-are-2-attributes
add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 10, 2 );
function custom_wc_ajax_variation_threshold( $qty, $product ) {
    return 1000;
}

/* Price range display change to only display 'From..' and min price */
add_filter( 'woocommerce_variable_sale_price_html', 'wc_varb_price_range', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_varb_price_range', 10, 2 );
function wc_varb_price_range( $wcv_price, $product ) {
    $prefix = sprintf('%s: ', __('Ab', 'wcvp_range'));
 
    $wcv_reg_min_price = $product->get_variation_regular_price( 'min', true );
    $wcv_min_sale_price    = $product->get_variation_sale_price( 'min', true );
    $wcv_max_price = $product->get_variation_price( 'max', true );
    $wcv_min_price = $product->get_variation_price( 'min', true );
 
    $wcv_price = ( $wcv_min_sale_price == $wcv_reg_min_price ) ?
        wc_price( $wcv_reg_min_price ) :
        '<del>' . wc_price( $wcv_reg_min_price ) . '</del>' . '<ins>' . wc_price( $wcv_min_sale_price ) . '</ins>';
 
    return ( $wcv_min_price == $wcv_max_price ) ?
        $wcv_price :
        sprintf('%s%s', $prefix, $wcv_price);
}

/* Change text of `Select Options` button in shop page */
add_filter( 'woocommerce_product_add_to_cart_text', 'change_add_to_cart_text', 10 );
function change_add_to_cart_text( $text ) {
	global $product;
	if ( $product instanceof WC_Product && $product->is_type( 'variable' ) ) {
		$text = $product->is_purchasable() ? __( 'Jetzt buchen', 'woocommerce' ) : __( 'Weiterlesen', 'woocommerce' );
	}
	return $text;
}

/**
* @snippet    Hide "Shipping Calculator" Fields @ WooCommerce Cart
* @how-to    Get CustomizeWoo.com FREE
* @sourcecode    https://www.businessbloomer.com/woocommerce-how-to-hide-shipping-calculator-fields-cart/
* @author    Rodolfo Melogli
* @testedwith    WooCommerce 3.4.2
*/
// 1 Disable State
add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false' );
// 2 Disable City
add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
// 3 Disable Postcode
add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );

// // Alter the Shop page title
// function my_alter_shop_page_header_title( $title ) {
//     if ( is_shop() ) {
//         $title = 'Tickets Kaufen';
//     }
//     return $title;
// }
// add_filter( 'ocean_title', 'my_alter_shop_page_header_title', 20 );

// Remove the Shop page subheading
function my_remove_shop_page_header_subheading( $subheading ) {
    if ( is_shop() ) {
        $subheading = false;
    }
    return $subheading;
}
add_filter( 'ocean_post_subheading', 'my_remove_shop_page_header_subheading' );

/* Woocommerce shop page, show more products per default*/
add_filter( 'loop_shop_per_page', 'shop_prod_new', 21 );
function shop_prod_new( $cols ) {
    $cols = 30;
    return $cols;
}
/* Woocommerce shop page, set columns*/
add_filter( 'loop_shop_columns', 'shop_col_new', 20 );
function shop_col_new( $cols ) {
    $cols = 4;
    return $cols;
}

// If user not logged in and trying to access a private page, redirect him/her to the login and then to the page in question
function redirect_private_content() {
    $queried_object = get_queried_object();

    // Check if queried object is a post and if it is private, if user is not logged in, redirect to login page
    if ( is_a( $queried_object, 'WP_Post' ) && $queried_object->post_status != null && $queried_object->post_status == "private" && !is_user_logged_in() ) {
        wp_redirect( home_url( '/wp-login?redirect_to='.get_permalink( $queried_object->ID ) ) );
        exit;
    } 
}
add_action( 'template_redirect', 'redirect_private_content', 9 );

// Remove zoom on product image in single product page
add_action( 'after_setup_theme', 'remove_pgz_theme_support', 100 );
function remove_pgz_theme_support() { 
    remove_theme_support( 'wc-product-gallery-zoom' );
}
