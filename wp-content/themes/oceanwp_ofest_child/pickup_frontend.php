<?php /* Template Name: PickupFrontEnd */
/**
 * Template for showing a pickup report with basic actions behind basic auth
 *
 * */

// quit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Get username and password from options
$username = get_option( 'ttb_collections_admin_username' );
$password = get_option( 'ttb_collections_admin_password' );

// Ask for password
if ( ! isset( $_SERVER['PHP_AUTH_USER'] ) ) {
    header( 'WWW-Authenticate: Basic realm="Tab Ticketbroker"' );
    header( 'HTTP/1.0 401 Unauthorized' );
    echo 'Please enter the password';
    exit;
} else {
    // Check password
    if ( $_SERVER['PHP_AUTH_USER'] != $username || $_SERVER['PHP_AUTH_PW'] != $password ) {
        header( 'WWW-Authenticate: Basic realm="Tab Ticketbroker"' );
        header( 'HTTP/1.0 401 Unauthorized' );
        echo 'Wrong password';
        exit;
    }
}

// Get header scripts and styles only
wp_head();

// Show report
echo '<h1>Pickups</h1>';

$pickup_report = new Inc\Reports\PickupReport;

$pickup_report->renderCollections();

// add hidden username field
echo '<input type="hidden" id="ttb_collections_username" value="' . $_SERVER['PHP_AUTH_USER'] . '">';






