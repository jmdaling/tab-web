<?php

// OFEST

/**
 * Load translations for tabticketbroker
 */
function load_tab_theme_translations(){
    
    load_theme_textdomain( 'tabticketbroker', get_stylesheet_directory().'/languages' );
}
add_action('after_setup_theme', 'load_tab_theme_translations');

function debug_load_textdomain( $domain, $mofile ){
    echo "Trying ",$domain," at ",$mofile,"<br />\n";
    // var_dump( $args );
}
// add_action('load_textdomain','debug_load_textdomain', 10, 2 );


// Enqueue parent theme
function my_theme_enqueue_styles() {
    $parent_style = 'parent-style'; 

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// function my_new_fonts( $array ) {
//     $array[] = 'Cookie';
//     return $array;
// }
// add_filter( 'ocean_google_fonts_array', 'my_new_fonts' );

// Attempting to hide variation that have no products
// https://stackoverflow.com/questions/23239003/wordpress-woocommerce-hide-unavailable-variations-when-there-are-2-attributes
add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 10, 2 );
function custom_wc_ajax_variation_threshold( $qty, $product ) {
    return 1000;
}

/**
* @snippet    Hide "Shipping Calculator" Fields @ WooCommerce Cart
* @how-to    Get CustomizeWoo.com FREE
* @sourcecode    https://www.businessbloomer.com/woocommerce-how-to-hide-shipping-calculator-fields-cart/
* @author    Rodolfo Melogli
* @testedwith    WooCommerce 3.4.2
*/
// 1 Disable State
add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false' );
// 2 Disable City
add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
// 3 Disable Postcode
add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );

/**
* @snippet      Identify sub site for differentiation in functions yet using the same theme
* @author       Jan-Marten Daling
*/
function get_sub_site() {
    $blog_id = get_current_blog_id();
    switch ( $blog_id ) {
        case 1:
            $sub_site_name = 'ofest';
            break;
        case 3:
            $sub_site_name = 'tab';
            break;
        default:
            $sub_site_name = 'ofest';
            break;
    }
    return $sub_site_name;
}

// Alter the Shop page title
add_filter( 'ocean_title', 'my_alter_shop_page_header_title', 20 );
function my_alter_shop_page_header_title( $title ) {
    $sub_site_name = get_sub_site();
    if ( is_shop() && $sub_site_name == 'ofest' ) {
        $title = 'Tischreservierungen buchen';
    }
    elseif ( is_shop() && $sub_site_name == 'tab' ) {
        $title = 'Tickets Kaufen';
    }
    
    return $title;
    
}

// Remove the Shop page subheading
add_filter( 'ocean_post_subheading', 'my_remove_shop_page_header_subheading' );
function my_remove_shop_page_header_subheading( $subheading ) {

    if ( is_shop() ) {
        $subheading = false;
    }
 
    // Return the subheading
    return $subheading;
}

/* Woocommerce shop page, show more products per default*/
add_filter( 'loop_shop_per_page', 'shop_prod_new', 21 );
function shop_prod_new( $cols ) {
    $cols = 30;
    return $cols;
}

/* Woocommerce shop page, set columns*/
add_filter( 'loop_shop_columns', 'shop_col_new', 20 );
function shop_col_new( $cols ) {
    $cols = 4;
    return $cols;
}

function get_image( $image_id ) {
    switch ( $image_id ) {
        case "express-package":
            $img_url = get_site_url().'/wp-content/uploads/2020/08/Express-Package-1.jpg';
            //"http://192.168.0.10/tab/wp-content/uploads/2020/08/Express-Package-1.jpg"
        break;
    }

    return $img_url;
}


// If user not logged in and trying to access a private page, redirect him/her to the login and then to the page in question
add_action( 'template_redirect', 'redirect_private_content', 9 );
function redirect_private_content() {
    $queried_object = get_queried_object();
    if ( isset( $queried_object->post_status ) && $queried_object->post_status == "private" && !is_user_logged_in()) {
        wp_redirect(home_url('/wp-login.php?redirect_to='.get_permalink($queried_object->ID)));
        exit;
    } 
}


/* Shop page customization */
// Remove the additional information tab
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}

/* Price range display change to only display 'From..' and min price */
add_filter( 'woocommerce_variable_sale_price_html', 'wc_varb_price_range', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_varb_price_range', 10, 2 );
function wc_varb_price_range( $wcv_price, $product ) {
    $prefix = sprintf('%s: ', __('Ab', 'wcvp_range'));
 
    $wcv_reg_min_price = $product->get_variation_regular_price( 'min', true );
    $wcv_min_sale_price    = $product->get_variation_sale_price( 'min', true );
    $wcv_max_price = $product->get_variation_price( 'max', true );
    $wcv_min_price = $product->get_variation_price( 'min', true );
 
    $wcv_price = ( $wcv_min_sale_price == $wcv_reg_min_price ) ?
        wc_price( $wcv_reg_min_price ) :
        '<del>' . wc_price( $wcv_reg_min_price ) . '</del>' . '<ins>' . wc_price( $wcv_min_sale_price ) . '</ins>';
 
    return ( $wcv_min_price == $wcv_max_price ) ?
        $wcv_price :
        sprintf('%s%s', $prefix, $wcv_price);
}

/* Change text of `Select Options` button in shop page */
add_filter( 'woocommerce_product_add_to_cart_text', 'change_add_to_cart_text', 10 );
function change_add_to_cart_text( $text ) {
	global $product;
	if ( $product instanceof WC_Product && $product->is_type( 'variable' ) ) {
		$text = $product->is_purchasable() ? __( 'Jetzt buchen', 'woocommerce' ) : __( 'Weiterlesen', 'woocommerce' );
	}
	return $text;
}

// Admin: Add product slug column after product name
add_filter('manage_edit-product_columns','add_product_slug_column_heading');
function add_product_slug_column_heading( $columns ) {
    $slug_column = array(
        'product_slug' => __( 'Slug' )
    );
    $columns = array_slice( $columns, 0, 3, true ) + $slug_column + array_slice( $columns, 3, count( $columns ) - 1, true );

    return $columns;
}

// Admin: Display product slug
add_action( "manage_product_posts_custom_column", 'add_product_slug_column_value', 10, 2 );
function add_product_slug_column_value( $column_name, $id ) {
    if ( 'product_slug' == $column_name ) {
        echo get_post_field( 'post_name', $id, 'raw' );
    }
}

// Remove zoom on product image in single product page
add_action( 'after_setup_theme', 'remove_pgz_theme_support', 100 );
function remove_pgz_theme_support() { 
    remove_theme_support( 'wc-product-gallery-zoom' );
}

// Change amount of variations displayed in admin product page 
add_filter( 'woocommerce_admin_meta_boxes_variations_per_page', 'increase_variations_per_page' );
function increase_variations_per_page() {
	return 100;
}

function getCustomImgSrc( string $img_name ='' )
{
    switch ($img_name) {
        case 'success':
            return '/wp-content/uploads/2021/06/Success.png';
            break;
        case 'logo':
            return '/wp-content/uploads/2021/11/Ofest-Logo-Mail.newcolour.png';
            break;
        default:
            return '/wp-content/uploads/woocommerce-placeholder.png';
            break;
    }
}



// add_action('shutdown', function() {
//     global $wpdb;
//     error_log(print_r($wpdb->queries, true));
// });