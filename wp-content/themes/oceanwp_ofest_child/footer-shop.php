<div id="shop_footer" class="wp-block-cover">
    <img class="wp-block-cover__image-background wp-image-22929" alt="" src="/wp-content/uploads/2021/06/Request-price-1440x296-1.png" data-object-fit="cover">
    <div class="wp-block-cover__inner-container">
        <h2 class="has-text-align-center has-white-color has-text-color"><?php _e( 'Didn\'t find what you were looking for? Send us your request.', 'tabticketbroker' ); ?></h2>
        <div class="wp-block-buttons is-content-justification-center">
            <div class="wp-block-button is-style-fill">
                <a class="wp-block-button__link has-background tab-button" href="/preis-anfrage/"><?php _e( 'RESERVATION REQUEST', 'tabticketbroker' ); ?></a>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>