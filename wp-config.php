<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', true );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );
define( 'NOBLOGREDIRECT', '' );
define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST'] );

/* Security */
define( 'DISALLOW_FILE_EDIT', true );
// define( 'ALLOW_UNFILTERED_UPLOADS' , true);


/* Environment dependant settings */
switch ( $_SERVER['SERVER_NAME'] ) {
	// OFEST Production:
	case 'oktoberfest-tischreservierungen.de':
		// Environment variable
		define( 'WP_ENVIRONMENT_TYPE', 'prod' );
		define( 'TTB_SITE', 'ofest' );
		
		// Hosting
		define( 'DOMAIN_CURRENT_SITE', $_SERVER['SERVER_NAME'] );
		define( 'PATH_CURRENT_SITE', '/' );
		
		// Debugging & Dev
		define( 'WP_DEBUG', false );
		define( 'WP_DEBUG_LOG', false );
		define( 'WP_DEBUG_DISPLAY', false );
		ini_set( 'display_errors', 0 );
		
		// System
		define( 'WP_MEMORY_LIMIT', '512M');
		define( 'ALLOW_UNFILTERED_UPLOADS' , true);
		
		// Database
		$table_prefix = 'wp_shcga_';
		define( 'DB_HOST', 'localhost' );
		define( 'DB_NAME', 'tab_web' );
		define( 'DB_USER', 'tab_web_sys' );
		define( 'DB_PASSWORD', 'U7&^hLT=&xQnd1=0' );
		define( 'DB_CHARSET', 'utf8mb4' );
		define( 'DB_COLLATE', '' );

		break;

	// TABTICKETS Production:
	case 'tab-ticketbroker.de':
		// Environment variable
		define( 'WP_ENVIRONMENT_TYPE', 'prod' );
		define( 'TTB_SITE', 'tabtickets' );
		
		// Hosting
		define( 'DOMAIN_CURRENT_SITE', $_SERVER['SERVER_NAME'] );
		define( 'PATH_CURRENT_SITE', '/' );
		
		// Debugging & Dev
		define( 'WP_DEBUG', false );
		define( 'WP_DEBUG_LOG', false );
		define( 'WP_DEBUG_DISPLAY', false );
		ini_set( 'display_errors', 0 );
		
		// System
		define( 'WP_MEMORY_LIMIT', '512M');
		define( 'ALLOW_UNFILTERED_UPLOADS' , true);

		// Database
		$table_prefix = 'wp_shcga_';
		define( 'DB_HOST', 'localhost' );
		define( 'DB_NAME', 'tab_web' );
		define( 'DB_USER', 'tab_web_sys' );
		define( 'DB_PASSWORD', 'U7&^hLT=&xQnd1=0' );
		define( 'DB_CHARSET', 'utf8mb4' );
		define( 'DB_COLLATE', '' );

		break;
		
	// OFEST Development:
	case 'dev.oktoberfest-tischreservierungen.de':
		// Environment variable
		define( 'WP_ENVIRONMENT_TYPE', 'dev' );
		define( 'TTB_SITE', 'ofest' );
		
		// Hosting
		define('DOMAIN_CURRENT_SITE', $_SERVER['SERVER_NAME'] );
		define('PATH_CURRENT_SITE', '/');
		
		// Debugging & Dev
		define( 'WP_DEBUG', true );
		define( 'WP_DEBUG_LOG', true );
		define( 'WP_DEBUG_DISPLAY', false );
		ini_set( 'display_errors', 0 );
		// define('SAVEQUERIES', true);

		// System
		define( 'WP_MEMORY_LIMIT', '512M');
		define( 'ALLOW_UNFILTERED_UPLOADS' , true); 

		define( 'FS_METHOD', 'direct' );

		// Database
		$table_prefix = 'wp_shcga_';
		define( 'DB_HOST', '127.0.0.1' );
		define( 'DB_NAME', 'tab_web' );
		define( 'DB_USER', 'tab_web_sys' );
		define( 'DB_PASSWORD', 'U7&^hLT=&xQnd1=0' );
		define( 'DB_CHARSET', 'utf8mb4' );
		define( 'DB_COLLATE', '' );
		
		break;
		
	// TABTICKETS Development:
	case 'dev.tab-ticketbroker.de':

		// Only for running ProductTools:resetVariationSkus() | Change to cron in the future
		set_time_limit(300);
		
		// Environment variable
		define( 'WP_ENVIRONMENT_TYPE', 'dev' );
		define( 'TTB_SITE', 'tabtickets' );
		
		// Hosting
		define('DOMAIN_CURRENT_SITE', $_SERVER['SERVER_NAME'] );
		define('PATH_CURRENT_SITE', '/');
		
		// Debugging & Dev
		define( 'WP_DEBUG', false );
		define( 'WP_DEBUG_LOG', false );
		define( 'WP_DEBUG_DISPLAY', false );
		ini_set( 'display_errors', 0 );

		// System
		define( 'WP_MEMORY_LIMIT', '512M');
		define( 'ALLOW_UNFILTERED_UPLOADS' , true); 
		
		// Database
		$table_prefix = 'wp_shcga_';
		define( 'DB_HOST', '127.0.0.1' );
		define( 'DB_NAME', 'tab_web' );
		define( 'DB_USER', 'tab_web_sys' );
		define( 'DB_PASSWORD', 'U7&^hLT=&xQnd1=0' );
		define( 'DB_CHARSET', 'utf8mb4' );
		define( 'DB_COLLATE', '' );
		
		break;
	
	case 'docs.oktoberfest-tischreservierungen.de':
		echo 'wp-config error: There must be an issue with the DNS settings. Trying to access the media wiki at '.$_SERVER['SERVER_NAME']. ' but getting to the wordpress installation at '.DOMAIN_CURRENT_SITE.'.';
		echo 'If you are seeing this message on the media wiki, please contact the webmaster.';
		echo '<h5>to continue in the meantime refresh the page</h5>';
		exit;

	default:
		echo 'ttb: wp-config error: Please check configuration file for the domain: '. $_SERVER['SERVER_NAME'];
		exit;
}

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g4Y)AAn9@AYlX6ok JO!q.qyi/!-w(h,<i~el7pe_ekQ;HZk`jJ/m~M&a}Yo)c/S' );
define( 'SECURE_AUTH_KEY',  'iDUvoDnmxi5u}t!kEH_-9M,V%M}EKdD[98et,r$c/sWLm+{<z2f{*0FW5FRHBaA]' );
define( 'LOGGED_IN_KEY',    '}Ij{fp>zO.+r#[:^y,3;sO^Whf`nGc>uBT0 TKaxDE/2./oTcdVzE2+so<}Aw@{a' );
define( 'NONCE_KEY',        '~BA]bAJaXg2aG/yzQ@,Cnk<*:.!yIj$5w}yK[)7t^3_YTa<CE(l:yPPv;~Zx&8k-' );
define( 'AUTH_SALT',        'zL.tX)YZ^oU1~n2bx2n=GEB9E%dr.4j.Ed>L fTJB}pntUbpf#$J]rDWrJe1#[|d' );
define( 'SECURE_AUTH_SALT', '6EgE!6tM_8_;<X^@Egti=4.w Hp43-V;E>m4x5j>P!S^IgAdE^fGdlJ5e;zJl8E|' );
define( 'LOGGED_IN_SALT',   'Cy};J;Wv1kd6=9PBPM/vZ*IY[xGW+yAomKA+&4`tl!0<me8fyf@}}?`lVK3ODH$G' );
define( 'NONCE_SALT',       '%mCRN<RkJ0fqM}%DiB%]H%5UHT/-OMt8>pO1hQaKIdv  ivfA@M]#nx9Yd1:|Lq5' );

// WooCommerce Germanized: Required key for general purpose encryption (https://vendidero.de/dokument/verschluesselung-sensibler-daten)
define( 'WC_GZD_ENCRYPTION_KEY', 'd26a64a62d000b0208c2842f69d20d41e12426e8045be895323388723cfc56f1' );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
